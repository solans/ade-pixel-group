<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Tower 180 nm</title>
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
  show_login();
?>


<p class="TITLE">Tower 180 nm</p>

<img class="IMAGE" src="<?=$gobase;?>Tower/TJ180NM.png">

<p class="SUBTITLE">Thesis</p>
<ul>
  <li><a href="https://theses.gla.ac.uk/83781/">Leyre Flores Sanz de Acedo. August 2023</a></li>
  <li><a href="https://cds.cern.ch/record/2833974">Kaan Oyulmaz. July 2022</a></li>
  <li><a href="https://cds.cern.ch/record/2791748">Ignacio Asensi Tortajada. October 2021</a></li>
  <li><a href="https://theses.gla.ac.uk/82418/13/2021ArgemiPhD.pdf">Lluis Simon Argemi. August 2021</a></li>
  <li><a href="https://repositum.tuwien.at/bitstream/20.500.12708/16521/2/Dachs%20Florian%20-%202020%20-%20Radiation%20hardness%20characterisation%20of%20CMOS%20sensors%20for...pdf">Florian Dachs. October 2020</a></li>
  <li><a href="https://cds.cern.ch/record/2733027">Bojan Hiti. Sept 2020</a></li>
  <li><a href="https://cds.cern.ch/record/2743291">Abhishek Sharma. March 2020</a></li>
  <li><a href="https://cds.cern.ch/record/2702884">Ivan Berdalovic. Oct 2019</a></li>
  <li><a href="https://cds.cern.ch/record/2702969">Roberto Cardella. Nov 2019</a></li>
</ul>

<p class="SUBTITLE">Presentations
<ul>
  <li><a href="https://indico.cern.ch/event/1307202/contributions/5498718/attachments/2822810/4930662/AIDA_Catania_final.pdf">M. Vazquez, AIDA Innova, March 2024</a></li>
  <li><a href="https://indico.cern.ch/event/1184921/contributions/5585234/attachments/2768523/4822980/HSTD13_Vlad.pdf">V. Berlea, HSTD13, December 2023</a></li>
  <li><a href="https://agenda.infn.it/event/35597/contributions/211641/attachments/111789/159571/2023.10.16.GGustavino.VERTEX2023.pdf">G. Gustavino, VERTEX, October 2023</a></li>
  <li><a href="https://indico.cern.ch/event/1191719/contributions/5368092/attachments/2634938/4558298/AIDA_Valencia_v3.pdf">M. Vazquez, AIDA Innova, April 2023</a></li>
  <li><a href="https://indico.cern.ch/event/1223972/contributions/5261991/attachments/2601421/4491856/Gazi_TREDI2023_MALTA.pdf">M. Gazi, TREDI 2023, March 2023</a></li>
  <li><a href="https://indico.cern.ch/event/829863/contributions/4479493/attachments/2568413/4428616/DaoValerio__Pixel2022.pdf">V. Dao, Pixel 2022, December 2022</a></li>
  <li><a href="https://www.eventclass.org/contxt_ieee2022/scientific/online-program/session?s=N-22#e464">V. Berlea, NSS-MIC, November 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1127562/contributions/4904905/attachments/2522032/4336777/ASharma_Poster-TWEPP2022.pdf">A. Sharma, TWEPP, September 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1127562/contributions/4904898/attachments/2454549/4320757/Julian_Weick__low-mass_module_concepts_based_on_MALTA.pdf">J. Weick, TWEPP, September 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1127562/contributions/4904688/attachments/2454443/4322488/Dobrijevic_TWEPP_final.pdf">D. Dobrijevic, TWEPP, September 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1120714/contributions/4867232/attachments/2472531/4242104/2022.06.16.GGustavinoIWORID2022.pdf">G. Gustavino, IWORID, June 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1058977/contributions/4631218/attachments/2465399/4227716/BTTB2021.pdf">M. van Riijnbach, BTTB, June 2022</a></li>
  <li><a href="https://agenda.infn.it/event/22092/contributions/166465/attachments/91257/123946/Elba-Poster-Pernegger-Final.pdf">H. Pernegger, ELBA, June 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1096847/contributions/4742327/attachments/2400093/4104325/TREDI2022_final.pdf">V. Berlea, TREDI, March 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1104064/contributions/4786215/attachments/2416784/4135828/Carlos_AIDA_20220328_2.pdf">C. Solans, AIDA innova, March 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1044975/contributions/4663664/attachments/2396996/4098732/MLB_VCI_Draft2.pdf">M. LeBlanc, VCI, February 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1044975/contributions/4663752/">D. Dobrijevic, VCI, February 2022</a></li>
  <li><a href="https://indico.cern.ch/event/1047531/contributions/4521226/attachments/2318944/3948280/Carlos_VERTEX_20210928_2.pdf">C. Solans, VERTEX, September 2021</a></li>
  <li><a href="https://indico.cern.ch/event/1019078/contributions/4444328/attachments/2314969/3941975/TWEPP2021.pdf">M. van Rijnbach, TWEPP, September 2021</a></li>
  <li><a href="https://indico.desy.de/event/28202/contributions/105645/attachments/67560/84141/ASharma_EPS-HEP_2021.pdf">A. Sharma, EPS-HEP 2021, July 2021</a></li>
  <li><a href="https://indico.cern.ch/event/981823/contributions/4295540/attachments/2250421/3817383/poster_675.pdf">L. Flores, TIPP, May 2021</a></li>
  <li><a href="https://indico.cern.ch/event/1003419/contributions/4310666/attachments/2225720/3771112/Carlos_AIDA_20210414_5.pdf">C. Solans, AIDA innova, April 2021</a></li>
  <li><a href="https://indico.cern.ch/event/983068/contributions/4223151/">P. Riedler, TREDI, February 2021</a></li>
  <li><a href="https://indico.cern.ch/event/983068/contributions/4223228/">F. Dachs, TREDI, February 2021</a></li>
  <li><a href="https://indico.cern.ch/event/945675/contributions/4174397/attachments/2186683/3694842/andrea_BTTB2021.pdf">A. Gabrielli, BTTB9, Fabruary 2021</a></li>
  <li><a href="https://indico.cern.ch/event/895924/contributions/3968859/attachments/2102495/3564038/Ignacio_VERTEX2020.pdf">I. Asensi, VERTEX, October 2020</a></li>
  <li><a href="https://indico.cern.ch/event/868940/contributions/3813920/attachments/2081459/3497946/Carlos_ICHEP_2020728_3.pdf">C. Solans, ICHEP, August 2020</a></li>
  <li><a href="https://indico.cern.ch/event/813597/contributions/3727766/attachments/1989153/3315645/TREDI2020_MALTA_miniMALTA.pdf">R. Cardella, TREDI, February 2020</a></li>
  <li><a href="https://indico.cern.ch/event/813822/contributions/3653067/attachments/1978263/3294212/mdyndal_Malta_bttb8.pdf">M. Dyndal, BTTB8, 30 Jan 2020</a></li>
  <li><a href="https://indico.cern.ch/event/803258/contributions/3582771/attachments/1962389/3262009/ID197_riedler_poster_hdt_2019.pdf">P. Riedler, HSTD12, 14 December 2019</a></li>
  <li><a href="https://indico.cern.ch/event/803258/contributions/3582758/attachments/1962418/3262097/196_Pernegger_HSTD2019.pdf">H. Pernegger, HSTD12, 14 December 2019</a></li>
  <li><a href="https://indico.cern.ch/event/803258/contributions/3582907/attachments/1962427/3263675/319-bespin-presentation.pdf">C. Bespin, HSTD12, 14 December 2019</a></li>
  <li><a href="https://indico.cern.ch/event/806731/contributions/3517231/attachments/1925885/3188586/VERTEX_talk_Freeman.pdf">P. Freeman, VERTEX 2019, 15 October 2019</a></li>
  <li><a href="https://indico.cern.ch/event/799025/contributions/3486386/attachments/1901230/3139045/TWEPP_ValerioDao.pdf">V. Dao, TWEPP 2019, 3 September 2019</a></li>
  <li><a href="https://indico.cern.ch/event/837850/contributions/3513874/attachments/1887379/3112107/miniMALTA_Diamond.pdf">M. Mironova, CMOS demonstrator 29 July 2019</a></li>
  <li><a href="https://indico.cern.ch/event/837850/contributions/3513670/attachments/1887575/3112091/ATLAS_20190729_WS.pdf">W. Snoeys, CMOS demonstrator 29 July 2019</a></li>
  <li><a href="https://indico.cern.ch/event/837850/contributions/3513665/attachments/1887560/3112303/mdyndal_miniMalta_CMOSmeeting_29-07-2019.pdf">M. Dyndal, CMOS demonstrator 29 July 2019</a></li>
  <li><a href="https://indico.cern.ch/event/799919/contributions/3396619/attachments/1829615/3017575/Carlos_CMOS_20190415_2.pdf">C. Solans, CMOS demonstrator 15 April 2019</a></li>
  <li><a href="https://indico.cern.ch/event/777112/contributions/3312288/attachments/1801090/2939412/Carlos_Trento_20190226_2.pdf">C. Solans, Trento February 2019</a></li>
  <li><a href="https://indico.cern.ch/event/716539/contributions/3246026/attachments/1799912/2935412/2019-02-TJ-MALTA-VCI2019.pdf">E.J. Schioppa, VCI February 2019</a></li>
  <li><a href="https://indico.cern.ch/event/728933/contributions/3293412/attachments/1788217/2912234/ATLAS_ITk_20190131_WS.pdf">W. Snoeys, ITK Week January 2019</a></li>
  <li><a href="https://indico.cern.ch/event/771784/contributions/3211969/attachments/1751766/2854015/2018-11-13_TJ-MALTA-AUW.pdf">E.J. Schioppa, AUW November 2018</a></li>
  <li><a href="https://indico.cern.ch/event/726191/contributions/3149674/attachments/1722014/2854021/2018-09-TJ-MALTA-ITK-Week-Oxford.pdf">E.J. Schioppa, ITK Week September 2018</a></li>
  <li><a href="https://indico.cern.ch/event/749906/contributions/3143323/attachments/1717099/2770591/20180917_TJ_MPW.pdf">T. Kugathasan, CMOS demonstrator 17 September 2018</a></li>
  <li><a href="https://indico.cern.ch/event/739384/contributions/3052534/attachments/1674277/2687647/20180625_CaicedoI_TJMONOPIX_CMOSMreport.pdf">I. Caceido, CMOS demonstrator 26 June 2018</a></li>
  <li><a href="https://indico.cern.ch/event/739384/contributions/3052533/attachments/1674138/2687102/MALTA_CMOS_20180625_3.pdf">C. Solans, CMOS demonstrator 26 June 2018</a></li>
  <li><a href="https://indico.cern.ch/event/739384/contributions/3052532/attachments/1674260/2687240/Summary_TJ.pdf">W. Snoeys, CMOS demonstrator 26 June 2018</a></li>
  <li><a href="https://indico.cern.ch/event/720588/contributions/2963608/attachments/1634121/2606479/TJ_Monopix_AUW_17_04.pdf">K. Moustakas AUW April 2018</a></li>
  <li><a href="https://indico.cern.ch/event/720588/contributions/2963607/attachments/1634452/2607097/ASharma_Malta_AUW_2018_04_17.pdf">A. Sharma AUW April 2018</a></li>
  <li><a href="https://indico.cern.ch/event/720585/contributions/2963536/attachments/1633532/2605298/ATLAS_20180416_ITK_WS.pdf">W. Snoeys AUW April 2018</a></li>
  <li><a href="https://indico.cern.ch/event/666427/contributions/2881304/attachments/1603219/2542430/Riegel_13thTrento.pdf">C. Riegel, 13th Trento Workshop on Advanced Silicon Radiation Detectors</a></li>
  <li><a href="https://indico.cern.ch/event/577879/contributions/2740317/attachments/1573548/2483981/TJ-Hiroshima-2017-Pernegger.pdf">H. Pernegger, HSTD11 December 2017</a></li>
  <li><a href="https://indico.cern.ch/event/678605/contributions/2782259/attachments/1557110/2450114/TJ_AUW_Nov2017.pdf">R. Cardella, AUW November 2017</a></li>
  <li><a href="https://indico.cern.ch/event/678602/contributions/2782231/attachments/1556946/2448928/2017-11-13_AUWWeek_Schioppa.pdf">E.J. Schioppa, AUW November 2017</a></li>
  <li><a href="https://indico.cern.ch/event/609082/contributions/2720481/attachments/1524680/2383625/Berdalovic_ITK_week.pdf">I. Berdalovic, ITK Week September 2017</a></li>
  <li><a href="https://indico.cern.ch/event/609082/contributions/2720517/attachments/1524733/2383708/Carlos_TJMALTA_20170918.pdf">C. Solans, ITK Week September 2017</a></li>
  <li><a href="https://indico.cern.ch/event/608587/contributions/2614081/attachments/1523908/2381891/Monolithic_ATLAS_CERN_TWEPP_2017_TK_v1.pdf">T. Kugathasan, TWEPP September 2017</a></li>
  <li><a href="https://indico.cern.ch/event/609081/contributions/2637992/attachments/1483203/2301063/Dachs_AITkW_270617.pdf">F. Dachs, ITK Week June 2017</a></li>
  <li><a href="https://indico.cern.ch/event/589150/contributions/2398249/attachments/1383564/2104364/Testbeam_results.pdf">M. Dalla, Group meeting 6 December 2016</a></li>
  <li><a href="https://indico.cern.ch/event/587239/contributions/2370705/attachments/1371773/2080882/testbeam_ITK_Nov15_updated.pdf">D. Schaefer, AUW November 2016</a></li>
  <li><a href="https://indico.cern.ch/event/587239/contributions/2370704/attachments/1371714/2080765/ITK_TowerJazz_15112016.pdf">T. Kugathasan, AUW November 2016</a></li>
  <li><a href="https://indico.cern.ch/event/562672/contributions/2295810/attachments/1334595/2006947/ATLAS_20160912_ITK_WS.pdf">W. Snoeys, ITK Week September 2016</a></li>
  <li><a href="https://indico.cern.ch/event/519680/contributions/2288220/attachments/1328576/1995484/Testbeam_status_aug30.pdf">D. Schaefer, Group meeting 30 August 2016</a></li>
  <li><a href="https://indico.cern.ch/event/521363/contributions/2136733/attachments/1259260/1860317/Riegel_AUW_180416.pdf">C. Riegl, AUW April 2016</a></li>
  <li><a href="https://indico.cern.ch/event/461382/session/1/contribution/127/attachments/1228172/1799159/Riegel_ITk_150216.pdf">C. Riegl, ITK Week February 2016</a></li>
  <li><a href="https://indico.cern.ch/event/464110/contribution/0/attachments/1232215/1806907/TJ-Feb2016-DemonstratorMeeting-HP-V2.pdf">H. Pernegger, CMOS demonstrator 22 February 2016</a></li>
</ul>
</p>

<p class="SUBTITLE">Reports</p>
<ul>
  <li><a href="https://cds.cern.ch/record/2852748">EP RnD report 2022, CERN-EP-RDET-2023-002</a></li>
  <li><a href="https://cds.cern.ch/record/2808204">EP RnD report 2021, CERN-EP-RDET-2022-006</a></li>
</ul>

<p class="SUBTITLE">Funding</p>
  <p>
  This project has received funding from the European Unions Horizon 2020 Research and Innovation programme under Grant Agreement numbers:
  <ul>
    <li><a href="https://cordis.europa.eu/project/id/101004761">101004761 (AIDAinnova)</a></li>
    <li><a href="https://cordis.europa.eu/project/id/675587">675587 (STREAM)</a></li>
    <li><a href="https://cordis.europa.eu/project/id/654168">654168 (AIDA-2020)</a></li>
  </ul>
</p>
<p>
The project has been funded by the Science and Engineering Research Board (SERB), India, and the Ministry of Human Resource Development (MHRD), India.
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
