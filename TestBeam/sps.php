<?php
$time_start = microtime(true);
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/jquery.tablesorter.pager.min.css" rel="stylesheet">
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-pager.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />

<title>Sample Database</title>
<style>
.ui-datepicker {
  background: #fff;
}

pre {
    font-size: 13px;
    font-family: Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,sans-serif;
    line-height: 1.30769231;
    color: var(--highlight-color);
    background-color: #DDDDDD;
    border-radius: 5px;
    margin: 0;
        margin-top: 0px;
        margin-bottom: 0px;
    padding: 11px;
    overflow: auto;
    scrollbar-color: var(--scrollbar) transparent;
}

</style>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
  //$dt1 = round(microtime(true) - $time_start,1);
?>
<div class="CONTENT">
<?php
    $dbOK=True;
    show_certificate();
    show_login(true); 
    //$dt2 = round(microtime(true) - $time_start,1);
    try {
      // Create connection
      $conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
      // Check connection
      if ($conn->connect_error) {
      throw new Exception('Database error');
      die("Connection failed: " . $conn->connect_error);
      }
      $sql = "SELECT * FROM configuration";
		  $conds=array();
      if(isset($_GET["run"])){$conds[]= " runNumber = '".$_GET["run"]."' ";}
		  if(count($conds)==0){$sql .= " ORDER BY runNumber DESC LIMIT 0,10000";}
      else{ $sql .= " WHERE ".join("AND",$conds); }
		  $result = $conn->query($sql);
      $conn->close();
    }catch (Exception $e) {
      $dbOK=False;
      echo "<h2>Error connecting to database</h2>";//.$e->getMessage();
    }
  ?>
<p class="TITLE">MALTA TB SPS</p>

<?php
if ($dbOK==True){?>
<!--
<table>
<tr><td>You can use basic filter operands such as:</td><td>For example, search for the sample: </td></tr>
<tr><td><pre>| or  OR , < <= >= >, *, ! or !=, etc</pre></td><td><pre>W**R2</pre></td></tr>
</table>
-->
<span id="search_list">
	<input  hidden class="getsearch" type="search" data-column="1">
	<button onclick="export_tablesorter('search_table');">CSV</button> Export <span class="search_table_row_counter"></span> <button id="reset-link" style="float: right;">Reset filters</button>
  <div id="pager" class="pager">
  <form>
    <img src="../img/first.png" class="first"/>
    <img src="../img/prev.png" class="prev"/>
    <!-- the "pagedisplay" can be any element, including an input -->
    <span class="pagedisplay" data-pager-output-filtered="{startRow:input} &ndash; {endRow} / {filteredRows} of {totalRows} total rows"></span>
    <img src="../img/next.png" class="next"/>
    <img src="../img/last.png" class="last"/>
    <select class="pagesize">
      <option value="10">10</option>
      <option value="20">20</option>
      <option value="30">30</option>
      <option value="40">40</option>
      <option value="all">All Rows</option>
    </select>
  </form>
</div>
	<table id="search_table" class="tablesorter" style="font-size: smaller">
		<thead>
			<th data-placeholder="Select...">Run</th>
			<th class="first-name filter-select" data-placeholder="Search...">Plane</th>
			<th class="first-name filter-select" data-placeholder="Search...">Sample </th>
			<th class="first-name filter-select" data-placeholder="Select...">IDB</th>
			<th class="first-name filter-select" data-placeholder="Select...">ITHR</th>
			<th class="first-name filter-select" data-placeholder="Select...">ICASN</th>
			<th class="first-name filter-select" data-placeholder="Select...">IRESET</th>
			<th class="first-name filter-select" data-placeholder="Select...">VCASN</th>
			<th class="first-name filter-select" data-placeholder="Select...">VRESET_P</th>
			<th class="first-name filter-select" data-placeholder="Select...">VRESET_D</th>
			<th class="first-name filter-select" data-placeholder="Select...">VCLIP</th>
			<th class="first-name filter-select" data-placeholder="Select...">SUB</th>
			<th class="first-name filter-select" data-placeholder="Select...">PWELL</th>
			<th class="first-name filter-select" data-placeholder="Select...">Type</th>
		</thead>
		<tbody id="search_tbody">
		<?php
		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
			 echo "<tr>";
       echo "<td>".$row["runNumber"]."</td>";
       echo "<td>".$row["planeNumber"]."</td>";
       echo "<td>".$row["sample"]."</td>";
       echo "<td>".$row["IDB"]."</td>";
       echo "<td>".$row["ITHR"]."</td>";
       echo "<td>".$row["ICASN"]."</td>";
       echo "<td>".$row["IRESET"]."</td>";
       echo "<td>".$row["VCASN"]."</td>";
       echo "<td>".$row["VRESET_P"]."</td>";
       echo "<td>".$row["VRESET_D"]."</td>";
       echo "<td>".$row["VCLIP"]."</td>";
       echo "<td>".$row["SUB"]."</td>";
       echo "<td>".$row["PWELL"]."</td>";
       echo "<td>".$row["planetype"]."</td>";
       echo "</tr>";
			}
		} else {
		  echo "<tr>";
      echo "<td>Empty</td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "<td></td>";
      echo "</tr>";
		}
		?>
		
		</tbody>
	</table>
  <!-- pager -->
<div id="pager" class="pager">
  <form>
    <img src="../img/first.png" class="first"/>
    <img src="../img/prev.png" class="prev"/>
    <!-- the "pagedisplay" can be any element, including an input -->
    <span class="pagedisplay" data-pager-output-filtered="{startRow:input} &ndash; {endRow} / {filteredRows} of {totalRows} total rows"></span>
    <img src="../img/next.png" class="next"/>
    <img src="../img/last.png" class="last"/>
    <select class="pagesize">
      <option value="10">10</option>
      <option value="20">20</option>
      <option value="30">30</option>
      <option value="40">40</option>
      <option value="all">All Rows</option>
    </select>
  </form>
</div>
<?php 
}else{
    echo "Error connecting to database";
}
?>
</span>
</div>
<?php
  show_footer();
  // Display Script End time
  ?>
</div>
<script>
  var pager=true;
  var def_visible_rows=50;
process_table2("search", 0,0, pager);
</script>
</body>
</html>

