<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Module development</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>
<p class="TITLE">Module development</p>

<p class="SUBTITLE">Module Activities
  <ul>
    <li><a href="quadassembly.php">Pixel Quad Module Assembly</a></li>
    <li><a href="rd53a-module-qc.php">RD53A Quad Module QC</a></li>
    <li><a href="itkpix-module-qc.php">ITkPix Quad Module QC</a></li>
    <li><a href="../TSV">Through Silicon Vias</a></li>
  </ul>
</p>


</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
