<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Quad modules</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>
<p class="TITLE">Module Assembly</p>
<!--<img class="IMAGE" src="flex-module.png"/>-->

<p>The team is actively involved in assembly and qualification tests needed to ensure successful mass production of the hybrid modules.</p>
<p>During this early stage of the work, the necessary qualification tests and assembly methods are still being defined.
To this end, systematic testing is being conducted in the DSF lab on topics such as glue deposition as well as wire-bond encapsulant material dispensing.
This is being achieved through the combination of an Nordson EFD Ultimus V air pressure dispenser / controller and a Nordson EFD Pro4 Robot.</p> 
<img class="MEDIUMIMAGE" src="images/Nordson_EFD_Ultimus_V_Dispenser.png"/>
<img class="MEDIUMIMAGE" src="images/Nordson_EFD_Pro4_Robot.png"/>

<p></>Dispensing tests are currently being performed using Araldite 2011, but will transition to Sylgard 184/186 for encapsulant material testing.</p> 
<img class="MEDIUMIMAGE" src="images/Dispensing_Example.png"/>
<p></>Additional aims in the qualification of the module assembly will be to conduct visual assessments as well as metrology tests. To that end, a HiROX microscope made available through the CERN QART lab will be used for detailed imagery as well as the possibility to conduct height measurements during reception tests.</p>
<p></>The below images show the device along with a high resolution 3D map of an RD53 module flex.</p>
<p>
	<img class="MEDIUMIMAGE" src="images/Keyance_Microscope.png"/>
	<img class="IMAGEE" src="images/HiROX_3DImage.png"/>
</p>






<p class="SUBTITLE">Documentation and links
  <ul>
    <li><a href="https://indico.cern.ch/event/563948/contributions/2290213/attachments/1329280/1996896/Cern_report_modules-20160901.pdf">S. Kuehn. Module assembly at CERN - 1st September 2016</a></li>
    <li><a href="http://icwiki.physik.uni-bonn.de/svn/USBpixI4/pcb/BonnQuadFlex/Small-Flex/">Small-Flex design at UniBonn SVN</a></li>
    <li><a href="https://indico.cern.ch/event/547264/">ITK quad module assembly meeting - 29th June 2016</a></li>
  </ul>
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
