<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Quad modules</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>

<p class="TITLE">RD53A Quad Module QC</p>

<div>
  <img class="MEDIUMIMAGE" src="images/rd53a-quad-module-qc-setup-2.png"/>
</div>
<p></p>
<div>
  <img class="MEDIUMIMAGE" src="images/rd53a-quad-module-qc-setup.png"/>
</div>

<p class="SUBTITLE">Documentation and links
  <ul>
    <li><a href="https://indico.cern.ch/event/1075789/contributions/4557930/attachments/2324020/3964565/ModulesForOBDemonstrator.pdf">RD53A module QC</a></li>
    <li><a href="/eos/atlas/atlascerngroupdisk/det-itk/general/pixels/systemtest">Result storage location in EOS</a></li>
    <li><a href="https://twiki.cern.ch/twiki/bin/viewauth/Atlas/ModuleQC#RD53A_Module_QC">Procedure to setup RD53A quad module QC</a></li>
  </ul>
</p>

<p class="SUBTITLE">Presentations
  <ul>
    <li><a href="https://indico.cern.ch/event/1149581/contributions/4862389/attachments/2441889/4183325/RD53A%20review-AUW11052022.pdf">L. Flores, AUW May 2022</a></li>
    <li><a href="https://indico.cern.ch/event/1151523/contributions/4832343/attachments/2444823/4189157/20220516_QAQC_discbump_lmeng.pdf">L. Meng, ITK Pixel QA/QC workshop, May 2022</a></li>
  </ul>
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
