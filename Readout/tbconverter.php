<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Software</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">TBConverter</p>

<p class="SUBTITLE">Description</p>

<img class="IMAGE" style="width:40%;max-width:300px;float:right;margin-left:20px" src="tbconverter-flow.png">

TBConverter is an application used to process different data formats into common inputs for further analysis
It is divided into reading, filtering (algorithm execution) and writing parts
Version 2 is implemented using plug-ins that allow independent development of new IO formats or analysis functions.
Plug-ins are loaded at runtime to adapt the behavior of the application to the user's specific needs.
<ul>
  <li>Readers: DRS, RCE, Ascii (multiple text files), Judith, Ntuple</li>
  <li>Writers: Ascii (multiple text files), Judith, Ntuple</li>
  <li>Filters: WaveformAna, Filter3Tcell</li>
</ul>
<pre>
//Class prototypes
Reader{
 void Config(ConfigParser*)
 void SetInputFile(string)
 bool IsGood()
 Event GetNextEvent()
 string GetFileSuffix()
}
Writer{
 void Config(ConfigParser*)
 void SetOutputFile(string)
 void Write(Event)
 string GetFileSuffix()
} 
Filter{
 void Config(ConfigParser*)
 void Cut(Event)
}
</pre>

<p class="SUBTITLE">Running TBConverter</p>
TBConverter is installed in lxplus under the adecmos account. You just need to setup the environment shell and run.
A job configuration file is needed.
<ol>
  <li>source ~adecmos/work/public/TBC/setup.sh</li>
  <li>tbConverter -i input.data -o output.data -f job.cfg</li>
</ol>

<p class="SUBTITLE">Job configuration</p>
Configuration file uses simple Section, Key, Value format in plain text. Comments preceded by hash (#).
Keys are case insensitive. Requires only Job section, all other sections are optional.
Section names should match the plug-in types.
<pre>
###################################
# DRS readout to Ntuple
###################################

[Job]
    reader file : DRS
    reader type : DrsReader
    filter file : Filter3Tcell
    filter type : Filter3Tcell
    writer file : Root
    writer type : NtupleWriter
[End Job]

[DrsReader]
   ...
[End DrsReader]
</pre>

<p class="SUBTITLE">Development</p>
TBConverter package contains the core classes that are compiled as a library, and the main file to compiled as the application binary
Each package contains a cmt folder with the requirements file and a src folder with the source code. 
Other folders are created at compilation time but are not under version control.
Other packages depend on the core package and contain plug-ins that are compiled as libraries
There is no need to compile the plugins you don't need, main application binary loads only required libraries


<p class="SUBTITLE">Packages
<table>
  <tr><td>Name</td><td>Description</td></tr>
  <tr><td><a href="https://gitlab.cern.ch/solans/tbconverter-tbconverter">tbconverter</a></td><td>Core package</td></tr>
  <tr><td><a href="https://gitlab.cern.ch/solans/tbconverter-RCE">RCE</a></td><td>RCE dataformat reader</td></tr>
  <tr><td><a href="https://gitlab.cern.ch/solans/tbconverter-LeCroy">LeCroy</a></td><td>LeCroy binary reader</td></tr>
  <tr><td><a href="https://gitlab.cern.ch/solans/tbconverter-Judith">Judith</a></td><td>Judith analysis dataformat writer</td></tr>
  <tr><td><a href="https://gitlab.cern.ch/solans/tbconverter-DRS">DRS</a></td><td>DRS binary reader and writer</td></tr>
  <tr><td><a href="https://gitlab.cern.ch/solans/tbconverter-Filter3Tcell">Filter3Tcell</a></td><td>3Tcell filter</td></tr>
  <tr><td><a href="https://gitlab.cern.ch/solans/tbconverter-policy">policy</a></td><td>Policy package</td></tr>
</table>
</p>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
