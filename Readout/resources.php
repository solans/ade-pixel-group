<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Software</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Resources</p>

<p class="SUBTITLE">Openstack resources</p>

<p>
The group has an account in CERN's openstack to host virtual machines.
Membership is controlled through the ep-ade-id-cloud-services egroup.
</p>

<ul>
  <li><a href="https://openstack.cern.ch">Link to openstack</a></li>
  <li><a href="https://egroups.cern.ch">Link to egroups</a></li>
</ul>

<p class="SUBTITLE">ELOG</p>

<p>ELOG is available in the following link: <a href="https://itk-pixel-elog.cern.ch">here</a></p>

<p class="SUBTITLE">EOS space</p>

<table>
  <thead class="SUBSUBTITLE">
  <tr><td>Name</td><td>Location</td><td>Quota</td></tr>
  </thead>
  <tr>
    <td>Pixel Upgrade</td>
    <td><pre style="display:inline;">/eos/atlas/atlascerngroupdisk/pixel-upgrade</pre></td>
    <td>10 TB</td>
  </tr>
  <tr>
    <td>CMOS RnD</td>
    <td><pre style="display:inline;">/eos/atlas/atlascerngroupdisk/proj-cmos-sensors</pre></td>
    <td>10 TB</td>
    <td><a href="https://ascigmon.web.cern.ch/d/ClOCuVkmz/eos-quota-atlascerngroupdisk?orgId=1&var-QuotaNode=%2Feos%2Fatlas%2Fatlascerngroupdisk%2Fproj-cmos-sensors%2F">monitoring</a></td>
  </tr>
  <tr>
    <td>ITK Pixel OB demonstrator</td>
    <td><pre style="display:inline;">/eos/project/a/atlas-itk-pixel-ob-demonstrator</pre></td>
    <td>10 TB</td>
  </tr>
</table>

<p class="SUBTITLE">Leica Microscope camera software</p>

<p>Leica microscope is equiped with a <a href="https://www.ximea.com">XIMEA</a> USB micro-camera model MQ013CG-E2.
XiCAM control software is available for Windows, Linux and Mac.</p>
<p><a href="https://www.ximea.com/support/wiki/allprod/XIMEA_CamTool">Link to Ximea webpage.</a>


<p class="SUBTITLE">Links</p>
<ul>
  <li><a href="https://gitlab.cern.ch/groups/PH-ADE-ID_Pixel_RnD">Group gitlab repository</a></li>
  <li><a href="tbconverter.php">TBConverter software for data format conversion</a></li>
  <li><a href="Hardware.php">Common used hardware control libraries</a></li>
  <li><a href="SimpleTemperatureLogger-2016-07-20.zip">Software for temperature logger</a></li>
  <li><a href="SimplePressureLog.zip">Software for pressure logger</a></li>
</ul>
</p>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
