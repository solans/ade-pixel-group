<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Proteus</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Proteus</p>

<p class="SUBTITLE">Description</p>

Proteus is the software used for testbeam data analysis for pixel telescopes. 
It is tailored to reconstruct minimum ionizing particle (MIP) tracks 
using a multi-plane reference device, and projecting them into the device under test (DUT) sensors.

<p class="SUBTITLE">Limitations</p>
<ul>
  <li>No tracking in magnetic fields.</li>
  <li>Poor performance for densely occupied sensors.</li>
  <li>Poor performance for very complex detector geometries.</li>
</ul>

<p class="SUBTITLE">Program goals</p>
<ul>
  <li>Transparent: the main goal is for the program to be transparent to users. 
    It is important for the user to be able to understand the algorithms to know: 
    how they apply to their analysis, what are their limitations, how they can be extended. 
    The program should be easily understood and extended by non-expert-programmers (i.e. physicists).</li>
  <li>Fast: this program is expected to be used in testbeam type situations where time is constrained 
    and very valuable. Testbeam users will need to quickly see results so they can react to them in real-time. 
    The time scale for performing tasks should be minutes at most, and preferably seconds.</li>
  <li>Simple: this follows from the two previous points. The program should implement simple algorithms 
    which are efficient, easily understood, and easily extended. 
    This does mean that efficiency is sometimes sacrificed for simplicity.</li>
  <li>Portable (except for Windows): the program should be deployable in a variety of contexts. 
    E.g. on a user's laptop which can be carried into different laboratories, on a DAQ desktop 
    controlling some testbeam hardware, on a work computer for analysis. 
    The targeted deployment systems are Linux and Mac.</li>
</ul>

<p class="SUBTITLE">Module contents</p>
<ul>
  <li><b>storage:</b> memory representation of objects (e.g. Hits), and their input and output.</li>
  <li><b>mechanics:</b> description and manipulation of the physical properties of detector and detector systems 
    (e.g. alignment, noise mask).</li>
  <li><b>utils:</b> general purpose algorithms used in various parts of the program (e.g. linear fit).</li>
  <li><b>processors:</b> classes which process event-by-event information (e.g. clustering).</li>
  <li><b>analyzers:</b> classes which combine processors (and other elements) to perform analysis tasks, 
    called on an event-by-event basis (e.g. plotting algorithms).</li>
  <li><b>loopers:</b> control structures which loop over a dataset of events 
    (e.g. synchronization loop, event processing loop).</li>
  <li><b>configuration:</b> configuration and instantiation of program elements from configuration files.</li>
</ul>

<p class="SUBTITLE">Documentation and links
<ul>
  <li><a href="https://zenodo.org/record/2586736">Proteus beam telescope reconstruction</a></li>
  <li><a href="https://indico.desy.de/indico/event/16161/session/10/contribution/20/material/slides/0.pdf">M.Kiehn, BTTB5 Barcelona, January 2017</a></li>
  <li><a href="https://gitlab.cern.ch/unige-fei4tel/proteus/">Proteus repository in gitlab</a></li>
</ul>
</p>

</div>
<?php
  show_footer();
?>
</div>

</body>
</html>
