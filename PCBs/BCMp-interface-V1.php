<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>MALTA Interface Board</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">BCMprime Interface Board V1</p>
<p>This is an interface board between VLDB+ and BCM. It is used to interface SMA I/O LVDS signals with the VLDB+ J2 FMC connector.
</p>

<img class="MEDIUMIMAGE" alt="BCMp-interface-V1" src="BCMp-interface-V1.png">

<table>
  <tr>
    <th>Signal on BOB</th>
    <th>Better name</th>
    <th>FMC pin</th>
    <th>SMA connector</th>
  </tr>
  <tr>
    <td>EDIN0(0)_P</td>
    <td>Data_0_P</td>
    <td>C26</td>
    <td>3</td>
  </tr>
  <tr>
    <td>EDIN0(0)_N</td>
    <td>Data_0_N</td>
    <td>C27</td>
    <td>4</td>
  </tr>
  <tr>
    <td>EDIN1(0)_P</td>
    <td>Data_1_P</td>
    <td>G36</td>
    <td>5</td>
  </tr>
  <tr>
    <td>EDIN1(0)_N</td>
    <td>Data_1_N</td>
    <td>G37</td>
    <td>6</td>
  </tr>
  <tr>
    <td>EDIN2(0)_P</td>
    <td>Data_2_P</td>
    <td>G21</td>
    <td>7</td>
  </tr>
  <tr>
    <td>EDIN2(0)_N</td>
    <td>Data_2_N</td>
    <td>G22</td>
    <td>8</td>
  </tr>
  <tr>
    <td>EDIN3(0)_P</td>
    <td>Data_3_P</td>
    <td>C10</td>
    <td>9</td>
  </tr>
  <tr>
    <td>EDIN3(0)_N</td>
    <td>Data_3_N</td>
    <td>C11</td>
    <td>10</td>
  </tr>
  <tr>
    <td>EDOUT0(0)_P</td>
    <td>Cmd_0_P</td>
    <td>F16</td>
    <td>1</td>
  </tr>
  <tr>
    <td>EDOUT0(0)_N</td>
    <td>Cmd_0_N</td>
    <td>F17</td>
    <td>2</td>
  </tr>
</table>

<p class="SUBTITLE">Documentation and Links</p>
<ul>
  <li><a href="https://edms.cern.ch/document/2799882/">Project in EDMS</a></li>
</ul>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
