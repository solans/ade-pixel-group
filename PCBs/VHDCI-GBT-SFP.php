<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>/img/ATLAS-icon.ico">
<title>VHDCI HDMI A</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="IMAGERIGHT"><img class="SMALLIMAGE" alt="VHDCI-GBTx-SFP" src="VHDCI-GBTx-SFP.png"></div>
<div class="CONTENT">

<p class="TITLE">VHDCI-GBTx-SFP</p>
<p>The VHDCI-GBTx-SFP board is an interface designed for the FELIX read-out of the ITK demonstrator prototype with FEI4.
This board connects seamlessly to the CSB through the VHDCI and to FELIX through the SFP.</p>

<img class="IMAGE" alt="VHDCI-GBTx-SFP" src="VHDCI-GBTx-SFP-2.png">

<p class="SUBTITLE">Documentation and Links</p>
<ul>
  <li><a href="https://edms.cern.ch/item/EDA-03936-V1-0/0">Project in EDMS</a></li>
  <li><a href="https://edms.cern.ch/document/2029198/">Board manufacturing data (EDMS)</a></li>
  <li><a href="https://edms.cern.ch/document/2029199/">Assembly data (EDMS)</a></li>  
</ul>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
