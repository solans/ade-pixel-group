<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>/img/ATLAS-icon.ico">
<title>PCB design</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>
<p class="TITLE">PCB desgin</p>

<p class="SUBTITLE">PCB 
  <ul>

    <li><a href="<?=$gobase;?>PCBs/VHDCI-HDMI-A.php">VHDCI-HDMI-A</a></li>
    <li><a href="<?=$gobase;?>PCBs/VHDCI-GBT-SFP.php">VHDCI-GBT-SFP</a></li>
    <li><a href="<?=$gobase;?>PCBs/ERF-8xDisplayPort.php">ERF-8xDisplayPort</a></li>
    <li><a href="<?=$gobase;?>PCBs/MALTA-interface-V4.php">MALTA Interface Board</a></li>
    
  </ul>
</p>

<p class="SUBTITLE">Documentation and links
  <ul>
    <li><a href="https://edms.cern.ch/document/2771983">How to install CERN Altium library</a></li>
    <li><a href="https://em-design-office-data.web.cern.ch/altium_lib.asp">CERN Altium library</a></li>
  </ul>
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
