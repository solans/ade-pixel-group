<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<title>VHDCI HDMI A</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="IMAGERIGHT"><img class="SMALLIMAGE" alt="VHDCI-HDMI-A" src="VHDCI-HDMI-A.jpg"></div>
<div class="CONTENT">

<p class="TITLE">VHDCI HDMI A</p>
<p>The VHDCI HDMI A is an interface board for the ATLAS ITK outer barrel demonstrator 
for the FELIX read-out. The VHDCI connects to the CSB, and the HDMI-A connect to the VLDB.</p>

<p class="SUBTITLE">Presentations</p>

<ul>
  <li><a href="https://indico.cern.ch/event/712222/contributions/2927603/attachments/1617911/2572258/ASharma_CSB_Update_2018_03_15.pdf">A. Sharma, ITK pixel barrel demonstrator general meeting, March 2018</a></li>
</ul>

<p class="SUBTITLE">Documentation and Links</p>
<ul>
  <li><a href="https://edms.cern.ch/document/1973940/">VHDCI-HDMI-A (EDMS)</a></li>
</ul>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
