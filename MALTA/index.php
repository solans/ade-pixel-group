<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>/img/ATLAS-icon.ico">
<title>MALTA</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>
<p class="TITLE">MALTA</p>



<p class="SUBTITLE">Sections
  <ul>

    <li><a href="<?=$gobase;?>PublicPlots">PublicPlots</a></li>
    <li><a href="<?=$gobase;?>Authorship">Authorship</a></li>
    <li><a href="<?=$gobase;?>Authorship/?page=conferences">Conferences</a></li>
    <li><a href="<?=$gobase;?>LAPA">LAPA</a></li>
    
  </ul>
</p>

<p class="SUBTITLE">Publications
  <ul>
    <li><a href="<?=$gobase;?>Publications">Publications</a></li>
  </ul>
</p>


<img class="MEDIUMIMAGE" alt="MALTA on board" src="MALTA_on_board.jpg">

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
