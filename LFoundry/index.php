<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>LFoundry</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>

<div>
  <img class="IMAGE" src="images/LFoundry.png">  
</div>

<p class="TITLE">CCPD LFoundry
  	<ul>
		<li>Large fill factor for radiation hardness and charge collection</li>
    <li>Full CMOS , isolation via deep p-well (PSUB), wafer 2k ohms.cm</li>
    <li>24x114 pixels of 33x125 um</li>
    <li>3 CCPD pixels connected to one FE-I4 pixel</li>
    <li>First wafer arrived, chips under wire-bonding</li>
    <li>One passive sensor IV curve measured with 2nA/50V and breakdown at 120 V</li>
    <li>5 wafers in thinning and backside implant processing</li>
	</ul>
</p>

<p class="SUBTITLE">Documentation and links
  <ul>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Atlas/ItkHVCMOSLF">ITK CMOS L-Foundry Twiki</a></li>
    <li><a href="http://www.lfoundry.com">LFoundry web page</a></li>
  </ul>  
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
