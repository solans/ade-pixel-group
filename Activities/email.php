<?
include_once('class.phpmailer.php');
include_once('class.smtp.php');
	
class email{

	private $from;
	private $confirmTo;
	private $replyTo;
	private $to;
	private $cc;
	private $bcc;
	private $headers;
	private $charset = "ISO-8859-1";
	private $subject;
	private $message;
	private $server;
	private $useSmtp;
	private $lastError;
  private $authtype;
		
	public function __construct(){
		$this->setFrom("postmaster");
		$this->setServer("localhost",25,"postmaster","password");
		$this->useSmtp=false;
	}
	
	public function mimemail(){
		$this->__construct();
	}
	
	public function setSmtp($boolean){
		$this->useSmtp = $boolean;
	}
  
  public function setAuthType($auth){
    //LOGIN, PLAIN, NTLM
    $this->authType = strtoupper($auth);   
  }

  public function setCrypto($crypto){
    //SSL or TLS
    $this->crypto = strtolower($crypto);   
  }
  
	public function setServer($host,$port,$user,$pass){
		$this->server = array('host'=>$host,'port'=>$port,'user'=>$user,'pass'=>$pass);	
	}
	
	public function appendHeader($_header){
		$this->headers .= $_header;
	}
	
	public function getSubject(){
		return $this->subject;
	}
	
	public function setSubject($subject){
		$this->subject = $subject;
	}
	
	public function setFrom($from){
		$this->from = $this->email_split($from);
	}

	public function setSender($name,$address){
		$this->from = array('name'=>$name,'email'=>$address);
	}

	public function setReplyTo($to){
		$this->replyTo = $this->email_split($to);
  }
	
	public function setMessage($message){
		$this->message = $message;
	}
	
	public function getMessage(){
		return $this->message;
	}
	
	public function setRecipient($to){
		$this->setTo($to);
	}
	
	public function addTo($to){
		if($this->to==null) {$this->to=array();}
		if(is_array($to)){
			foreach($to as $item){ 
				$this->to[] = $this->email_split($item);
			}
		}else{
			$this->to[] = $this->email_split($to);
		}
	}

	public function addCC($cc){
	    if($this->cc==null) {$this->cc=array();}
	    if(is_array($cc)){
	        foreach($cc as $item){
	            $this->cc[] = $this->email_split($item);
	        }
	    }else{
	        $this->cc[] = $this->email_split($cc);
	    }
	}
	
	public function setTo($to){
		$this->to=array();
		if(is_array($to)){
			foreach($to as $item){ 
				$this->to[] = $this->email_split($item);
			}
		}else{
			$this->to[] = $this->email_split($to);
		}
	}
    
	public function setCC($cc){
		$this->cc=array();
		if(is_array($cc)){
			foreach($cc as $item){ 
				$this->cc[] = $this->email_split($item);
			}
		}else{
			$this->cc[] = $this->email_split($cc);
		}
	} 
	
	public function setBCC($bcc){
  	$this->bcc=array();
		if(is_array($bcc)){
			foreach($bcc as $item){ 
				$this->bcc[] = $this->email_split($item);
			}
		}else{
			$this->bcc[] = $this->email_split($bcc);
		}
	}
	
	public function setConfirm($to){
		$this->confirmTo = $this->email_split($to);
	}
	
	public function removeConfirm(){
		$this->confirmTo = null;
	}
	
	private function email_split( $str ){
    $name = $email = '';
    if (substr($str,0,1)=='<') {
        // first character = <
        $email = str_replace( array('<','>'), '', $str );
    } else if (strpos($str,' <') !== false) {
        // possibly = name <email>
        list($name,$email) = explode(' <',$str);
        $email = str_replace('>','',$email);
        $name = str_replace(array('"',"'"),'',$name);
    } else {
        // unknown
        $email = $str;
    }
    return array( 'name'=>trim($name), 'email'=>trim($email) );
	}

	public function send (){
		// Transport mail with phpmailer class	
		$mail=new phpmailer();
		$mail->XMailer='WebMailer by Carlos Solans based on PHPMailer';
		if($this->useSmtp==true){
			$mail->SMTPDebug=false;
			$mail->IsSMTP();
		    $mail->SMTPAuth = true;
            $mail->AuthType = $this->authType;
            $mail->SMTPSecure = $this->crypto;
			$mail->Host = $this->server['host'];
			$mail->Port = $this->server['port'];
			$mail->Username = $this->server['user'];
			$mail->Password = $this->server['pass'];
		}
		
		$mail->SetFrom($this->from['email'],$this->from['name']);
		if($this->confirmTo){
			$mail->ConfirmReadingTo=$this->confirmTo['email'];
		}
		if($this->replyTo){
			$mail->AddReplyTo($this->replyTo['email'],$this->replyTo['name']);
		}
		foreach($this->to as $recipient){
			$mail->AddAddress($recipient['email'],$recipient['name']);
		}
		if($this->cc){
			foreach($this->cc as $recipient){
				$mail->AddCC($recipient['email'],$recipient['name']);
			}
		}	
		if($this->bcc){
			foreach($this->bcc as $recipient){
				$mail->AddCC($recipient['email'],$recipient['name']);
			}
		}
		$mail->Subject=$this->subject;
		$mail->Body=$this->message;
		
		$send = $mail->Send();
		$this->lastError = $mail->ErrorInfo;
		return $send;
		
	}
	
	public function getLastError(){
		return $this->lastError;
	}
}

?>
