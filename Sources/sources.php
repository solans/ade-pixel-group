<h2>Sources</h2>
<button id="sources_reset">Reset filters</button>
<button id="sources_export">Export</button>
<div id="sources_found" style="display:inline-block"></div>
<table id="sources" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">Source ID</th>
		<th data-placeholder="Search...">Isotope</th>
		<th data-placeholder="Search...">Measurement</th>
		<th data-placeholder="Search...">Measurement date</th>
		<th data-placeholder="Search...">Location</th>
		<th data-placeholder="Search...">Location date</th>
	</thead>
	<tbody id="sources_body">
	</tbody>
</table>

<div id="sources_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#sources").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_sources();
});

$("#sources").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
});

$("#sources").on("filterEnd",function(){
  $("#sources_found").html("Found: "+($("#sources tr:visible").length-2))
});

$("#sources_export").click(function() {
  $("#sources").trigger("outputTable");
});

$("#sources_reset").click(function() {
  $("#sources").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

/** Then load members **/
function load_sources(){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {cmd:"get_sources_summary"},
    success: function(data) {
      console.log(data);
      sources=JSON.parse(data);
      $("#sources_body").empty();
      for (source of sources){
        tt="<tr>\n";
        tt+="<td><a href=\"index.php?page=source&source_id="+source["source_id"]+"\" title=\"See source\">"+source["source_id"]+"</a></td>";
        tt+="<td>"+source["isotope"]+"</td>";
        tt+="<td>"+source["measurement"]+"&nbsp;"+source["measurement_unit"]+"</td>";
        tt+="<td>"+source["measurement_date"]+"</td>";
        tt+="<td>"+source["location"]+"</td>";
        tt+="<td>"+source["location_date"]+"</td>";
        tt+="</tr>\n"; 
        $("#sources_body").append(tt);
      }
      $('#sources').trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#sources_found").html("Found: "+($("#sources tr:visible").length-2));
    }
  });
}


</script>
