<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_GET;

if($d["cmd"]=="add_measurement"){
  $sql ="INSERT INTO measurements ";
  $sql.=" (`source_id`, `date`, `measurement`, `unit`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["source_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["measurement"]."', ";
  $sql.="'".$d["unit"]."' ";
  $sql.=");";
}
if($d["cmd"]=="update_measurement"){
  $sql ="UPDATE measurements SET ";
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`measurement`='".$d["measurement"]."', ";
  $sql.="`unit`='".$d["unit"]."' ";
  $sql.="WHERE `id`='".$d["measurement_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="add_location"){
  $sql ="INSERT INTO locations ";
  $sql.=" (`source_id`, `date`, `location`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["source_id"]."', "; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["location"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_location"){
  $sql ="UPDATE locations SET ";
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`location`='".$d["location"]."' ";
  $sql.="WHERE `source_id`='".$d["source_id"]."' "; 
  $sql.=";";
}

echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
}else{
  $ret["error"]=$conn->error;  
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>