<h2>Locations</h2>
<button id="locations_reset">Reset filters</button>
<button id="locations_export">Export</button>
<div id="locations_found" style="display:inline-block"></div>
<table id="locations" class="tablesorter">
	<thead>
		<th class="filter-select">Location</th>
		<th class="filter-select">Date</th>
		<th class="filter-select">Tracking</th>
		<th class="filter-select">Reference</th>
		<th class="filter-select">Responsible</th>		
		<th>Actions</th>		
	</thead>
	<tbody id="locations_body">
	</tbody>
</table>

<script>

$(function() {
  $("#locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_locations("<?=@$_GET["sample_id"];?>");
});

$("#locations").tablesorter({
  theme: 'blue',
  sortList: [[1, 0]],
  widgets: ['filter','zebra','output']
});

$("#locations").on("filterEnd",function(){
  $("#locations_found").html("Found: "+($("#locations tr:visible").length-2))
});

$("#locations_export").click(function() {
  $("#locations").trigger("outputTable");
});

$("#locations_reset").click(function() {
  $("#locations").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_locations(sample_id){
  $.ajax({
    url: '<?=$gobase;?>/EnvMon/dbread.php',
    type: 'get',
    data: {cmd:"get_locations",sample_id:sample_id},
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#locations_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["tracking"]+"</td>";
        tt+="<td>"+row["reference"]+"</td>";
        tt+="<td>"+row["responsible"]+"</td>";
        tt+="<td>";
				tt+="<a href=\"index.php?page=location&location_id="+row["location_id"]+"\" title=\"Open\">view</a>&nbsp;";
        <? if ($authorised){?>
				tt+="<a href=\"#\" onclick=\"delete_location('"+row["location_id"]+"')\")>delete</a>";        
        <? } ?>
				tt+="</td>";
        tt+="</tr>\n"; 
        $("#locations_body").append(tt);
      }
      $("#locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#locations_found").html("Found: "+($("#locations tr:visible").length-2));
    }
  });
}
function delete_location(location_id){
  $.ajax({
    url: "<?=$gobase;?>/EnvMon/dbwrite.php",
    type: "get",
    data: {
      cmd:"delete_location",
      location_id:location_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      load_locations($("#sample_id").val());
    }
  });
}

</script>
