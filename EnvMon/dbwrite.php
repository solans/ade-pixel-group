<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_GET;

if($d["cmd"]=="add_sample"){
  $sql ="INSERT INTO samples ";
  $sql.=" (`other_id`, `description`,`type`,`product`,`material`,`handling_unit`,`lot`,`production_date`,`rohs`,`batch`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["other_id"]."',"; 
  $sql.="'".$d["description"]."', ";
  $sql.="'".$d["type"]."', ";
  $sql.="'".$d["product"]."', ";
  $sql.="'".$d["material"]."', ";
  $sql.="'".$d["handling_unit"]."', ";
  $sql.="'".$d["lot"]."', ";
  $sql.="'".$d["production_date"]."', ";
  $sql.="'".$d["rohs"]."', ";
  $sql.="'".$d["batch"]."' ";
  $sql.=");";
}
if($d["cmd"]=="update_sample"){
  $sql ="UPDATE samples SET ";
  $sql.="`other_id`='".$d["other_id"]."', ";
  $sql.="`description`='".$d["description"]."', ";
  $sql.="`type`='".$d["type"]."', ";
  $sql.="`product`='".$d["product"]."', ";
  $sql.="`material`='".$d["material"]."', ";
  $sql.="`handling_unit`='".$d["handling_unit"]."', ";
  $sql.="`lot`='".$d["lot"]."', ";
  $sql.="`production_date`='".$d["production_date"]."', ";
  $sql.="`rohs`='".$d["rohs"]."', ";
  $sql.="`batch`='".$d["batch"]."' ";
  $sql.="WHERE `sample_id`='".$d["sample_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_sample"){
  $sql ="DELETE FROM samples ";
  $sql.="WHERE sample_id='".$d["sample_id"]."';";
}
if($d["cmd"]=="add_measurement"){
	$chks=array("sample_id","date","measurement","unit","env_temp_celsius","shifter","type","equipment","result");
  $cols=array();
	$vals=array();
	foreach ($chks as $chk){
		if(@$d[$chk]!=""){$cols[]="`".$chk."`";$vals[]="'".$d[$chk]."'";}
	}
	$sql ="INSERT INTO measurements (";
  $sql.=implode(",",$cols);
	$sql.=") VALUES ( ";
  $sql.=implode(",",$vals);
  $sql.=");";
}
if($d["cmd"]=="update_measurement"){
	$chks=array("sample_id","date","measurement","unit","env_temp_celsius","shifter","type","equipment","result");
  $vals=array();
	foreach ($chks as $chk){
		if(@$d[$chk]!=""){$vals[]="`".$chk."`='".$d[$chk]."'";}
	}
  $sql ="UPDATE measurements SET ";
  $sql.=implode(",",$vals);
	$sql.="WHERE `measurement_id`='".$d["measurement_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_measurement"){
  $sql ="DELETE FROM measurements ";
  $sql.="WHERE measurement_id='".$d["measurement_id"]."';";
}
else if($d["cmd"]=="add_location"){
  $sql ="INSERT INTO locations ";
  $sql.=" (`sample_id`, `date`, `location`,`tracking`,`reference`,`responsible`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["sample_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["location"]."', ";
  $sql.="'".$d["tracking"]."', ";
  $sql.="'".$d["reference"]."', ";
  $sql.="'".$d["responsible"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_location"){
	$chks=array("sample_id","date","location","tracking","reference","responsible");
  $vals=array();
	foreach ($chks as $chk){
		if(@$d[$chk]!=""){$vals[]="`".$chk."`='".$d[$chk]."'";}
	}
  $sql ="UPDATE locations SET ";
  $sql.=implode(",",$vals);
	$sql.="WHERE `location_id`='".$d["location_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_location"){
  $sql ="DELETE FROM locations ";
  $sql.="WHERE location_id='".$d["location_id"]."';";
}

echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
}else{
  $ret["error"]=$conn->error;  
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>