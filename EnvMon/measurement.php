<h2>Measurement</h2>
<form method="GET" id="measurement">
<table id="sources" class="tablesorter">
  <tr><th>Measurement</th><td><input id="measurement_value" type="text"/></td></tr>
  <tr><th>Measurement unit</th><td>
    <select id="measurement_unit">
      <option value=""></option>
      <option value="Ohm">Ohm</option>
      <option value="kOhm">kOhm</option>
      <option value="MOhm">MOhm</option>
    </select>
  </td></tr>
  <tr><th>Measurement date</th><td><input id="measurement_date" type="text"/></td></tr>
  <tr><th>Env temperature celsius</th><td><input id="measurement_env_temp_celsius" type="text"/></td></tr>
  <tr><th>Shifter</th><td><input id="measurement_shifter" type="text"/></td></tr>
  <tr><th>Measurement type</th><td>
   <select id="measurement_type">
     <option value=""></option>
     <option value="Sensor">on sensor</option>
     <option value="Wires">after wires</option>
   </select>
  </td></tr>
  <tr><th>Test equipment</th><td><input id="measurement_equipment" type="text"/></td></tr>
  <tr><th>Result</th><td>
   <select id="measurement_result">
     <option value=""></option>
     <option value="Pass">Pass</option>
     <option value="Fail">Fail</option>
   </select>
  </td></tr>
</table>
<input type="hidden" id="measurement_id" value="<?=@$_GET["measurement_id"];?>">
<input type="hidden" id="sample_id" value="<?=@$_GET["sample_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="measurement_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#measurement_date").datepicker({dateFormat:"yy-mm-dd"});
 load_measurement();
});

$("#measurement").submit(function(){
  update_measurement();
  return false;
});

function load_measurement(){
  $.ajax({
    url: "<?=$gobase;?>/EnvMon/dbread.php",
    type: "get",
    data: {
      cmd:"get_measurement",
      measurement_id:$("#measurement_id").val()
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      reply=rows[0];
      $("#measurement_value").val(reply["measurement"]);
      $("#measurement_unit").val(reply["unit"]);
      $("#measurement_date").val(reply["date"]);
      $("#measurement_env_temp_celsius").val(reply["env_temp_celsius"]);
      $("#measurement_equipment").val(reply["equipment"]);
      $("#measurement_shifter").val(reply["shifter"]);
      $("#measurement_type").val(reply["type"]);
      $("#measurement_result").val(reply["result"]);
    }
  });  
}

function update_measurement(){
  $.ajax({
    url: "<?=$gobase;?>/EnvMon/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_measurement",
      measurement_id:$("#measurement_id").val(),
      sample_id:$("#sample_id").val(),
      measurement:$("#measurement_value").val(),
      unit:$("#measurement_unit").val(),
      date:$("#measurement_date").val(),
      env_temp_celsius:$("#measurement_env_temp_celsius").val(),
      equipment:$("#measurement_equipment").val(),
      shifter:$("#measurement_shifter").val(),
      type:$("#measurement_type").val(),
      result:$("#measurement_result").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#measurement_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#measurement_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#measurement_reply").text("Measurement stored");
        load_measurement();
      }
    }
  });
}
</script>
