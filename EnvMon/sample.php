<h2>Sample</h2>
<form method="GET" id="sample">
	<table id="sample">
  	<tr><th>ATLAS ID</th><td><input id="atlas_id" type="text"/></td></tr>
  	<tr><th>Serial number</th><td><input id="serialnumber" type="text"/></td></tr>
  	<tr><th>Description</th><td><input id="description" type="text"/></td></tr>
  	<tr><th>Type</th><td><input id="type" type="text"/></td></tr>
  	<tr><th>Product</th><td><input id="product" type="text"/></td></tr>
  	<tr><th>Material</th><td><input id="material" type="text"/></td></tr>
  	<tr><th>Handling unit</th><td><input id="handling_unit" type="text"/></td></tr>
  	<tr><th>Lot</th><td><input id="lot" type="text"/></td></tr>
  	<tr><th>Production date</th><td><input id="production_date" type="text"/></td></tr>
  	<tr><th>ROHS</th><td>
	    <select id="rohs">
	      <option value=""></option>
	      <option value="compliant">comliant</option>
	      <option value="non-compliant">non-compliant</option>
	    </select>
		</td></tr>
  	<tr><th>Batch</th><td><input id="batch" type="text"/></td></tr>
	</table>
	<input type="hidden" id="sample_id" value="<?=@$_GET["sample_id"];?>">
	<input type="submit" value="Save">
	<input type="reset" value="Reset">
</form>

<div id="sample_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#production_date").datepicker({dateFormat:"yy-mm-dd"});
  if($("#sample_id").val()!=""){load_sample();}
});

$("#sample").submit(function(){
  if($("#sample_id").val()!=""){update_sample();}else{add_sample();}
  return false;
});

function load_sample(){
  $.ajax({
    url: '<?=$gobase;?>/EnvMon/dbread.php',
    type: 'get',
    data: {
      cmd:"get_sample",
      sample_id:$("#sample_id").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      sample=reply[0];
      $("#atlas_id").val(sample['atlas_id']);
      $("#serialnumber").val(sample['serialnumber']);
      $("#description").val(sample['description']);
      $("#type").val(sample['type']);
      $("#product").val(sample['product']);
      $("#material").val(sample['material']);
      $("#handling_unit").val(sample['handling_unit']);
      $("#lot").val(sample['lot']);
      $("#production_date").val(sample['production_date']);
      $("#rohs").val(sample['rohs']);
      $("#batch").val(sample['batch']);

    }
  });
}

function add_sample(){
  $.ajax({
    url: "<?=$gobase;?>/EnvMon/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_sample",
      atlas_id:$("#atlas_id").val(),
      serialnumber:$("#serialnumber").val(),
      description:$("#description").val(),
      type:$("#type").val(),
      product:$("#product").val(),
      material:$("#material").val(),
      handling_unit:$("#handling_unit").val(),
      lot:$("#lot").val(),
      production_date:$("#production_date").val(),
      rohs:$("#rohs").val(),
      batch:$("#batch").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_sample_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_sample_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_sample_reply").text("Stored");
      }
    }
  });
}

function update_sample(){
  $.ajax({
    url: "<?=$gobase;?>/EnvMon/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_sample",
      sample_id:$("#sample_id").val(),
      atlas_id:$("#atlas_id").val(),
      serialnumber:$("#serialnumber").val(),
      description:$("#description").val(),
      type:$("#type").val(),
      product:$("#product").val(),
      material:$("#material").val(),
      handling_unit:$("#handling_unit").val(),
      lot:$("#lot").val(),
      production_date:$("#production_date").val(),
      rohs:$("#rohs").val(),
      batch:$("#batch").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#sample_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#sample_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#sample_reply").text("Sample stored");
      }
    }
  });
}

</script>
