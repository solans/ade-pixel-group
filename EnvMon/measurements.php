<h2>Measurements</h2>
<button id="measurements_reset">Reset filters</button>
<button id="measurements_export">Export</button>
<div id="measurements_found" style="display:inline-block"></div>
<table id="measurements" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">Measurement</th>
		<th data-placeholder="Search...">Date</th>
		<th data-placeholder="Search...">Type</th>
		<th data-placeholder="Search...">Equipment</th>
		<th data-placeholder="Search...">Env temp [C]</th>		
		<th data-placeholder="Search...">Result</th>		
		<th data-placeholder="Search...">Shifter</th>		
		<th>Actions</th>
	</thead>
	<tbody id="measurements_body">
	</tbody>
</table>

<div id="measurements_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#measurements").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_measurements("<?=@$_GET["sample_id"];?>");
});

$("#measurements").tablesorter({
  theme: 'blue',
  sortList: [[1, 0]],
  widgets: ['filter','zebra','output']
});

$("#measurements").on("filterEnd",function(){
  $("#measurements_found").html("Found: "+($("#measurements tr:visible").length-2))
});

$("#measurements_export").click(function() {
  $("#measurements").trigger("outputTable");
});

$("#measurements_reset").click(function() {
  $("#measurements").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_measurements(sample_id){
	req={cmd:"get_measurements"};
	if(sample_id!=""){req["sample_id"]=sample_id;}
	$.ajax({
    url: '<?=$gobase;?>/EnvMon/dbread.php',
    type: 'get',
    data: req,
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#measurements_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["measurement"]+"&nbsp;"+row["unit"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["type"]+"</td>";
        tt+="<td>"+row["equipment"]+"</td>";
        tt+="<td>"+row["env_temp_celsius"]+"</td>";
        tt+="<td>"+row["result"]+"</td>";
        tt+="<td>"+row["shifter"]+"</td>";
        tt+="<td>";
				tt+="<a href=\"index.php?page=measurement&sample_id="+row["sample_id"]+"&measurement_id="+row["measurement_id"]+"\">update</a>&nbsp;";
        <? if ($authorised){?>
				tt+="<a href=\"#\" onclick=\"delete_measurement('"+row["measurement_id"]+"');return false;\">delete</a>";
				<? } ?>
				tt+="</td>";
				tt+="</tr>\n"; 
        $("#measurements_body").append(tt);
      }
      $("#measurements").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#measurements_found").html("Found: "+($("#measurements tr:visible").length-2));
    }
  });
}

function delete_measurement(id){
  if(!window.confirm("Are you sure to delete measurement?")) return false;
  $("#measurements_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/EnvMon/dbwrite.php',
    type: 'get',
    data: {cmd:"delete_measurement",measurement_id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; 
        if(typeof load_measurements === 'function'){load_measurements("<?=@$_GET["sample_id"];?>");}
      }
      $("#measurements_reply").text(sreply);
    }
  });
  return false;
};
</script>
