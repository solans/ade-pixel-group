<h2>Samples</h2>
<button id="samples_reset">Reset filters</button>
<button id="samples_export">Export</button>
<div id="samples_found" style="display:inline-block"></div>
<table id="samples" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">ATLAS ID</th>
		<th data-placeholder="Search...">Serial number</th>
		<th class="filter-select">Batch</th>
		<th class="filter-select">Handling unit</th>
		<th class="filter-select">Lot numbe</th>
		<th class="filter-select">Production date</th>
		<th class="filter-select">Material</th>
		<th class="filter-select">ROHS</th>
		<th data-placeholder="Search...">Type</th>
		<th data-placeholder="Search...">Product</th>
		<th data-placeholder="Search...">Description</th>
		<th>Actions</th>
	</thead>
	<tbody id="samples_body">
	</tbody>
</table>

<div id="samples_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#samples").trigger("update").trigger("appendCache").trigger("applyWidgets");
	samples_load();
});

$("#samples").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
});

$("#samples").on("filterEnd",function(){
  $("#samples_found").html("Found: "+($("#samples tr:visible").length-2))
});

$("#samples_export").click(function() {
  $("#samples").trigger("outputTable");
});

$("#samples_reset").click(function() {
  $("#samples").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

/** Then load members **/
function samples_load(){
  $.ajax({
    url: '<?=$gobase;?>/EnvMon/dbread.php',
    type: 'get',
    data: {cmd:"get_samples"},
    success: function(data) {
      console.log(data);
      samples=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#samples_body").empty();
      mem=[];
      for (sample of samples){
        if(mem.includes(sample["sample_id"])) continue;
        mem.push(sample["sample_id"]);
        tt="<tr>\n";
        tt+="<td>"+sample["atlas_id"]+"</td>";
        tt+="<td>"+sample["serialnumber"]+"</td>";
        tt+="<td>"+sample["batch"]+"</td>";
        tt+="<td>"+sample["handling_unit"]+"</td>";
        tt+="<td>"+sample["lot"]+"</td>";
        tt+="<td>"+sample["production_date"]+"</td>";
        tt+="<td>"+sample["material"]+"</td>";
        tt+="<td>"+sample["rohs"]+"</td>";
        tt+="<td>"+sample["type"]+"</td>";
        tt+="<td>"+sample["product"]+"</td>";
        tt+="<td>"+sample["description"]+"</td>";
        tt+="<td>";
				tt+="<a href=\"index.php?page=sample&sample_id="+sample["sample_id"]+"\" title=\"Open\">view</a>&nbsp;";
        <? if ($authorised){?>
					tt+="<a href=\"#\" onclick=\"sample_delete('"+sample["sample_id"]+"');return false;\">delete</a>";
        <?}?>
				tt+="</td>";
        tt+="</tr>\n"; 
        $("#samples_body").append(tt);
      }
      $('#samples').trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#samples_found").html("Found: "+($("#samples tr:visible").length-2));
    }
  });
}

/* delete */
function sample_delete(id){
  if(!window.confirm("Are you sure to delete sample?")) return false;
  $("#samples_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/dbwrite.php',
    type: 'get',
    data: {cmd:"delete_sample",sample_id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; 
        if(typeof samples_load === 'function'){samples_load();}
      }
      $("#samples_reply").text(sreply);
    }
  });
  return false;
};

</script>
