<h2>Location</h2>
<form method="GET" id="location">
<table id="sources" class="tablesorter">
  <tr><th>Location</th><td><input id="location_location" type="text"/></td></tr>
  <tr><th>Date</th><td><input id="location_date" type="text"/></td></tr>
  <tr><th>Tracking</th><td>
    <select id="location_tracking">
      <option value=""></option>
      <option value="EDH">EDH</option>
      <option value="DHL">DHL</option>
      <option value="UPS">UPS</option>
    </select>  
  </td></tr>
  <tr><th>Reference</th><td><input id="location_reference" type="text"/></td></tr>
  <tr><th>Responsible</th><td><input id="location_responsible" type="text"/></td></tr>
</table>
<input type="hidden" id="location_sample_id" value="<?=@$_GET["sample_id"];?>">
<input type="hidden" id="location_id" value="<?=@$_GET["location_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="location_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#location_date").datepicker({dateFormat:"yy-mm-dd"});
 if($("#location_id").val()!=""){load_location();}
});

$("#location").submit(function(){
  if($("#location_id").val()!=""){update_location();}else{add_location();}
  return false;
});

function load_location(){
  $.ajax({
    url: '<?=$gobase;?>EnvMon/dbread.php',
    type: 'get',
    data: {
      cmd:"get_location",
      location_id:$("#location_id").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      row=reply[0];
      $("#location_location").val(row['location']);
      $("#location_date").val(row['date']);
      $("#location_tracking").val(row['tracking']);
      $("#location_reference").val(row['reference']);
      $("#location_responsible").val(row['responsible']);
      $("#location_sample_id").val(row['sample_id']);
    }
  });
}

function add_location(){
  $.ajax({
    url: "<?=$gobase;?>EnvMon/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_location",
      sample_id:$("#location_sample_id").val(),
      location:$("#location_location").val(),
      date:$("#location_date").val(),
      tracking:$("#location_tracking").val(),
      reference:$("#location_reference").val(),
      responsible:$("#location_responsible").val()
  },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#location_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#location_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#location_reply").text("Stored");
        if(typeof load_locations === 'function'){load_locations($("#location_sample_id").val())};
      }
    }
  });
}

function update_location(){
  $.ajax({
    url: "<?=$gobase;?>EnvMon/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_location",
      location_id:$("#location_id").val(),
      sample_id:$("#location_sample_id").val(),
      location:$("#location_location").val(),
      date:$("#location_date").val(),
      tracking:$("#location_tracking").val(),
      reference:$("#location_reference").val(),
      responsible:$("#location_responsible").val()
  },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#location_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#location_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#location_reply").text("Stored");
        if(typeof load_locations === 'function'){load_locations($("#location_sample_id").val())};
      }
    }
  });
}
</script>
