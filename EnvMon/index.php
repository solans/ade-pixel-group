<?php
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<title>Environmental monitoring sample database</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<div class="CONTENT">
<?php
  show_login(); 
?>
<p class="TITLE">Environmental monitoring Sample database</p>

<ul>
  <li><a href="?page=summary">Summary</a></li>
  <li><a href="?page=samples">Samples</a></li>
  <li><a href="?page=measurements">Measurements</a></li>
  <li><a href="?page=locations">Locations</a></li>
</ul>
 
<?php
if(@$_GET['page']=="summary"){
  include("summary.php");
}
else if(@$_GET['page']=="samples"){
  include("samples.php");
  include("sample.php");
}
else if(@$_GET['page']=="sample"){
  include("sample.php");
  include("measurements.php");
  include("measurement.php");
  include("locations.php");
  include("location.php");
}
else if(@$_GET['page']=="measurements"){
  include("measurements.php");
  include("measurement.php");
}
else if(@$_GET['page']=="measurement"){
  include("measurement.php");
}
else if(@$_GET['page']=="locations"){
  include("locations.php");
  include("location.php");
}
else if(@$_GET['page']=="location"){
  include("location.php");
}
?>



</div>
<?php
  show_footer();
?>
</div>
</body>
</html>

