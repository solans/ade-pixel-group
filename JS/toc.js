function loadToc(){
  console.log("Building TOC");
  var toc = "<nav role='navigation' class='table-of-contents'>" +
            "<h2>Table of contents:</h2>" +
            "<ul>";

  $(".CONTENT h2").each(function() {
    var el = $(this);
    var title = el.text();
    var id = el.attr("id");
    if (typeof id=="undefined"){
      id=Math.random().toString(36).substring(7);
      el.attr("id",id);
    }
    var link = "#" + id;
    var newLine = "<li><a href='" + link + "'>"+title+"</a></li>";
    toc += newLine;
   });

   toc += "</ul></nav>";

   $(".CONTENT").prepend(toc);
   console.log("TOC built");
}