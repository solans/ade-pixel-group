<?php
  include_once('../functions.php');
  function sortbyyear($a, $b){
      return strcmp($b->year, $a->year);
  }
  function randomstring(){
      $arr = str_split('abcdefghijklmnopqrstuwxyz'); // get all the characters into an array
      shuffle($arr); // randomize the array
      $arr = array_slice($arr, 0, 6); // get the first six (random) characters out
      return implode('', $arr); // smash them back into a string
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<title>Publications</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">
<?php   
  show_certificate(); 
  show_login();
?>

<?php
  $authorised=isAuthorised();
?>


<p class="TITLE">List of publications</p>

<table style="border:0">
<?
  
		
  include_once("Parser.php");
  include_once("publications_helper.php");
	
  $filename = getcwd()."/data/publications.xml";
  $parser = new Parser();
  $helper = new Publications_Helper();
  $helper->verbose = (@$_GET["debug"]=="true"?true:false);
		
  $parser->SetFilename($filename);
  $parser->SetDataHolder($helper);
  $parser->Parse();
		
  $publications = $helper->GetData();
		
  include_once("querystring.php");
  $qs = new querystring();
  ?>
  <tr>
    <td> 
	<? 
	if(@$_GET["action"]=="admin" && $authorised){
	    $qs->remove("action");
	    $qs->remove("do");
	    $qs->remove("id");
	    ?><a href="<?=$_SERVER['SCRIPT_NAME'].$qs->toFullString();?>">View</a><? 
	    $qs->set("action",$_GET["action"]);
	    if(isset($_GET["do"])) $qs->set("do",$_GET["do"]);
        if(isset($_GET["id"])) $qs->set("id",$_GET["id"]);
	}else if($authorised){
        $qs->remove("do");
        $qs->remove("id");
	    $qs->set("action","admin");
        ?><a href="<?=$_SERVER['SCRIPT_NAME'].$qs->toFullString();?>">Admin</a><? 
    }
	?>
	</td>
	</tr>
	<?		
	if(@$_GET["action"]=="admin" && $authorised){	
	if(@$_POST["action"]=="add"){
        $publication = new Publication();
        $publication->note=$_POST["note"];
        $publication->title = $_POST["title"];
        $publication->author = $_POST["author"];
        $publication->journal = $_POST["journal"];
        $publication->volume = $_POST["volume"];
        $publication->year = $_POST["year"];
        $publication->url = $_POST["url"];
		$publication->id = $publications->GetLastId()+1;
		//parse note
		$id=randomstring();
		//parse tags with a comma in between
        $tags=array();
        if($_POST["tags"]!=""){ $tags=array_map('trim', explode(",",str_replace(";",",",$_POST["tags"])));}
        $publication->tags = $tags;
        //Append to file
		$publications->AddPublication($publication);
		$fh = fopen($filename, 'w');
		fwrite($fh, $publications->ToXml());
		fclose($fh);	
		$qs = new querystring(); 
		$qs->set('action','admin');
		$qs->remove('do');
		redirect($_SERVER['PHP_SELF'].$qs->toFullString());
	}else if(@$_POST["action"]=="edit"){
		$publication = $publications->GetPublication($_GET["id"]);
		$publication->note=$_POST["note"];
		$publication->title = $_POST["title"];
		$publication->author = $_POST["author"];
		$publication->journal = $_POST["journal"];
		$publication->volume = $_POST["volume"];
		$publication->year = $_POST["year"];
		$publication->url = $_POST["url"];
		//parse tags with a comma in between
        $tags=array();
        if($_POST["tags"]!=""){ $tags=array_map('trim', explode(",",str_replace(";",",",$_POST["tags"])));}
        $publication->tags = $tags;
        //Append to file
		//$publications->AddPublication($publication);
		$fh = fopen($filename, 'w');
		fwrite($fh, $publications->ToXml());
		fclose($fh);	
		$qs = new querystring(); 
		$qs->set('action','admin');
		$qs->remove('do');
		redirect($_SERVER['PHP_SELF'].$qs->toFullString());
	}else if(@$_GET["do"]=="remove" && $authorised){
		$publications->DelPublication($_GET["id"]);
		$publications->Dump();
		$fh = fopen($filename, 'w');
		fwrite($fh, $publications->ToXml());
		fclose($fh);	
		$qs = new querystring(); 
		$qs->remove('do');
		$qs->remove('id');
		redirect($_SERVER['PHP_SELF'].$qs->toFullString());
	}else{
	   ?>
		<tr>
        <td style="text-align:left">
	    <table style="text-align:left; width:100%">
       	<?
       	usort($publications->data, "sortbyyear");
       	foreach($publications->data as $p){
		?>
		<tr>
		  <td>
			<?=$p->author;?>,&nbsp;<?=$p->title;?>,&nbsp;<?=$p->journal;?>&nbsp;<?=$p->volume;?>&nbsp;(<?=$p->year;?>) &nbsp;<?=$p->note;?>,&nbsp;<a target="_blank" href="<?=$p->url;?>">link</a>
			<a href="<? $qs->set('do','edit'); $qs->set('id',urlencode($p->id)); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">edit</a> 
			<a href="<? $qs->set('do','remove'); $qs->set('id',urlencode($p->id)); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">delete</a> 
          </td>
		</tr>
		<?
		} 
		?>
		</table>
        
 		</div>
		<a href="<? $qs->remove('do'); $qs->remove('id'); echo $_SERVER['PHP_SELF'].'?'.$qs->toString();?>" >Add</a>
		<form name="editor" action="<?=$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];?>" method="POST">
		  <input type="hidden" name="action" value="add"/>
		  <input type="hidden" name="id" value=""/>
		  <label style="display:inline-block;width:100px;">Title(*):</label><input type="text" name="title" size="80"/><br/>
		  <label style="display:inline-block;width:100px;">Author:</label><input type="text" name="author" size="80"/><br/>
		  <label style="display:inline-block;width:100px;">Journal:</label><input type="text" name="journal" size="80"/><br/>
		  <label style="display:inline-block;width:100px;">Volume:</label><input type="text" name="volume" size="80"/><br/>	
		  <label style="display:inline-block;width:100px;">Year:</label><input type="text" name="year" size="80"/><br/>
		  <label style="display:inline-block;width:100px;">Pages/Note:</label><input type="text" name="note" size="80"/><br/>	
		  <label style="display:inline-block;width:100px;">URL:</label><input type="text" name="url" size="80"/><br/>
		  <label style="display:inline-block;width:100px;">DOI:</label><input type="text" name="doi" size="80"/><br/>
          <label style="display:inline-block;width:100px;">Tags:</label><input type="text" name="tags" size="80"/> <br>
          (separated by comma)<br/>
		  <input name="submit" type="submit" value="Add" /><br>
          * Required fields
		</form>
        <?
		if(@$_GET["do"]=="edit" && $authorised){
		  $cc = $publications->GetPublication($_GET["id"]);
		  ?>
		  <script>
		  document.forms["editor"].elements["action"].value='edit';
		  document.forms["editor"].elements["id"].value='<?=$cc->id;?>';
		  document.forms["editor"].elements["author"].value='<?=$cc->author;?>';
		  document.forms["editor"].elements["title"].value='<?=$cc->title;?>';
		  document.forms["editor"].elements["journal"].value='<?=$cc->journal;?>';
		  document.forms["editor"].elements["year"].value='<?=$cc->year;?>';
          document.forms["editor"].elements["volume"].value='<?=$cc->volume;?>';
		  document.forms["editor"].elements["note"].value='<?=$cc->note;?>';
		  document.forms["editor"].elements["url"].value='<?=$cc->url;?>';
          document.forms["editor"].elements["tags"].value='<?=join(", ",$cc->tags);?>';
          document.forms["editor"].elements["submit"].value='Update';
		  </script>
		  <?
		}
		?>
        </td>
        </tr>
        <?
	}
	}else{
    	?>
        <tr>
          <td>
        	<?
			if(@$_GET["debug"]=="true"){$publications->Dump();}
			?>
          </td>
        </tr>
		<?
		usort($publications->data, "sortbyyear");
		foreach($publications->data as $p){
		?>
        <tr>
			<td>
				<!-- <?php $p->Dump(); ?> -->
        <?=$p->author;?>,&nbsp;<?=$p->title;?>,&nbsp;<?=$p->journal;?>&nbsp;<?=$p->volume;?>&nbsp;(<?=$p->year;?>) &nbsp;<?=$p->note;?>,&nbsp;<a target="_blank" href="<?=$p->url;?>">link</a>
        	</td> 
		</tr>
		<?
		} 
	}
	?>
	</table>
</div>
<?
show_footer();
?>
</body>
</html>