<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="/JS/toc.js"></script>

<title>X-ray machine help</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>


<div class="CONTENT">

<script>
$(document).ready(loadToc);
</script>

<h1 class="TITLE">X-ray Irradiator Help page</h1>

This page contains information concerning the operation of the X-ray machine in 161-1-007.
For any questions please contact the installation supervisor.

<h2 class="SUBTITLE">Start the irradiation</h2>

<ol>
<li>
The irradiaion of the X-ray machine is controlled from the touch screen on the side.
Insert the system key and turn it to ON. 
Wait for the system to boot and the user interface to start.
<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/touch-screen.png"/>
</li>
<li>
On the application login screen, enter the username and password provided by the supervisor.
<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/login-screen.png"/>
</li>
<li>
After the login screen, you will be presented with a page to select the program to run.
Press MANUAL MODE.
<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/welcome-screen.png"/></li>
<li>
Once in manual mode enter the desired voltage, current, exposure time, and SSD. Typical values are the following:
  <ul>
    <li>Voltage: 160 kV</li>
    <li>Current: 18.25 mA</li>
    <li>SSD: 50/0</li>
    <li>Exposure time maximum: 99999 s</li>
  </ul>
To continue, press on CONTINUE.<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/manual-mode.png"/>
</li>
<li>
The last step before irradiation is the warm-up sequence. A message will display requesting it.
Press on "X-RAYS START" to start the warm-up sequence. 
<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/warmup-screen.png"/>
</li>
<li>
After the warm up sequence, it is necessary to press on the "X-RAYS START" button again.
Press on "X-RAYS START" to start the irradiation.
<br/>
X-ray irradiation will last for the amount of time set in exposure time.
After that the irradiation can be resumed.
</li>
</ol>
  
  
<h2 class="SUBTITLE">How do I measure the distance of my sample</h2>
<p>Distance is measured from the <b>red dot</b> to the center of the cross-hair from the Laser pointer. 
<img class="MEDIUMIMAGE" style="margin:4em" src="images/distance-measurement.jpg" />
</p>

<h2 class="SUBTITLE">Error: Interlock one open</h2>
This error might show up when you press START. Things to be checked are:
<ol>
<li>The key is in ON position.<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/touch-rad-key.png"/>
</li>
<li>The emergency switch is not pushed.<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/touch-rad-button.png"/></li>
<li>The pins on the door are touching the connectors in the chamber.</li>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/20241016_163708.jpg"/></li>
<li>Baffle is closed and both interlocks touch the casing.<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/baffle-interlock.png"/>
</li>
<li>Cooling is on and all connectors are tight.<br/>
<img class="IMAGEMEDIUM" style="margin:2em" src="images/cooling-pipes.png"/>
</li>
<li>X-ray light bulb on top of the machine is not fused.</li>
</ol>


<h2 class="SUBTITLE">Chiller liquid low</h2>
<ol>
  <li>Power off the chiller</li>
  <li>Look for the bottle of coolant liquid.</li>
  <li>Open the top of the chiller</li>
  <li>Fill the chiller up to the rim</li>
  <li>Close the lid</li>
  <li>Power on the chiller</li>
</ol>

<h2 class="SUBTITLE">Humidity and temperature monitoring</h2>
<ol>
  <li>Check the probes are installed inside the chamber.<br/>
      <img class="IMAGEMEDIUM" src="images/logo-1.png"/>
  </li>
  <li>Check the LOGO chasis is powered and connected to the network.<br/>
    <img class="IMAGEMEDIUM" src="images/logo-2.png"/>
  </li>
</ol>

<h2 class="SUBTITLE">Other errors from X-ray machine</h2>
<table>
  <tr>
    <td>Error message</td><td>Explanation</td>
  </tr>
  <tr>
    <td>Application error 3</td><td>No communication between touchRAD & spellman generator due cable connection or ip problem </td>
  </tr>
  <tr>
    <td>Application error 11</td><td>Usb camera mixing, someone unplugged the usb camera outside of the cabinet. </td>
  </tr>
  <tr>
    <td>Short Circuit</td><td></td>
  </tr>
  <tr>
    <td>Error Filament feedbac</td><td></td>
  </tr>
  <tr>
    <td>Error power supply 11</td><td></td>
  </tr>
  <tr>
    <td>Other errors on the Spellman Generator</td><td></td>
  </tr>
  <tr>
    <td>Power supply fault</td><td></td>
  </tr>
  <tr>
    <td>Interlock Open fault</td><td></td>
  </tr>
  <tr>
    <td>UC fault</td><td></td>
  </tr>
  <tr>
    <td>Over power fault</td><td></td>
  </tr>
  <tr>
    <td>Filament foult</td><td></td>
  </tr>
  <tr>
    <td>High voltage invert ion fault</td><td></td>
  </tr>
  <tr>
    <td>Over temperature fault</td><td></td>
  </tr>
</table>


</div>
<?php
  show_footer();
?>
</div>

</body>
</html>

