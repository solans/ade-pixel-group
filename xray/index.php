<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>X-ray machine</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">X-ray Irradiator</p>

<p class="SUBTITLE">Machine details</p>

<img class="IMAGERIGHT IMAGEW600" src="images/web-xray-location.png"/>

<p class="TEXT"><a href="http://www.pxinc.com">Precision X-Ray</a> X-RAD iR160 biological irradiator 
is installed in the far end corner of the laboratory 161-1-007 with CERN machinery ID M33-023.
The machine is composed of an irradiation chamber, a control panel display, a HV system and a cooling box.
</p>

<p class="SUBTITLE">Installation details</p>

<p class="TEXT">
The irradiator is calibrated with a silicon sensor up to a dose rate of 3.75 MRad/h at a distance of 10 cm from the source.
This allows the study of irradiation effects on pixel detectors, 
imitating as much as possible the variable, low dose irradiation over long periods of time of the ATLAS experiment. 
The chips can be placed inside the machine at different heights and exposed to different dose rates. 
</p>

<p>
Cooling pipes, power and read-out cables required by the samples are fed through a baffle into the irradiation chamber. 
A workbench located to the right of the irradiator is meant for the installation of 
temporary power supplies and measurement instrumentation for the in-situ characterization of the sample being irradiated. 
Irradiated modules are stored in a freezer to the other side of the irradiator. 
Radioactive sources are also present in the laboratory (Sr-90, Cd-109 et Am-241) 
and can be used to characterize the front-end systems before and after irradiation to a known source.
The radioactive sources are located beside the freezer in a dedicated box. 
</p>

<p class="SUBTITLE">Irradiation chamber</p>
Inside the x-ray machine is a horizontal mobile platform onto which samples can be placed. It can be moved in the vertical
direction through the use linear stage which is remotely control. This remote located on the exterior wall of the x-ray chamber
and can also be used to operate a pair of laser crosshairs indicating the centre of the x-ray tube.
</p>

<img class="SMALLIMAGE" src="images/Xray-Chamber-small.jpg" />
<img class="SMALLIMAGE" src="images/Xray-LaserPointers-small.jpg" /> <!-- style="transform:rotate(90deg) -->
<img class="SMALLIMAGE" src="images/Xray-TrayRemote.jpg" />
<img class="SMALLIMAGE" src="images/distance-measurement.jpg" />


<p class="SUBTITLE">Cooling</p>
The x-ray machine is equipped with a cold chuck plate that is cooled down by a 
<a href="../Labs/JULABO_FP50-HE.pdf">JULABO FP50-HE</a> chiller.
Flexible cooling lines are installed from the chiller to the cold plate.
The minimum operating temperature is -20 C. 
The cold chuck has a matric of M3 holes 1 cm appart. It also cools down the dry air supply.

<ul>
  <li><a href="e4.1289.07.11.1Blatt2offen.pdf">Cold chuck documentation (1)</a></li>
  <li><a href="e4.1289.07.11.2Blatt1zu.pdf">Cold chuck documentation (2)</a></li>
</ul>

<img class="SMALLIMAGE" src="images/cold-chuck.png"/>

<p class="SUBTITLE">Beam details</p>
<ul>
  <li>Beam energy: 15 - 160 keV </li>
  <li>Maximum absorbed dose: 3.75 Mrad/h </li>
  <li>Absorbed dose: <br><img height="30px" alt="frac{375}{x[cm]^2} Mrad/h" src="images/absorbed_dose.png"/> </li>
  <li>Minimum distance to source: 10 cm </li>
  <li>Beam divergence: 14 degrees</li>
</ul>

<p class="SUBTITLE">Beam monitoring</p>
An opto-coupler is connected to the shutter signal lamp to monitor the status of the x-ray irradiation 
by an external DAQ application.
The optocoupler is model ACLPL-2471, and it can be connected through an SMA cable. 
When x-rays are off, the shutter is closed and the lamp is off. The optocoupler is in high impedance, and the output signal is 1.
When x-rays are on, the shutter is open and the lamp is on. The optocoupler will pull the output to ground, and the output signal is 0.
</p>

<img class="MEDIUMIMAGE" src="images/optocoupler.png"/>

<p class="SUBTITLE">Humidity monitoring</p>
Several humidity and temperature probes are available inside the chamber, and connected to a data logger.
A web monitoring application is <a href="http://xraylogorh.cern.ch">available</a> within the CERN network.
System was installed by Andromachi Tsirou and Piero Giorgio Verdini.
</p>
<p><a href="http://xraylogorh.cern.ch">http://xraylogorh.cern.ch</a> (Only working inside CERN)</p>

<img class="MEDIUMIMAGE" src="images/logo-3.png"/>
<img class="MEDIUMIMAGE" src="images/logo-4.png"/>


<script>

  var maxDoseRadPerHour = 375000000;
	var halfDivergence = 14;
	
  //X-ray calibration by Abhi
  //Dose (MRad/h) = p[1] + 1/(p[0]+x^2) 
  // x : Distance in cm
   
  var abhiData={
    40: { 
      1: [0.125, 0.024],
      5: [0.024, 0.021],
     10: [0.012, 0.017],
     15: [0.008, 0.013],
     20: [0.006, 0.008],
     25: [0.005, 0.005],
     30: [0.004, 0.001],
    },
    160: { 
      1: [0.044, 0.028],
      5: [0.008, 0.020],
     10: [0.004, 0.012],
     15: [0.00265, 0.009],
     18: [0.0022, -0.001],
    }
  };
  
  
  var convert_Rad={'GRad': 0.000000001, 
                   'MRad': 0.000001,  
                   'kRad': 0.001, 
                   'Rad': 1,    
                   'cRad': 100,  
                   'mRad': 1000, 
			             'MGy': 0.00000001, 
                   'Gy': 0.01,     
                   'cGy':1,     
                   'mGy':10,
			             'Sv':0.01,
                   'cSv':1,
                   'mSv':10,
	};

	function units(){
		ss="";
		ss+="<option value='...'>...</option>";
		for(var k in convert_Rad){
			ss+="<option value='"+k+"'>"+k+"</option>";
		}
		return ss;
	}
  
	function energies(){
		ss="";
		ss+="<option value='...'>...</option>";
		for(var k in abhiData){
			ss+="<option value='"+k+"'>"+k+"</option>";
		}
		return ss;
	}

  function currents(dest, energy){
    console.log("Currents for energy:"+energy+" in id:"+dest);
		if(energy==".."){return;}
    ss="";
		ss+="<option value='...'>...</option>";
		for(var k in abhiData[energy]){
			ss+="<option value='"+k+"'>"+k+"</option>";
		}
		document.getElementById(dest).innerHTML = ss;
  }
   
	function calcSize(){
		console.log("Calc size");

		var dist = document.getElementById("g0_distance").value;
		console.log("Distance [cm]: "+ dist);

		var beamRadius = dist*Math.tan(halfDivergence * Math.PI/180);

		var diameter = 2*beamRadius;
		document.getElementById("g0_diameter").innerHTML=Math.round(diameter*1000)/1000;
		
		var area = Math.PI*beamRadius*beamRadius;
		document.getElementById("g0_area").innerHTML=Math.round(area*1000)/1000;
	}
	
	function calcTime(){
		console.log("Calc time");

		var dose = document.getElementById("g1_dose").value;
		console.log("Target dose: "+ dose);

		var ele = document.getElementById("g1_units");
		var sel_units = ele[ele.selectedIndex].value;
		console.log("Units : "+sel_units);

		if(sel_units=="...") return;
		
	  var dose_rad = dose / convert_Rad[sel_units];
	  console.log("Target dose in Rad: "+dose_rad);
	   	
	 	var hours = dose_rad / maxDoseRadPerHour;
	  console.log("Minimum irrad time [hours]: "+hours);

	  var dist = document.getElementById("g1_distance").value;
		console.log("Distance [cm]: "+ dist);

		if(dist<10 || dist>80){
		  document.getElementById("g1_ttt").innerHTML="<span style='color:red;'>Distance out of range: [10,80]</span>";
		 	return;
		}

		var rateAtDistance = dose_rad / ((dist)*(dist));
		console.log("Rate at "+dist+" cm [Dose/hour]: "+rateAtDistance);
		
		var hoursAtDistance = hours * ((dist)*(dist));
		console.log("Minimum irrad time at "+dist+" cm [hours]: "+hoursAtDistance);
	  document.getElementById("g1_ttt").innerHTML=Math.round(hoursAtDistance*1000)/1000;

		var daysAtDistance = hoursAtDistance/24.;
    console.log("Minimum irrad time at "+dist+" cm [days]: "+daysAtDistance);
	  document.getElementById("g1_ttd").innerHTML=Math.round(daysAtDistance*1000)/1000;
		
		var beamRadius = dist*Math.tan(halfDivergence * Math.PI/180);

		var diameter = 2*beamRadius;
		document.getElementById("g1_diameter").innerHTML=Math.round(diameter*1000)/1000;
		
		var area = Math.PI*beamRadius*beamRadius;
		document.getElementById("g1_area").innerHTML=Math.round(area*1000)/1000; 	
	}

	function calcTime2(){
		console.log("CalcTime2");

		var dose = document.getElementById("g6_dose").value;
		var units = document.getElementById("g6_units")[document.getElementById("g6_units").selectedIndex].value;
    var energy = document.getElementById("g6_energies")[document.getElementById("g6_energies").selectedIndex].value;
    if (document.getElementById("g6_currents").selectedIndex==-1){return;}
    var current = document.getElementById("g6_currents")[document.getElementById("g6_currents").selectedIndex].value;     
	  var dist = document.getElementById("g6_distance").value;
		
		console.log("CalcTime2: dose:"+dose+", units:" + units + ", energy:" + energy + ", current:"+ current + ", distance:" + dist);
		
    if(energy == "..." || current == "..." || units == "..."){
		  document.getElementById("g6_ttt").innerHTML="<span style='color:red;'>Missing parameters</span>";
		 	return;
		}

		if(dist<10 || dist>80){
		  document.getElementById("g6_ttt").innerHTML="<span style='color:red;'>Distance out of range: [10,80]</span>";
		 	return;
		}
    
	  var doseTarget = dose / (convert_Rad[units] * 1e6);
	  console.log("doseTarget [MRad]: "+doseTarget);
    
    var dosePerHour = 1. / (abhiData[energy][current][0] * (dist*dist)) + abhiData[energy][current][1];
    console.log("dosePerHour [MRad]:"+dosePerHour);
    
    //results
    
    var hours = doseTarget / dosePerHour;
	  console.log("Time to target [hours]: "+hours);
	  document.getElementById("g6_ttt").innerHTML=Math.round(hours*1000)/1000;

		var daysAtDistance = hours/24.;
    console.log("Time to target [days]: "+daysAtDistance);
	  document.getElementById("g6_ttd").innerHTML=Math.round(daysAtDistance*1000)/1000;
		
		var beamRadius = dist*Math.tan(halfDivergence * Math.PI/180);

		var diameter = 2*beamRadius;
		document.getElementById("g6_diameter").innerHTML=Math.round(diameter*1000)/1000;
		
		var area = Math.PI*beamRadius*beamRadius;
		document.getElementById("g6_area").innerHTML=Math.round(area*1000)/1000; 	
	}

	function calcDistance(){
		console.log("Calc distance");

		var rate = document.getElementById("g2_rate").value;
		console.log("Target rate: "+ rate);
		
		var ele = document.getElementById("g2_units");
		var sel_units = ele[ele.selectedIndex].value;
		console.log("Units : "+sel_units);

		if(sel_units=="...") return;
		
	  var rate_rad = rate / convert_Rad[sel_units];
	 	console.log("Target rate [Rad]: "+rate_rad);
	   	
  	var distance = Math.sqrt(maxDoseRadPerHour/rate_rad);
  	document.getElementById("g2_distance").innerHTML=Math.round(distance*1000)/1000;
	}

	function calcUnits(){
		console.log("Calc units");

		var src = document.getElementById("g3_mag_1").value;
		console.log("Source: " + src);
		
		var ele = document.getElementById("g3_unit_1");
		var src_units = ele[ele.selectedIndex].value;
		console.log("Units : " + src_units);

		var ele = document.getElementById("g3_unit_2");
		var dst_units = ele[ele.selectedIndex].value;
		console.log("Units2: " + dst_units);

		if(src_units=="...") return;
		if(dst_units=="...") return;
		
   	var dst = src * convert_Rad[dst_units] / convert_Rad[src_units];
   	console.log("Dest : " + dst);
	   	
   	document.getElementById("g3_mag_2").innerHTML=Math.round(dst*1000)/1000;
	}

	function calcDose(){
		console.log("Calc dose");

		var dist = document.getElementById("g4_distance").value;
		console.log("Distance: " + dist);
		
    if(dist<10 || dist>80){
		  document.getElementById("g4_dose").innerHTML="<span style='color:red;'>Distance out of range: [10,80]</span>";
		 	return;
		}
    
    var dosePerHour = maxDoseRadPerHour / (dist*dist);
		
		var ele = document.getElementById("g4_units");
		var units = ele[ele.selectedIndex].value;
		console.log("Units : " + units);
    
    var doseUnits = dosePerHour * convert_Rad[units];
    
	  document.getElementById("g4_dose").innerHTML=Math.round(doseUnits*100)/100;
  }

	function calcDose2(){
		console.log("Calc dose2");

		var dist = document.getElementById("g5_distance").value;
    var energy = document.getElementById("g5_energies")[document.getElementById("g5_energies").selectedIndex].value;
    if (document.getElementById("g5_currents").selectedIndex==-1){return;}
    var current = document.getElementById("g5_currents")[document.getElementById("g5_currents").selectedIndex].value;
    var units = document.getElementById("g5_units")[document.getElementById("g5_units").selectedIndex].value;

		console.log("distance:" + dist + ", energy:" + energy + ", current:"+ current + ", units:" + units);
		
    if(dist<10 || dist>80){
		  document.getElementById("g5_dose").innerHTML="<span style='color:red;'>Distance out of range: [10,80]</span>";
		 	return;
		}

    if(energy == "..." || current == "..." || units == "..."){
		  document.getElementById("g5_dose").innerHTML="<span style='color:red;'>Missing parameters</span>";
		 	return;
		}
    
    var dosePerHour = 1. / (abhiData[energy][current][0] * (dist*dist)) + abhiData[energy][current][1];
		 
    console.log("dosePerHour [MRad]:"+dosePerHour);
    
    var doseUnits = dosePerHour * 1e6 * convert_Rad[units];
    
	  document.getElementById("g5_dose").innerHTML=Math.round(doseUnits*100)/100;
  }

</script>

<p class="SUBTITLE">Calculate beam size</p>
<table>
  <tr><td>Distance (10-80) [cm]</td><td><input id="g0_distance" style="width:50px;"/></td></tr>
  <tr><td>Beam diameter [cm]</td><td><div id="g0_diameter"></div></td></tr>
  <tr><td>Beam area [cm2]</td><td><div id="g0_area"></div></td></tr>
  <tr><td><button onclick="calcSize();">Calc</button></td><td></td></tr>
</table>

<p class="SUBTITLE">Unit conversion</p>
<table>
  <tr><td>Source magnitude</td><td><input id="g3_mag_1" style="width:50px;"/></td></tr>
  <tr><td>Source unit</td><td><select id="g3_unit_1"></select></td></tr>
  <tr><td>Destination unit</td><td><select id="g3_unit_2"></select></td></tr>
  <tr><td>Destination magnitude</td><td><div id="g3_mag_2"></div></td></tr>
  <tr><td><button onclick="calcUnits();">Calc</button></td><td></td></tr>
</table>
<script>
document.getElementById("g3_unit_1").innerHTML = units();
document.getElementById("g3_unit_2").innerHTML = units();
</script>

<p class="SUBTITLE">Calculate irradiation time</p>
<table>
  <tr><td>Target dose</td><td><input id="g1_dose" style="width:50px;"/></td></tr>
  <tr><td>Unit</td><td><select id="g1_units"></select></td></tr>
  <tr><td>Distance (10-80) [cm]</td><td><input id="g1_distance" style="width:50px;"/></td></tr>
  <tr><td>Beam diameter [cm]</td><td><div id="g1_diameter"></div></td></tr>
  <tr><td>Beam area [cm2]</td><td><div id="g1_area"></div></td></tr>
  <tr><td>Time to target [h]</td><td><div id="g1_ttt"></div></td></tr>
  <tr><td>Time to target [d]</td><td><div id="g1_ttd"></div></td></tr>
  <tr><td><button onclick="calcTime();">Calc</button></td><td></td></tr>
<script>
document.getElementById("g1_units").innerHTML = units();
</script>
</table>

<p class="SUBTITLE">Calculate irradiation time 2</p>
<table>
  <tr><td>Target dose</td><td><input id="g6_dose" style="width:50px;"/></td></tr>
  <tr><td>Unit</td><td><select id="g6_units"></select></td></tr>
  <tr><td>Energy [kV]</td><td><select id="g6_energies" onchange="currents('g6_currents',this.value)"></select></td></tr>
  <tr><td>Current [mA]</td><td><select id="g6_currents"></select></td></tr>
  <tr><td>Distance (10-80) [cm]</td><td><input id="g6_distance" style="width:50px;"/></td></tr>
  <tr><td>Beam diameter [cm]</td><td><div id="g6_diameter"></div></td></tr>
  <tr><td>Beam area [cm2]</td><td><div id="g6_area"></div></td></tr>
  <tr><td>Time to target [h]</td><td><div id="g6_ttt"></div></td></tr>
  <tr><td>Time to target [d]</td><td><div id="g6_ttd"></div></td></tr>
  <tr><td><button onclick="calcTime2();">Calc</button></td><td></td></tr>
<script>
document.getElementById("g6_units").innerHTML = units();
document.getElementById("g6_energies").innerHTML = energies();
</script>
</table>

<p class="SUBTITLE">Calculate irradiation distance</p>
<table>
  <tr><td>Target rate</td><td><input id="g2_rate" style="width:50px;"/></td></tr>
  <tr><td>Unit / hour</td><td><select id="g2_units"></select></td></tr>
  <tr><td>Distance (10-80) [cm]</td><td><div id="g2_distance" style="width:50px;"></div></td></tr>
  <tr><td><button onclick="calcDistance();">Calc</button></td><td></td></tr>
<script>
document.getElementById("g2_units").innerHTML = units();
</script>
</table>

<p class="SUBTITLE">Calculate dose at distance</p>
<table>
  <tr><td>Distance (10-80) [cm]</td><td><input id="g4_distance" style="width:50px;"/></td></tr>
  <tr><td>Energy [160 keV]</td><td></td></tr>
  <tr><td>Unit / hour</td><td><select id="g4_units"></select></td></tr>
  <tr><td>Dose [Unit/h]</td><td><div id="g4_dose" style="width:50px;"></div></td></tr>
  <tr><td><button onclick="calcDose();">Calc</button></td><td></td></tr>
<script>
document.getElementById("g4_units").innerHTML = units();
</script>
</table>

<p class="SUBTITLE">Calculate dose at distance 2</p>
<table>
  <tr><td>Distance (10-80) [cm]</td><td><input id="g5_distance" style="width:50px;"/></td></tr>
  <tr><td>Energy [kV]</td><td><select id="g5_energies" onchange="currents('g5_currents',this.value)"></select></td></tr>
  <tr><td>Current [mA]</td><td><select id="g5_currents"></select></td></tr>
  <tr><td>Unit / hour</td><td><select id="g5_units"></select></td></tr>
  <tr><td>Dose [Unit/h]</td><td><div id="g5_dose" style="width:50px;"></div></td></tr>
  <tr><td><button onclick="calcDose2();">Calc</button></td><td></td></tr>
<script>
  document.getElementById("g5_units").innerHTML = units();
  document.getElementById("g5_energies").innerHTML = energies();
</script>
</table>

<p class="SUBTITLE">Documents and links</p>
<ul>
  <li><a href="../xrayschedule/schedule.php">Usage request and schedule (restricted)</a></li>
  <li><a href="xrayhelp.php">X-ray help page</a></li>
  <li><a href="SF_form.pdf">Safety form SF-SO-12-0-1</a></li>
  <li><a href="X-Rad_User_Manual.pdf">User manual</a></li>
  <li><a href="https://edms.cern.ch/document/2768547">CERN Safety file</a></li>
  <li><a href="xraycalibration.pdf">X-ray calibration</a></li>
  <li><a href="XrayMonitoring.pdf">Monitoring signal for the x-ray machine</a></li>
  <li><a href="X-RAD_iR160-225_C1R1_Facility_Requirements.pdf">Facility requirements</a></li>
  <li><a href="declaration_of_conformity.pdf">EC declaration of conformity</a></li>
  <li><a href="https://edms.cern.ch/document/1692434/1">EDMS Safety inspection report</a></li>
  <li><a href="Calibration_Data_Profile_XRAD_iR_160_Appendix.pdf">Calibration data profile</a></li>
  <li><a href="XReport.pdf">Xray chamber beamspot measurement</a></li>
  <li><a href="https://nucleonica.com">Shielding calculator (Nucleonica)</a></li>
  <li><a href="http://pdg.lbl.gov/2018/AtomicNuclearProperties/index.html">Atomic and nuclear properties (PDG)</a></li>
</ul>

</div>
<?php
  show_footer();
?>
</div>

</body>
</html>

