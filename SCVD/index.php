<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/img/ATLAS-icon.ico">
<title>SCVD</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>

<div>
  <img class="IMAGE" src="images/SCVD.png">
</div>
  
<p class="TITLE">Diamond detector
	<ul>
		<li>External bias field across detector - connect on to amplifier</li>
		<li>Charge trapping limits signal in pCVD -> aim for high fields</li>
    <li>Charge collec\on distance (ccd) used to characterize diamonds</li>
	</ul>
</p>


<p class="SUBTITLE">Tests performed
  <ul>
    <li>Operational experience in ATLAS DBM detector</li>
    <li>TCT measurements with alpha source <sup>241</sup>Am</li>
    <li>Charge collection measurements with <sup>90</sup>Sr source</li>
    <li>Characterization of signal prodiles for neutron and gamma deposits</li>
  </ul>
  </p>

<p class="SUBTITLE">Documentation
  <ul>
    <li>
      <a href="https://indico.cern.ch/event/276370/session/0/contribution/6/attachments/501708/692856/TALENT_annual_2014_MC.pdf">
        M.Cerv Talent Midterm Review 19/11/2013</a>
    </li>
    <li><a href="http://indico.cern.ch/event/226971/contribution/23/2/attachments/373832/520048/CERV-ATLAB_20130218_small.pdf">
      M.Cerv Talent status update 18/02/2013
    </a></li>
    <li><a href="https://cds.cern.ch/record/1630832/files/ATL-INDET-PROC-2013-021.pdf">M.Cerv Proceedings 2013</a></li>
  </ul>
</p>
</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
