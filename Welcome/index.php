<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/img/ATLAS-icon.ico">
<title>CERN ATLAS Pixel group - Welcome page</title>
<meta name="description" content="CERN ATLAS Team Pixel group, EP-ADE-TK" />
<meta name="keywords" content="CERN, ATLAS, Pixel, EP-ADE, EP-ADE-TK" />
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>
<p class="TITLE">Welcome to the CERN ATLAS Pixel group page</p>

<p class="SUBTITLE"><a href="../Publications">Publications</a></p>
<p class="SUBTITLE"><a href="../PublicPlots">Public plots</a></p>


<img class="IMAGE" src="images/ITK-Thermal.png">

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
