<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_GET;

foreach($d as $k=>$v){
  $d[$k]=strip_tags($v);
}

if($d["cmd"]=="add_supply"){
  $sql ="INSERT INTO supplies ";
  $sql.=" (`reference`, `store`, `manufacturer`, `partnumber`, `description`, `url`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["reference"]."',"; 
  $sql.="'".$d["store"]."', ";
  $sql.="'".$d["manufacturer"]."', ";  
  $sql.="'".$d["partnumber"]."', ";  
  $sql.="'".$d["description"]."', ";  
  $sql.="'".$d["url"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_supply"){
  $sql ="UPDATE supplies SET ";
  $sql.="`reference`='".$d["reference"]."', "; 
  $sql.="`store`='".$d["store"]."', ";
  $sql.="`manufacturer`='".$d["manufacturer"]."', ";  
  $sql.="`partnumber`='".$d["partnumber"]."', ";  
  $sql.="`description`='".$d["description"]."', ";  
  $sql.="`url`='".$d["url"]."' ";
  $sql.=" WHERE ";
  $sql.=" id='".$d["id"]."';";
}
else if($d["cmd"]=="delete_supply"){
  $sql ="DELETE FROM supplies ";
  $sql.=" WHERE ";
  $sql.=" id='".$d["id"]."';";
}

echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
}else{
  $ret["error"]=$conn->error;  
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>