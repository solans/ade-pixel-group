<h2>Supplies</h2>
<button id="supplies_reset">Reset filters</button>
<button id="supplies_export">Export</button>
<div id="supplies_found" style="display:inline-block"></div>
<table id="supplies" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">Reference</th>
		<th class="first-name filter-select">Store</th>
		<th class="first-name filter-select">Manufacturer</th>
		<th data-placeholder="Search...">PN</th>
		<th data-placeholder="Search...">Description</th>
		<th>Actions</th>
	</thead>
	<tbody id="supplies_body">
	</tbody>
</table>

<div id="supplies_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#supplies").trigger("update").trigger("appendCache").trigger("applyWidgets");
  supplies_load();
});

$("#supplies").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
});

$("#supplies").on("filterEnd",function(){
  $("#supplies_found").html("Found: "+($("#supplies tr:visible").length-2))
});

$("#supplies_export").click(function() {
  $("#supplies").trigger("outputTable");
});

$("#supplies_reset").click(function() {
  $("#supplies").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

/** Then load members **/
function supplies_load(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_supplies"},
    success: function(data) {
      supplies=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#supplies_body").empty();
      for (supply of supplies){
        tt="<tr>\n";
        tt+="<td>"+supply["reference"]+"</td>";
        tt+="<td>"+supply["store"]+"</td>";
        tt+="<td>"+supply["manufacturer"]+"</td>";
        tt+="<td>"+supply["partnumber"]+"</td>";
        tt+="<td>"+supply["description"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=supply&id="+supply["id"]+"\" title=\"See supply\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"supplies_delete('"+supply["id"]+"')\">delete</a>&nbsp;";
        tt+="<a href=\""+supply["url"]+"\" title=\"See supply\">link</a>";
        tt+="</td>";
        tt+="</tr>\n"; 
        $("#supplies_body").append(tt);
      }
      $('#supplies').trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#supplies_found").html("Found: "+($("#supplies tr:visible").length-2));
    }
  });
}

/* delete */
function supplies_delete(id){
  if(!window.confirm("Are you sure to delete supply?")) return false;
  $("#supplies_reply").text("");
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"delete_supply",id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; supplies_load();}
      $("#supplies_reply").text(sreply);
    }
  });
  return false;
}


</script>
