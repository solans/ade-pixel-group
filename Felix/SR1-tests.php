<?php
include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../ATLAS-icon.ico">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/JS/toc.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>FELIX tests with RD53A</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<h1 class="TITLE">FELIX tests in SR1</h1>

<script>
  $(document).ready(loadToc);
</script>

<div class="CONTENT">

<h2 id="Introduction" class="SUBTITLE">Introduction</h2>


<h2 id="DP_to_SMA" class="SUBTITLE">DP to SMA adapter board</h2>

	<img class="SMALLIMAGE ZOOM" src="images/DP_to_SMA_Bonn_front.jpg"/>
	<img class="SMALLIMAGE ZOOM" src="images/DP_to_SMA_Bonn_back.jpg"/>

  Mapping of the DP to front-end
  
  <table>
    <tr><td>DP adapter [L0-L3]</td><td>Quad [1-4]</td></tr>
    <tr><td>L0</td><td>4</td></tr>
    <tr><td>L1</td><td>3</td></tr>
    <tr><td>L2</td><td>2</td></tr>
    <tr><td>L3</td><td>1</td></tr>
  </table>
 

<h2 id="DP_to_pigtail_data">DP to pigtail data</h2>

 

<h2 id="config_opto" class="SUBTITLE">Config Optoboard</h2>

Checkout slim_config from optoboard_felix package 
<ol>
  <li>git checkout slim_config</li> 
  <li>git clone ssh://git@gitlab.cern.ch:7999/bat/optoboard_felix.git</li>
</ol>

Configure the opto_board: Remember to change N to the physical felix fiber link
<ol>
  <li>cd optoboard_felix</li>
  <li>python quick_start.py -v 1.2 -s 524d0002 -G N -i 0 -d 0 </li>
</ol>

<h2 id="connectivity" class="SUBTITLE">Connectivity</h2>

<pre>
{
  "connectivity" : [
    {"name": "QUAD11", "config" : "quad_1.json", "rx" :  0, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD12", "config" : "quad_2.json", "rx" :  4, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD13", "config" : "quad_3.json", "rx" : 12, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD14", "config" : "quad_4.json", "rx" : 16, "tx" :  0, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD21", "config" : "quad_1.json", "rx" : 64, "tx" : 64, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD22", "config" : "quad_2.json", "rx" : 68, "tx" : 64, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD23", "config" : "quad_3.json", "rx" : 76, "tx" : 64, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD24", "config" : "quad_4.json", "rx" : 80, "tx" : 64, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD31", "config" : "quad_1.json", "rx" :128, "tx" :128, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD32", "config" : "quad_2.json", "rx" :132, "tx" :128, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD33", "config" : "quad_3.json", "rx" :140, "tx" :128, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD34", "config" : "quad_4.json", "rx" :144, "tx" :128, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD41", "config" : "quad_1.json", "rx" :192, "tx" :192, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD42", "config" : "quad_2.json", "rx" :196, "tx" :192, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD43", "config" : "quad_3.json", "rx" :204, "tx" :192, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD44", "config" : "quad_4.json", "rx" :208, "tx" :192, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD51", "config" : "quad_1.json", "rx" :256, "tx" :256, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD52", "config" : "quad_2.json", "rx" :260, "tx" :256, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD53", "config" : "quad_3.json", "rx" :268, "tx" :256, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD54", "config" : "quad_4.json", "rx" :272, "tx" :256, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD61", "config" : "quad_1.json", "rx" :320, "tx" :320, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD62", "config" : "quad_2.json", "rx" :324, "tx" :320, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD63", "config" : "quad_3.json", "rx" :332, "tx" :320, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD64", "config" : "quad_4.json", "rx" :336, "tx" :320, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD71", "config" : "quad_1.json", "rx" :384, "tx" :384, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD72", "config" : "quad_2.json", "rx" :388, "tx" :384, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD73", "config" : "quad_3.json", "rx" :396, "tx" :384, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD74", "config" : "quad_4.json", "rx" :400, "tx" :384, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD81", "config" : "quad_1.json", "rx" :448, "tx" :448, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD82", "config" : "quad_2.json", "rx" :452, "tx" :448, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD83", "config" : "quad_3.json", "rx" :460, "tx" :448, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0},
    {"name": "QUAD84", "config" : "quad_4.json", "rx" :464, "tx" :448, "host": "127.0.0.1", "cmd_port": 12340, "data_port": 12350, "enable" : 1, "locked" : 0}
    ]
}
</pre>

<h2 id="disclaimer" class="SUBTITLE"></h2>
This page describes the tests done at CERN with FELIX and the RD53A.
This page aims to be the documentation of such tests.

<p>Please refer to the FELIX manual in the following <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/">link</a></p>

<h2 id="requirements" class="SUBTITLE">Requirements</h2>
<ol>
  <li>Xilinx Vivado (to program through JTAG)</li>
  <li>FELIX Phase-I server (Supermicro X10SRW-F)</li>
  <li>FELIX Phase-I card (FLX-712)</li>
  <li>FELIX drivers (<a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/">link</a>)</li>
  <li>FELIX software (<a href="https://gitlab.cern.ch/atlas-tdaq-felix/software/-/blob/master/README.md">link</a>)</li>
  <li>FELIX firmware (<a href="rd53a-firmware.php">link</a>)</li>
  <li>LPGBT card (VLDB+ or Optoboard)</li>
  <li>LPGBT programmer (PiGBT or else)</li>
</ol>

<h2 id="setup_at_cern" class="SUBTITLE">Setup at CERN</h2>
<p>
Currently the FLX-712 is set-up at pcatlidros02 with a remote DCS setup.
It's composed of:
<ul>
<li> FELIX Phase-I server: pcatlidros02</li>
<li> PiGBT to configure LpGBT: <a href="ep-ade-pigbt-01:8080">ep-ade-pigbt-01:8080</a></li>
<li> LpGBT Board (Powered by DCS)</li>
<li> RD53A (Powered by DCS)</li>
<li> Software path is: /afs/cern.ch/work/a/adecmos/public/ItkFelix </li>
<li> Recommended software path is: /home/itkpix </li>
</ul>

<p>See more details of the setup at CERN in the following <a href="rd53a-setup.php">link</a></p>

<h2 id="Power_control" class="SUBTITLE">Powering up the setup</h2>
To power LpGBT and RD53A you can use the DCS functionality in pcatlidros02. To use it:
</p>
<pre>
cd /afs/cern.ch/work/a/adecmos/public/ItkFelix/itk-felix-sw
source setup.sh
PSUControlGUI.py -f PySerialComm/CRCard/FLX712.card
</pre>

<img class="MEDIUMIMAGE" src="images/psu-control-gui.png"/>

<h2 id="firmware_versions" class="SUBTITLE">Firmware versions</h2>
<p>
List of fimrware versions can be found in <a href="rd53a-firmware.php"> 
  https://ade-pixel-group.web.cern.ch/Felix/rd53a-firmware.php
</a>
<p>
Else you can check the CERNbox folder from Marco Trovato in <a href="https://cernbox.cern.ch/index.php/s/jUKu6ayuPVqD5qB">
  https://cernbox.cern.ch/index.php/s/jUKu6ayuPVqD5qB
</a>

<h2 id="program_felix" class="SUBTITLE">How to program FELIX through PCI</h2>

<pre>
source /afs/cern.ch/work/a/adecmos/public/ItkFelix/felix/felix-5.0-latest/setup.sh 
fflashprog -f3 [path_to_mcs_file] prog
</pre>
  
<h2 id="program_felix712" class="SUBTITLE">How to program FELIX through JTAG</h2>

<ol>
  <li>Open Vivado 2020
    <pre>
export XILINXD_LICENSE_FILE="2112@lxlicen01.cern.ch,2112@lxlicen02.cern.ch,2112@lxlicen03.cern.ch"
source /afs/cern.ch/work/f/fschreud/public/Xilinx/Vivado/2020.1/settings64.sh
vivado
    </pre>
  </li>
  <li>Open the hardware manager</li>
  <li>Connect to the existing device. If no device exists, try installing cable drivers, and flx-712 drivers</li>
  <li>Program device with <b>.bit</b> file and <b>.ltx</b> debug file provided in the link above.</li>
  <li>Restart the PC.</li>
</ol>


<h2 id="check_felix" class="SUBTITLE">Load and check FELIX drivers</h2>

<ol>
	<li>Download as superuser the lastest Felix driver from the following <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/">link</a>.</li>
	<li>Check the installed felix driver
		<pre>rpm -qa | grep tdaq</pre>
	</li>
	<li>Uninstall the previous driver
		<pre>rpm -e [filename given by previous command]</pre>
	</li>
	<li>Install a new driver: 
		<pre>yum install [new_downloaded_rpm_file]</pre>
	</li>
	<li>Start the newly installed drivers:
		<pre>./etc/init.d/drivers_flx start</pre>
	</li>
</ol>

<h2 id="compile-felix-sw" class="SUBTITLE">Compile the FELIX SW</h2>

<ol>
<li>Checkout the atlas-tdaq-felix software package of atlas-tdaq-felix
<pre>git clone https://:@gitlab.cern.ch:8443/atlas-tdaq-felix/software.git felix-5.0-latest</pre>
</li>

<li>Change into the software directory
<pre>cd felix-5.0-latest</pre>
</li>

<li>Clone all the packages
<pre>./clone_all.sh ssh</pre>
</li>

<li>Setup the environment
<pre>
export REGMAP_VERSION=0x0500
source cmake_tdaq/bin/setup.sh x86_64-centos7-gcc8-opt
</pre>
</li>

<li>Prepare the release for building
<pre>cmake_config x86_64-centos7-gcc8-opt</pre>
</li>

<li>Change to the build directory
<pre>cd x86_64-centos7-gcc8-opt</pre>
</li>

<li>Compile the software
<pre>make</pre>
</li>

<li>(Optional) Build the distribution release
<pre>
cd ../distribution
./build.sh installed x86_64-centos7-gcc8-opt 98python3 98python3 . 
</pre>
</li>
</ol>


<h2 id="init-felix" class="SUBTITLE">Initialize the FELIX card</h2>
<p>
To configure Felix you need a felix software with Register Map (RM) 5.0. <br/>
The installed one can be found in:  /afs/cern.ch/work/a/adecmos/public/ItkFelix/felix/felix-5.0-latest.<br/>
To setup the elinks source the envoriment in the folder above using <b>setup.sh</b> command.
</p>

<pre>
cd /afs/cern.ch/work/a/adecmos/public/ItkFelix/felix/felix-rm5.0-latest
source setup.sh
flx-init -c X
</pre>

<h2 id="config-elink" class="SUBTITLE">Configure the FELIX elinks</h2>
Each optical (LPGBT) link can be divided into many logical elinks. These have to be configured in order to be recognized by the felix-sw. The e-link configuration is done with <b>elinkconfig</b> that is a tool provided by the felix-sw.

<ol>
	<li>Run <b>elinkconfig</b>
	<pre>
source /afs/cern.ch/work/a/adecmos/public/ItkFelix/felix/felix-5.0-latest/setup.sh 
elinkconfig
	</pre>	
	</li>
	<li>Click on <b>Open...</b><br/>
	</li>
	<li>Select the <b>.yelc</b> file<br/>
	</li>
	<li>Click on <b>Generate/Upload</b> <br/>
	</li>
	<li>Click on <b>Upload</b> <br/>
	</li>
</ol>
	
	<img class="SMALLIMAGE" src="images/rd53a-elinkconfig-1.png"/>
	<img class="SMALLIMAGE" src="images/rd53a-elinkconfig-2.png"/>
	<img class="SMALLIMAGE" src="images/rd53a-elinkconfig-3.png"/>
	<img class="SMALLIMAGE" src="images/rd53a-elinkconfig-4.png"/>
	<img class="SMALLIMAGE" src="images/rd53a-elinkconfig-5.png"/>
	<img class="SMALLIMAGE" src="images/rd53a-elinkconfig-6.png"/>
		
<p>
In addition you need to disable the emulators.
</p>
<p>
Then run rest of the commands. 
</p>
<pre>
flx-config -d Y set DECODING_LINK00_EGROUP0_CTRL_PATH_ENCODING=0x3
flx-config -d Y set DECODING_LINK00_EGROUP0_CTRL_EPATH_ENA=0x1
flx-config -d Y set DECODING_LINK00_EGROUP0_CTRL_EPATH_WIDTH=0x4
flx-config -d Y set DECODING_REVERSE_10B=0x1
</pre>

<h2 id="config-lpgbt-pigbt" class="SUBTITLE">Configure LpGBT with PiGBT</h2>
<p>
After powering the LpGBT (Check DCS section for more details), star a browser connected to CERN network and go to the following url: <a href="ep-ade-pigbt-01:8080">ep-ade-pigbt-01:8080</a> 

Then select Real LpGBT and connect to the hardware.
The following setting are used: 
<ul>
<li> TRX
<li> TXdata rate = 10 Gbps
<li> TX encoding = FEC5
<li> Uplink EPRX (Click gear symbol) 
<ul>
<li> EPRX0 
<li> Data Rate = 1280
<li> Track Mode = Continues
<li> Control TERM
<li> Equalization OFF

<li> EPRX1<li>1-6
<li> Data Rate off
</ul>		
<li> Downlink EPTX
<ul>
<li> Data Rate 160 Mpbs
<li> Drive strength 4.0 mA
</ul>		 		  
<li> click on the menu -> high speed
<ul>
<li> Invert high speed data output enabled (Invert polarity uplinks)
</ul>
</ul>
</p>

<img class="SMALLIMAGE" src="images/rd53a-pigbt-1.png"/>
<img class="SMALLIMAGE" src="images/rd53a-pigbt-2.png"/>
<img class="SMALLIMAGE" src="images/rd53a-pigbt-3.png"/>
<img class="SMALLIMAGE" src="images/rd53a-pigbt-4.png"/>
<img class="SMALLIMAGE" src="images/rd53a-pigbt-5.png"/>
<img class="SMALLIMAGE" src="images/rd53a-pigbt-6.png"/>
<img class="SMALLIMAGE" src="images/rd53a-pigbt-7.png"/>
<img class="SMALLIMAGE" src="images/rd53a-pigbt-8.png"/>

<h2 id="config-lpgbt-fice" class="SUBTITLE">Configure LpGBT with FICE</h2>

Alternatively, you can configure the LPGBT through the optical link.
This requires that the LPGBT has been initialized to communicate through the IC bus.
This will be the default configuration mode during production in SR1, and operation in the detector.

To configure a single register with fice you can use the following command:
<pre>
fice -G 0 -I 70 -a 0x1c5
</pre>
If you are dealing with opto-board V2 there needs to be 2 modifications.
<ul>
<li>Change the channel to 70 -> 74
<li>The lane pollarity needs to be changed: fgpolarity -r set
<li>The FEC5 sohuld be changed to FEC12:  flx-config set GBT_DATA_RXFORMAT2=0x1 
</ul>


Important not here is that, to acceses the different channels in the fice configuration you need to do the following configuration:
<ul>
<li> Channel 0: fice -G 0 -i 0 -d 0 # This is identical to fice -G 0
<li> Channel 1: fice -G 1 -i 1 -d 0 # 
<li> Channel 2: fice -G 0 -i 0 -d 1 # 
<li> Channel 3: fice -G 1 -i 1 -d 1 # 
</ul>


<H2 id="config-lpgbt-optotool" class="SUBTITLE">Configure LpGBT with Optoboard GUI</h2>

This tool is developed by Roman Muller. It allows to configure the LpGBT on the Optoboards through the IC link.
<a href="https://gitlab.cern.ch/bat/optoboard_felix/">link</a>

<img class="SMALLIMAGE" src="images/rd53a-optotool-GBCR1.png"/>
<img class="SMALLIMAGE" src="images/rd53a-optotool-GUI_lpGBT1_master.png"/>
<img class="SMALLIMAGE" src="images/rd53a-optotool-GUI_lpGBT4_slave.png"/>


<h2 id="voltage_tuning" class="SUBTITLE">Votlage triming</h2>
<a href="https://travelling-module.readthedocs.io/en/latest/procedure/#trim">
  https://travelling-module.readthedocs.io/en/latest/procedure/#trim
</a>


<h2 id="felixcore" class="SUBTITLE">Running scans: felixcore and scan_manager</h2>

Felixcore is an application provided by the felix-sw. It has to be running during the scan.
It receives commands from the scan code through the ethernet using the NETIO protocol and forwards them to the front-end through the optical links, and it receives the data from the front-end through the optical links and forwards it to the scan through NETIO.

In one terminal run:
<pre>
  source /afs/cern.ch/work/a/adecmos/public/ItkFelix/felix/felix-5.0-latest/setup.sh 
  x86_64-centos7-gcc8-opt/felixcore/felixcore -d 0 --data-interface lo --elinks 0,4,8,12    
</pre>
  
In another terminal run:
<pre>
  source /afs/cern.ch/work/a/adecmos/public/ItkFelix/itk-felix-sw/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -c [front-end-name] -s [scan-name] 
</pre>


</div>
</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
