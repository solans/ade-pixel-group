<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/JS/toc.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<title>FELIX setup for FEI4</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<h1 class="SUBTITLE">FELIX setup for FEI4</h1>

<script>
$(document).ready(loadToc);
</script>

<div class="CONTENT">

	<img src="images/itk-felix-readout-chain.png" class="IMAGE"/>

    <h2 id="Computer" class="SUBSUBTITLE">Computer</h2>
    	SuperMicro X10SRA-F mainboard
    <ul>
         <li>Intel Xeon E5-2630 v4 2.2 GHz</li>
         <li>4 x 8GB DDR4 2400mhz ecc reg memory</li>
         <li>1 x Samsung 256GB 850PRo + bracket</li>
         <li>743TQ-865B-SQ 865w Tower case</li>
    </ul>
    or<br>
    <br> 
    SuperMicro X10DRG-Q 
    <ul>
        <li>2x Haswell CPU, up to 10 cores</li> 
        <li>6x PCIe Gen-3 slots </li>
        <li>64 GB DDR4 Memory</li>
    </ul>
    
    
    <h2 id="MiniFELIX" class="SUBSUBTITLE">Mini-FELIX</h2>
    	DK-V7-VC709-G
    <ul>
        <li>VC709 evaluation board featuring the Virtex-7 XC7VX690T-2FFG1761C FPGA</li>
        <li>Northwest Logic PCI Express DMA Back-End IP</li>
        <li>Four 10Gb Ethernet transceiver modules and two fiber optic patch cables</li>
    </ul>
    
    
    <h2 id="TTCfx" class="SUBSUBTITLE">TTCfx</h2>
    <ul>
    	<li>Weismann Institute for Physics Lorne Levinson</li>
    </ul>
    	
    <img src="images/TTC.jpeg" class="IMAGE"></img> 
    
    <h2 id="VLDB" class="SUBSUBTITLE">VLDB</h2>
    <ul>
    	<li>Adquired at CERN EP-ESE-BE</li>
    </ul>
    <img src="images/VLDB.jpeg" class="MEDIUMIMAGE"></img>
    
    
    <h2 id="VHDCI-GBTx-SFP" class="SUBSUBTITLE">VHDCI-GBTx-SFP</h2>
    <ul>
    	<li>By at EP-ADE-ID</li>
    	<li><a href="https://edms.cern.ch/item/EDA-03936-V1-0/0">EDMS link</a></li>
    </ul>
    <img src="images/VHDCI-GBTX-SFP.jpg" class="MEDIUMIMAGE"></img>



</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
