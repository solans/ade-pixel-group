<?php
include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../ATLAS-icon.ico">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/JS/toc.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>RD53A FELIX read-out documentation</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<h1 class="TITLE">FELIX tests with ITKPix: UNDER CONSTRUCTION</h1>

<script>
  $(document).ready(loadToc);
</script>

<div class="CONTENT">

<h2 id="introduction" class="SUBTITLE">Introduction</h2>
This page describes the tools and steps requried to read-out RD53A with FELIX Phase-I hardware.
Distribution of FELIX Phase-I servers (Supermicro X10SRW-F) and cards (FLX-712) is responsibility of the <a href="mailto:atlas-itk-common-electronics-coordinators@NOSPAM">ATLAS ITK Common Electronics coordinators</a>.
Background information about FELIX can be found in the FELIX <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/felix-user-manual/">manual</a>.

<h2 id="requirements" class="SUBTITLE">Requirements</h2>
<ol>
  <li>Xilinx Vivado (to program through JTAG)</li>
  <li>FELIX Phase-I server (Supermicro X10SRW-F)</li>
  <li>FELIX Phase-I card (FLX-712)</li>
  <li>FELIX drivers (<a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/">link</a>)</li>
  <li>FELIX software (<a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/apps/4.x/">link</a> <a href="https://gitlab.cern.ch/atlas-tdaq-felix/software/-/blob/master/README.md">readme</a>)</li>
  <li>FELIX firmware (<a href="rd53a-firmware.php">link</a>)</li>
  <li>LPGBT card (VLDB+ or Optoboard)</li>
  <li>LPGBT programmer (PiGBT for VLDB+ or Optoboard GUI for Optoboard)</li>
  <li>Fibers and adapter boards (<a href="https://ade-pixel-group.web.cern.ch/Felix/rd53a-setup.php">link</a>)</li>
  <li>ITK FELIX SW (<a href="https://gitlab.cern.ch/itk-felix-sw">link</a>)</li>
</ol>


<h2 id="setup_at_cern" class="SUBTITLE">Setup at CERN</h2>
<?
$HOME="/home/itkpix";
$FELIXSW=$HOME."/felix-sw/";
$FELIXDR=$HOME."/felix-ke/";
$FELIXFW=$HOME."/felix-fw/";
$ITKSW=$HOME."/itk-felix-sw";
$TDAQ_VERSION="11.2.1";
$FLX_FW_VERSION="FLX712_PIXEL_24CH_CLKSELECT_GIT_phase2-release-5.0_rm-5.0_3465_231014_0109";
$FLX_SW_VERSION="4.3.0 RM5";
$FLX_DR_VERSION="4.15";
$FLX_DR_FILE="tdaq_sw_for_Flx-4.15.0-2dkms.noarch.rpm";
$FLX_SW_FILE="felix-05-00-03-rm5-stand-alone.tar.gz";
$FLX_FW_FILE="FLX712_PIXEL_24CH_CLKSELECT_GIT_phase2-release-5.0_rm-5.0_3465_231014_0109.mcs";
$FLX_SW_PATH="felix-05-00-03-rm5-stand-alone";
$BINARY_TAG="x86_64-el9-gcc13-opt";
$ITK_FLX_SW_VERSION="master";
$SCRIPTS_REPO="ssh://git@gitlab.cern.ch:7999/itk-felix-sw/itk-pix-felix-scripts.git";
$SCRIPTS_DIR="itk-pix-felix-scripts";
?>

<p>
Currently the FLX-712 is set-up at pcatlidros02 with a remote DCS setup.
It's composed of:
<ul>
<li> FELIX Phase-I server: pcatlidros02</li>
<li> PiGBT to configure LpGBT: <a href="ep-ade-pigbt-01:8080">ep-ade-pigbt-01:8080</a></li>
<li> LpGBT Board (Powered by DCS)</li>
	 <li> RD53A (Powered by DCS)</li>
<li> Felix software: <?=$FELIXSW;?> </li>
<li> Felix driver:  <?=$FELIXDR;?> </li>
<li> Felix firmware:  <?=$FELIXFW;?> </li>
<li> Custom ITK sotware:  <?=$ITKSW;?> </li>
</ul>

<p>See more details of the setup at CERN in the following <a href="rd53a-setup.php">link</a></p>



<h2 id="program_felix" class="SUBTITLE">How to program FELIX through PCI</h2>

<ol>
  <li>Download the desired FELIX firmware</li>
  <li>Setup the FELIX-SW
    <pre>
      source <?=$FELIXSW;?>/setup.sh 
    </pre>
  </li>
  <li>Program the card through PCI
    <pre>
      fflashprog -f3 [path_to_mcs_file] prog
    </pre>
  </li>
</ol>

<h2 id="program_felix712" class="SUBTITLE">How to program FELIX through JTAG</h2>

<ol>
  <li>Open Vivado 2020
    <pre>
export XILINXD_LICENSE_FILE="2112@lxlicen01.cern.ch,2112@lxlicen02.cern.ch,2112@lxlicen03.cern.ch"
source /afs/cern.ch/work/f/fschreud/public/Xilinx/Vivado/2020.1/settings64.sh
vivado
    </pre>
  </li>
  <li>Open the hardware manager</li>
  <li>Connect to the existing device. If no device exists, try installing cable drivers, and flx-712 drivers</li>
  <li>Program device with <b>.bit</b> file and <b>.ltx</b> debug file provided in the link above.</li>
  <li>Restart the PC.</li>
</ol>


<h2 id="check_felix" class="SUBTITLE">Load and check FELIX drivers</h2>

<ol>
	<li>Login as root (superuser) to the FELIX server</li>
  <li>Download the recomended Felix driver (<?=$FLX_DR_FILE;?>) from the following <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/">link</a>.</li>
	<li>Check the installed felix driver
		<pre>rpm -qa | grep tdaq</pre>
	</li>
	<li>Uninstall the previous driver
		<pre>rpm -e [filename given by previous command]</pre>
	</li>
	<li>Install a new driver: 
		<pre>yum install <?=$FLX_DR_FILE;?></pre>
	</li>
	<li>Start the newly installed drivers:
		<pre>./etc/init.d/drivers_flx start</pre>
	</li>
</ol>



<h2 id="init-felix" class="SUBTITLE">Initialize the FELIX card and setup elinks</h2>

<ol>
  <li>Install the recommended FELIX driver</li>
  <li>Flash the recommended FELIX firmware</li>
  <li>Setup the recommended FELIX software</li>
  <li>Initialize the FELIX card to synchronize with the LPGBT 
    <pre>
      source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh
      flx-init -c X
    </pre>
   </li>
   <li> Setup e-links , for this we need an extra scripting library (Old scripts from Marco travato)
   <pre>
     git clone https://gitlab.cern.ch/itk-felix-sw/itk-pix-felix-scripts.git #do it only once
	 cd itk-pix-felix-scripts 
	 source config_encoding_decoding.sh 0
	 source config_encoding_decoding.sh 1
   </pre>
  </li>
</ol>
  






<h2 id="config-lpgbt-fice" class="SUBTITLE">Configure LpGBT through the optical link (recommended for Optoboard)</h2>

Alternatively, you can configure the LPGBT through the optical link.
This requires that the LPGBT has been initialized to communicate through the IC bus.
This will be the default configuration mode during production in SR1, and operation in the detector.

To configure a single register with fice you can use the following command:
<pre>
fice -G 0 -I 70 -a 0x1c5
</pre>
If you are dealing with opto-board V2 there needs to be 2 modifications.
<ul>
<li>Change the channel to 70 -> 74
<li>The lane pollarity needs to be changed: fgpolarity -r set
<li>The FEC5 sohuld be changed to FEC12:  flx-config set GBT_DATA_RXFORMAT2=0x1 
</ul>


Important not here is that, to acceses the different channels in the fice configuration you need to do the following configuration:
<ul>
<li> Channel 0: fice -G 0 -i 0 -d 0 # This is identical to fice -G 0
<li> Channel 1: fice -G 1 -i 1 -d 0 # 
<li> Channel 2: fice -G 0 -i 0 -d 1 # 
<li> Channel 3: fice -G 1 -i 1 -d 1 # 
</ul>


<h2 id="config-lpgbt-optotool" class="SUBTITLE">Configure LpGBT with Optoboard FELIX</h2>

Checkout slim_config from optoboard_felix package 
<ol>
  <li>git checkout slim_config</li> 
  <li>git clone ssh://git@gitlab.cern.ch:7999/bat/optoboard_felix.git</li>
</ol>

Configure the opto_board: Remember to change N to the physical felix fiber link
<ol>
  <li>cd optoboard_felix</li>
  <li>python quick_start.py -v 1.2 -s 524d0002 -G N -i 0 -d 0 </li>
</ol>




<h2 id="Power_control" class="SUBTITLE">Powering up the setup</h2>
To power LpGBT and RD53A you can use the DCS functionality in pcatlidros02. 
Fan needs to be always on. To use it:
</p>
<pre>
source <?=$ITKSW;?>/setup.sh
PSUControlGUI.py -f <?=$ITKSW;?>/PySerialComm/CRCard/FLX712.card
</pre>

<img class="MEDIUMIMAGE" src="images/itkpix-psu-control-gui.png"/>

<h2 id="aurora_alignment" class="SUBTITLE">Checking aurora alignment</h2>
We can check if the decoding of each RD53A front-end is running through the FELIX registers.

<ol>
    <li>List the FELIX registers
        <pre>flx-config list | grep DECODING_LINK_ALIGNED</pre>
    </li>
    <li>Only the front-ends being decoded in each link will show as high bit
<pre style="font-size:xx-small;">
0x2180 [R  57:00]                   DECODING_LINK_ALIGNED_00   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2190 [R  57:00]                   DECODING_LINK_ALIGNED_01   0x000000030000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21a0 [R  57:00]                   DECODING_LINK_ALIGNED_02   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21b0 [R  57:00]                   DECODING_LINK_ALIGNED_03   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21c0 [R  57:00]                   DECODING_LINK_ALIGNED_04   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21d0 [R  57:00]                   DECODING_LINK_ALIGNED_05   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21e0 [R  57:00]                   DECODING_LINK_ALIGNED_06   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x21f0 [R  57:00]                   DECODING_LINK_ALIGNED_07   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2200 [R  57:00]                   DECODING_LINK_ALIGNED_08   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2210 [R  57:00]                   DECODING_LINK_ALIGNED_09   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2220 [R  57:00]                   DECODING_LINK_ALIGNED_10   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2230 [R  57:00]                   DECODING_LINK_ALIGNED_11   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2240 [R  57:00]                   DECODING_LINK_ALIGNED_12   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2250 [R  57:00]                   DECODING_LINK_ALIGNED_13   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2260 [R  57:00]                   DECODING_LINK_ALIGNED_14   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2270 [R  57:00]                   DECODING_LINK_ALIGNED_15   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2280 [R  57:00]                   DECODING_LINK_ALIGNED_16   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x2290 [R  57:00]                   DECODING_LINK_ALIGNED_17   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22a0 [R  57:00]                   DECODING_LINK_ALIGNED_18   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22b0 [R  57:00]                   DECODING_LINK_ALIGNED_19   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22c0 [R  57:00]                   DECODING_LINK_ALIGNED_20   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22d0 [R  57:00]                   DECODING_LINK_ALIGNED_21   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22e0 [R  57:00]                   DECODING_LINK_ALIGNED_22   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
0x22f0 [R  57:00]                   DECODING_LINK_ALIGNED_23   0x000000000000000  Every bit corresponds to an E-link on one (lp)GBT or FULL-mode
</pre>
</li>
</ol>




<h2 id="felix-tofromhost" class="SUBTITLE">Running FELIX servers</h2>

Felix-tohost and felix-fromhosts  applications provided by the felix-sw. It has to be running during the scan.
It receives commands from the scan code through the ethernet using the NETIONEXT protocol and forwards them to the front-end through the optical links, and it receives the data from the front-end through the optical links and forwards it to the scan through NETIONEXT.

<ol>
	<li> Start two different terminals
    <li> In both terminals source the envoriment
        <pre>source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh</pre>
    </li>
    <li> Terminal 1 Start ToFelix 
        <pre>felix-toflx --unbuffered  --bus-dir /tmp/bus --bus-groupname FELIX --cid=0x0 --device 0 --iface lo</pre>
    </li>
    <li> Terminal 2 Start ToHost 
        <pre>felix-tohost --bus-dir /tmp/bus --bus-groupname FELIX --netio-timeout 1 --cid 0x0 --device 0 --iface lo -v</pre>
    </li>

    <li>Wait until the message both serves are up and running</li>
</ol>

<h2 id="itk-felix-sw" class="SUBTITLE">Compile the ITK FELIX SW</h2>

ITK FELIX SW is a collection of ITK specific tools for FELIX.
More details can be found in the following <a href="https://ade-pixel-group.web.cern.ch/itk-felix-sw/html/">link</a>


<ol>
	<li> First lets setup the base directory for ITk-FELIX-SW
	<pre> git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/itk-felix-sw.git</pre>
	</li>
	<li> Then get the required software packages for ITkPix 
	<pre> cd itk-felix-sw </pre>
	<pre> git clone ssh://git@gitlab.cern.ch:7999/itk-felix-sw/RD53BEmulator.git </pre>	
	<pre> git clone https://:@gitlab.cern.ch:8443/itk-felix-sw/itk-ic-over-netio-next.git #Not mendatory </pre>		
	</li>

	<li>To Compile the library 
	<pre> mkdir build; </pre>
	<pre> cd build; </pre>
	<pre> cmake3 ../ </pre>
	<pre> make -j </pre>
	<pre> make install </pre>
	</li>

	<li> Linking felix libraries for enabling felix client 
	<pre> cd <itk-felix-sw base directory> # cd ../ </pre>
	<pre> python share/copy_netionext_to_external.py -f <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/lib # or link your own felix library </pre>
	If you don't do this step, when you run itk-felix-sw you will proabably get an error on unable to connect and or a library not found error
	</li>
</ol>	

<h2 id="scan_manager" class="SUBTITLE">Running scan_manager</h2>
  
<pre>
  source /home/itkpix/itk-felix-sw/setup.sh 
  scan_manager_rd53a -n [connectivity-file.json] -s [scan-name] -B [Bus directory /tmp/bus if you use the defaults]
</pre>

The allowed list of scans are:
<ol>
<li> Digital Scan </li>
<li> Analog Scan </li>
<li> ThresholdScan </li>
<li> ToTScan </li>
<li> GlobalThresholdTune </li>
<li> PixelThresholdTune </li>
<li> CommScan: Scans pre-emphasis parameters for communucation check</li>
<li> TrimScan: Scans the internal chip voltage registers </li>
<li> ExternalTriggerScan: Scan used for taking data in test beam</li>
<li> CrossTalkScan </li>
<li> DisconnectedBumpScan</li>
<li> SelfTriggerScan </li>
<li> ReadRegisterScan: Scan for reading internal registers of itkpix </li>

</ol>



<h2 id="connectivity" class="SUBTITLE">Connectivity file</h2>

Each line of the file needs the following:

<table>
  <tr><td>Attribute</td><td>Type</td><td>Description</td></tr>
  <tr><td>name</td><td>string</td><td>Front-end identifier name (rd53a_1, A_BM_01_1, Quad13_1...)</td></tr>
  <tr><td>config</td><td>string</td><td>JSON configuration file: (same as YARR) </td></tr>
  <tr><td>rx</td><td>int</td><td>is the data FID link (you need the full FID that looks like 1152921504606846976)</td></tr>
  <tr><td>tx</td><td>int</td><td>is the command FID link (you need the full FID that looks like 1152921504607141888)</td></tr>
  <tr><td>host</td><td>string</td><td>"127.0.0.1" if you run  scan_manager_rd53 on the same FELIX server (This is absoluted in NETIONEXT but we keep it for comapbility) </td></tr>
  <tr><td>cmd_port</td><td>int</td><td>the port used by felix-core to receive the commands (12340) (This is absoluted in NETIONEXT but we keep it for comapbility) </td></tr>
  <tr><td>data_port</td><td>int</td><td>the port used by felix-core to receive the subscription for the data (12350) (This is absoluted in NETIONEXT but we keep it for comapbility) </td></tr>
  <tr><td>enabled</td><td>int</td><td>1 to mark you want to use that front-end</td></tr>
</table>

<pre>
{
  "connectivity" : [
    {"name": "rd53a_1", 
     "config" : "default_rd53a.json", 
     "rx" :  1152921504606846976, 
     "tx" :  1152921504607141888, 
     "host": "127.0.0.1", 
     "cmd_port": 12340, 
     "data_port": 12350, 
     "enable" : 1, 
     "locked" : 0
    }    
  ]
}
</pre>



</div>
</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
