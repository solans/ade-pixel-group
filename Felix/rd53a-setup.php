<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/JS/toc.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<title>FELIX setup for RD53A</title>
</head>
<body>

<?
//FELIX config
$HOME="/home/itkpix";
$FELIXSW=$HOME."/felix-sw";
$FELIXDR=$HOME."/felix-dw";
$FELIXFW=$HOME."/felix-fw";
$ITKSW=$HOME."/itk-felix-sw";
$TDAQ_VERSION="9.4.0";
$FLX_FW_VERSION="FLX712_PIXEL_24CH_2022-03-14";
$FLX_SW_VERSION="4.2.0 RM5";
$FLX_DR_VERSION="4.9";
$FLX_DR_FILE="tdaq_sw_for_Flx-4.9.0-2dkms.noarch.rpm";
$FLX_SW_FILE="felix-04-02-01-rm5-stand-alone-x86_64-centos7-gcc11-opt.tar.gz";
$FLX_FW_FILE="FLX712_PIXEL_24CH_CLKSELECT_GIT_phase2-master_FLXUSERS-477_rm-5.0_2163_220223_10_34.mcs";
$FLX_SW_PATH="felix-04-02-01-rm5-stand-alone";
$BINARY_TAG="x86_64-centos7-gcc11-opt";
$ITK_FLX_SW_VERSION="master";
?>


<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<h1 class="TITLE">FELIX setup for RD53A</h1>

<script>
$(document).ready(loadToc);
</script>

<div class="CONTENT">

<a href="/Felix">Back</a>

<h2 id="Components" class="SUBTITLE">Components</h2>

<table>
  <tr><td><b>Component</b></td><td><b>Procurement</b></td><td><b>Picture</b></td></tr>
  <tr><td>FELIX Phase-I server</td><td>Contact ITK Common Electronics coordinators</td><td><img class="TINYIMAGE ZOOM" src="images/felix-phase-1-server.png"/></td></tr>
  <tr><td>FELIX Phase-I FLX-712</td><td>Contact ITK Common Electronics coordinators</td><td><img class="TINYIMAGE ZOOM" src="images/felix-phase-1-card.png"/></td></tr>
  <tr><td>VLDB+ (LPGBT carrier board)</td><td>Contact CERN EP-ESE team <a href="https://vldbplus.web.cern.ch/">here</a></td><td><img class="TINYIMAGE ZOOM" src="images/vldbplus.png"></td></tr>
  <tr><td>Goettingen Break-Out Board</td><td>Contact CERN EP-ADE team <a href="https://edms.cern.ch/document/2937397/">EDMS</a></td><td><img class="TINYIMAGE ZOOM" src="images/bob.png"/></td></tr>
  <tr><td>TAG programmer</td><td>RS 134-6435</td><td><img class="TINYIMAGE ZOOM" src="images/jtag-programmer.png"/></td></tr>
  <tr><td>LC-LC adapter</td><td>FS Article 48497 Model HD-LCU-DX-SMF</td><td><img class="TINYIMAGE ZOOM" src="images/FS-48497-LC-LC.jpg"/></td></tr>
  <tr><td>MPO12-12LC</td><td>FS Article 31098 Model 12FMTPLCOM3 (Please note it has to be Male, length is not relevant)</td><td><img class="TINYIMAGE ZOOM" src="images/FS-31098-MPO12-12LC.jpg"/></td></tr>
  <tr><td>MPO24-24LC</td><td>FS Article 31184 Model 24FMPOLCOM3</td><td><img class="TINYIMAGE ZOOM" src="images/FS-31184-MPO24-24LC.jpg"/></td></tr>
  <tr><td>ZIF to DP adapter card V3</td><td>Contact Osaka team <a href="https://edms.cern.ch/document/2937427/">EDMS</a></td><td><img class="TINYIMAGE ZOOM" src="images/ZIF_to_DP.png"></td></tr>
</table>

<h2 id="fiber-assembly">Fiber assembly</h2>

<img class="LARGEIMAGE" src="images/rd53a-fiber-assembly.png"/>



<h2 id="vldbplus-bob">VLDB+ BOB</h2>

<img class="LARGEIMAGE" src="images/rd53a-vldb-bob-cabling.png"/>


<h2 id="install-felix-dr">Install FELIX driver</h2>

<ol>
  <li>Make the felix driver directory
    <pre>mkdir <?=$FELIXDR;?></pre>
  </li>
  <li>Change to that directory
    <pre>cd <?=$FELIXDR;?></pre>
  </li>
  <li>Download felix driver
    <pre>curl -O https://ade-pixel-group.web.cern.ch/Felix/felix-ke/<?=$FLX_DR_FILE;?></pre>
  </li>
  <li>Install the felix driver
    <pre>yum install <?=$FLX_DR_FILE;?></pre>
  </li>
  <li>Start the felix driver
    <pre>systemctl start drivers_flx</pre>
  </li>
</ol>

<h2 id="install-felix-sw">Install FELIX SW</h2>

<ol>
  <li>Make the felix-sw directory
    <pre>mkdir <?=$FELIXSW;?></pre>
  </li>
  <li>Change to that directory
    <pre>cd <?=$FELIXSW;?></pre>
  </li>
  <li>Download felix driver
    <pre>curl -O https://ade-pixel-group.web.cern.ch/Felix/felix-sw/<?=$FLX_SW_FILE;?></pre>
  </li>
  <li>Uncompress the felix-sw
    <pre>tar xzvf <?=$FLX_SW_FILE;?></pre>
  </li>
</ol>

<h2 id="install-felix-sw">Install FELIX FW</h2>

<ol>
  <li>Make the felix firmware directory
    <pre>mkdir <?=$FELIXFW;?></pre>
  </li>
  <li>Change to that directory
    <pre>cd <?=$FELIXFW;?></pre>
  </li>
  <li>Download felix firmware
    <pre>curl -O https://ade-pixel-group.web.cern.ch/Felix/firmware-rd53a/<?=$FLX_FW_VERSION;?>/<?=$FLX_FW_FILE;?></pre>
  </li>
  <li>Install the felix firmware
    <ol>
      <li><pre>source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh</pre></li>
      <li><pre>fflashprog -f0 <?=$FLX_FW_FILE;?> prog</pre></li>
      <li><pre>fflashprog -f1 <?=$FLX_FW_FILE;?> prog</pre></li>
      <li><pre>fflashprog -f2 <?=$FLX_FW_FILE;?> prog</pre></li>
      <li><pre>fflashprog -f3 <?=$FLX_FW_FILE;?> prog</pre></li>
    </ol>
  </li>
  <li>Reboot the system
    <pre>sudo reboot</pre>
  </li>
  <li>Check the firmware version
    <ol>
      <li><pre>source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh</pre></li>
      <li><pre>flx-info</pre></li>
    </ol>
  </li>
</ol>


<h2 id="init-felix" class="SUBTITLE">Initialize the FELIX card</h2>

<ol>
  <li>Initialize the FELIX card  
    <pre>
      source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh
      flx-init
    </pre>
  </li>
  <li>Download MarcoScripts
    <pre>
      cd <?=$FELIXSW;?>    
      git clone https://gitlab.cern.ch/itk-felix-sw/MarcoScripts.git
    </pre>
  </li>
  <li>Configure the encoding and decoding
    <pre>
      source <?=$FELIXSW;?>/MarcoScripts/configure_encode_decode.sh 0
      source <?=$FELIXSW;?>/MarcoScripts/configure_encode_decode.sh 1
    </pre>
  </li>
</ol>

<h2 id="config-vldb" class="SUBTITLE">Configure the VLDB+</h2>

<div>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-1.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-2.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-3.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-4.png"/>
</div>
<div>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-5.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-6.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-7.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-8.png"/>
</div>

<ol>
  <li> Open webpage for pigbt </li>
  <li> Select platform: Real LPGBT </li>
  <li> Select hardware: VLDB+ </li>
  <li> Select the address in the list </li>
  <li> Click on OK </li>
  <li> Do you want to initialize the LPGBT: yes </li>
  <li> LpGBT mode: TRX </li>
  <li> TX data rate: 10 Gbps </li>
  <li> TX encoding: FEC5 </li>
  <li> Uplink EPRX (Click gear symbol) </li> 
  <li> Data Rate: 1280 </li>
  <li> Track Mode: Continues </li>
  <li> Control: TERM </li>
  <li> Equalization: OFF </li>
  <li> Downlink EPTX </li>
  <li> Data Rate 160 Mpbs </li>
  <li> Drive strength 4.0 mA </li>
  <li> click on the menu -> high speed </li>
  <li> Invert high speed data output enabled (Invert polarity uplinks)
</ol>

<h2 id="next-step" class="SUBTITLE">Next steps</h2>
Congratulations. You are ready to start doing scans

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
