<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>RD53A FELIX firmware</title>
<style>
</style>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">

<h1 class="TITLE">RD53A Firmware for FELIX</h1>

<?php
$path='<a href="'.$_SERVER['PHP_SELF'].'">firmware-rd53a</a>/';
$path='firmware-rd53a/';
if(isset($_GET["path"])){
  $path.=$_GET["path"]."/"; 
  echo '<h2><a href="'.$_SERVER['PHP_SELF'].'">firmware-rd53a</a>/'.$_GET["path"].'</h2>';
}
?>

<table>
<tr>
  <td><strong>File</strong></td>
</tr>
<?php
$files = scandir($path);
foreach($files as $f){
  if ($f=="." || $f==".." || strpos($f,".cmt")!=FALSE || strpos($f,".info")!=FALSE ){continue;}
  if ($f[0]=="."){continue;}
  ?>
<tr>

  <?php 
  if (is_dir($path.$f)) {
  ?>
  <td><a href="?path=<?=$f;?>"><?=$f;?></a></td>
  <?php 
  }else{
  ?>  
  <td><a href="<?=$path.$f;?>"><?=$f;?></a></td>
  <?php 
  }
  ?>  
</tr>  
  <?php
}
?>
</tr>
</table>
</div>


<?php
  show_footer();
?>
</div>

</body>
</html>
