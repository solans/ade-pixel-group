<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/img/ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="/JS/toc.js"></script>
<title>Felix</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<script>
$(document).ready(loadToc);
</script>

<div class="CONTENT">

<h1 class="TITLE">FELIX</h1>
<p>FELIX (Front-End Link Exchange) is the read-out system for the ATLAS Phase-II upgrade.
Detector optical Front-End links connect to FELIX that acts as a gateway to a commodity switched network 
to communicate with sub-detector specifc data handling and processing components.
</p>

<p>
<img class="IMAGEW600" src="images/atlas-itk-strips-tdr.png"/>
</p>

<h2 id="documentation_and_links" class="SUBTITLE">Documentaion and links</h2>
<p>
  <ul>
    <li><a href="https://gitlab.cern.ch/itk-felix-sw">ITK FELIX SW repository</a></li>
    <li><a href="<?=$gobase;?>itk-felix-sw/html/">ITK FELIX SW documentation</a></li>
    <li><a href="<?=$gobase;?>Felix/itkpix-setup.php">ITK Pixel FELIX ITKPix setup</a></li>
    <li><a href="<?=$gobase;?>Felix/itkpix-tests.php">ITK Pixel FELIX ITKPix tests</a></li>
    <li><a href="<?=$gobase;?>Felix/itkpix-firmware.php">ITK Pixel FELIX ITKPix firmware</a></li>
    <li><a href="<?=$gobase;?>Felix/rd53a-mms-SR1.php">ITK Pixel FELIX RD53A multi-module setup in SR1</a></li>
    <li><a href="<?=$gobase;?>Felix/rd53a-tests.php">ITK Pixel FELIX RD53A testing documentation</a></li>
    <li><a href="<?=$gobase;?>Felix/rd53a-scans-161.php">ITK Pixel FELIX RD53A running scans in 161</a></li>
    <li><a href="<?=$gobase;?>Felix/rd53a-setup.php">ITK Pixel FELIX RD53A setup</a></li>
    <li><a href="<?=$gobase;?>Felix/rd53a-firmware.php">ITK Pixel FELIX RD53A Firmware repository</a></li>
    <li><a href="<?=$gobase;?>Felix/fei4b-tests.php">ITK Pixel FELIX FEI4B Tests</a></li>
    <li><a href="<?=$gobase;?>Felix/fei4b-setup.php">ITK Pixel FELIX FEI4B Setup</a></li>
    <li><a href="http://www.atlas.uni-wuppertal.de/~wensing/felix">ITK Pixel FELIX FEI4B Firmware</a></li>
    <li><a href="<?=$gobase;?>Felix/bcmp-tests.php">BCMPrime FELIX Tests</a></li>
    <li><a href="<?=$gobase;?>Felix/bcmp-firmware.php">BCMPrime FELIX Firmware</a></li>
    <li><a href="<?=$gobase;?>Felix/felix-sw.php">FELIX SW repository clone</a></li>
    <li><a href="<?=$gobase;?>Felix/felix-ke.php">FELIX Kernel Drivers repository clone</a></li>
    <li><a href="https://atlas-project-felix.web.cern.ch">FELIX project page</a></li>
    <li><a href="https://gitlab.cern.ch/atlas-tdaq-felix">FELIX gitlab repository</a></li>
  </ul>
</p>


<h2 id="presentations" class="SUBTITLE">Presentations</h2>
<p>
  <ul>
    <li><a href="https://indico.cern.ch/event/1348172/contributions/5697680/attachments/2762922/4811930/PixelFelix-SW-Architecture.pdf">I. Siral, ITK online software, November 2023</a></li>
    <li><a href="https://indico.cern.ch/event/1223749/contributions/5669052/attachments/2752797/4792301/Status%20of%20of%20SW%20integration%20with%20TDAQ.pdf">I. Siral, AUW November 2023</a></li>
    <li><a href="https://indico.cern.ch/event/1342692/contributions/5652575/attachments/2745356/4776815/Carlos_DAQ_20231102_2.pdf">C. Solans, ITK online software, November 2023</a></li>
    <li><a href="https://indico.cern.ch/event/1334668/contributions/5634049/attachments/2737302/4761274/Carlos_DAQ_20231019_2.pdf">C. Solans, ITK online software, October 2023</a></li>
    <li><a href="https://agenda.infn.it/event/35597/contributions/211783/attachments/111605/159282/VERTEX_2023_Poster_BrianMoser_v2.pdf">B. Moser, VERTEX, October 2023</a></li>
    <li><a href="https://indico.cern.ch/event/1309296/contributions/5507357/attachments/2688147/4664145/Itk-felix-sw-Performance-17July.pdf">I. Siral, ITK online software, July 2023</a></li>
    <li><a href="https://indico.cern.ch/event/1309296/contributions/5507382/attachments/2688143/4664136/SKBM_20230718_SoftwareMeeting_v1.pdf">B. Moser, ITK online software, July 2023</a></li>
    <li><a href="https://indico.cern.ch/event/1291542/contributions/5432779/attachments/2658080/4603827/Carlos_DAQ_20230601.pdf">C. Solans, ITK online software, June 2023</a></li>
    <li><a href="https://indico.cern.ch/event/1286301/contributions/5408953/attachments/2649379/4586697/FELX-Informecial.pdf">I. Siral, ITK online software, June 2023</a></li>
    <li><a href="https://indico.cern.ch/event/1179940/contributions/5042163/attachments/2506953/4308209/Carlos_DAQ_20220913_2.pdf">C. Solans, ITK week, September 2022</a></li>
    <li><a href="https://indico.cern.ch/event/1184320/contributions/4975869/attachments/2484613/4268102/Carlos_DAQ_20220726.pdf">C. Solans, ITK read-out meeting, July 2022</a></li>
    <li><a href="https://indico.cern.ch/event/1160515/contributions/4884423/attachments/2447974/4194837/DAQtoDCScommuncation_May20.pdf">A. Palazzo, ITK DCS meeting, May 2022</a></li>
    <li><a href="https://indico.cern.ch/event/1036189/contributions/4351355/attachments/2243163/3803713/Kaan_Carlos_FELIX_DCS_20210511.pdf">K. Oyulmaz, ITK-TDAQ discussion, May 2021</a></li>
    <li><a href="https://indico.cern.ch/event/950039/contributions/4022451/attachments/2108699/3547165/thielmann_SwRodReadout.pdf">O. Thielman, ITK Week September 2020</a></li>
	  <li><a href="https://indico.cern.ch/event/948391/contributions/3985032/attachments/2101377/3532824/thielmann_QTUpdate_11_9_20.pdf">O. Thielman, ITK read-out meeting September 2020</a></li>
    <li><a href="https://indico.cern.ch/event/936207/contributions/3953678/attachments/2079637/3492872/thielmann_QTPresentation.pdf">O. Thielman, ITK read-out meeting July 2020</a></li>
    <li><a href="https://indico.cern.ch/event/708041/contributions/3276154/attachments/1810012/2955590/Carlos_ACAT_20190311_6.pdf">C. Solans, ACAT 2019, March 2019</a></li>
    <li><a href="https://indico.cern.ch/event/804702/contributions/3348075/">L. Franconi, ITK-TDAQ discussion, March 2019</a></li>
    <li><a href="https://indico.cern.ch/event/789197/contributions/3278440/attachments/1778790/2893375/Carlos_Network_20190115.pdf">C. Solans, ITK-TDAQ discussion, January 2019</a></li>
 	  <li><a href="https://indico.cern.ch/event/771325/contributions/3204567/attachments/1756381/2847800/Carlos_FELIX_20181120_1.pdf">C. Solans, ITK-TDAQ discussion, November 2019</a></li>
 	  <li><a href="https://indico.cern.ch/event/772290/contributions/3212029/attachments/1751612/2838393/Carlos_AUW_20181113.pdf">R. Sipos, AUW November 2018</a></li>
	  <li><a href="https://indico.cern.ch/event/730816/contributions/3134881/attachments/1715352/2767109/Carlos_FELIX_20180913.pdf">C. Solans, TDAQ Week Cracow, September 2018</a></li>
    <li><a href="https://indico.cern.ch/event/700657/contributions/2874540/attachments/1646003/2631430/ASharma_FELIX_Readout_2018_05_08.pdf">A. Sharma, Phase-II FELIX Discussion, May 2018</a></li>
    <li><a href="https://indico.cern.ch/event/694189/contributions/2890335/attachments/1599383/2535096/FELIX_ITk.pdf">A. Miucci, ITK Week February 2018</a></li>
    <li><a href="https://indico.cern.ch/event/678600/contributions/2783054/attachments/1556880/2448950/Carlos_NETIO_20171113.pdf">C. Solans, AUW November 2017</a></li>
    <li><a href="https://indico.cern.ch/event/680621/contributions/2788507/attachments/1557695/2450486/14112017-AUW-ITKFELIX.pdf">R. Sipos, AUW November 2017</a></li>
    <li><a href="https://indico.cern.ch/event/609082/contributions/2717710/attachments/1525891/2385915/20170919_ITK_demo_preparations.pdf">G. Lehman, ITK Week, September 2017</a></li>
  </ul>
</p>



</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
