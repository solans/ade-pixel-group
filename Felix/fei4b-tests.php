<?php
include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link href="/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="../ATLAS-icon.ico">
    <script src="https://code.jquery.com/jquery-3.1.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/JS/toc.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>FELIX tests with FEI4</title>
    </head>
    <body>

    <div class="ARTBOARD">
    <?php
    show_header();
    show_navbar();
?>

<h1 class="SUBTITLE">Felix tests with FEI4</h1>

  <script>
  $(document).ready(loadToc);
</script>

<div class="CONTENT">

  <h2 id="disclaimer" class="SUBSUBTITLE">Disclaimer</h2>
  This tests have been performed on CERN Centos7 with CVMFS. 
  Other configurations may not be supported.
  Please check the <a href="docs">docs</a> for more information.

  <h2 id="pre_requirements" class="SUBSUBTITLE">Pre-requirements</h2>
  Before proceeding to install the software, you should install some things:
  <ol>
  <li>Xilinx Vivado
  <pre> https://www.xilinx.com/support/download.html </pre>
  The WebPack is perfectly fine.<br /><br /> 

  </li>

  <li>Drivers for FELIX <br/>
  This requires super user privilegies. Please, refer to the <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/docs/FelixUserManual.pdf">FelixUserManual</a> for more information.  
To check if a driver is already installed issue the following command:
  <pre> rpm -qa | grep tdaq</pre>
  If a driver rpm is installed you'll see a response along the lines of:
  <pre> tdaq_sw_for_Flx-1.0.6-2dkms.noarch </pre>
  To remove the driver (substituting "filename" for the results of the search in the previous step):
  <pre> rpm -e filename</pre> 
  Download the felix driver from the following <a href="https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/">link</a>
  <pre>https://atlas-project-felix.web.cern.ch/atlas-project-felix/user/dist/software/driver/</pre>
  Once this operation is complete you will be in a position to install the latest FELIX driver.
  <pre>sudo yum install tdaq_sw_for_Flx-4.0.5-2dkms.noarch.rpm </pre>
  </li>
  <li>Latest version of java
  <pre>sudo yum install java-1.8.0 </pre>
  </li>
  <li>Install the required library
  <pre>sudo yum install librdmacm</pre>
  </li>

  </ol>
    
  <h2 id="compile" class="SUBSUBTITLE">Installing the software</h2>
  
  Compile the FELIX software and build a distribution
  <ol>
  <li>Create the needed folders
  <pre> mkdir FelixSW; cd FelixSW; mkdir felix; cd felix </pre>
  </li>
  <li>Checkout the software package of atlas-tdaq-felix
  <pre> git clone https://:@gitlab.cern.ch:8443/atlas-tdaq-felix/software.git felix-4.0.5 </pre>
</li>
<li>Change into the software directory
<pre> cd felix-4.0.5</pre>
</li>
<li>Clone all the packages
<pre> ./clone_all.sh krb5</pre>
</li>
<li>Checkout felix tag 4.0.5
<pre> ./checkout_all.sh felix-04-00-05</pre>
</li>
<li>Setup the environment
<li>Delete felix-config package because is not well tagged
<pre> rm -rf felix-config</pre>
</li>
<pre> source cmake_tdaq/bin/setup.sh x86_64-centos7-gcc62-opt</pre>
</li>
<li>Prepare the release for building
<pre> cmake_config x86_64-centos7-gcc62-opt</pre>
</li>
<li>Change to the build directory
<pre> cd x86_64-centos7-gcc62-opt</pre>
</li>
<li>Compile the software
<pre> make -j7</pre>
</li>
<li>Change to the distribution directory
<pre> cd ../distribution</pre>
</li>
<li>Build the release
<pre> ./build.sh installed x86_64-centos7-gcc62-opt 93 93</pre>
</li>
<li>Return to the folder FelixSW 
<pre> cd ../../../</pre>
</li>
</ol>

Compile the ItkPixDaq software for FELIX
<ol>
<li>Checkout ItkPixDaq package
<pre> git clone https://gitlab.cern.ch/solans/itkpixdaq.git ItkPixDaq</pre>
</li>
<li>Change to the ItkPixDaq directory
<pre> cd ItkPixDaq</pre>
</li>
<li>Checkout the following packages
<pre>
git clone https://gitlab.cern.ch/solans/itkpixdaq-Yarr.git Yarr
git clone https://gitlab.cern.ch/solans/itkpixdaq-Util.git Util
git clone https://gitlab.cern.ch/solans/itkpixdaq-tools.git tools
git clone https://gitlab.cern.ch/solans/itkpixdaq-Fei4.git Fei4
git clone https://gitlab.cern.ch/solans/itkpixdaq-NetioHW.git NetioHW
git clone https://gitlab.cern.ch/solans/itkpixdaq-FelixFW.git FelixFW
git clone https://gitlab.cern.ch/solans/GbtxCliProgrammer.git GbtxCliProgrammer 
git clone https://gitlab.cern.ch/solans/FpgaTools.git FpgaTools 
</pre>
</li>
<li> Settings for the Gbtx
<pre> sudo cp GbtxCliProgrammer/share/99-gbtx.rules /etc/udev/rules.d/ 
sudo udevadm control --reload
sudo udevadm trigger
</pre>
<li>Setup the environment  
<pre> source ./setup.sh</pre>
</li>
<li>Make a build directory
<pre> mkdir build</pre>
</li>
<li>Change to the build directory
<pre>cd build</pre>
</li>
<li>Config the release for building
<pre> cmake ..</pre>
</li>
<li>Compile and install the software
<pre> make -j6 install</pre>
</li>
</ol>
    
  <h2 id="program_felix" class="SUBSUBTITLE">Program the Mini-FELIX from the command line (Recommended)</h2>
  <ol>
  <li>Connect your Felix board with a mini-USB cable to the computer where you are running Vivado.</li>
  
  <li>Select firmware to program from FelixFW
  <pre>/afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq/FelixFW</pre>
  Recommended fimrware is the following:
  <pre>FLX709_FE-I4B_RM0406_4CH_CLKSELECT_GIT_FEI4backport_rm-4.6_50_190507_13_31.bit</pre>
  </li>
  <li>Flash the Mini-FELIX
  	<pre> cd /afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq/</pre>
	<pre> source ./setup.sh</pre>
    <pre> fpgacliprogrammer.py -f FelixFW/FLX709_FE-I4B_RM0406_4CH_CLKSELECT_GIT_FEI4backport_rm-4.6_50_190507_13_31.bit</pre>
  </li>
  <li>Restart the pc after the Mini-FELIX programming. 
  <br/>Be careful: the VC709 board must not lose power during the reboot, therefore it has to be powered via the external power supply (not internally to the server). 
  </li>
  </ol>
  
  <h2 id="program_felix_with_vivado" class="SUBSUBTITLE">Program the Mini-FELIX using the GUI</h2>
  <ol>
  <li>Connect your Felix board with a mini-USB cable to the computer where you are running Vivado.</li>
  <li>Open Vivado
  <pre> /afs/cern.ch/work/a/adecmos/public/Xilinx/start_vivado.sh</pre>
  or, alternatively, cd to the folder where you have installed Vivado:
  <pre> cd /afs/cern.ch/work/a/adecmos/public/Xilinx/Vivado/2017.2 </pre>
  <pre> ./settings64.sh</pre>
  <pre>vivado </pre>
  </li>
  <li>Open the hardware manager
  <pre>
  Hardware manager > Open target > auto connect
  Click on xc7vx690t > Program device
  </pre>
  If the xc7vx690t is not present, you can either open Vivado with superuser privilegies, or you can make the USB port writable by all users.<br/><br/>
  </li>
  <li>Select firmware to program from FelixFW
  <pre>/afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq/FelixFW</pre>
  You can also download the firmware directly from <a href="http://www.atlas.uni-wuppertal.de/~wensing/felix">http://www.atlas.uni-wuppertal.de/~wensing/felix</a>.<br/>
  Recommended firmware is the following:
  <pre>FLX709_FE-I4B_RM0406_4CH_CLKSELECT_GIT_FEI4backport_rm-4.6_50_190507_13_31.bit</pre>
  </li>
  <li>Restart the pc after the Mini-FELIX programming. 
  <br/>Be careful: the VC709 board must not lose power during the reboot, therefore it has to be powered via the external power supply (not internally to the server). 
  </li>
  </ol>

  <h2 id="program_felix_to_avoid_reflashing_after_reboot" class="SUBSUBTITLE">Program the Mini-FELIX to avoid need to reflash after rebooting</h2>
  <ol>
  <li>In Vivado, right-click on the target FPGA and select "Add configuration memory".
  </li>
  <li>Select the firmware (see above)
  </li>
  <li>Power cycle the Mini-Felix board
  </li>
</ol>	

  <h2 id="config_felix" class="SUBSUBTITLE">Configure Mini-FELIX</h2>
  <ol>
  <li>Check that the Mini-FELIX has been programmed with the desired firmware and the new fimrware is found after reboot.
     <pre>service drivers_flx status</pre>
     The output should look like the following
     <pre>
Card 0: 
Card type                   : 709
Device type                 : 0x7038
FPGA_DNA                    : 0x006071021141b054
Reg Map Version             : 4.6
Build revision (GIT version): rm-4.6
Date and time               : 6-5-2019 at 10h54
GIT commit number           : 50
GIT hash                    : 0xe8772d6d
Firmware mode               : Unknown (firmware_mode = 3)
Number of descriptors       : 2, Number of interrupts        : 8
     </pre>
  </li>
  <li>Configure the Mini-FELIX: initialize the card, configure the TTCfx, set polarity, set emulation, set active elinks. 
  The default configuration will enable e-links 0,2,4,6 to-host (data from the front-end), 
  and e-links 1, 5 from-host (commands to the front-end).
  Run the following script.
  <pre>
  cd /afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq
  source setup_felix_4.0.5.sh
  source configure_felix_4.0.5.sh
  </pre>
  </li>
  <li>Change the elink configuration to match your needs with elinkconfig. See next section.
  </li>
  </ol>

 <h2 id="elinkconfig" class="SUBSUBTITLE">Configure the elinks on the Mini-FELIX</h2>
  <ol>
  <li>Open elinkconfig: 
  <pre> cd /afs/cern.ch/work/a/adecmos/public/ItkFelix/felix/felix-4.0.5/</pre>
  <pre> source cmake_tdaq/bin/setup.sh</pre>
  <pre> cd x86_64-slc6-gcc62-opt</pre>
  <pre> elinkconfig/elinkconfig</pre>
  <br/>
  <img class="IMAGE" src="images/ElinkConfig.png"/>
  </li>
  <li>
  Click on <span>Read Cfg</span> to read the configuration of the card (top left).
  </li>
  <li>Select the to-host (data from the front-end) and the from-host (commands to the front-end) elinks that you want to use. 
  Refer to <a href="#gbt_elink_config">next section</a> for more details.</li>
  <li>Click on <span>Generate/Upload</span> and then on <span>Upload</span> to upload the configuration to the Mini-FELIX.</li>
  </ol>

  <h2 id="gbt_elink_config" class="SUBSUBTITLE">The GBTx and e-link configuration</h2>
  <ul>
  <li>The GBTx is divided into two parts, the serializer that is the part for the optical transceiver, 
  and the eports that is the part for the communication with the front-end.
  There are 40 eports available in the GBTx, each one has one clock (CLK) signal to the front-end, 
  one data out (CMD) to the front-end, and one data in (DATA) from the front-end.
  </li>
  <li>When deciding which configuration to run, one must take into account what is the physical layout of the eports to the front-end.
  The VLDB and the SFP-GBT-VHDCI only have every second eport routed to the front-end connectors. 
  This means that only the even number eports can be accessed from the GBT (0,2,4,6...).
  </li>
  <li>
  When using the VLDB each mini-HDMI output connector corresponds to each available eport (0,2,4,6...).
  </li>
  <li>
  When using the VHDCI-RJ45 adapter board, each RJ45 corresponds to two eports at a time, ((0,2), (4,6), ...).
  Thus if we want to connect one signle chip card to each RJ45 connector, we can only use one out of four eports (0,4,8,12...).
  The maximum number of single chip cards routable from the SFP-GBT-VHDCI + VHDCI-RJ45 setup is 8 (0,4,8,12,16,20,24,28).
  </li>
  <li>
   The data rate from each FEI4 is 160 Mbps, which matches the bandwitdh of a 4-bit e-link. 
   The command rate for each FEI4 is 80 Mbps at a clock speed of 40 MHz, because the command line is DC-balanced and Manchester encoded.
   However, in the default configuration, a 320 Mbps 8-bit e-link is used with a 40 MHz clock due to the needs of the firmware.
  </li> 
  </ul>

 <h2 id="configure_gbtx" class="SUBSUBTITLE">Configure the GBTx from the command line</h2>
  <ol>
    <li>Connect the USB-I2C dongle to the host from where you want to program the GBTx.</li>
    <li>Make sure that the USB is writable by all users.</li> 
 	<li>Program the GBTx on the VLDB
 	 <pre> cd /afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq/</pre>
	 <pre> source ./setup.sh</pre>
   <pre> source ./setup_felix_4.0.5.sh</pre>
	 <pre> source ./configure_felix_4.0.5.sh</pre>
	 <pre> gbtxcliprogrammer 1 GbtxConfig/GBTX_config_20190111.txt</pre>
   </li>
   <li>
   Check on the output that the state after programming is <span>IDLE</span>.
   </li>
   <li>Check that link is up by doing <pre> flx-info SFP </pre> on the felix host. You should see at least one Link (depending on how many you have connected) in OK status.
     Similarly,  by doing <pre> flx-info GBT</pre> you should see the alignment properly done for at least one Link. <br/>
    <br/>
     See <a href="#things_that_can_go_wrong_and_how_to_fix_them">Things that can go wrong</a> if you encounter errors in the setting up. 
    </li>
  </ol>

 <h2 id="configure_gbtx_with_gui" class="SUBSUBTITLE">Configure the GBTx using the GUI (Recommended)</h2>
  <ol>
    <li>Connect the USB-I2C dongle to the host from where you want to program the GBTx.</li>
    <li>Make sure that the USB is writable by all users.</li> 
  	<li>Make sure you have java 1.8 installed.</li> 
  	<li>Open the GBTx I2C GUI
  	<pre> cd /afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq/</pre>
	<pre> cd installed/share/lib/</pre>
	<pre>java -jar programmerv2.20180725.jar </pre>
	</li>
	<li>
	Read the status of the GBTx. It should be in pauseForConfig.
	<br/>
    <img class="IMAGE" src="images/GBTxConfigurator_1.png"/>
    </li>
	<li>
	Select the file to write
	<pre>/afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq/GbtxConfig/GBTX_config_20190111.txt</pre>
	</li>
    <li>
	Program the GBTx
	<pre>Write to GBTx</pre>
	</li>
    <li>
	Check the status of the GBTx is IDLE
	<br/>
    <img class="IMAGE" src="images/GBTxConfigurator_2.png"/>
    </li>
  </ol>

 
<h2 id="Extra_GBTx_configuration_for_SR1" class="SUBSUBTITLE">Extra GBTx configuration for SR1</h2>
<ol>
  <pre> fgpolarity -G 0 reset -t <br/> fgpolarity -G 1 reset -t</pre>
</ol>

<h2 id="things_that_can_go_wrong_and_how_to_fix_them" class="SUBSUBTITLE">Things that can go wrong and how to fix them</h2>
  This list is not complete, it just contains some of the problems encountered by some users while installing the software.
  <ol>
  <li> During the source setupFlx.sh, you get the message "ERROR. Exception thrown: Failed to open /dev/flx" <br/>
  This is related to the wrong drivers installed. To fix it, check back to the <a href="http://ade-pixel-group.web.cern.ch/ade-pixel-group/Felix/tests.php#pre_requirements"> pre-requirements.</a>  
  </li>
<br/>
  <li> If you do not have the TTCfx board, you should comment out the setupFlx.sh first lines related to it. 
  </li>
  <br/>	
  <li> If there are problems connecting the GBTx, you can try to open and play around with the java interface that controls the GBTx:
  <pre> cd installed/share/lib/
 java -jar programmerv2.20180725.jar </pre>
 E.g. Check that the GBTx is recognised and the dongle is present (on the top right of the GUI interface). 
 Click on "Import Im.." and choose the configuration for the GBTx. Then click on "Write GB..".
  </li>
  <br/>
  <li><b>Dongle</b> is not recognized (green light does not turn on). Maybe port not accessible. Try as root. </li>
  </ol>
  </p>

<h2 id="Configuration_files" class="SUBSUBTITLE">Configuration files</h2>

Felixcore with one thread (felixcore -t1) runs the RX on port 12350 and the TX on port 12340.

<h2 id="start_scan" class="SUBSUBTITLE">Run the Scans</h2>

<ol>
 <li>Source the environment
 	 <pre> source /afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq/setup.sh</pre>
 </li>
  <li>Source Felix (the sourcing of Felix is kept separate from the setup of the analysis, with the intentions to simplify the dependencies and avoid conflicts in the libraries)
 	 <pre> source /afs/cern.ch/work/a/adecmos/public/ItkFelix/ItkPixDaq/setup_felix_4.0.5.sh</pre>
 </li>  	 
 <li>Start Felix
  <pre> felixcore -t1</pre>
 </li>
 <li>Perform a scan
  <pre> scanManager -c elink.json -C -s scanType -w hw.json </pre>
 </li>
Where the scanType is the name of the scan you want to perform. The list of the names of the scans (and the functions that they call during the analysis) is in ItkPixDaq/Fei4/share/Fei4.scans.json. 
</ol>



<h2 id="recompile_felix" class="SUBSUBTITLE">Recompile FELIX distribution for ItkPixDaq</h2>

<ol>
 <li>Imagine the felix software is in:
  <pre>
  /afs/cern.ch/user/a/adecmos/work/public/ItkFelix/software/
  </pre>
 </li>

 <li>after you compile the felix software, go to the distribution directory
 <pre>
  cd /afs/cern.ch/user/a/adecmos/work/public/ItkFelix/software/distribution
 </pre>
 </li>

 <li>
 Build the distribution that will show up in 
 <code>/afs/cern.ch/user/a/adecmos/work/public/ItkFelix/software/distribution/felix-regmap-3</code>
 <pre>
   ./build.sh release x86_64-centos7-gcc82-opt 95 95
 </pre>
 </li>
 
 <li>Change the <code>FELIX_INST_PATH</code> of <code>ItkPixDaq/setup.sh</code> to point to this new path.
 </li>

 <li>
 And finally copy the cmake files into the distribution folder.
 <pre>
 cp PatchFelix/felix-03-08-00/felixConfig* 
    /afs/cern.ch/user/a/adecmos/work/public/ItkFelix/software/distribution/release/.
 </pre>
 </li>

</ol>




<h2 id="setup_sr1" class="SUBTITLE">Setup in SR1</h2>

<ol>
  <li>Vivado gui flash FLX709_FE-I4B_RM0402_4CH_CLKSELECT_GIT_phase2-itk-pix_rm-4.2_97_180605_11_25.bit</li>
  <li>Reboot</li>
  <li>source setup.sh</li>
  <li>source setup_felix_4.0.5.sh</li>
  <li>source configure_felix_4.0.5.sh</li> 
  <li>fgpolarity reset -t</li>
  <li>elinkconfig disable all not being used</li>
</ol>

<img class="MEDIUMIMAGE" src="images/mini-FELIX.jpg"/>
<br/>
<img class="MEDIUMIMAGE" src="images/GBT_box.jpg">

<pre>
    {"config" : "BM1_1.json", "rx" :  0, "tx" :  1, "enable" : 1, "locked" : 0},
    {"config" : "BM1_2.json", "rx" :  2, "tx" :  1, "enable" : 1, "locked" : 0},
    {"config" : "BM1_3.json", "rx" :  4, "tx" :  1, "enable" : 1, "locked" : 0},
    {"config" : "BM1_4.json", "rx" :  6, "tx" :  1, "enable" : 1, "locked" : 0},
    {"config" : "BM2_1.json", "rx" :  8, "tx" :  9, "enable" : 1, "locked" : 0},
    {"config" : "BM2_2.json", "rx" : 10, "tx" :  9, "enable" : 1, "locked" : 0},
    {"config" : "BM2_3.json", "rx" : 12, "tx" :  9, "enable" : 1, "locked" : 0},
    {"config" : "BM2_4.json", "rx" : 14, "tx" :  9, "enable" : 1, "locked" : 0},
    {"config" : "BM3_1.json", "rx" : 16, "tx" : 17, "enable" : 1, "locked" : 0},
    {"config" : "BM3_2.json", "rx" : 18, "tx" : 17, "enable" : 1, "locked" : 0},
    {"config" : "BM3_3.json", "rx" : 20, "tx" : 17, "enable" : 1, "locked" : 0},
    {"config" : "BM3_4.json", "rx" : 22, "tx" : 17, "enable" : 1, "locked" : 0},
    {"config" : "BM4_1.json", "rx" : 64, "tx" : 65, "enable" : 1, "locked" : 0},
    {"config" : "BM4_2.json", "rx" : 66, "tx" : 65, "enable" : 1, "locked" : 0},
    {"config" : "BM4_3.json", "rx" : 68, "tx" : 65, "enable" : 1, "locked" : 0},
    {"config" : "BM4_4.json", "rx" : 70, "tx" : 65, "enable" : 1, "locked" : 0},
    {"config" : "BM5_1.json", "rx" : 72, "tx" : 73, "enable" : 1, "locked" : 0},
    {"config" : "BM5_2.json", "rx" : 74, "tx" : 73, "enable" : 1, "locked" : 0},
    {"config" : "BM5_3.json", "rx" : 76, "tx" : 73, "enable" : 1, "locked" : 0},
    {"config" : "BM5_4.json", "rx" : 78, "tx" : 73, "enable" : 1, "locked" : 0},
    {"config" : "BM6_1.json", "rx" : 80, "tx" : 81, "enable" : 1, "locked" : 0},
    {"config" : "BM6_2.json", "rx" : 82, "tx" : 81, "enable" : 1, "locked" : 0},
    {"config" : "BM6_3.json", "rx" : 84, "tx" : 81, "enable" : 1, "locked" : 0},
    {"config" : "BM6_4.json", "rx" : 86, "tx" : 81, "enable" : 1, "locked" : 0},
    {"config" : "BM7_1.json", "rx" : 88, "tx" : 89, "enable" : 1, "locked" : 0},
    {"config" : "BM7_2.json", "rx" : 90, "tx" : 89, "enable" : 1, "locked" : 0},
    {"config" : "BM7_3.json", "rx" : 92, "tx" : 89, "enable" : 1, "locked" : 0},
    {"config" : "BM7_4.json", "rx" : 94, "tx" : 89, "enable" : 1, "locked" : 0},
    {"config" : "IM1_1.json", "rx" :128, "tx" :129, "enable" : 1, "locked" : 0},
    {"config" : "IM1_2.json", "rx" :130, "tx" :129, "enable" : 1, "locked" : 0},
    {"config" : "IM2_1.json", "rx" :132, "tx" :133, "enable" : 1, "locked" : 0},
    {"config" : "IM2_2.json", "rx" :134, "tx" :133, "enable" : 1, "locked" : 0},
    {"config" : "IM3_1.json", "rx" :136, "tx" :137, "enable" : 1, "locked" : 0},
    {"config" : "IM3_2.json", "rx" :138, "tx" :137, "enable" : 1, "locked" : 0},
    {"config" : "IM4_1.json", "rx" :140, "tx" :141, "enable" : 1, "locked" : 0},
    {"config" : "IM4_2.json", "rx" :142, "tx" :141, "enable" : 1, "locked" : 0},
    {"config" : "IM5_1.json", "rx" :144, "tx" :145, "enable" : 1, "locked" : 0},
    {"config" : "IM5_2.json", "rx" :146, "tx" :145, "enable" : 1, "locked" : 0},
    {"config" : "IM6_1.json", "rx" :148, "tx" :149, "enable" : 1, "locked" : 0},
    {"config" : "IM6_2.json", "rx" :150, "tx" :149, "enable" : 1, "locked" : 0}
</pre>

<h2 id="test_sequence" class="SUBTITLE">Test sequence</h2>

  <ol>
    <li>Open elinkconfig. </li>
    <li>Select the group for the corresponding module<br/>
      <pre>
       BM1: Link 0, Group 0
       BM2: Link 0, Group 1
       BM3: Link 0, Group 2
       BM4: Link 1, Group 0
       BM5: Link 1, Group 1
       BM6: Link 1, Group 2
       BM7: Link 1, Group 3
     </pre>
   </li>
   <li>
     Enable 1 TX 8-bit FEI4 (bottom), enable 4 RX 4-bit 8b10b<br/>
     <img src="images/elinkconfig-sr1.png" class="MEDIUMIMAGE"/>
   </li>
   <li>Start felixcore (-t1)</li>
   <li>Perform a scan from the following sequence from fei4ScanGUI.py<br/>
     <pre>
     Digital scan, reset mask, no retune
     Analog scan, last mask, no retune
     Global threshold tune, last mask, no retune
     Pixel threshold tune, last mask, no retune
     Global threshold tune, last mask, retune
     Global preamplifier tune, last mask, no retune
     Pixel preamplifier tune, last mask, no retune
     Global preamplifier tune, last mask, retune
     Threshold scan, last mask, no retune
     Tot scan, last mask, no retune
     </pre>
     <img src="images/fei4ScanGUI.png" class="MEDIUMIMAGE"/>
   </li>
   <li>Stop felixcore (ctrl+c)</li>
   <li>Continue with next scan. Go back to point 4 until all scans are done.</li>
   <li>Continue with next module. Go back to point 1 until all modules are done.</li>
  </ol>
  
<h2 id="ttc_crate" class="SUBTITLE">TTC Crate Setup</h2>

<ol>
 <li>Log into SBC using credentials provided for TDAQ TestBed. Hostname: sbccsc-tcc-sr1-01. Then use following commands:
<li>Execute the below commands in full:
	<pre> source /afs/cern.ch/atlas/project/tdaq/cmake/cmake_tdaq/bin/cm_setup.sh prod</pre>
	<pre> menuRCDTtcvi</pre>
Will be asked for base address for which provide: ff2000
<li>go to L1A menu (4)
<li>go to (3) set L1A random generator frequency
<li>set Hertz: option 1
<li>how many times: 1000000
<li>source code for this is on gitlab, to see frequency values not explicitly mentioned in shell.
</ol>

<h2 id="checking_regmap_version" class="SUBSUBTITLE">Checking the Regmap version</h2>

Check the value of the Regmap in the running driver

<pre>
service drivers_flx status
</pre>

should return 

<pre>
Reg Map Version           : 4.1
</pre>

<h2 id="enable_netio_functionalities" class="SUBSUBTITLE">Enable/Disable Netio plugin functionalities</h2>

NetioTxCore offers the posibility of doing stretching, byte flipping and manchester encoding in software.
All paramters are configurable through the hardware json configuration file.

<pre>
"TxCore":{"library":"NetioHW","class":"NetioTxCore",
          "NetIO":{"host":"lhepat25-atlas-felix",
                   "txport":12340,
                   "manchester":false,
                   "extend":true,
                   "flip":true}
                   }
         }
</pre>


<p class="SUBTITLE">Documentation and links</p>
<p>
  <ul>
    <li><a href="https://twiki.cern.ch/twiki/bin/view/Atlas/ITkPixelSR1SystemSetup">ITK Pixel SR1 system tests twiki</a></li>
  </ul>
</p>

</div><!-- Content -->

<?php
	show_footer();
?>
</div>

</body>
</html>
