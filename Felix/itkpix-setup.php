<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link href="/css/nicetable.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/JS/toc.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<title>FELIX setup for ITKPix</title>
</head>
<body>


<?
//FELIX config
$HOME="/home/itkpix";
$FELIXSW=$HOME."/felix-sw/";
$FELIXDR=$HOME."/felix-ke/";
$FELIXFW=$HOME."/felix-fw/";
$ITKSW=$HOME."/itk-felix-sw";
$TDAQ_VERSION="11.2.1";
$FLX_FW_VERSION="FLX712_PIXEL_24CH_CLKSELECT_GIT_phase2-release-5.0_rm-5.0_3465_231014_0109";
$FLX_SW_VERSION="4.3.0 RM5";
$FLX_DR_VERSION="4.15";
$FLX_DR_FILE="tdaq_sw_for_Flx-4.15.0-2dkms.noarch.rpm";
$FLX_SW_FILE="felix-05-00-03-rm5-stand-alone.tar.gz";
$FLX_FW_FILE="FLX712_PIXEL_24CH_CLKSELECT_GIT_phase2-release-5.0_rm-5.0_3465_231014_0109.mcs";
$FLX_SW_PATH="felix-05-00-03-rm5-stand-alone";
$BINARY_TAG="x86_64-el9-gcc13-opt";
$ITK_FLX_SW_VERSION="master";
$SCRIPTS_REPO="ssh://git@gitlab.cern.ch:7999/itk-felix-sw/itk-pix-felix-scripts.git";
$SCRIPTS_DIR="itk-pix-felix-scripts";
?>



<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<h1 class="TITLE">FELIX setup for ITKPix: For Alma9</h1>

<div class="CONTENT">
  
<a href="/Felix">Back</a>

<script>
$(document).ready(loadToc);
</script>



<h2 id="Components" class="SUBTITLE">Components</h2>

<table>
  <tr><td><b>Component</b></td><td><b>Procurement</b></td><td><b>Picture</b></td></tr>
  <tr><td>FELIX Phase-I server</td><td>Contact ITK Common Electronics coordinators</td><td><img class="TINYIMAGE ZOOM" src="images/felix-phase-1-server.png"/></td></tr>
  <tr><td>FELIX Phase-I FLX-712</td><td>Contact ITK Common Electronics coordinators</td><td><img class="TINYIMAGE ZOOM" src="images/felix-phase-1-card.png"/></td></tr>
  <tr><td>VLDB+ (LPGBT carrier board)</td><td>Contact CERN EP-ESE team <a href="https://vldbplus.web.cern.ch/">here</a></td><td><img class="TINYIMAGE ZOOM" src="images/vldbplus.png"></td></tr>
  <tr><td>Goettingen Break-Out Board</td><td>Contact CERN EP-ADE team <a href="https://edms.cern.ch/document/2937397/">EDMS</a></td><td><img class="TINYIMAGE ZOOM" src="images/bob.png"/></td></tr>
  <tr><td>TAG programmer</td><td>RS 134-6435</td><td><img class="TINYIMAGE ZOOM" src="images/jtag-programmer.png"/></td></tr>
  <tr><td>LC-LC adapter</td><td>FS Article 48497 Model HD-LCU-DX-SMF</td><td><img class="TINYIMAGE ZOOM" src="images/FS-48497-LC-LC.jpg"/></td></tr>
  <tr><td>MPO12-12LC</td><td>FS Article 31098 Model 12FMTPLCOM3 (Please note it has to be Male, length is not relevant)</td><td><img class="TINYIMAGE ZOOM" src="images/FS-31098-MPO12-12LC.jpg"/></td></tr>
  <tr><td>MPO24-24LC</td><td>FS Article 31184 Model 24FMPOLCOM3</td><td><img class="TINYIMAGE ZOOM" src="images/FS-31184-MPO24-24LC.jpg"/></td></tr>
  <tr><td>FMC to SMA</td><td>BCMp interface board <a href="/PCBs/BCMp-interface-V1.php">Link</a></td><td><img class="TINYIMAGE ZOOM" src="images/BCMp-interface-V1.png"></td></tr>
  <tr><td>SMA to DP</td><td>Contact CERN EP-ADE team <a href="https://edms.cern.ch/document/2937478">LINK</a></td><td><img class="TINYIMAGE ZOOM" src="images/DP_to_SMA_Bonn_front.jpg"></td></tr>
  <tr><td>OptoBoard</td><td>BERN Group </td><td><img class="TINYIMAGE ZOOM" src="images/OptoBoard.jpg"></td></tr>
  <tr><td>OptoBoard BreakOutBoard</td><td> Designed by BERN Group, extras exists at CERN Group </td><td><img class="TINYIMAGE ZOOM" src="images/CamieBoB.jpg"></td></tr>
  <tr><td>Zaza Board</td><td> ?? </td><td><img class="TINYIMAGE ZOOM" src="images/Zaza.png"></td></tr>
</table>



<h2 id="install-hardware" class="SUBTITLE" >Setting Up the Hardware</h2> 
<h3> Setting up hardware and fiber connections </h3>
<p>
The basic configuration of FELIX HW can be viewed as below. 
<div>
  <img class="LARGEIMAGE" src="images/HW/Overview.png"/>
</div>
</p>
<p>

<p>
Attach the FELIX card into the FELIX PC in the correct PCI slot, and attach the power. You can use the following diagram as a basis. 
You can attach an optional JTAG cable if wanted, but this is not necessary. 
<div>
  <img class="LARGEIMAGE" src="images/HW/Instalation.png"/>
</div>
</p>

<p>
The FELIX fiber needs to be matched properly with the OptoBoard. On the FELIX card, the first 24 channel MPO should be connected to the inner MPO connector as shown in the picture below. 
<div>
  <img class="LARGEIMAGE" src="images/HW/FiberConnect.png"/>
</div>
</p>

 If a single OptoBoard is used the following fibering scheme can be used.
<div>
  <img class="LARGEIMAGE" src="images/HW/BasicFiber.png"/>
</div>

<p>
If you are using the compilcated FIBER setup that is degined for the OptoBox pre built, you just need to connected the 24 MPO connector with the pre-mapped connector. Further details on this connector and mapping can be found in this document: <a href=docs/Testing_procedure_for_fiber_assembly.pdf > Document </a>

</p>
<h3> Setting up Electrical Links </h3>
<p> Depending on the optical converter you use, you will have a different e-link setup. </p>
<ol>
<li> Zaza board is going to be standard board used that will be used by the optoBoard tests, it connects the optoBoard into six different display ports. Each display port contains four electrical links and one command link that is comming from a module. Each of these electrical links (elinks) are mapped to a different e-link/optical fiber. The full mapping can be found in the following table, mind you these links also come with different polarities, and when running scans proper polarities would need to be set. 

<div>
  <img class="LARGEIMAGE" src="images/HW/ZazaMapping.png"/>
</div>

</li>
<li> When using OptoBoard BreakoutBoard (BoB), Here the mapping depends on which DP port is connected to which SMA.
The labels on the SMA on the BOB board are the correcsponding e-links of the OptoBoard. The only important note here is, the DP cable inverts the order of cables. For this reason BoB SMA's channel 0 should be mapped to 3rd channel of DP to SMA and the remaning should go as 0->3,1->2,2->1,3->0. A built example of this readout structure can be seen below. 

<div>
  <img class="LARGEIMAGE" src="images/HW/OptoBoB.png"/>
</div>



</li>


<li> When using VLDB+, currently the preffered readout BoB is the FMC to SMA board, which in conjection should be used to SMA to DP board.  Here the mapping depends on which DP port is connected to which SMA.
The labels on the FMC to SMA board are the correcsponding e-links of the VLDB+. The only important note here is, the DP cable inverts the order of cables. For this reason FMC to SMA's SMA channel 0 should be mapped to 3rd channel of DP to SMA and the remaning should go as 0->3,1->2,2->1,3->0. A built example of this readout structure can be seen below. 

<div>
  <img class="LARGEIMAGE" src="images/HW/VLDBBoB.png"/>
</div>



</li>




</ol>






<h2 id="install-alma9" class="SUBTITLE" >Installing Alma9</h2>
<ol>
	<li> The instructions on how to install Alma9 can be found here: 	< a href="https://linux.web.cern.ch/almalinux/alma9/install/"> Link </a>  </li>
	<li> It's highly recommended to enable CMVFS during the installation of Alma9 which can be found in the instructions. 
</ol>

<h2 id="install-felix-dr" class="SUBTITLE">Install FELIX driver</h2>
The first thing to do on a FELIX machine is to install the FELIX driver. 
<ol>
  <li>Make the felix driver directory
    <pre>mkdir <?=$FELIXDR;?></pre>
  </li>
  <li>Change to that directory
    <pre>cd <?=$FELIXDR;?></pre>
  </li>
  <li>Download felix driver
    <pre>wget https://ade-pixel-group.web.cern.ch/Felix/felix-ke/Alma9/<?=$FLX_DR_FILE;?></pre>
  </li>
  <li>Install the felix driver
    <pre>sudo yum dkms</pre>
    <pre>sudo yum install <?=$FLX_DR_FILE;?></pre>
  </li>
  <li>Start the felix driver 
    <pre>systemctl start drivers_flx</pre>
  </li>
  <li> It's recommended to reboot the PC here as sometimes the FELIX drivers don't start properly </li>
</ol>

<h2 id="install-felix-sw" class="SUBTITLE" >Install FELIX SW</h2>
There are multiple methods to install FELIX SW. The first method is to use a pre-compiled FELIX SW, alternatively you can use DeMi dockers or compile FELIX SW from scratch.
As FELIX SW and FW constantly changes, the FW and SW versions needs to be matching. For this reason we recommend installing the pre-compiled FELIX SW that matches the FELIX FW you want to use or the right docker version. As CERN group we package each FELIX FW release with a matching FELIX SW which can be found in the same directory. Unless you have specific needs you can use the defualt firmware and software version in this manual. 

<h3>Pre-Compiled FELIX SW</h3>
<ol>
  <li>Make the felix-sw directory
    <pre>mkdir <?=$FELIXSW;?></pre>
  </li>
  <li>Change to that directory
    <pre>cd <?=$FELIXSW;?></pre>
  </li>
  <li>Download felix driver
    <pre>wget https://ade-pixel-group.web.cern.ch/Felix/felix-sw/Alma9/<?=$FLX_SW_FILE;?></pre>
  </li>
  <li>Uncompress the felix-sw
    <pre>tar xzvf <?=$FLX_SW_FILE;?></pre>
  </li>
  <li> Every time you want to use the felix-sw, you would need the source the directory
  	   <pre>cd <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?> </pre>
	   <pre>source setup.sh </pre>
  </li>
</ol>
<h3>Using DeMi Docker</h3>
<ol>
<li>
</li>
</ol>
<h3>Compiling FELIX SW from scratch</h3>
<ol>
<li>
To compile felix-sw from scratch please follow the instructions in the following readme:
<a href=https://gitlab.cern.ch/atlas-tdaq-felix/felix-distribution/-/blob/master/README.md?ref_type=heads> Link </a>
</li>
</ol>



<h2 id="install-felix-sw" class="SUBTITLE">Install FELIX FW</h2>

<ol>
  <li>Make the felix firmware directory
    <pre>mkdir <?=$FELIXFW;?></pre>
  </li>
  <li>Change to that directory
    <pre>cd <?=$FELIXFW;?></pre>
  </li>
  <li>Download felix firmware
    <pre>wget https://ade-pixel-group.web.cern.ch/Felix/firmware-itkpix/<?=$FLX_FW_VERSION;?>/<?=$FLX_FW_FILE;?></pre>
  </li>
  <li>Install the felix firmware
    <ol>
      <li><pre>source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh</pre></li>
      <li><pre>fflashprog -f0 <?=$FLX_FW_FILE;?> prog</pre></li>
      <li><pre>fflashprog -f1 <?=$FLX_FW_FILE;?> prog</pre></li>
      <li><pre>fflashprog -f2 <?=$FLX_FW_FILE;?> prog</pre></li>
      <li><pre>fflashprog -f3 <?=$FLX_FW_FILE;?> prog</pre></li>
    </ol>
  </li>
  <li>shutdown the system, after the shutdown please cut the power from the PC. The power cut is necessary for FELIX FW to be loaded.
    <pre>sudo shutdown</pre>
  </li>
  <li> Cut the power from the FELIX PC, wait few seconds and boot the pc. </li>

  <li>Check the firmware version
    <ol>
      <li><pre>source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh</pre></li>
      <li><pre>flx-info</pre></li>
    </ol>
  </li>
</ol>

<h2 id="download-scripts" class="SUBTITLE"> Downloading Usefull Scripts</h2>
<ol>
<li>We have a set of scripts that are used for configuring felix setups.
	<pre> cd <?=$FELIXSW;?> </pre> 
	<pre> git clone <?=$SCRIPTS_REPO;?> </pre>
</ol>


<h2 id="init-felix" class="SUBTITLE">Initialize the FELIX card</h2>
These instructions needs to be done after every-reboot to activate the FELIX card. 
<ol>
  <li>Initialize the FELIX card  
    <pre> source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh  </pre>
    <pre> flx-init -c X  </pre>
    
  </li>
  <li> Now we need to enable/disable individual FELIX elinks that are going to be used. There are multiple methods to do this. The default way is as follows.
    <pre> source <?=$FELIXSW;?>/<?=$SCRIPTS_DIR;?>/configure_encode_decode.sh 0 </pre>
    <pre> source <?=$FELIXSW;?>/<?=$SCRIPTS_DIR;?>/configure_encode_decode.sh 1 </pre>
  </li>
</ol>

<h2 id="config-opto" class="SUBTITLE">Configure the OptoBoard (If using OptoBoard)</h2>
<ol>
	<li> In order to configure the OptoBoard you need the following software from the BERN group. 
	<a href=https://gitlab.cern.ch/bat/optoboard_felix> gitlab </a>
	<pre>cd <?=$FELIXSW;?>; </pre>
	<pre>pip install optoboard-felix --index-url https://gitlab.cern.ch/api/v4/projects/113584/packages/pypi/simple </pre>
	</li>
	<li> To configure optoBoard
	
	<pre> source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh </pre> 
	<pre> InitOpto -optoboard_serial [OptoBoard Serial] -vtrx_v [VTRX_VERION] -flx_G [Fiber link number] -flx_d [FELIX_DEVICE ID] -i -configure 1 </pre>
	Default Example: <pre> InitOpto -optoboard_serial 30000000 -vtrx_v 1.3 -flx_G 0 -flx_d 0 -i -configure 1 </pre> 
	</li>
	<li> Further details can be found here: <a href=https://gitlab.cern.ch/bat/optoboard_felix> gitlab </a> </li> 

</ol>

<h2 id="config-vldb" class="SUBTITLE">Configure the VLDB+ (If using VLDB+)</h2>

<div>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-1.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-2.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-3.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-4.png"/>
</div>
<div>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-5.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-6.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-7.png"/>
  <img class="SMALLIMAGE ZOOM" src="images/rd53a-pigbt-8.png"/>
</div>

<ol>
  <li> Open webpage for pigbt </li>
  <li> Select platform: Real LPGBT </li>
  <li> Select hardware: VLDB+ </li>
  <li> Select the address in the list </li>
  <li> Click on OK </li>
  <li> Do you want to initialize the LPGBT: yes </li>
  <li> LpGBT mode: TRX </li>
  <li> TX data rate: 10 Gbps </li>
  <li> TX encoding: FEC5 </li>
  <li> Uplink EPRX (Click gear symbol) </li> 
  <li> Data Rate: 1280 </li>
  <li> Track Mode: Continues </li>
  <li> Control: TERM </li>
  <li> Warning!!: Depending on the polirity of the elinks, you can swap the polarity of the links by selecting INV option here </li>
  <li> Equalization: OFF </li>
  <li> Downlink EPTX </li>
  <li> Data Rate 160 Mpbs </li>
  <li> Drive strength 4.0 mA </li>
  <li> click on the menu -> high speed </li>
  <li> Invert high speed data output enabled (Invert polarity uplinks)
</ol>


<h2 id="checking-comms" class="SUBTITLE"> Checking Communication</h2>

<ol>
	<li> If everything is setup correctly, at this step you should be be to check communications with your FE> 
	<ol>
	<li> Power on your FE </li>
	<li> <pre> source <?=$FELIXSW;?>/<?=$FLX_SW_PATH;?>/<?=$BINARY_TAG;?>/setup.sh </pre> </li>
	<li> <pre> flx-config list | grep ALIGNED_00 </pre> </li> 
	</ol>
	<li> Here you should see a string like this <pre> DECODING_LINK_ALIGNED_00  0x000000030001010 </pre></li>

	<li> Here the "1"'s represents individual elinks that are aligned from the FE, if you only see "0x000000030000000", it means something failed in your setup.
	

</ol>



<h2 id="next-step" class="SUBTITLE">Next steps</h2>
Congratulations. You are ready to start doing scans, or building more complicated setups.  you can checkout the following links on how to install different software for running scans.
<ol>

<li><a href=itk-felix-sw/html/> ITk-FELIX-SW </a> (Responsible: Carlos Solans)</li>
<li><a href=https://demi.docs.cern.ch/itk-demo-felix/> Docker DeMi Container Setup </a> (Responsible: Gerhard Brandt) </li>
<li><a href=https://demi.docs.cern.ch/containers/docker/> Building your own DeMi images</a> (Responsible: Gerhard Brandt) </li>
<li><a href=https://yarr.web.cern.ch/yarr/> YARR</a> (Responsible: Timon Heim)</li>
<li><a href=https://orion.docs.cern.ch/> Orion [ITk-DAQ-SW] </a> (Responsible: Vakhtang Tsiskaridze)  </li>

</ol>


</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
