<?
/* Date: November 2016 */

include_once("helper.php");

class Material extends Collectible{

  function __construct(){
    $this->members = array("reference"=>"",
                          "name"=>"",
                          "responsible"=>"",
                          "startdate"=>"",
                          "enddate"=>"",
                          "location"=>"");
    $this->classname = "Material";
    $this->tag = "material";
    $this->id = "reference";
  }
  
}

?>