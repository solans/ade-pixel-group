<?
interface CollectibleInterface {
    public function GetId();
    public function GetTag();
    public function Dump();
    public function ToXml($ident);
    public function FromArray($arr);
    public function GetClassName();
}

class Collectible implements CollectibleInterface{
  public $members;
  public $classname;
  public $tag;
  public $id;
  
  //Constructor: should be overloaded
  function __construct(){}
  
  //GetId
  function GetId(){
    return $this->members[$this->id];
  }
  
  //GetTag
  function GetTag(){
    return $this->tag;
  }
  
  //Get class name
  function GetClassName(){
    return $this->classname;
  }
  
  //Get data
  function GetData(){
    return $this->members;
  }
  
  //Get member
  function GetMember($key){
    return $this->members[$key];
  }
  
  //Dump
  function Dump(){
    foreach($this->members as $v){
      echo $v."&nbsp;";
    }
  }
  
  //Dump to XML
  function ToXml($ident=0){
    $s = str_repeat(" ",$ident);
    $xml = $s."<".$this->tag.">\n";
    foreach($this->members as $k=>$v){
      $xml .= $s.$s."<".$k.">".htmlentities($v)."</".$k.">\n";
    }
    $xml .= $s."</".$this->tag.">\n";
    return $xml;
  }
  
  //From array
  function FromArray($arr){
    //print_r($arr);
    foreach($arr as $k=>$v){
      $k2=strtolower($k);
      foreach($this->members as $mk=>$mv){
        if($k2==$mk){$this->members[$mk]=html_entity_decode($v);} 
      }
    }
  }
}

class Collection{

  //internal data
  var $data;
  
  //Constructor
  function __construct(){
    $this->data = array();
  }
  
  //dump function
  function Dump(){
    ?>
    <ul>
    <?
    foreach($this->data as $c){
      ?>
      <li><?=$c->Dump();?></li>
      <?
    }
    ?>
    </ul>
    <?
  }
  
  //helper
  function Add($p){
    $this->data[]=$p;
  }
  
  //Get a given publications
  function Get($id){
    foreach($this->data as $e){
      if($e->GetId() == $id){
        return $e;
      }
    }
    return NULL;
  }
  
  //Remove a given publications
  function Del($id){
    for($i=0;$i<count($this->data);$i++){
      if($this->data[$i]->GetId()==$id){
        unset($this->data[$i]);
      }
    }
  }
  
  //Dump to XML
  function ToXml(){
    $nn = "\n";
    $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>'.$nn;
    $xml .= '<collection>'.$nn;
    foreach($this->data as $e){
      $xml .= $e->ToXml(1);
    }
    $xml .= '</collection>'.$nn;
    return $xml;
  }
  
}

class Helper{
  
  //data
  var $collection;
  var $element;
  
  //internal
  var $verbose;
  var $types;
    
  //Constructor
  function __construct(){
    $this->tag = "";
    $this->collection = new Collection();
    $this->types = array();
  }
  
  //Register collectible
  function addType($type){
    $this->types[]=$type;
  }
  
  //helper function
  function GetData(){
    return $this->collection;
  }
  
  //Open tag
  function startElement($parser, $name, $attrs){
    if($this->verbose){echo "Open tag: ".$name."<br>\n";}
    $this->tag=$name;
    foreach($this->types as $type){
      if($this->tag==strtoupper($type)){
        $this->element = array();
        break;
      }
    } 
  }

  // Close tag
  function endElement($parser, $name){
    if($this->verbose){echo "Close tag: ".$name."<br>\n";}    
    foreach($this->types as $type){
      if($name==strtoupper($type)){
        $obj = new $type();
        $obj->FromArray($this->element);
        $this->collection->Add($obj);
       break;
      }
    }
    //unset($this->element);
    $this->tag="";
  } 
  
  // Value inside a tag
  function characterData($parser, $data){
    if($this->verbose){echo "Data: ".$data."<br>\n";}
    if(!isset($this->element[$this->tag])){$this->element[$this->tag]="";}
    $this->element[$this->tag].=$data;
  }
}
?>