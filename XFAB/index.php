<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>XFAB</title>
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link href="<?=$gobase;?>css/nicetable.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
  show_login();
?>

<div>
  <img width="30%" src="<?=$gobase;?>/XFAB/images/XTB01.png">
  <img width="40%" src="<?=$gobase;?>/XFAB/images/XTB01_cross.png">  
</div>

<p class="TITLE">XFAB Trench SOI 180 nm CMOS low power
	<ul>
		<li>Electronics isolated from substrate by BOX </li>
    <li>Small charge collecting electrode without competing wells</li>
    <li>Double wells in transistor body with particular geometry: No back-gate effect and high radiation tolerance in full CMOS logic</li>
    <li>Full depletion possible</li>
    <li>Three transistor cell readout</li>
	</ul>
</p>

<p class="SUBTITLE">Tests performed
  <ul>
    <li>Radiation hardness measurements</li>
    <li>Charge collection properties
    <ul>
      <li>MPV of collected charge at different bias voltages with irradiated and non irradiated samples</li>
      <li>Depletion depth vs bias voltage for un-irradiated sample</li>
    </ul>
    </li>
    <li>Edge transient current technique measurements</li>
    <li>Testbeam measurements
    <ul>
      <li>Hit efficiency vs bias voltage</li>
    </ul>
    </li>
  </ul> 
</p>

<p class="SUBTITLE">Test chips</p>

<?php if(isAuthorised()){ ?>

<ul>
  <li><a href="<?=$gobase;?>XFAB/login/XT02-3.pdf">S. Fernandez - XTB02 documentation</a></li>
  <li><a href="<?=$gobase;?>XFAB/login/XTB02-documentation_new.pdf">S. Fernandez - XTB02 documentation new</a></li>
  <li><a href="<?=$gobase;?>XFAB/login/220615_report_eTCT-TCT.pdf">S. Fernandez - E-TCT measurements 22nd June 2015</a></li>
</ul>
  
<p class="SUBTITLE">Samples</p>
<div class="nicetable">
<table>
  <thead>
    <td>Sample</td><td>Identifier</td><td>Fluence [n<sub>eq</sub> cm<sup>2</sup>]</td>
  </thead>
  <tr><td>XTB01</td><td>0B</td><td>unirradiated</td></tr>
  <tr><td>XTB01</td><td>2B</td><td>1E13</td></tr>
  <tr><td>XTB01</td><td>3B</td><td>5E13</td></tr>
  <tr><td>XTB01</td><td>5B</td><td>1E14</td></tr>
  <tr><td>XTB01</td><td>4B</td><td>5E14</td></tr>
  <tr><td>XTB01</td><td>1A</td><td>unirradiated</td></tr>
  <tr><td>XTB02</td><td>2A</td><td>2E14</td></tr>
</table>
</div>

<?php }else{ ?>

  Please <a href="login/">login</a>.

<?php } ?>

<p class="SUBTITLE">Documentation and links
  <ul>
    <li><a href="http://arxiv.org/pdf/1412.3973.pdf">10.1016/j.nima.2015.02.052</a></li>
    <li><a href="https://indico.cern.ch/event/357738/session/9/contribution/3/attachments/1162396/1674208/TWEEP2015_SFernandez.pdf">Sonia Fernandez Perez, TWEPP 2015</a></li>
    <li><a href="https://indico.cern.ch/event/461323/contributions/1134874/attachments/1187885/1723097/XFAB-AUW-Nov2015.pdf">Sonia Fernandez Perez, XFAB AUW November 2015</a></li>
    <li><a href="http://cds.cern.ch/record/2155750">Sonia Fernandez Perez, Thesis 2016</a></li>
  </ul>
</p>
 
</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
