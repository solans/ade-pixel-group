<?php
//echo "hello";
include_once("includes.php");

open_db();

$d=$_GET;

if($d["cmd"]=="get_hosts"){
  $sql ="SELECT * FROM hosts ORDER BY host_sn;";
}
else if($d["cmd"]=="get_host"){
  $sql ="SELECT * FROM hosts WHERE host_id='".$d["host_id"]."';";
}
else if($d["cmd"]=="get_host_locations"){
  $sql ="SELECT host_locations.*, hosts.host_name, hosts.host_sn FROM host_locations INNER JOIN hosts ON host_locations.host_id=hosts.host_id;";
}
else if($d["cmd"]=="get_locations_of_host"){
  $sql ="SELECT * FROM host_locations WHERE host_id='".$d["host_id"]."';";
}
else if($d["cmd"]=="get_host_location"){
  $sql ="SELECT host_locations.*, hosts.host_name, hosts.host_sn ";
  $sql.="FROM host_locations INNER JOIN hosts ON host_locations.host_id=hosts.host_id ";
  $sql.="WHERE host_location_id='".$d["host_location_id"]."';";
}
else if($d["cmd"]=="get_host_cards"){
  $sql ="SELECT host_cards.*, hosts.host_name, hosts.host_sn, cards.card_id, cards.card_sn ";
  $sql.="FROM host_cards ";
  $sql.="INNER JOIN hosts ON host_cards.host_id=hosts.host_id ";
  $sql.="INNER JOIN cards ON host_cards.card_id=cards.card_id;";
}
else if($d["cmd"]=="get_hosts_of_card"){
  $sql ="SELECT ";
  $sql.="host_cards.host_card_id, host_cards.host_id, host_cards.card_id, host_cards.start_date, host_cards.end_date, ";
  $sql.="hosts.host_name, hosts.host_sn, hosts.model, ";
  $sql.="cards.card_id, cards.card_sn ";
  $sql.="FROM host_cards ";
  $sql.="INNER JOIN hosts ON hosts.host_id=host_cards.host_id ";
  $sql.="INNER JOIN cards ON host_cards.card_id=cards.card_id ";
  $sql.="WHERE host_cards.card_id='".$d["card_id"]."' ;";
}
else if($d["cmd"]=="get_host_card"){
  $sql ="SELECT host_cards.*, hosts.host_name, hosts.host_sn, cards.card_id, cards.card_sn ";
  $sql.="FROM host_cards ";
  $sql.="INNER JOIN hosts ON host_cards.host_id=hosts.host_id ";
  $sql.="INNER JOIN cards ON host_cards.card_id=cards.card_id ";
  $sql.="WHERE host_card_id='".$d["host_card_id"]."';";
}
else if($d["cmd"]=="get_hosts_with_last_location"){
  $sql ="SELECT hosts.host_id, hosts.host_name, hosts.host_sn, hosts.model, tt.* FROM hosts LEFT JOIN (";
  $sql.=" SELECT host_locations.host_id, host_locations.date, host_locations.location, host_locations.system, host_locations.edh FROM host_locations INNER JOIN (";
  $sql.="  SELECT host_id, max(date) AS date FROM host_locations GROUP BY host_id";
  $sql.=" ) AS aux ON aux.host_id=host_locations.host_id AND aux.date=host_locations.date";
  $sql.=") as tt ON tt.host_id=hosts.host_id;";
}
else if($d["cmd"]=="get_cards"){
  $sql ="SELECT * FROM cards ORDER BY card_id;";
}
else if($d["cmd"]=="get_card"){
  $sql ="SELECT * FROM cards WHERE card_id='".$d["card_id"]."';";
}
else if($d["cmd"]=="get_cards_hosts"){
  $sql ="SELECT ";
  $sql.="host_cards.host_card_id, host_cards.host_id, host_cards.card_id, host_cards.start_date, host_cards.end_date, ";
  $sql.="hosts.host_name, hosts.host_sn ";
  $sql.="FROM host_cards ";
  $sql.="INNER JOIN hosts ON hosts.host_id=host_cards.host_id WHERE host_cards.card_id='".$d["card_id"]."' ;";
}
else if($d["cmd"]=="get_card_locations"){
  $sql ="SELECT * FROM card_locations;";
}
else if($d["cmd"]=="get_locations_of_card"){
  $sql ="SELECT * FROM card_locations WHERE card_id='".$d["card_id"]."';";
}
else if($d["cmd"]=="get_card_location"){
  $sql ="SELECT * FROM card_locations WHERE card_location_id='".$d["card_location_id"]."';";
}
else if($d["cmd"]=="get_cards_with_last_location"){
  $sql ="SELECT * ";
  $sql.="FROM cards "; 
  $sql.="LEFT JOIN ( ";
  $sql.="SELECT card_locations.card_id AS cid, card_locations.date, card_locations.location, card_locations.system ";
  $sql.="FROM card_locations ";
  $sql.="INNER JOIN (SELECT card_id, max(date) AS date FROM card_locations GROUP BY card_id) AS aux ON aux.card_id=card_locations.card_id AND aux.date=card_locations.date ";
  $sql.=") as tt ON tt.cid = cards.card_id;";
}
else if($d["cmd"]=="get_cards_of_host"){
  $sql ="SELECT ";
  $sql.="host_cards.host_card_id, host_cards.host_id, host_cards.card_id, host_cards.start_date, host_cards.end_date, ";
  $sql.="hosts.host_name, hosts.host_sn, ";
  $sql.="cards.card_id, cards.card_sn ";
  $sql.="FROM host_cards ";
  $sql.="INNER JOIN hosts ON hosts.host_id=host_cards.host_id ";
  $sql.="INNER JOIN cards ON host_cards.card_id=cards.card_id ";
  $sql.="WHERE host_cards.host_id='".$d["host_id"]."' ;";
}
  
$ret = array();
//echo $sql;
$result=$conn->query($sql);
if($result){
  while($row = $result->fetch_assoc()) {
    $row2=array();
    foreach($row as $k=>$v){
      $row2[$k]=mb_convert_encoding($v,"UTF-8","ISO-8859-1");
    }
    $ret[]=$row2;
  }
}else{
  $ret[]=$sql;
}
//echo "close";
close_db();

echo json_encode($ret);
?>