<?php
include_once('includes.php');

open_db();

$d=$_GET;

if($d["cmd"]=="add_host"){
  $sql ="INSERT INTO hosts (";
  $sql.="`host_name`,";
  $sql.="`host_sn`,";
  $sql.="`model`,";
  $sql.="`mb`,";
  $sql.="`cpu`,";
  $sql.="`ram`,";
  $sql.="`disk`,";
  $sql.="`chassis`,";
  $sql.="`mac0`,";
  $sql.="`mac1`,";
  $sql.="`edh`";
  $sql.=") VALUES ( ";
  $sql.="'".$d["host_name"]."',"; 
  $sql.="'".$d["host_sn"]."', ";
  $sql.="'".$d["model"]."', ";
  $sql.="'".$d["mb"]."', ";
  $sql.="'".$d["cpu"]."', ";
  $sql.="'".$d["ram"]."', ";
  $sql.="'".$d["disk"]."', ";
  $sql.="'".$d["chassis"]."', ";
  $sql.="'".$d["mac0"]."', ";
  $sql.="'".$d["mac1"]."', ";
  $sql.="'".$d["edh"]."' ";
  $sql.=");";
}else if($d["cmd"]=="update_host"){
  $sql ="UPDATE hosts SET ";
  $sql.="host_name='".$d["host_name"]."', ";
  $sql.="host_sn='".$d["host_sn"]."', ";
  $sql.="mac0='".$d["mac0"]."', ";
  $sql.="mac1='".$d["mac1"]."', ";
  $sql.="mell_sn='".$d["mell_sn"]."', ";
  $sql.="mell_mac='".$d["mell_mac"]."', ";
  $sql.="edh='".$d["edh"]."' ";
  $sql.="WHERE host_id='".$d["host_id"]."';";
}else if($d["cmd"]=="add_host_location"){
  $sql ="INSERT INTO host_locations ( ";
  $sql.="`host_id`, ";
  $sql.="`date`, ";
  $sql.="`location`, ";
  $sql.="`system`, ";
  $sql.="`responsible`, ";
  $sql.="`edh`";
  $sql.=") VALUES ( ";
  $sql.="'".$d["host_id"]."', "; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["location"]."', ";
  $sql.="'".$d["system"]."', ";
  $sql.="'".$d["responsible"]."', ";
  $sql.="'".$d["edh"]."' ";
  $sql.=");";
}else if($d["cmd"]=="update_host_location"){
  $sql ="UPDATE host_locations SET ";
  $sql.="`host_id`='".$d["host_id"]."',"; 
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`location`='".$d["location"]."', ";
  $sql.="`system`='".$d["system"]."', ";
  $sql.="`responsible`='".$d["responsible"]."', ";
  $sql.="`edh`='".$d["edh"]."' ";
  $sql.="WHERE host_location_id='".$d["host_location_id"]."';";  
}else if($d["cmd"]=="delete_host_location"){
  $sql = "DELETE FROM host_locations WHERE host_location_id='".$d["host_location_id"]."';";
}else if($d["cmd"]=="add_card"){
  $sql ="INSERT INTO cards (";
  $sql.="`card_id`,";
  $sql.="`card_sn`,";
  $sql.="`card_model`";
  $sql.=") VALUES ( ";
  $sql.="'".$d["card_id"]."',"; 
  $sql.="'".$d["card_sn"]."', ";
  $sql.="'".$d["card_model"]."' ";
  $sql.=");";
}else if($d["cmd"]=="update_card"){
  $sql ="UPDATE cards SET ";
  $sql.="`card_sn`='".$d["card_sn"]."',"; 
  $sql.="`card_model`='".$d["card_model"]."' "; 
  $sql.="WHERE card_id='".$d["card_id"]."';";  
}else if($d["cmd"]=="add_card_location"){
  $sql ="INSERT INTO card_locations ("; 
	$sql.="`card_id`, "; 
	$sql.="`date`, "; 
	$sql.="`location`, "; 
	$sql.="`system`, "; 
	$sql.="`responsible`,"; 
	$sql.="`edh`,"; 
	$sql.="`comment`"; 
	$sql.=") VALUES ( ";
  $sql.="'".$d["card_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["location"]."', ";
  $sql.="'".$d["system"]."', ";
  $sql.="'".$d["responsible"]."', ";
  $sql.="'".$d["edh"]."', ";
  $sql.="'".$d["comment"]."' ";
  $sql.=");";
}else if($d["cmd"]=="update_card_location"){
  $sql ="UPDATE card_locations SET ";
  $sql.="`card_id`='".$d["card_id"]."',"; 
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`location`='".$d["location"]."', ";
  $sql.="`system`='".$d["system"]."', ";
  $sql.="`responsible`='".$d["responsible"]."', ";
  $sql.="`edh`='".$d["edh"]."', ";
  $sql.="`comment`='".$d["comment"]."' ";
  $sql.="WHERE card_location_id='".$d["card_location_id"]."';";  
}else if($d["cmd"]=="delete_card_location"){
  $sql = "DELETE FROM card_locations WHERE card_location_id='".$d["card_location_id"]."'";
}else if($d["cmd"]=="add_host_card"){
  $sql ="INSERT INTO host_cards (";
	$sql.="`host_id`,";
	$sql.="`card_id`,";
	$sql.="`start_date`,";
	$sql.="`end_date`";
	$sql.=") VALUES ( ";
  $sql.="'".$d["host_id"]."',"; 
  $sql.="'".$d["card_id"]."',"; 
  $sql.="'".$d["start_date"]."', ";
  $sql.="'".$d["end_date"]."' ";
  $sql.=");";
}else if($d["cmd"]=="update_host_card"){
  $sql ="UPDATE host_cards SET ";
	$sql.="host_id='".$d["host_id"]."', ";
	$sql.="card_id='".$d["card_id"]."', ";
	$sql.="start_date='".$d["start_date"]."', ";
  $sql.="end_date='".$d["end_date"]."' ";
  $sql.="WHERE host_card_id='".$d["host_card_id"]."';";    
}else if($d["cmd"]=="delete_host_card"){
  $sql = "DELETE FROM host_cards WHERE host_card_id='".$d["host_card_id"]."'";
}

//echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
  $ret["last_insert_id"]=$conn->insert_id;
}else{
  $ret["error"]=$conn->error;  
  $ret["sql"]=$sql;
}
//echo "close";

close_db();

echo json_encode($ret);
?>