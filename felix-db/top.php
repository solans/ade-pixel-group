<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<div class="CONTENT">
<?php   
  show_login(true); 
?>

<p class="TITLE">ITK FELIX database</p>

<img class="SMALLIMAGE" src="felix-logo.png">
<img class="SMALLIMAGE" src="itk-logo.png">

<h2>
  <a href="javascript:history.back()">Go Back</a> - 
  <a href="?page=cards">Cards</a> - 
  <a href="?page=card_locations">Card locations</a> -
  <a href="?page=hosts">Hosts</a> -
  <a href="?page=host_locations">Host locations</a> -
  <a href="?page=host_cards">Host cards</a>
</h2>
