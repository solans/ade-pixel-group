<h2>Locations of card <pre id="card_details"></pre></h2>
<button id="card_locations_export">CSV</button>Export <span id="card_locations_counter"></span> 
<button id="card_locations_reset">Reset filters</button>	
<table id="card_locations" class="tablesorter" style="font-size: smaller;width:initial">
  <thead>
    <tr>
		  <th data-placeholder="Search...">Date</th>
		  <th data-placeholder="Search...">Location</th>
		  <th data-placeholder="Search...">System</th>
		  <th data-placeholder="Search...">Responsible</th>
		  <th data-placeholder="Search...">Comment</th>
		  <th data-placeholder="Search...">EDH</th>
		  <th >Actions</th>
	  </tr>
  </thead>
	<tbody id="card_locations_body">
	</tbody>
</table>
<div id="card_locations_reply"></div>

<input type="hidden" id="card_id" value="<?=@$_GET["card_id"];?>">

<script>

$(function() {
  $("#card_locations_body").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_card_locations();
});

$("#card_locations").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra']
}).bind('filterEnd', function() {
  $("#card_locations_counter").html("("+($("#card_locations tr:visible").length-2)+")");
});

/* clear the filters */
$("#card_locations_reset").click(function() {
  $("#card_locations").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

/* declare the export locations */
$("#card_locations_export").click(function() {
  $("#card_locations").trigger('outputTable');
});

/* load the locations */
function load_card_locations(){
  if($("#card_id").val()==""){return;}
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_locations_of_card", card_id:$("#card_id").val()},
    success: function(data) {
      console.log(data);   
      locations=JSON.parse(data);
      $("#card_locations_body").empty();
      for (row of locations){
        tt ="<tr>\n";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+row["system"]+"</td>";
        tt+="<td>"+row["responsible"]+"</td>";
        tt+="<td>"+row["comment"]+"</td>";
        tt+="<td><a href='https://edh.cern.ch/Document/SupplyChain/SHIP/"+row["edh"]+"' target=\"_blank\">"+row["edh"]+"</td>";
        tt+="<td><a href='?page=card_location&card_location_id="+row["card_location_id"]+"'\">edit</a>";
        tt+="&nbsp;<a href=\"#\" title=\"Delete card location\" onclick=\"delete_card_location("+row["card_location_id"]+");\">delete</a></td>";
        tt+="</tr>\n";
        $("#card_locations_body").append(tt);
      }
      $("#card_locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete a location */
function delete_card_location(card_location_id){
  if(!window.confirm("Are you sure to delete card location?")) return false;
  $("#card_locations_reply").text("");
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_card_location",
      card_location_id:card_location_id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==1){
        $("#card_locations_reply").text("Member deleted");
        load_card_locations();
      }else if (reply["affected_rows"]==0){
         $("#card_locations_reply").text("Something went wrong");
      }
    }
  });
  return false;
};

</script>

