<h2>Host</h2>
<form id="host">
  <table class="border">
    <tr><th class='regform-done-caption'>SN</th><td class="regform-done-title"><input id="host_sn" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>Name</th><td class="regform-done-title"><input id="host_name" type="text"></td></tr>
    <tr><th class='regform-done-caption'>Model</th><td class="regform-done-title"><input id="host_model" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>Motherboard</th><td class="regform-done-title"><input id="host_mb" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>CPU</th><td class="regform-done-title"><input id="host_cpu" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>RAM</th><td class="regform-done-title"><input id="host_ram" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>HDD</th><td class="regform-done-title"><input id="host_disk" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>Chassis</th><td class="regform-done-title"><input id="host_chassis" type="text" disabled></td></tr>
    <tr><th class='regform-done-caption'>MAC0</th><td class="regform-done-title"><input id="host_mac0" type="text"></td></tr>
    <tr><th class='regform-done-caption'>MAC1</th><td class="regform-done-title"><input id="host_mac1" type="text"></td></tr>
    <tr><th class='regform-done-caption'>MELL MAC</th><td class="regform-done-title"><input id="host_mell_mac" type="text"></td></tr>
    <tr><th class='regform-done-caption'>MELL SN</th><td class="regform-done-title"><input id="host_mell_sn" type="text"></td></tr>
    <tr><th class='regform-done-caption'>EDH</th><td class="regform-done-title"><input id="host_edh" type="text"></td></tr>
  </table>
  <input type="hidden" id="host_id" value="<?=@$_GET["host_id"];?>">
  <input type="submit" value="Save">
  <input type="reset" id="host_details_reset" value="Reset">
</form>
<div id="host_reply"></div>

<script>

$(function() {
  load_host();
});
  
$("#host").submit(function () {
  update_host();
  return false;
});

function load_host(){
  if($("#host_id").val()==""){return;}
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {
      cmd:"get_host", 
      host_id:$("#host_id").val()
    },
    success: function(data) {
      console.log(data);   
      rows=JSON.parse(data);
      row=rows[0];
      $("#host_name").val(row["host_name"]);
      $("#host_sn").val(row["host_sn"]);
      $("#host_model").val(row["model"]);
      $("#host_mb").val(row["mb"]);
      $("#host_cpu").val(row["cpu"]);
      $("#host_ram").val(row["ram"]);
      $("#host_disk").val(row["disk"]);
      $("#host_chassis").val(row["chassis"]);
      $("#host_mac0").val(row["mac0"]);
      $("#host_mac1").val(row["mac1"]);
      $("#host_mell_mac").val(row["mell_mac"]);
      $("#host_mell_sn").val(row["mell_sn"]);
      $("#host_edh").val(row["edh"]);      
    }
  });
}


function update_host(){
  $("#host_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    data: {
      cmd:($("#host_id").val()==""?"add_host":"update_host"),
      host_id:$("#host_id").val(),
      host_name:$("#host_name").val(),
      host_sn:$("#host_sn").val(),
      model:$("#host_model").val(),
      mb:$("#host_mb").val(),
      cpu:$("#host_cpu").val(),
      ram:$("#host_ram").val(),
      disk:$("#host_disk").val(),
      chassis:$("#host_chassis").val(),
      mac0:$("#host_mac0").val(),
      mac1:$("#host_mac1").val(),
      mell_mac:$("#host_mell_mac").val(),
      mell_sn:$("#host_mell_sn").val(),
      edh:$("#host_edh").val()
    },
    type: 'get',
    success: function(data) {
       console.log(data);        
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
       if (reply["affected_rows"]==0){
         $('#host_reply').text("No changes to be saved");
       }
       else if (reply["affected_rows"]==1){
         $('#host_reply').text("Record updated");
       }
    }
  });
}

</script>

