<h2>Host Location</h2>
<form method="POST" id="host_location">
  <table class="border" >
	  <tr><th class="regform-done-caption">Host ID</th><td class="regform-done-title"><input id="host_location_host_id"/></td></tr>
	  <tr><th class="regform-done-caption">Host name</th><td class="regform-done-title"><input id="host_location_host_name"/></td></tr>
	  <tr><th class="regform-done-caption">Host sn</th><td class="regform-done-title"><input id="host_location_host_sn"/></td></tr>
	  <tr><th class="regform-done-caption">Date</th><td class="regform-done-title"><input id="host_location_date"/></td></tr>
	  <tr><th class="regform-done-caption">Location</th><td class="regform-done-title"><input id="host_location_location" type="text"/></td></tr>
	  <tr><th class="regform-done-caption">System</th><td class="regform-done-title">
	   <select id="host_location_system">
				<?php foreach (array('Pixel','Strips','BCM','HGTD','MDT','ADE','None','Other','Stock','PLR','') as $k => $v) {echo "<option value=\"$v\">$v</option>";}?>
			</select>
	  </td></tr>
	  <tr><th class="regform-done-caption">Responsible</th><td class="regform-done-title"><input id="host_location_responsible" type="text"/></td></tr>
    <tr><th class="regform-done-caption">EDH number</th><td class="regform-done-title"><input id="host_location_edh" type="number"/></td></tr>
  </table>
<input type="hidden" id="host_location_id" name="host_location_id" value="<?=@$_GET["host_location_id"];?>" >
<input type="submit" value="Save location">
<input type="reset" value="Reset">
</form>
<div id="host_location_reply"></div>

<script>

/* Trigger the date picker */
$(function() {
  $("#host_location_date").datepicker({dateFormat:'yy-mm-dd'});
  load_host_location();
});

/* load the locations */
function load_host_location(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {
      cmd:"get_host_location", 
      host_location_id:$("#host_location_id").val()
    },
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data);
      for (row of reply){
        console.log(row);
        $("#host_location_host_id").val(row["host_id"]);
        $("#host_location_host_name").val(row["host_name"]);
        $("#host_location_host_sn").val(row["host_sn"]);
        $("#host_location_date").val(row["date"]);
        $("#host_location_location").val(row["location"]);
        $("#host_location_system").val(row["system"]);
        $("#host_location_responsible").val(row["responsible"]);
        $("#host_location_edh").val(row["edh"]);
      }
    }
  });
}

/* add a location */
$("#host_location").submit(function () {
  update_host_location();
  return false;
});

function update_host_location(){
  $("#host_location_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {
      cmd:($("#host_location_id").val()==""?"add_host_location":"update_host_location"),
      host_location_id:$("#host_location_id").val(),
      host_id:$("#host_location_host_id").val(),
      date:$("#host_location_date").val(),
      location:$("#host_location_location").val(),
      system:$("#host_location_system option:selected").text(),
      responsible:$("#host_location_responsible").val(),
      edh:$("#host_location_edh").val()
    },
    success: function(data) {
      console.log(data);        
			reply=JSON.parse(data);
      if (reply["affected_rows"]>0){
        $("#host_location_reply").text("Location added successfully");
      }else if (reply["affected_rows"]==0){
        $("#host_location_reply").text("No changes made");
      }else{
        $("#host_location_reply").text("Error: "+data);
      }
    }
  });
}

</script>

