<?php
include_once("../functions.php");
include_once("includes.php");
head(isset($_GET["page"])?$_GET["page"]:"Home");
?>
<body>

<?php top();?>

<?php
if(@$_GET['page']=="cards"){
  include("cards.php");
  include("card.php");
}else if(@$_GET['page']=="cards_of_host"){
	include("cards_of_host.php");
}else if(@$_GET['page']=="card"){
  include("card.php");
  include("locations_of_card.php");
  include("location_of_card.php");
  include("hosts_of_card.php");
  include("host_of_card.php");
}else if(@$_GET['page']=="card_locations"){
  include("card_locations.php");
  include("card_location.php");
}else if(@$_GET['page']=="locations_of_card"){
  include("locations_of_card.php");
  include("location_of_card.php");
}else if(@$_GET['page']=="card_location"){
  include("card_location.php");
}else if(@$_GET['page']=="hosts"){
  include("hosts.php");
  include("host.php");
}else if(@$_GET['page']=="host"){
  include("host.php");
  include("locations_of_host.php");
  include("location_of_host.php");
  include("cards_of_host.php");
  include("card_of_host.php");
}else if(@$_GET['page']=="host_locations"){
  include("host_locations.php");
  include("host_location.php");
}else if(@$_GET['page']=="locations_of_host"){
  include("locations_of_host.php");
}else if(@$_GET['page']=="host_location"){
  include("host_location.php");
}else if(@$_GET['page']=="host_cards"){
  include("host_cards.php");
  include("host_card.php");
}else if(@$_GET['page']=="host_card"){
  include("host_card.php");
}
?>

<?php
onload();
tail();
?>
</body>
</html>
