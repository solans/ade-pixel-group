<h2>Card Location</h2>
<form method="POST" id="card_location">
  <table class="border" >
	  <tr><th class='regform-done-caption'>Card ID</th><td class="regform-done-title"><input id="card_location_card_id"/></td></tr>
	  <tr><th class='regform-done-caption'>Card SN</th><td class="regform-done-title"><input id="card_location_card_sn"/></td></tr>
	  <tr><th class='regform-done-caption'>Date</th><td class="regform-done-title"><input id="card_location_date"/></td></tr>
	  <tr><th class='regform-done-caption'>Location</th><td class="regform-done-title"><input id="card_location_location" type="text"/></td></tr>
	  <tr><th class='regform-done-caption'>System</th><td class="regform-done-title">
	   <select id="card_location_system">
				<?php foreach (array('Pixel','Strips','BCM','HGTD','MDT','ADE','None','Other','Stock','PLR','Broken','') as $k => $v) {echo "<option value=\"$v\">$v</option>";}?>
			</select>
	  </td></tr>
	  <tr><th class='regform-done-caption'>Comment</th><td class="regform-done-title"><input id="card_location_comment" type="text"/></td></tr>
	  <tr><th class='regform-done-caption'>Responsible</th><td class="regform-done-title"><input id="card_location_responsible" type="text"/></td></tr>
    <tr><th class='regform-done-caption'>EDH number</th><td class="regform-done-title"><input id="card_location_edh" type="number"/></td></tr>
  </table>
<input type="hidden" id="card_location_id" name="card_location_id" value="<?=@$_GET["card_location_id"];?>" >
<input type="submit" value="Save location">
<input type="reset" value="Reset">
</form>
<div id="card_location_reply"></div>


<script>

$(function() {
  $("#card_location_date").datepicker({dateFormat:'yy-mm-dd'});
	load_card_location();
});

/* load the locations */
function load_card_location(){
	if($("#card_location_id").val()==""){return;}
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {
      cmd:"get_card_location", 
      card_location_id:$("#card_location_id").val()
    },
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data);
      for (row of reply){
        console.log(row);
        $("#card_location_card_id").val(row["card_id"]);
        $("#card_location_card_sn").val(row["card_sn"]);
        $("#card_location_date").val(row["date"]);
        $("#card_location_location").val(row["location"]);
        $("#card_location_system").val(row["system"]);
        $("#card_location_responsible").val(row["responsible"]);
        $("#card_location_edh").val(row["edh"]);
        $("#card_location_comment").val(row["comment"]);
        if(typeof(load_card)=="function"){load_card();}
      }
    }
  });
}

/* add a location */
$("#card_location").submit(function () {
  update_card_location();
  return false;
});

function update_card_location(){
  $("#card_location_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {
      cmd:($("#card_location_id").val()==""?"add_card_location":"update_card_location"),
      card_location_id:$("#card_location_id").val(),
      card_id:$("#card_location_card_id").val(),
      date:$("#card_location_date").val(),
      location:$("#card_location_location").val(),
      system:$("#card_location_system option:selected").text(),
      responsible:$("#card_location_responsible").val(),
      comment:$("#card_location_comment").val(),
			edh:$("#card_location_edh").val()
    },
    success: function(data) {
       console.log(data);        
			 reply=JSON.parse(data);
       if (reply["affected_rows"]>0){
         $("#card_location_reply").text("Location updated successfully");
       }else{
         $("#card_location_reply").text("Error: "+data);
         if(typeof(load_card_locations)=="function"){load_card_locations();}
			 }
    }
  });
}


</script>

