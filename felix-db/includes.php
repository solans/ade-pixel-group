<?php
include_once("../functions.php");

$db=array(
  "host"=>"dbod-adepix.cern.ch",
  "user"=>"admin",
  "pass"=>base64_decode("QWRlUGl4MTY3MA=="),
  "name"=>"FELIX",
  "port"=>5504
);

function check_session(){
  if(!isset($_SESSION['uname'])){
      header('Location: index.php');
  }
}

function get_page(){
  return isset($_GET["page"])?$_GET["page"]:"Homepage";
}

function open_db(){
  global $conn;
  global $db;
  $conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
  if ($conn->connect_error) {
    die("Connection failed: " . mysqli_connect_error());
  }  
}

function close_db(){
  global $conn;
  $conn->close();
}

$onloads=array();
function add_onload($cmd){
  global $onloads;
  $onloads[]=$cmd;
}

function onload(){
  ?>
  <script>
    $( document ).ready(function() {
      <?php
      global $onloads;
      foreach($onloads as $cmd){
        echo $cmd;
      } 
      ?>
    });
  </script>
  <?php
}

function head($page){
  global $gobase;
  include("head.php");
}

function tail(){
  include("tail.php");
}

function top(){
  include("top.php");
}

?>
