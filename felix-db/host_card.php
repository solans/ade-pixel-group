<h2>Host card</h2>
<form method="POST" id="host_card">
  <table class="border" >
	  <tr><th class="regform-done-caption">Host ID</th><td class="regform-done-title"><input id="host_card_host_id"/></td></tr>
	  <tr><th class="regform-done-caption">Host name</th><td class="regform-done-title"><input id="host_card_host_name"/></td></tr>
	  <tr><th class="regform-done-caption">Host SN</th><td class="regform-done-title"><input id="host_card_host_sn"/></td></tr>
	  <tr><th class="regform-done-caption">Card ID</th><td class="regform-done-title"><input id="host_card_card_id"/></td></tr>
	  <tr><th class="regform-done-caption">Card SN</th><td class="regform-done-title"><input id="host_card_card_sn"/></td></tr>
	  <tr><th class="regform-done-caption">Start date</th><td class="regform-done-title"><input id="host_card_start_date"/></td></tr>
	  <tr><th class="regform-done-caption">End date</th><td class="regform-done-title"><input id="host_card_end_date"/></td></tr>
  </table>
<input type="hidden" id="host_card_id" name="host_card_id" value="<?=@$_GET["host_card_id"];?>" >
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>
<div id="host_card_reply"></div>

<script>

$(function() {
  $("#host_card_start_date").datepicker({dateFormat:'yy-mm-dd'});
  $("#host_card_end_date").datepicker({dateFormat:'yy-mm-dd'});
  load_host_card();
});



function load_host_card(){
  if($("#host_card_id").val()==""){return;}
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {
      cmd:"get_host_card", 
      host_card_id:$("#host_card_id").val()
    },
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data);
      for (row of reply){
        console.log(row);
        $("#host_card_host_id").val(row["host_id"]);
        $("#host_card_host_name").val(row["host_name"]);
        $("#host_card_host_sn").val(row["host_sn"]);
        $("#host_card_card_id").val(row["card_id"]);
        $("#host_card_card_sn").val(row["card_sn"]);
        $("#host_card_start_date").val(row["start_date"]);
        $("#host_card_end_date").val(row["end_date"]);
        if(typeof load_host == "function"){load_host(row["host_id"]);}
        if(typeof load_card == "function"){load_card(row["card_id"]);}
      }
    }
  });
}

$("#host_card").submit(function () {
  update_host_card();
  return false;
});

function update_host_card(){
  $("#host_card_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {
      cmd:($("#host_card_id").val()==""?"add_host_card":"update_host_card"),
      host_card_id:$("#host_card_id").val(),
      host_id:$("#host_card_host_id").val(),
      card_id:$("#host_card_card_id").val(),
      start_date:$("#host_card_start_date").val(),
      end_date:$("#host_card_end_date").val()
	  },
    success: function(data) {
       console.log(data);        
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
       if (reply["affected_rows"]==0){
         $("#host_card_reply").text("No changes to be saved");
       }
       else if (reply["affected_rows"]==1){
         $("#host_card_reply").text("Record updated");
         load_host_card();
       }
    }
  });
}

</script>

