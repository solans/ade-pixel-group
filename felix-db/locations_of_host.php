<h2>Locations of host</h2>
<button id="host_locations_export">CSV</button>Export <span id="host_locations_counter"></span> 
<button id="host_locations_reset">Reset filters</button>	
<table id="host_locations" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<tr>
      <th data-placeholder="Search...">Date</th>
      <th data-placeholder="Search...">Location</th>
		  <th data-placeholder="Search...">System</th>
		  <th data-placeholder="Search...">Responsible</th>
		  <th data-placeholder="Search...">EDH</th>
		  <th >Actions</th>
    </tr>
	</thead>
	<tbody id="host_locations_body">
  </tbody>
</table>
<div id="host_locations_reply"></div>

<input type="hidden" id="host_id" value="<?=@$_GET["host_id"];?>">

<script>

/* Trigger the tablesorter */
$(function() {
  $("#host_locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_host_locations();
});

/* Load the table sorter **/
$("#host_locations").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter', 'zebra',]
}).bind('filterEnd', function() {
  $("#host_locations_counter").html("("+($("#host_locations tr:visible").length-2)+")");
});

/* clear the filters */
$("#host_locations_reset").click(function() {
  $("#host_locations").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

/* declare the export locations */
$("#host_locations_export").click(function() {
  $("#host_locations").trigger('outputTable');
});


/* load the locations */
function load_host_locations(){
  if($("#host_id").val()==""){return;}
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {
      cmd:"get_locations_of_host",
			host_id:$("#host_id").val()
    },
    success: function(data) {
      console.log(data);   
      locations=JSON.parse(data);
      $("#host_locations_body").empty();
      for (row of locations){
        tt ="<tr>\n";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+row["system"]+"</td>";
        tt+="<td>"+row["responsible"]+"</td>";
        tt+="<td><a href='https://edh.cern.ch/Document/"+row["edh"]+"' target=\"_blank\">"+row["edh"]+"</a></td>";
        tt+="<td>";
        tt+="<a href='?page=host_location&host_location_id="+row["host_location_id"]+"'>edit</a>&nbsp;";
        tt+="<a href='#' onclick='delete_host_locations("+row["host_location_id"]+")'>delete</a>&nbsp;";
        tt+="</td>";
        tt+="</tr>\n";
        $("#host_locations_body").append(tt);
      }
      $("#host_locations_counter").html("("+locations.length+")");
      $("#host_locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete a location */
function delete_host_locations(host_location_id){
  if(!window.confirm("Are you sure to delete host location?")) return false;
  $("#card_hosts_reply").text("");
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_host_location",
      host_location_id:host_location_id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==1){
        $("#host_locations_reply").text("location deleted");
        if(typeof(load_host_locations) == "function") load_host_locations();
      }else if (reply["affected_rows"]==0){
        $("#host_locations_reply").text("Something went wrong");
      }
    }
  });
  return false;
};


</script>


