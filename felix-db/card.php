<h2>Card</h2>
<form id="card">
  <table class="border" >
    <tr><th class='regform-done-caption'>Card ID</th><td class="regform-done-title"><input id="card_id" type="text" disabled value="<?=@$_GET["card_id"];?>"/></td></tr>
    <tr><th class='regform-done-caption'>Card SN</th><td class="regform-done-title"><input id="card_sn" type="text"/></td></tr>
    <tr><th class='regform-done-caption'>Card model</th><td class="regform-done-title"><input id="card_model" type="text"/></td></tr>
  </table>
  <input type="hidden" id="card_id" value="<?=@$_GET["card_id"];?>">
  <input type="submit" value="Save">
  <input type="reset" id="card_reset" value="Reset">
</form>
<div id="card_reply"></div>

<script>

$( document ).ready(function() {
  load_card();
});

$("#card").submit(function () {
  update_card();
  return false;
});

/* load the card */
function load_card(){
  if($("#card_id").val()==""){return;}
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {
      cmd:"get_card", 
      card_id:$("#card_id").val()
    },
    success: function(data) {
      console.log(data);   
      cards=JSON.parse(data);
      console.log(cards);
      $("#card_sn").val(cards[0]["card_sn"]);
      $("#card_model").val(cards[0]["card_model"]);
    }
  });
}

/* update the card */
function update_card(){
  $("#card_reply").text("");
  $.ajax({
	  url: 'dbwrite.php',
    type: 'get',
    data: {
      cmd:($("#card_id").val()==""?"add_card":"update_card"),
      card_id:$("#card_id").val(),
      card_sn:$("#card_sn").val(),
      card_model:$("#card_model").val()
    },
    success: function(data) {
       console.log(data);        
       reply=JSON.parse(data.slice(data.indexOf("{"),data.lastIndexOf("}")+1));
       if (reply["affected_rows"]==0){
         $("#card_reply").text("No changes to be saved");
       }
       else if (reply["affected_rows"]==1){
         $("#card_reply").text("Record updated");
       }
    }
  });
}
  
</script>

