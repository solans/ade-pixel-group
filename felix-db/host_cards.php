<h2>Host cards</h2>
<button id="host_cards_export">CSV</button>Export <span id="host_cards_counter"></span> 
<button id="host_cards_reset">Reset filters</button>
<table id="host_cards" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
    <tr>
		  <th class="filter-select" data-placeholder="Search...">Host name</th>
		  <th class="filter-select" data-placeholder="Search...">Host sn</th>
		  <th class="filter-select" data-placeholder="Search...">Card id</th>
		  <th class="filter-select" data-placeholder="Search...">Card sn</th>
		  <th data-placeholder="Search...">Start date</th>
		  <th data-placeholder="Search...">End date</th>
	    <th >Actions</th>
    </tr>
	</thead>
	<tbody id="host_cards_body">
	</tbody>
</table>
<div id="host_cards_reply"></div>
<script>

/* Load the table sorter **/
$("#host_cards").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#host_cards_counter").html("("+($('#host_cards tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#host_cards").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_host_cards();
});

/* clear the filters */
$("#host_cards_reset").click(function() {
  $("#host_cards").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

/* declare the export */
$("#host_cards_export").click(function() {
  $("#host_cards").trigger('outputTable');
});

/* load the cards */
function load_host_cards(){
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {cmd:"get_host_cards"},
    success: function(data) {
      //console.log(data);   
      rows=JSON.parse(data);
      $("#host_cards_body").empty();
      for (row of rows){
        tt ="<tr>\n";
        tt+="<td>"+row["host_name"]+"</td>";
        tt+="<td>"+row["host_sn"]+"</td>";
        tt+="<td>"+row["card_id"]+"</td>";
        tt+="<td>"+row["card_sn"]+"</td>";
        tt+="<td>"+row["start_date"]+"</td>";
        tt+="<td>"+row["end_date"]+"</td>";
				tt+="<td>";
        tt+="<a href=\"?page=host&host_id="+row["host_id"]+"\">host</a>&nbsp;";
        tt+="<a href=\"?page=card&card_id="+row["card_id"]+"\">card</a>&nbsp;";
        tt+="<a href=\"?page=host_card&host_card_id="+row["host_card_id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_host_cards('"+row["host_card_id"]+"')\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#host_cards_body").append(tt);
      }
      $("#host_cards_counter").html("("+rows.length+")");
      $("#host_cards").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete a location */
function delete_host_cards(host_card_id){
  if(!window.confirm("Are you sure to delete host card?")) return false;
  $("#host_cards_reply").text("");
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"delete_host_card",host_card_id:host_card_id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==1){
        $("#host_cards_reply").text("Member deleted");
        if(typeof load_host_cards == "function") load_host_cards();
      }else if (reply["affected_rows"]==0){
         $("#host_cards_reply").text("Something went wrong");
      }
    }
  });
  return false;
};


</script>

