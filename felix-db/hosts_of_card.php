<h2>Hosts of card</h2>
<button id="card_hosts_export">CSV</button>Export <span id="card_hosts_counter"></span> 
<button id="card_hosts_reset">Reset filters</button>
<table id="card_hosts" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
    <tr>
		<th data-placeholder="Search...">Name</th>
		<th data-placeholder="Search...">Serial</th>
		<th data-placeholder="Search...">Model</th>
		<th data-placeholder="Search...">Start date</th>
		<th data-placeholder="Search...">End date</th>
    <th>Action</th>
	  </tr>	
  </thead>
	<tbody id="card_hosts_body">
	</tbody>
</table>
<div id="card_hosts_reply"></div>
<input type="hidden" id="card_hosts_card_id" value="<?=@$_GET["card_id"];?>">

<script>

/* Trigger the tablesorter */
$(function() {
  $("#card_hosts").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_card_hosts();
});
  
/* Load the table sorter **/
$("#card_hosts").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter', 'zebra','output']
}).bind('filterEnd', function() {
  $("#hosts_counter").html("("+($('#hosts tr:visible').length-2)+")");
});

/* clear the filters */
$("#card_hosts_reset").click(function() {
  $("#card_hosts").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

/* declare the export hosts */
$("#card_hosts_export").click(function() {
  $("#card_hosts").trigger('outputTable');
});

/* load the hosts */
function load_card_hosts(){
  if($("#card_hosts_card_id").val()==""){return;}
  $.ajax({
    url: 'dbread.php',
    type: 'get',
    data: {
      cmd:"get_hosts_of_card",
      card_id:$("#card_hosts_card_id").val()
    },
    success: function(data) {
      //console.log(data);   
      rows=JSON.parse(data);
      $("#card_hosts_body").empty();
      for (row of rows){
        tt ="<tr>\n";
        tt+="<td>"+row["host_name"]+"</td>";
        tt+="<td>"+row["host_sn"]+"</td>";
        tt+="<td>"+row["model"]+"</td>";
        tt+="<td>"+row["start_date"]+"</td>";
        tt+="<td>"+row["end_date"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"?page=host&host_id="+row["host_id"]+"\">host</a>&nbsp;";
				tt+="<a href=\"?page=host_card&host_card_id="+row["host_card_id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_host_cards('"+row["host_card_id"]+"')\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#card_hosts_body").append(tt);
      }
      $("#card_hosts_counter").html("("+rows.length+")");
      $("#card_hosts").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete a location */
function delete_host_cards(host_card_id){
  if(!window.confirm("Are you sure to delete host card?")) return false;
  $("#card_hosts_reply").text("");
  $.ajax({
    url: 'dbwrite.php',
    type: 'get',
    data: {cmd:"delete_host_card",host_card_id:host_card_id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==1){
        $("#card_hosts_reply").text("Member deleted");
        if(typeof(load_card_hosts) == "function") load_card_hosts();
      }else if (reply["affected_rows"]==0){
         $("#card_hosts_reply").text("Something went wrong");
      }
    }
  });
  return false;
};

</script>


