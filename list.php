<?
$dir="./";
if(isset($_GET["dir"])&&@$_GET["dir"]!=""){$dir=$_GET["dir"]."/";}
?>
<html>
<body>
<table>
<tr>
<td>
<table border=1 cellpadding=5 cellspacing=0 >
  <tr><th>Filename</th><th>Size (KB)</th><th>Last modified</th></tr>
<?
$files = scandir($dir);
// loop through the array of files and print them all
foreach ($files as $file){
  if($file==".."&&$dir!="./"){/*good to go*/}
  else if (substr($file, 0, 1) == ".") continue; // don't list hidden files
  ?>
  <tr>
  <td>
    <?
    $ext=pathinfo($file, PATHINFO_EXTENSION);
    if(in_array($ext,array('png','jpg'))){
      ?>
      <a href="<?=$_SERVER['PHP_SELF']."?display=".$dir.$file;?>">
      <?=$file;?>
      </a>
      <?
    }else if(is_dir($dir.$file)){
      $link=$dir.$file;
      if($file==".."){$link=substr($dir,0,strrpos($dir, "/",-2));}
      ?>
      <a href="<?=$_SERVER['PHP_SELF']."?dir=".$link;?>">
      <?=$file;?>
      </a>
      <?
    }else{
  	  ?>
  	  <a href="<?=$dir.$file;?>"><?=$file;?></a>
		  <?
 	  }
 	  ?>
  </td>
  <td align="right">
  <?=round(filesize($dir.$file)/1024);?>
  </td>
  <td>
  <?=date("l, dS F, Y @ h:ia", filemtime($dir.$file));?>
  </td>
  </tr>
  <?
}
?>
</table>
</td>
<? if(isset($_GET["display"])){ ?>
<td style="vertical-align:top; border:solid">
  <table>
    <tr>
      <td><h2><?=$_GET["display"];?></h2></td>
    </tr>
    <tr>
      <td>
      <? if(strpos($_GET["display"],".png")!==false || strpos($_GET["display"],".jpg")!==false) {?>
         <img src="<?=$dir.$_GET['display'];?>"/>
      <? } ?>
			</td>
    </tr>
    <tr>
      <td><a href="<?=$dir.$_GET['display'];?>"><?=$_GET['display'];?></a>
    </td>
    </tr>
  </table>
</td>
<? } ?>
</tr>
</table>
</body>
</html>