<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Page title</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">This is a Title</p>
<p class="SUBTITLE">This is a subtitle</p>
<p>CONTENT GOES HERE</p>

<p>Consider using relative dimensions for images so they scale properly in all devices</p>
<img width="100%" src="cropped-blue-circuit-board.jpg">

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
