<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
if(!isset($gobase)){$gobase="../";}
$authorised=false;

if(isset($_GET["source"])){
  readfile($_SERVER["SCRIPT_FILENAME"]);
  exit();
}

function show_header()
{
  global $gobase;
?>
<div class="HEADER">
	<div style="float:left">
		<img id="SIDEBUTTON" src="<?=$gobase;?>img/sidebar-hide.png" style="height:15px" onclick="toggle_sidebar()">
	</div>
	<script>
		function toggle_sidebar(){
			bar = document.getElementById("SIDEBAR");
			but = document.getElementById("SIDEBUTTON");
			if(bar.style.display!="none"){bar.style.display="none";but.src="<?=$gobase;?>img/sidebar-show.png";}
			else{bar.style.display="block";but.src="<?=$gobase;?>img/sidebar-hide.png";}
		}
	</script>
	<div class="HEADERLEFT"><img class="HEADERIMAGE" src="<?=$gobase;?>img/ATLAS-logo.png"></div>
	<div class="HEADERCENTER HEADERTEXT">CERN ATLAS Pixel group</div>
</div>
<?php
}

function show_navbar()
{
global $gobase;
?>

<div id="SIDEBAR" class="SALON">
<div class="FLOAT">
<div id="BAR" class="NAV"> 
 <a href="<?=$gobase;?>Welcome/">Welcome</a><br />
  <a href="<?=$gobase;?>Publications">Publications</a><br />
  <a href="<?=$gobase;?>MALTA/">MALTA</a><br />
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>PublicPlots">PublicPlots</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Authorship">Authorship</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Authorship/?page=conferences">Conferences</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>malta-db">Sample DB</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>LAPA">LAPA</a><br /></div>
  <a href="<?=$gobase;?>SensorCharacterization/">Sensor characterization</a><br />
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Tower">Tower</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>LFoundry">LFoundry</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>XFAB">XFAB</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>AMS">AMS</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>SCVD">SCVD</a><br /></div>
  <a href="<?=$gobase;?>Modules/">Module development</a><br />
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Modules/quadassembly.php">Module Assembly</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Modules/itkpix-module-qc.php">ITkPix Module QC</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Modules/rd53a-module-qc.php">RD53A Module QC</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>TSV">TSV</a><br /></div>
  <a href="<?=$gobase;?>Staves/">Stave prototyping</a><br />
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>SystemTests">System tests</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>TFM">TFM</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>CSB">CSB</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>EnvMon">Env Monitoring</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Logistics">Logistics</a><br /></div>
  <a href="<?=$gobase;?>Readout/">Readout and Software</a><br />
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>MaltaSW">Malta</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Felix">Felix</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>felix-db">Felix DB</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>TestBeam">Testbeam</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Readout/proteus.php">Proteus</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Readout/resources.php">Resources</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>itk-felix-sw/html">ITk Felix SW</a><br /></div>
  <a href="<?=$gobase;?>PCBs/">PCB design</a><br />
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>PCBs/VHDCI-HDMI-A.php">VHDCI-HDMI-A</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>PCBs/VHDCI-GBT-SFP.php">VHDCI-GBT-SFP</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>PCBs/ERF-8xDisplayPort.php">ERF-to-8xDP</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>PCBs/MALTA-interface-V4.php">MALTA Interface Board</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>PCBs/BCMp-interface-V1.php">BCMp Interface Board</a><br /></div>
  <a href="<?=$gobase;?>Labs/">Labs</a><br />
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Labs/Lab07.php">Lab07</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Labs/Lab23.php">Lab23</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Labs/Lab24.php">Lab24</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Labs/LaserSetup.php">Laser setup</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>xray/">X-ray machine</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>xrayschedule/schedule.php">X-ray schedule</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>IrradiatedSamples/">Irr. samples</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>Sources/">Sources</a><br /></div>
  <div id="BARSUB" class="NAV"><a href="<?=$gobase;?>LabSupplies/">Supplies</a><br /></div>
</div>
</div>
</div>
<?php
}

function show_login($forced=false){
  global $authorised;
?>
<div class="LOGIN NAV">
<? $authorised=isAuthorised();
if($authorised || $forced){
  ?>User: <?=$_SERVER['OIDC_CLAIM_preferred_username'];?>&nbsp;<a href="..">Logout</a><?
}else{
  $url="login/";
  if(strlen($_SERVER['QUERY_STRING'])>0){$url.="?".$_SERVER['QUERY_STRING'];}
  ?><a href="<?=$url;?>">Login</a><?
}
?>
</div>
<?  
}

function show_certificate()
{
	$certfile="../".basename(dirname($_SERVER["PHP_SELF"]))."/cert.txt";
	if(!file_exists($certfile)) return;
	$json=json_decode(@file_get_contents($certfile));
	if(!isset($json['reviewer'])) return;
	?>	
	<div class="CERT">
		Reviewed by <?=$json['reviewer'];?><br/> 
		(<?=$json['date'];?>)
	</div>
	<?php
}

function show_footer()
{
?>
<div class="FOOTER">
  <div class="FOOTERELEMENT">
  	E-group: 
  	<a href="mailto:ep-ade-tk-group_AT_cern.ch">ep-ade-tk-group</a>
  </div>
</div>
<?php
}

function isAuthorised(){
  global $gobase;
  $authorised=isset($_SERVER['OIDC_CLAIM_preferred_username']);
  if($authorised){
    $login=$_SERVER['OIDC_CLAIM_preferred_username'];
    $authorised=false;
    foreach( json_decode(file_get_contents("../groups.json")) as $group => $members){
      if(in_array($login,$members)){
        $authorised=true;
      }
    }
  }  
  return $authorised;
}

function show_authorised(){
  global $authorised;
  ?>
  <script>
    function isAuthorised(){return <?=($authorised?"true":"false");?>;}
  </script>
  <?
}

function mailuser(){
  $ret=array("user"=>"adecmos","email"=>"adecmos@cern.ch","pass"=>base64_decode("S2lnZUJlbGUyNAo="));
  return $ret;
}

function redirect($href){
  if(@$_GET["debug"]!="true"){
    ?>
    <script>window.location.href = "<?=$href;?>"</script>
    <?
    }
  ?>
  <a href="<?=$href;?>">Click here</a>
  <?
}

function path(){
  return dirname(__FILE__);
}
?>
