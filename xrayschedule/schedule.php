<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link href="<?=$gobase;?>xrayschedule/SimpleCalendar.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<title>X-ray machine schedule</title>
<style>
.ui-datepicker {
  background: #fff;
}
</style>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<?php
  $authorised=isAuthorised();
?>

<div class="CONTENT">
<?php   
  show_certificate();
  show_login(); 
?>


<p class="TITLE">X-ray Machine schedule</p>

<table style="border:none;">
  <?
    
    include_once("parser.php");
    include_once("helper.php");
    include_once("event.php");
  
    $filename = getcwd()."/data/data.xml";
    $parser = new Parser();
    $helper = new Helper();
    $helper->verbose = (@$_GET["debug"]=="true"?true:false);
    $helper->addType("Event");
        
    $parser->SetFilename($filename);
    $parser->SetDataHolder($helper);
    $parser->Parse();
    
    $collection = $helper->GetData();
    
    include_once("querystring.php");
    $qs = new querystring();
  ?>
    <tr>
      <td> 
      <? 
      if(@$_GET["action"]=="admin" && $authorised){
        $qs->remove("action");
        $qs->remove("do");
        $qs->remove("id");
        ?><a href="<?=$_SERVER['SCRIPT_NAME'].$qs->toFullString();?>">View</a><? 
        $qs->set("action",$_GET["action"]);
        if(isset($_GET["do"])) $qs->set("do",$_GET["do"]);
        if(isset($_GET["id"])) $qs->set("id",$_GET["id"]);
      }else if($authorised){
        $qs->remove("do");
        $qs->remove("id");
        $qs->set("action","admin");
        ?><a href="<?=$_SERVER['SCRIPT_NAME'].$qs->toFullString();?>">Admin</a><? 
      }
      ?>
      </td>
    </tr>
    <?
    
    function randomstring(){
      $arr = str_split('abcdefghijklmnopqrstuwxyz'); // get all the characters into an array
      shuffle($arr); // randomize the array
      $arr = array_slice($arr, 0, 6); // get the first six (random) characters out
      return implode('', $arr); // smash them back into a string
    }
    
    if(@$_POST["action"]=="add"){
        if(!isset($_POST["live"])){$_POST["live"]="off";}
        if(strptime($_POST["startdate"],"Y-m-d" )===false){
          $_POST["startdate"] = date("Y-m-d", strtotime($_POST["startdate"]));
        }
        if(strptime($_POST["enddate"],"Y-m-d" )===FALSE){
          $_POST["enddate"] = date("Y-m-d", strtotime($_POST["enddate"]));
        }
        //Pre-process POST
        //continue
        $obj = new Event();
        $obj->FromArray($_POST);
        //Append to file
        $collection->Add($obj);
        $fh = fopen($filename, 'w');
        fwrite($fh, $collection->ToXml());
        fclose($fh);  
        $qs = new querystring(); 
        //$qs->set('action','admin');
        $qs->remove('do');
        $qs->remove('id');
        $qs->set('reply','thankyou');
        //send a mail
        include_once('email.php');
        $muser = mailuser();
        $email = new email();
        $email->setSender("no-reply", "no-reply@cern.ch");
        $email->setServer("cernmx.cern.ch",25,"","");
        $email->setSmtp(false);
        $email->setSubject("X-ray schedule request");
        $email->addTo("carlos.solans@cern.ch");
        $message  = "Responsible: ".$_POST["responsible"]."\n";
        $message .= "E-mail: ".$_POST["email"]."\n";
        $message .= "Group: ".$_POST["group"]."\n";
        $message .= "Start date: ".$_POST["startdate"]."\n";
        $message .= "End date: ".$_POST["enddate"]."\n";
        $message .= "Comments: ".$_POST["comments"]."\n";
        $message .= "http";
        if(@$_SERVER["HTTPS"]=="on"){$message .= "s";}
        $message .= "://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?action=admin&do=edit&id=".$_POST["id"];
        $email->setMessage($message);
        $enviado=$email->send();
        //redirect 
        redirect($_SERVER['PHP_SELF'].$qs->toFullString());
    }
    
    if(@$_GET["action"]=="admin" && $authorised){ 
      
      
      if(@$_POST["action"]=="edit"){
        if(!isset($_POST["live"])){$_POST["live"]="off";}
        if(strptime($_POST["startdate"],"Y-m-d" )===FALSE){
          $_POST["startdate"] = date("Y-m-d", strtotime($_POST["startdate"]));
        }
        if(strptime($_POST["enddate"],"Y-m-d" )===FALSE){
          $_POST["enddate"] = date("Y-m-d", strtotime($_POST["enddate"]));
        }
        $obj = $collection->Get($_GET["id"]);
        $obj->FromArray($_POST);
        $fh = fopen($filename, 'w');
        fwrite($fh, $collection->ToXml());
        fclose($fh);  
        $qs = new querystring(); 
        $qs->set('action','admin');
        $qs->remove('do');
        
        //send a mail
        if($_POST["live"]=="on"){
            include_once('email.php');
            $muser = mailuser();
            $email = new email();
            $email->setSender("ATLAS Pixel group", $muser["email"]);
            $email->setServer("smtp.cern.ch",587,$muser["user"],$muser["pass"]);
            $email->setAuthType("LOGIN");
            $email->setCrypto("TLS");
            $email->setSmtp(true);
            $email->setSubject("X-ray schedule request");
            $email->addCC("carlos.solans@cern.ch");
            $email->addTo($_POST["responsible"].' <'.$_POST["email"].'>');
            $message  = "Dear ".$_POST["responsible"].",\n";
            $message .= "Your x-ray request has been added to the schedule with the following details:\n\n";
            $message .= "Responsible: ".$_POST["responsible"]."\n";
            $message .= "E-mail: ".$_POST["email"]."\n";
            $message .= "Group: ".$_POST["group"]."\n";
            $message .= "Start date: ".$_POST["startdate"]."\n";
            $message .= "End date: ".$_POST["enddate"]."\n";
            $message .= "Comments: ".$_POST["comments"]."\n";
            $message .= "\n";
            $message .= "http";
            if(@$_SERVER["HTTPS"]=="on"){$message .= "s";}
            $message .= "://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?action=view&id=".$_POST["id"];
            $message .= "\n";
            $message .= "Have a nice day.\n";
            $message .= "X-ray schedule request.\n";
            $email->setMessage($message);
            $enviado=$email->send();
        }
        //redirect
        redirect($_SERVER['PHP_SELF'].$qs->toFullString());
      }else if(@$_GET["do"]=="remove" && $authorised){
        $collection->Del($_GET["id"]);
        $collection->Dump();
        $fh = fopen($filename, 'w');
        fwrite($fh, $collection->ToXml());
        fclose($fh);  
        $qs = new querystring(); 
        $qs->remove('do');
        $qs->remove('id');
        redirect($_SERVER['PHP_SELF'].$qs->toFullString());
      }else{
        ?>
      <tr><td style="text-align:left">
	    <a href="<? $qs->remove('do'); $qs->remove('id'); echo $_SERVER['PHP_SELF'].'?'.$qs->toString();?>" >Add</a>
        <form name="editor" action="<?=$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];?>" method="POST">
          <input type="hidden" name="action" value="add"/>
          <input type="hidden" name="id" value="<?=randomstring();?>"/>
          <label style="display:inline-block;width:100px;">Group(*):</label><input type="text" name="group" size="80"/><br/>
          <label style="display:inline-block;width:100px;">Responsible:</label><input type="text" name="responsible" size="80"/><br/>
          <label style="display:inline-block;width:100px;">Email:</label><input type="text" name="email" size="50"/><br/>
          <label style="display:inline-block;width:100px;">Start date:</label><input id="startdate" type="text" name="startdate" size="50"/><br/>
          <label style="display:inline-block;width:100px;">End date:</label><input id="enddate" type="text" name="enddate" size="50"/><br/>
          <label style="display:inline-block;width:100px;">Dose rate:</label><input type="text" name="doserate" size="50"/><br/>
          <label style="display:inline-block;width:100px;vertical-align:top;">Comments:</label><textarea name="comments" cols="40" rows="4"></textarea><br/>
          <label style="display:inline-block;width:100px;">Live:</label><input type="checkbox" name="live"/><br/>
          <label style="display:inline-block;width:100px;">No show:</label><input type="checkbox" name="noshow"/><br/>
          <input name="submit" type="submit" value="Add" /><br>
          * Required fields
        </form>
        <script language="javascript">
            $(function() {
              $( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd', firstDay: 1}); 
              $( "#enddate" ).datepicker({ dateFormat: 'yy-mm-dd', firstDay: 1}); 
            });
        </script>

        <table style="text-align:left; width:100%">
        <thead>
          <td>Group</td>
          <td>Responsible</td>
          <td>Start date</td>
          <td>End date</td>
          <td>Live</td>
          <td>No show</td>
          <td>Actions</td>
        </thead>
        <?
        $sorted=usort($collection->data, function ($a, $b) {
            return strcmp(strtolower($b->startdate), strtolower($a->startdate));
        });
        foreach($collection->data as $p){
          ?>
          <tr>
          <?php 
          /*
          <td>
            <?=$p->group;?>&nbsp;
            <?=$p->responsible;?>&nbsp;
            <?=$p->startdate;?>&nbsp;
            <?=$p->enddate;?>&nbsp;
            <?=$p->live;?>&nbsp;
            <?=$p->noshow;?>&nbsp;
            <a href="<? $qs->set('do','edit'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">edit</a> 
            <a href="<? $qs->set('do','remove'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">delete</a> 
          </td>
          */
          ?>
          <td><?=$p->group;?></td>
          <td><?=$p->responsible;?></td>
          <td><?=$p->startdate;?></td>
          <td><?=$p->enddate;?></td>
          <td><?=$p->live;?></td>
          <td><?=$p->noshow;?></td>
          <td>
            <a href="<? $qs->set('do','edit'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">edit</a> 
            <a href="<? $qs->set('do','remove'); $qs->set('id',urlencode($p->GetId())); echo $_SERVER['PHP_SELF'].$qs->toFullString()?>">delete</a> 
          </td>
          </tr>
          <?
        } 
        ?>  
        </table>

        <?
        if(@$_GET["do"]=="edit" && $authorised){
          $cc = $collection->Get($_GET["id"]);
          ?>
          <script>
            document.forms["editor"].elements["id"].value='<?=$cc->id;?>';
            document.forms["editor"].elements["group"].value='<?=$cc->group;?>';
            document.forms["editor"].elements["responsible"].value='<?=addslashes($cc->responsible);?>';
            document.forms["editor"].elements["email"].value='<?=$cc->email;?>';
            document.forms["editor"].elements["startdate"].value='<?=$cc->startdate;?>';
            document.forms["editor"].elements["enddate"].value='<?=$cc->enddate;?>';
            document.forms["editor"].elements["doserate"].value='<?=$cc->doserate;?>';
            document.forms["editor"].elements["comments"].value='<?=addslashes(str_replace("\r","",str_replace("\n","",$cc->comments)));?>';
            document.forms["editor"].elements["live"].checked=<?=($cc->live=="on"?"true":"false");?>;
            document.forms["editor"].elements["noshow"].checked=<?=($cc->noshow=="on"?"true":"false");?>;
            document.forms["editor"].elements["action"].value='edit';
            document.forms["editor"].elements["submit"].value='Update';
          </script>
          <?
        }
      }
      ?>
        </td>
    </tr>
    <?    
    }else if (@$_GET["action"]=="view" && isset($_GET["id"])){ 
        $cc = $collection->Get($_GET["id"]);
        ?>
    
   
    <table>
    <tr><td>Request ID:</td><td><?=$cc->id;?></td></tr>
    <tr><td>Group:</td><td><?=$cc->group;?></td></tr>
    <tr><td>Responsible:</td><td><?=$cc->responsible;?></td></tr>
    <tr><td>E-mail:</td><td><?=$cc->email;?></td></tr>
    <tr><td>Start date:</td><td><?=$cc->startdate;?></td></tr>
    <tr><td>End date:</td><td><?=$cc->enddate;?></td></tr>
    <tr><td>Comments:</td><td><?=$cc->comments;?></td></tr>
    <tr><td>Approved:</td><td><?=($cc->live=="on"?"YES":"NO");?></td></tr>
    <tr><td colspan="2">Please remember to read the x-ray documentation and safety documents.</div></td></tr>
    </table>
    
    <ul>
  		<li><a href="<?=$gobase;?>/xray/SF_form.pdf">Safety form to be returned before irradiation campaign</a></li>
  		<li><a href="<?=$gobase;?>/xray/X-Rad_User_Manual.pdf">User manual</a></li>
  	</ul>
    <div style="height:300px;"></div>
    <?    
    }else{
    ?>
    <tr>
      <td>
          <?
            if(@$_GET["debug"]=="true"){$collection->Dump();}
          ?>
      </td>
    </tr>
    <tr>
        <td>
        <h2>
        <table>
          <tr>
            <td><a href="<?$qs=new querystring();$qs->set('date',date('d-m-Y', strtotime(@$_GET['date'].' -1 month')));echo $qs->toFullString();?>">&#10096;</a></td>
            <td><?=date('M Y', strtotime(@$_GET['date'].' +0 days'));?></td>
            <td><a href="<?$qs=new querystring();$qs->set('date',date('d-m-Y', strtotime(@$_GET['date'].' +1 month')));echo $qs->toFullString();?>">&#10097;</a></td>
          </tr>
        </table>
        </h2>
        <?
        include_once('SimpleCalendar.php');
        $calendar = new donatj\SimpleCalendar();
        $calendar->setStartOfWeek('Monday');
        foreach($collection->data as $p){
          if($p->live!="on"){continue;}
          $calendar->addDailyHtml( $p->group, $p->startdate, $p->enddate );
        }
        
        if(!isset($_GET['date'])){$_GET["date"]=date("d-m-Y");}
        $calendar->setDate($_GET["date"]);
        print $_GET["date"];
        
        //Add no support on weekends
        $date0 = strtotime($_GET["date"]."-3 weeks");
        $date1 = strtotime($_GET["date"]."+4 weeks");
        while (date("Y-m-d", $date0) != date("Y-m-d", $date1)) {
            $day_index = date("w", $date0);
            if ($day_index == 0 || $day_index == 6) {
                $calendar->addDailyHtml( "No support", date("d-m-Y",$date0) );
            }
            $date0 = strtotime(date("Y-m-d", $date0) . "+1 day");
        }
        
        //Add holidays
        $holidays=array(
        "01-01-2019"=>"New year",
        "02-01-2019"=>"CERN closed",
        "03-01-2019"=>"CERN closed",
        "04-01-2019"=>"CERN closed",
        "05-01-2019"=>"CERN closed",
        "06-01-2019"=>"CERN closed",
        "19-03-2019"=>"Good Friday",
        "22-04-2019"=>"Easter Monday",
        "01-05-2019"=>"1st May",
        "30-05-2019"=>"Ascension day",
        "10-06-2019"=>"Whit Monday",
        "05-09-2019"=>"Jeune Genevois",
        "21-12-2019"=>"CERN closed",
        "22-12-2019"=>"CERN closed",
        "23-12-2019"=>"CERN closed",
        "24-12-2019"=>"Christmas Eve",
        "25-12-2019"=>"Christmas",
        "26-12-2019"=>"CERN closed",
        "27-12-2019"=>"CERN closed",
        "28-12-2019"=>"CERN closed",
        "29-12-2019"=>"CERN closed",
        "30-12-2019"=>"CERN closed",
        "31-12-2019"=>"New Year's eve",
        "01-01-2020"=>"New Year",
        "02-01-2020"=>"CERN closed",
        "03-01-2020"=>"CERN closed",
        "10-04-2020"=>"Good Friday",
        "13-04-2020"=>"Easter Monday",
        "01-05-2020"=>"1st May",
        "21-05-2020"=>"Ascension day",
        "01-06-2020"=>"Whit Monday",
        "10-09-2020"=>"Jeune genevois",
        "21-12-2020"=>"CERN closed",
        "22-12-2020"=>"CERN closed",
        "23-12-2020"=>"CERN closed",
        "24-12-2020"=>"Christmas Eve",
        "25-12-2020"=>"Christmas Day",
        "26-12-2020"=>"CERN closed",
        "27-12-2020"=>"CERN closed",
        "28-12-2020"=>"CERN closed",
        "29-12-2020"=>"CERN closed",
        "30-12-2020"=>"CERN closed",
        "31-12-2020"=>"New Year's Eve",
        "01-01-2021"=>"New year",
        "02-01-2021"=>"CERN closed",
        "03-01-2021"=>"CERN closed",
        "02-04-2021"=>"Good Friday",
        "05-04-2021"=>"Easter Monday",
        "13-05-2021"=>"Ascension day",
        "14-05-2021"=>"Compensation for 1st of May",
        "24-05-2021"=>"Whit Monday",
        "09-09-2021"=>"Jeune genevois",
        "23-12-2021"=>"CERN closed",
        "24-12-2021"=>"Christmas Eve",
        "25-12-2021"=>"Christmas Day",
        "26-12-2021"=>"CERN closed",
        "27-12-2021"=>"CERN closed",
        "28-12-2021"=>"CERN closed",
        "29-12-2021"=>"CERN closed",
        "30-12-2021"=>"CERN closed",
        "31-12-2021"=>"New Year's Eve",
        "01-01-2022"=>"New year",
        "02-01-2022"=>"CERN closed",
        "03-01-2022"=>"CERN closed",
        "04-01-2022"=>"CERN closed",
        "15-04-2022"=>"Good Friday",
        "18-04-2022"=>"Easter Monday",
        "26-05-2022"=>"Ascension day",
        "27-05-2022"=>"Compensation for 1st of May",
        "06-06-2022"=>"Whit Monday",
        "08-09-2022"=>"Jeune Genevois",
        "22-12-2022"=>"Compensation for Christmas Eve",
        "23-12-2022"=>"Compensation for Christmas Day",
        "26-12-2022"=>"CERN closed",
        "27-12-2022"=>"CERN closed",
        "28-12-2022"=>"CERN closed",
        "29-12-2022"=>"CERN closed",
        "30-12-2022"=>"Compensation for New Year's Eve",
        "02-01-2023"=>"Compensation for 1st of January, New year",
        "03-01-2023"=>"CERN closed",
        "04-01-2023"=>"CERN closed",
        "07-04-2023"=>"Good Friday",
        "01-05-2023"=>"1st of May",
        "18-05-2023"=>"Ascension day",
        "29-05-2023"=>"Whit day",
        "07-09-2023"=>"Jeune genevois",
        "25-12-2023"=>"Christmas day",
        "26-12-2023"=>"Compensation for Christmas Eve",
        "27-12-2023"=>"CERN closed",
        "29-12-2023"=>"compensation for 31 December, New Year's Eve",
        "01-01-2024"=>"CERN closed",
        "02-01-2024"=>"CERN closed",
        "03-01-2024"=>"CERN closed",
        "04-01-2024"=>"CERN closed",
        "05-01-2024"=>"CERN closed",     
        "29-03-2024"=>"Good Friday",
        "01-04-2024"=>"New Year",
        "01-05-2024"=>"1st of May",
        "09-05-2024"=>"Ascension day",
        "20-05-2024"=>"Whit Monday",
        "05-09-2024"=>"Jeune Genevois",
        "21-12-2024"=>"CERN closed",
        "22-12-2024"=>"CERN closed",
        "23-12-2024"=>"CERN closed",
        "24-12-2024"=>"Christmas eve",
        "25-12-2024"=>"Christmas",
        "26-12-2024"=>"CERN closed",
        "27-12-2024"=>"CERN closed",
        "28-12-2024"=>"CERN closed",
        "29-12-2024"=>"CERN closed",
        "30-12-2024"=>"CERN closed",
        "31-12-2024"=>"New year's eve",
        "01-01-2025"=>"CERN closed",
        "02-01-2025"=>"CERN closed",
        "03-01-2025"=>"CERN closed",
        "04-01-2025"=>"CERN closed",
        "05-01-2025"=>"CERN closed",   
           
      );
        
        foreach($holidays as $k=>$v){
            $calendar->addDailyHtml( $v, $k, $k );
        }
        
        $calendar->show(true);
        ?>
        <!--CONTACT FORM-->
        <p></p>
        <p>Use the following contact form to request usage of the machine.</p>
        <?php if(@isset($_GET['reply'])){?><p style="color:blue;">Your request has been noted.</p><?} ?>
        <form name="editor" action="<?=$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];?>" method="POST">
          <input type="hidden" name="action" value="add"/>
          <input type="hidden" name="id" value="<?=randomstring();?>"/>
          <label style="display:inline-block;width:140px;">Group(*):</label><input type="text" name="group" size="50"/><br/>
          <label style="display:inline-block;width:140px;">Name:</label><input type="text" name="responsible" size="50"/><br/>
          <label style="display:inline-block;width:140px;">E-mail(*):</label><input type="text" name="email" size="50"/><br/>
          <label style="display:inline-block;width:140px;">Preferred start date(*):</label><input id="startdate" type="text" name="startdate" size="50"/><br/>
          <label style="display:inline-block;width:140px;">Preferred end date(*):</label><input id="enddate" type="text" name="enddate" size="50"/><br/>
          <label style="display:inline-block;width:140px;">Dose rate:</label><input type="text" name="doserate" size="50"/><br/>
          <label style="display:inline-block;width:140px;vertical-align:top;">Comments:</label><textarea name="comments" cols="40" rows="4"></textarea><br/>
          <input name="submit" type="submit" value="Request" /><br>
          * Required fields
          <script language="javascript">
            $(function() {
              $( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd', firstDay: 1}); 
              $( "#enddate" ).datepicker({ dateFormat: 'yy-mm-dd', firstDay: 1}); 
            });
          </script>
        </form>
        </td>
      </tr>     
      <?
      }
      ?>
<?php
?>
</table>

</div>
<?php
  show_footer();
?>
</div>

</body>
</html>

