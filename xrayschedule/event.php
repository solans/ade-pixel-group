<?
include_once("helper.php");

class Event implements Collectible{
  var $id;
  var $startdate;
  var $enddate;
  var $group;
  var $responsible;
  var $email;
  var $doserate;
  var $comments;
  var $live; //if event is visible or not
  var $noshow;
  
  //Constructor
  function Material(){
    $this->__construct(); 
  }
  
  function __construct(){
  }
  
  //GetId
  function GetId(){
    return $this->id;
  }
  
  //GetTag
  function GetTag(){
    return "EVENT";
  }
  
  //Get class name
  function GetClassName(){
    return "Event";
  }
  
  //Dump
  function Dump(){
    foreach(get_object_vars($this) as $v){
      ?><?=$v;?>&nbsp;<?
    }
  }
  
  //Dump to XML
  function ToXml($ident=0){
    $s = str_repeat(" ",$ident);
    $xml = $s."<".strtolower($this->GetTag()).">\n";
    foreach(get_object_vars($this) as $k=>$v){
      $xml .= $s.$s."<".$k.">".$v."</".$k.">\n";
    }
    $xml .= $s."</".strtolower($this->GetTag()).">\n";
    return $xml;
  }
  
  //From array
  function FromArray($arr){
    //print_r($arr);
    foreach($arr as $k=>$v){
      $k=strtolower($k);
      foreach(get_object_vars($this) as $k2=>$v2){
        if($k==$k2){$this->$k2=$v;} 
      }
    }
  }
}
?>