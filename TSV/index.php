<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Through Silicon Vias</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Module devlopment with Through Silicon Vias</p>
<img class="IMAGE" src="TSV-concept.jpg">

<p>
We plan to devise a module integration concept without wire-bonds as technology driver for development of TSV on FEI4
with LETI and direct laser soldering for RDL to flex.
There is an excellent starting point for both technologies through the development of Bonn with IZM, 
MEDIPIX for TSV with LETI and  ALICE on laser soldering. 
The work will be carried out in tight collaboration between CERN, Glasgow, LAL &amp; MPI with LETI and Bonn and IZM.
The goal are FEI4b based demonstrator modules with Sensor + FEI4 with TSV + Laser soldered Flex. 
In parallel we look into technologies to allow wafer-wafer bonding (or die-wafer) to benefit from industrial technologies 
to easy assembly and possibly costs.   
</p>

<p class="SUBTITLE">Documents and links
<ul>
  <li><a href="www-leti.cea.fr">CEA LETI web page</a></li>
  <li><a href="https://indico.cern.ch/event/461330/contribution/4/attachments/1188234/1723801/ModuleConcept-AUW-Nov2015.pdf">H. Pernegger AUW October 2015</a></li>
</ul>
</p>

</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
