<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>HV-CMOS AMS</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">
<?php 	
	show_certificate(); 
?>

<div class="IMAGE">
  <img class="IMAGEE" src="images/AMS.png">
</div>

<p class="TITLE">HVCMOS</p>
Capacitive Coupled Pixel Detector to the FEI4 chip based on the twin-well structure of the standard HV CMOS technology.
The deep n-well in a p-substrate is used as the signal-collecting electrode, which is reversely biased with respect to the substrate. 
The entire CMOS pixel electronics are placed inside the deep n-well. 
PMOS transistors are placed directly inside the deep n-well, 
NMOS transistors are situated in their p-wells that are embedded in the deep n-well as well.

Prototypes exist from v1 to v5 in the AMS H18 process.
HV2FEI4 produced in the AMS H35 process submitted in October 2015.

<p class="SUBTITLE">Tests performed
  <ul>
    <li>Edge TCT measurements</li>
    <li>Characterization of prototypes exposed to 10E16 (1 MeV neq) /cm<sup>2</sup> and 862 Mrad</li>
    <li>Irradiation with 24 GeV PS protons to 3.5e13 p/cm<sup>2</sup></li>
    <li>Leakage current studies on neutron irradiated samples</li>
    <li>Study of surface effects on the sensor electronics with ionizing radiation</li>
    <li>Test beam measurements in CERN North Area at the H8 beam line</li>
  </ul>
</p>

<p class="SUBTITLE">Documentation and links
  <ul>
    <li><a href="https://indico.cern.ch/event/386338/session/5/contribution/38/attachments/1193383/1733296/final_TALENT_Saleh.pdf">
       M.J. Tavassoli, AMS 350 nm HV-CMOS, November 2015</a></li>
    <li><a href="https://indico.cern.ch/event/444057/contribution/4/attachments/1158100/1666053/2014-09-22_CERN_Group_Meeting_-_Simon.pdf">
      Simon Feigl, Group meeting 22/09/2015</a></li>
    <li><a href="https://indico.cern.ch/event/353791/contribution/0/attachments/1186653/1720671/TCAD_AMS_H35.pdf">
      M.J. Tavassoli, AMS 350 nm HV-CMOS TCAD simulation, June 2015</a></li>
    <li><a href="https://indico.cern.ch/event/361445/session/8/contribution/101/attachments/719592/987781/ITK1.pdf">
      M.J. Tavassoli, AMS 350 nm HV-CMOS, February 2015</a></li>
    <li><a href="http://iopscience.iop.org/article/10.1088/1748-0221/9/03/C03020/meta">JINST (2014) C03020</a></li>
    <li><a href="http://pos.sissa.it/archive/conferences/213/280/TIPP2014_280.pdf">Simon Feigl, PoS (TIPP2014) 280</a></li>
    <li><a href="http://indico.cern.ch/event/351695/session/3/contribution/3/attachments/695853/955480/Mandic-HVCMOS-Talk.pdf">I. Mandic, ETCT measurements on HVCMOS sensors</a>
    <li><a href="http://www.sciencedirect.com/science/article/pii/S0168900210026252">NIM A 650 (2011) 158</a></li>
    <li><a href="http://www.sciencedirect.com/science/article/pii/S0168900207015914">NIM A 582 (2007) 876</a></li>
    <li><a href="http://ams.com/eng/Products/Full-Service-Foundry/Process-Technology/High-Voltage-CMOS/0.18-m-HV-CMOS-process">AMS 0.18 &micro;m HV CMOS process</a></li>
    <li><a href="http://ams.com/eng/Products/Full-Service-Foundry/Process-Technology/High-Voltage-CMOS/0.35-m-HV-CMOS-process">AMS 0.35 &micro;m HV CMOS process</a></li>
  </ul>
</p>
 
 
</div>
<?php
	show_footer();
?>
</div>

</body>
</html>