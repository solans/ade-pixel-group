<h2>Add attachment</h2>
<form method="GET" id="add_attachment">
<table>
  <tr><th>PDF</th><td><input id="pdf" type="text" style="width: 500px;"/></td></tr>
  <tr><th>PNG</th><td><input id="png" type="text" style="width: 500px;"/></td></tr>
</table>
<input type="hidden" id="plot_id" value="<?=@$_GET["plot_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_attachment_reply" style="display:inline-block;"></div>

<script>
$("#add_attachment").submit(function(){
  add_attachment();
  return false;
});

function add_attachment(){
  if($("#plot_id").val()=="-1"){
    alert("please register the plot first");
    return;
  }
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_attachment",
      plot_id:$("#plot_id").val(),
      pdf:$("#pdf").val(),
      png:$("#png").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_attachment_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_attachment_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_attachment_reply").text("Stored");
        load_attachments($("#plot_id").val());
      }
    }
  });
}
</script>
