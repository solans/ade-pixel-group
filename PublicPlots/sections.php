<h2>Sections</h2>
<button id="sections_reset">Reset filters</button>
<button id="sections_export">Export</button>
<div id="sections_found" style="display:inline-block"></div>
<table id="sections" class="tablesorter">
	<thead>
		<th class="filter-select" data-placeholder="Search...">Topic</th>
		<th class="filter-select" data-placeholder="Search...">Section</th>
		<th data-placeholder="Search...">Description</th>
		<th>Actions</th>
	</thead>
	<tbody id="sections_body">
	</tbody>
</table>
<div id="sections_reply" style="display:inline-block"></div>

<script>

$(function() {
  $("#sections").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_sections();
});

$("#sections").tablesorter({
  theme: 'blue',
  sortList: [[0,0],[1,0]],
  widgets: ['filter','zebra','output']
});

$("#sections").on("filterEnd",function(){
  $("#sections_found").html("Found: "+($("#sections tr:visible").length-2))
});

$("#sections_export").click(function() {
  $("#sections").trigger("outputTable");
});

$("#sections_reset").click(function() {
  $("#sections").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_sections(){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data:{
      cmd:"get_sections"
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#sections_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["topic_id"]+"</td>";
        tt+="<td>"+row["section_id"]+"</td>";
        tt+="<td>"+row["name"].substr(0,300)+"</td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=view&topic_id="+row["topic_id"]+"&section_id="+row["section_id"]+"\">view</a>";
        tt+="&nbsp;";
        tt+="<a href=\"index.php?page=section&section_id="+row["section_id"]+"\">edit</a>";
        tt+="&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_section("+row["section_id"]+");\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n"; 
        $("#sections_body").append(tt);
      }
      $("#sections").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#sections_found").html("Found: "+($("#sections tr:visible").length-2));
    }
  });
};

function delete_section(section_id){
  if(!window.confirm("Are you sure to delete the section? All the plots in that section will be unavailable.")) return false;
  $("#section_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_section",
      section_id:section_id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#section_reply").text("Something went wrong");
      }else if (reply["affected_rows"]==1){
        $("#section_reply").text("Section deleted");
        load_sections();
      }
    }
  });
  return false;
};
</script>
