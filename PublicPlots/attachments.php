<h2>Attachments</h2>
<button id="attachments_reset">Reset filters</button>
<button id="attachments_export">Export</button>
<div id="attachments_found" style="display:inline-block"></div>
<table id="attachments" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">PDF</th>
		<th data-placeholder="Search...">PNG</th>
		<th>Actions</th>
	</thead>
	<tbody id="attachments_body">
	</tbody>
</table>
<div id="attachments_reply" style="display:inline-block"></div>

<script>

$(function() {
  $("#attachments").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_attachments($("#plot_id").val());
});

$("#attachments").tablesorter({
  theme: 'blue',
  sortList: [[1, 0]],
  widgets: ['filter','zebra','output']
});

$("#attachments").on("filterEnd",function(){
  $("#attachments_found").html("Found: "+($("#attachments tr:visible").length-2))
});

$("#attachments_export").click(function() {
  $("#attachments").trigger("outputTable");
});

$("#attachments_reset").click(function() {
  $("#attachments").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_attachments(plot_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_plot_attachments",
      plot_id:plot_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#attachments_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["pdf"]+"</td>";
        tt+="<td>"+row["png"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=plot&plot_id="+row["attachment_id"]+"&id="+row["id"]+"\">update</a>";
        tt+="&nbsp;"
        tt+="<a href=\"#\" title=\"Delete attachment "+row["attachment_id"]+"\" onclick=\"delete_attachment("+row["attachment_id"]+");\">delete</a>";
        tt+="</td>"; 
        tt+="</tr>\n"; 
        $("#attachments_body").append(tt);
      }
      $("#attachments").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#attachments_found").html("Found: "+($("#attachments tr:visible").length-2));
    }
  });
}
function delete_attachment(attachment_id){
  if(!window.confirm("Are you sure to delete attachment?")) return false;
  $('#attachments_reply').text("");
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_attachment",
      attachment_id:attachment_id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#attachments_reply").text("Something went wrong");
      }else if (reply["affected_rows"]==1){
        $("#attachments_reply").text("Attachment deleted");
        load_attachments($("#plot_id").val());
      }
    }
  });
  return false;
};
</script>
