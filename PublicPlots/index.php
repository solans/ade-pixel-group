<?php
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="<?=$gobase;?>JS/jquery-simple-upload.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<link href="<?=$gobase;?>PublicPlots/publicplots.css" rel="stylesheet" type="text/css" />
<title>Public Plots</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<div class="CONTENT">
<?php
  show_login(); 
?>
<p id="title" class="TITLE">Public plots</p>

<ul>
  <li><a href="?page=view&topic_id=1">MALTA</a></li> 
  <li><a href="?page=view&topic_id=2">MALTA2</a></li>
  <li><a href="?page=view&topic_id=3">Mini-MALTA</a></li>
  <li><a href="?page=view&topic_id=4">Mini-MALTA3</a></li> 
  <li><a href="?page=view_sections">List sections</a></li>
  <li><a href="?page=plots">List plots</a></li>
  <li><a href="?page=view">View plots</a></li>
  <?php if(isAuthorised()){ ?>
  <li><a href="?page=plot&plot_id=-1">Add plot</a></li>
  <li><a href="?page=sections">Edit sections</a></li>
  <?php } ?>
</ul>
 
<?php
if(@$_GET['page']=="view"){
  include("view.php");
}
else if(@$_GET['page']=="add_plot"){
  include("add_plot.php");
}
else if(@$_GET['page']=="plot"){
  include("plot.php");
  include("attachments.php");
  include("add_attachment.php");
  include("upload_attachment.php");
}
else if(@$_GET['page']=="plots"){
  include("plots.php");
  include("add_plot.php");
}
else if(@$_GET['page']=="view_plot"){
  include("view_plot.php");
}
else if(@$_GET['page']=="view_sections"){
  include("view_sections.php");
}
else if(@$_GET['page']=="add_attachment"){
  include("add_attachment.php");
}
else if(@$_GET['page']=="sections"){
  include("sections.php");
  include("section.php");
}
else if(@$_GET['page']=="section"){
  include("section.php");
}
?>



</div>
<?php
  show_footer();
?>
</div>
</body>
</html>

