<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_GET;

if($d["cmd"]=="add_plot"){
  $sql ="INSERT INTO plots ";
  $sql.=" (`topic_id`, `section_id`, `caption`, `author`, `date`, `reference`, `reference_url`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["topic_id"]."',"; 
  $sql.="'".$d["section_id"]."', ";
  $sql.="'".$d["caption"]."', ";
  $sql.="'".$d["author"]."', ";
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["reference"]."', ";
  $sql.="'".$d["reference_url"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_plot"){
  $sql ="UPDATE plots SET ";
  $sql.="`topic_id`='".$d["topic_id"]."', ";
  $sql.="`section_id`='".$d["section_id"]."', ";
  $sql.="`caption`='".$d["caption"]."', ";
  $sql.="`author`='".$d["author"]."', ";
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`reference`='".$d["reference"]."', ";
  $sql.="`reference_url`='".$d["reference_url"]."' ";
  $sql.="WHERE ";
  $sql.="`plot_id`='".$d["plot_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_plot"){
  $sql ="DELETE FROM plots WHERE ";
  $sql.="`plot_id`='".$d["plot_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="add_attachment"){
  $sql ="INSERT INTO attachments ";
  $sql.=" (`plot_id`, `pdf`, `png`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["plot_id"]."',"; 
  $sql.="'".$d["pdf"]."', ";
  $sql.="'".$d["png"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="delete_attachment"){
  $sql ="DELETE FROM attachments WHERE ";
  $sql.="`attachment_id`='".$d["attachment_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_plot_and_attachments"){
  $sql ="DELETE plots.*, attachments.* ";
  $sql.="FROM plots LEFT JOIN attachments ";
  $sql.="ON plots.plot_id=attachments.plot_id ";
  $sql.="WHERE plots.plot_id='".$d["plot_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="add_section"){
  $sql ="INSERT INTO sections ";
  $sql.=" (`topic_id`, `name`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["topic_id"]."',"; 
  $sql.="'".$d["name"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_section"){
  $sql ="UPDATE sections SET ";
  $sql.="`topic_id`='".$d["topic_id"]."', ";
  $sql.="`name`='".$d["name"]."' ";
  $sql.="WHERE ";
  $sql.="`section_id`='".$d["section_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_section"){
  $sql ="DELETE FROM sections WHERE ";
  $sql.="`section_id`='".$d["section_id"]."' "; 
  $sql.=";";
}


echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
  $ret["last_insert_id"]=$conn->insert_id;
}else{
  $ret["error"]=$conn->error;  
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>