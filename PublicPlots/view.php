<div id="view" style="display:inline-block"></div>



<script>

var canEdit=<?php echo (isAuthorised()?"true":"false");?>;

$(function() {
	load_view();
});

function load_view(){
  <?php if(@$_GET["topic_id"] && @$_GET["section_id"]){?>
    tt="<div id='topic_<?=$_GET["topic_id"];?>'>";
    tt+="<h3 id='title_section_<?=$_GET["section_id"];?>'>Section</h3>";
    tt+="<table class='pp-table' id='section_<?=$_GET["section_id"];?>'>\n";
    tt+="</table>";
    tt+="</div>";
    $("#view").append(tt);
    load_plots(<?=$_GET["section_id"];?>);
    set_title_for_topic(<?=$_GET["topic_id"];?>);
    set_title_for_section(<?=$_GET["section_id"];?>);
  <?php }else if(@$_GET["topic_id"]){?>
    tt="<div id='topic_<?=$_GET["topic_id"];?>'>";
    tt+="</div>";
    $("#view").append(tt);
    load_topic(<?=$_GET["topic_id"];?>);
    set_title_for_topic(<?=$_GET["topic_id"];?>);
  <?php }else{ ?>
    load_topics();
  <?php } ?>
};


function load_topics(){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_topics"
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#view").empty();
      for (row of rows){
        tt ="<h2>"+row["name"]+"</h2>";
        tt+="<div id='topic_"+row["topic_id"]+"'>\n";
        tt+="</div>\n"; 
        $("#view").append(tt);
        load_topic(row["topic_id"]);
      }
    }
  }); 
}

function set_title_for_topic(topic_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_topic",
      topic_id:topic_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      for (row of rows){
        $("title").text(row["name"]+" Public Plots");
        $("#title").text(row["name"]+" Public Plots");
      }
    }
  }); 
}

function set_title_for_section(section_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_section",
      section_id:section_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      for (row of rows){
        $("#title_section_"+section_id).text("Section: "+row["name"]);
      }
    }
  }); 
}



function load_topic(topic_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_sections_by_topic",
      topic_id:topic_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#topic_"+topic_id).empty();
      for (row of rows){
        tt ="<h3>"+row["name"]+"&nbsp;<a href='?page=view&topic_id="+topic_id+"&section_id="+row["section_id"]+"'>link</a></h3>";
        tt+="<table class='pp-table' id='section_"+row["section_id"]+"'>\n";
        tt+="</table>\n"; 
        $("#topic_"+topic_id).append(tt);
        load_plots(row["section_id"]);
      }
    }
  }); 
}

function load_plots(section_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_plots_by_section",
      section_id:section_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#section_"+section_id).empty();
      for (plot of rows){
        tt="<tr>\n";
        tt+="<td>\n";
        if(canEdit){
			tt+="<a href=\"index.php?page=plot&plot_id="+plot["plot_id"]+"\">edit</a>&nbsp;";
			tt+="<a href=\"#\" title=\"Delete plot "+plot["plot_id"]+"\" onclick=\"delete_plot("+plot["plot_id"]+");\">delete</a>&nbsp";
        }
        tt+="<a href=\"index.php?page=view_plot&plot_id="+plot["plot_id"]+"\">view</a>";
        tt+="<br/>";
        tt+="<b>Caption:</b>&nbsp;"+plot["caption"]+"<br/>";
        tt+="<b>Contact:</b>&nbsp;<b>"+plot["author"]+"</b><br/>";
        tt+="<b>Reference:</b>&nbsp;<a href='"+plot["reference_url"]+"'>"+plot["reference"]+"</a><br/>";
        tt+="<b>Date:</b>&nbsp;<a href='"+plot["reference_url"]+"'>"+plot["date"]+"</a>";
        tt+="</td>\n";
        tt+="<td id='plot_"+plot["plot_id"]+"' style='text-align:center;'>\n";
        tt+="</td>\n";
        tt+="</tr>\n"; 
        $("#section_"+section_id).append(tt);
        load_attachments(plot["plot_id"]);
      }
    }
  });
}

function load_attachments(plot_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_attachments",
      plot_id:plot_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#plot_"+plot_id).empty();
      for (plot of rows){
        tt="<img style='width:200px' src='<?=$gobase;?>PublicPlots/attachments/"+plot["png"]+"'>\n";
        tt+="<br/>\n"; 
        tt+="<a href='<?=$gobase;?>PublicPlots/attachments/"+plot["pdf"]+"'>pdf</a>";
        $("#plot_"+plot_id).append(tt);
      }
    }
  });
}

function delete_plot(plot_id){
  if(!window.confirm("Are you sure to delete plot?")) return false;
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_plot",
      plot_id:plot_id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
		console.log("Something went wrong.");
	  }else if (reply["affected_rows"]==1){
        //load_view();
      }

    }
  });
  return false;
};

</script>
