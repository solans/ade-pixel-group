<h2>Plots</h2>
<button id="plots_reset">Reset filters</button>
<button id="plots_export">Export</button>
<div id="plots_found" style="display:inline-block"></div>
<table id="plots" class="tablesorter">
	<thead>
		<th class="filter-select" data-placeholder="Search...">Topic</th>
		<th class="filter-select" data-placeholder="Search...">Section</th>
		<th data-placeholder="Search...">Caption</th>
		<th data-placeholder="Search...">Author</th>
		<th data-placeholder="Search...">Reference</th>
		<th data-placeholder="Search...">Date</th>
		<th>Plots</th>
		<th>Actions</th>
	</thead>
	<tbody id="plots_body">
	</tbody>
</table>
<div id="plots_reply" style="display:inline-block"></div>

<script>

var canEdit=<?php echo (isAuthorised()?"true":"false");?>;


$(function() {
  $("#plots").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_plots();
});

$("#plots").tablesorter({
  theme: 'blue',
  sortList: [[0,0],[1,0]],
  widgets: ['filter','zebra','output']
});

$("#plots").on("filterEnd",function(){
  $("#plots_found").html("Found: "+($("#plots tr:visible").length-2))
});

$("#plots_export").click(function() {
  $("#plots").trigger("outputTable");
});

$("#plots_reset").click(function() {
  $("#plots").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

$(".plotter").css("text-align","center");

function load_plots(){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data:{
      cmd:"get_plots"
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#plots_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["topic"]+"</td>";
        tt+="<td>"+row["section"]+"</td>";
        tt+="<td>"+row["caption"].substr(0,300)+"</td>";
        tt+="<td>"+row["author"]+"</td>";
        tt+="<td>"+row["reference"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td class='plotter' id='plot_"+row["plot_id"]+"'></td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=view_plot&plot_id="+row["plot_id"]+"\">view</a>";
        if(canEdit){
			tt+="&nbsp;";
        	tt+="<a href=\"index.php?page=plot&plot_id="+row["plot_id"]+"\">edit</a>";
        	tt+="&nbsp;";
        	tt+="<a href=\"#\" onclick=\"delete_plot("+row["plot_id"]+");\">delete</a>";
        }
    	tt+="</td>";
        tt+="</tr>\n"; 
        $("#plots_body").append(tt);
		load_attachments(row["plot_id"]);
      }
      $("#plots").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#plots_found").html("Found: "+($("#plots tr:visible").length-2));
    }
  });
};

function delete_plot(plot_id){
  if(!window.confirm("Are you sure to delete plot?")) return false;
  $("#plots_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_plot_and_attachments",
      plot_id:plot_id
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#plots_reply").text("Something went wrong");
      }else if (reply["affected_rows"]==1){
        $("#plots_reply").text("Plot deleted");
        load_plots();
      }
    }
  });
  return false;
};

function load_attachments(plot_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_attachments",
      plot_id:plot_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#plot_"+plot_id).empty();
      for (plot of rows){
        tt="<img style='width:100px' src='<?=$gobase;?>PublicPlots/attachments/"+plot["png"]+"'>\n";
        tt+="<br/>\n"; 
        tt+="<a href='<?=$gobase;?>PublicPlots/attachments/"+plot["pdf"]+"'>pdf</a>";
        $("#plot_"+plot_id).append(tt);
      }
    }
  });
}

</script>
