<h2>Plot</h2>
<form method="GET" id="plot">
<table>
  <tr><th>Topic</th><td><select id="topic_id"></select></td></tr>
  <tr><th>Section</th><td><select id="section_id"></select></td></tr>
  <tr><th>Caption</th><td><textarea id="caption" style="width:800px;height:150px;"></textarea></td></tr>
  <tr><th>Author</th><td><input id="author" type="text" style="width:800px;"/></td></tr>
  <tr><th>Date</th><td><input id="date" style="width:800px;"/></td></tr>
  <tr><th>Reference</th><td><input id="reference" type="text" style="width:800px;"/></td></tr>
  <tr><th>URL</th><td><input id="reference_url" type="text" style="width:800px;"/></td></tr>
</table>
<input type="hidden" id="plot_id" value="<?=@$_GET["plot_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_plot_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#date").datepicker({dateFormat:"yy-mm-dd",firstDay: 1});
 load_topics();
 params = new URLSearchParams(window.location.href);
 if(params.has("plot_id") && params.get("plot_id")!="-1"){
   setTimeout(check_load_plot,100);
 }
});

$("#plot").submit(function(){
  if($("#plot_id").val()=="-1"){alert("add new plot?");add_plot();}
  else{alert("update plot "+$("#plot_id").val()+"?");update_plot();}
  return false;
});

$("#topic_id").on("change",function(){select_sections($("#topic_id").val())});

function add_plot(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_plot",
      topic_id:$("#topic_id").val(),
      section_id:$("#section_id").val(),
      caption:$("#caption").val(),
      author:$("#author").val(),
      date:$("#date").val(),
      reference:$("#reference").val(),
      reference_url:$("#reference_url").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_plot_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_plot_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_plot_reply").text("Stored");
        if(typeof load_plots === 'function'){
          load_plots();
        }
        $("#plot_id").val(reply["last_insert_id"]);
      }
    }
  });
}

function update_plot(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_plot",
      plot_id:$("#plot_id").val(),
      topic_id:$("#topic_id").val(),
      section_id:$("#section_id").val(),
      caption:$("#caption").val(),
      author:$("#author").val(),
      date:$("#date").val(),
      reference:$("#reference").val(),
      reference_url:$("#reference_url").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_plot_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_plot_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_plot_reply").text("Stored");
      }
    }
  });
}

var topics=[];
var sections=[];

function load_sections(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbread.php",
    type: "get",
    data: {
      cmd:"get_sections"
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      for(section of reply){
        sections.push({"topic_id":section["topic_id"],"section_id":section["section_id"],"name":section["name"]});
      }
      select_topic_and_section("1","0")
    }
  });
}

function load_topics(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbread.php",
    type: "get",
    data: {
      cmd:"get_topics"
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      for(topic of reply){
        topics.push({"topic_id":topic["topic_id"],"name":topic["name"]});        
      }
      load_sections();
    }
  });
}

function select_topic_and_section(topic_id, section_id){

  $("#topic_id").html("");
  for(topic of topics){
    $("#topic_id").append('<option value="'+topic["topic_id"]+'"'+(topic["topic_id"]==topic_id?'selected':'')+'>'+topic["name"]+'</option>');
  }
  
  $("#section_id").html("");
  for(section of sections){
    if(section["topic_id"]!=topic_id) continue;
    $("#section_id").append('<option value="'+section["section_id"]+'"'+(section["section_id"]==section_id?'selected':'')+'>'+section["name"]+'</option>');
  }

}

function select_sections(topic_id){
  $("#section_id").html("");
  for(section of sections){
    if(section["topic_id"]!=topic_id) continue;
    $("#section_id").append('<option value="'+section["section_id"]+'"'+(section["section_id"]==section_id?'selected':'')+'>'+section["name"]+'</option>');
  }
}

function check_load_plot(){
  console.log("check_load_plot");
  if(topics.length==0) return 0;
  console.log("load_plot");
  load_plot();
}

function load_plot(){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_plot",
      plot_id:$("#plot_id").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      plot=reply[0];
      //$("#topic_id").val(plot["topic_id"]);
      //$("#section_id").val(plot["section_id"]);
      select_topic_and_section(plot["topic_id"],plot["section_id"]);
      $("#caption").val(plot["caption"]);
      $("#author").val(plot["author"]);
      $("#date").val(plot["date"]);
      $("#reference").val(plot["reference"]);
      $("#reference_url").val(plot["reference_url"]);
    }
  });
}
</script>
