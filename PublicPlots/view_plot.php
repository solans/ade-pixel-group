<div id="view" style="display:inline-block">
<div>
<h2 id="topic_title">Topic</h2>
<h3 id="section_title">Section</h3>
<table class='pp-table' id="section">
</table>
</div>
</div>

<script>

$(function() {
  load_plot(<?=$_GET["plot_id"];?>);
});

function load_plot(plot_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_plot",
      plot_id:plot_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#section").empty();
      for (plot of rows){
        tt="<tr>\n";
        tt+="<td>\n";
        tt+="<b>Caption:</b>&nbsp;"+plot["caption"]+"<br/>";
        tt+="<b>Contact:</b>&nbsp;<b>"+plot["author"]+"</b><br/>";
        tt+="<b>Reference:</b>&nbsp;<a href='"+plot["reference_url"]+"'>"+plot["reference"]+"</a><br/>";
        tt+="<b>Date:</b>&nbsp;<a href='"+plot["reference_url"]+"'>"+plot["date"]+"</a>";
        tt+="</td>\n";
        tt+="<td id='plot_"+plot["plot_id"]+"' style='text-align:center;'>\n";
        tt+="</td>\n";
        tt+="</tr>\n"; 
        $("#topic_title").html("Topic:&nbsp;"+plot["topic"]);
        $("#section_title").html("Section:&nbsp;"+plot["section"]);
        $("#section").append(tt);
        load_attachments(plot["plot_id"]);
      }
    }
  });
}

function load_attachments(plot_id){
  $.ajax({
    url: '<?=$gobase;?>/PublicPlots/dbread.php',
    type: 'get',
    data: {
      cmd:"get_attachments",
      plot_id:plot_id
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#plot_"+plot_id).empty();
      for (plot of rows){
        tt="<img style='width:200px' src=<?=$gobase;?>/PublicPlots/attachments/"+plot["png"]+">\n";
        tt+="<br/>\n"; 
        tt+="<a href='<?=$gobase;?>/PublicPlots/attachments/"+plot["pdf"]+"'>pdf</a>";
        $("#plot_"+plot_id).append(tt);
      }
    }
  });
}

</script>
