<?php
//echo "hello";
include_once("includes.php");

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  die("Connection failed: " . mysqli_connect_error());
}
//echo "open";

$d=$_GET;

if($d["cmd"]=="get_plot"){
  $sql ="SELECT *, sections.name AS section, topics.name AS topic FROM plots ";
  $sql.="INNER JOIN sections ON plots.section_id=sections.section_id ";
  $sql.="INNER JOIN topics ON plots.topic_id=topics.topic_id "; 
  $sql.="WHERE plot_id='".$d["plot_id"]."' ";
  $sql.=";";
}
else if($d["cmd"]=="get_plots"){
  $sql ="SELECT *, sections.name AS section, topics.name AS topic FROM plots ";
  $sql.="INNER JOIN sections ON plots.section_id=sections.section_id ";
  $sql.="INNER JOIN topics ON plots.topic_id=topics.topic_id "; 
  $sql.=";";
  echo $sql;
}
else if($d["cmd"]=="get_plots_by_section"){
  $sql ="SELECT * FROM plots WHERE section_id='".$d["section_id"]."' ORDER BY date;";
}
else if($d["cmd"]=="get_plots_by_topic"){
  $sql ="SELECT * FROM plots WHERE topic_id='".$d["topic_id"]."';";
}
else if($d["cmd"]=="get_sections_by_topic"){
  $sql ="SELECT * FROM sections WHERE topic_id='".$d["topic_id"]."';";
}
else if($d["cmd"]=="get_plot_attachments"){
  $sql ="SELECT * FROM attachments WHERE plot_id='".$d["plot_id"]."';";
}
else if($d["cmd"]=="get_attachments"){
  $sql ="SELECT * FROM attachments WHERE plot_id='".$d["plot_id"]."';";
}
else if($d["cmd"]=="get_sections"){
  $sql ="SELECT * FROM sections;";
}
else if($d["cmd"]=="get_topics"){
  $sql ="SELECT * FROM topics;";
}
else if($d["cmd"]=="get_topic"){
  $sql ="SELECT * FROM topics WHERE topic_id='".$d["topic_id"]."';";
}
else if($d["cmd"]=="get_section"){
  $sql ="SELECT * FROM sections WHERE section_id='".$d["section_id"]."';";
}
  
//echo $sql;
$result=$conn->query($sql);
$ret = array();
while($row = $result->fetch_assoc()) {
  //$ret[] = $row;
  $row2=array();
  foreach($row as $k=>$v){
    //$row2[$k]=htmlentities($v,ENT_COMPAT,'ISO-8859-1', true);
    $row2[$k]=mb_convert_encoding($v,"UTF-8","ISO-8859-1");
    //$row2[$k]=utf8_encode($v);
    //$row2[$k]=$v;
  }
  $ret[]=$row2;
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>