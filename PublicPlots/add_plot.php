<h2>Add plot</h2>
<form method="GET" id="add_plot">
<table>
  <tr><th>Topic</th><td><select id="topic_id"></select></td></tr>
  <tr><th>Section</th><td><select id="section_id"></select></td></tr>
  <tr><th>Caption</th><td><textarea id="caption" style="width:500px;height:200px;"></textarea></td></tr>
  <tr><th>Author</th><td><input id="author" type="text" style="width: 500px;"/></td></tr>
  <tr><th>Reference</th><td><input id="reference" type="text" style="width: 500px;"/></td></tr>
  <tr><th>URL</th><td><input id="reference_url" type="text" style="width: 500px;"/></td></tr>
</table>
<input type="hidden" id="plot_id" value="<?=@$_GET["plot_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_plot_reply" style="display:inline-block;"></div>

<script>
$(function() {
 $("#plot_date").datepicker({dateFormat:"yy-mm-dd"});
 load_sections();
 load_topics();
});

$("#add_plot").submit(function(){
  if($("#plot_id").val()){update_plot();}
  else{add_plot();}
  return false;
});

function add_plot(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_plot",
      topic_id:$("#topic_id").val(),
      section_id:$("#section_id").val(),
      caption:$("#caption").val(),
      author:$("#author").val(),
      reference:$("#reference").val(),
      reference_url:$("#reference_url").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_plot_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_plot_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_plot_reply").text("Stored");
        if(typeof load_plots === 'function'){
          load_plots();
        }
      }
    }
  });
}

function update_plot(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_plot",
      plot_id:$("#plot_id").val(),
      topic_id:$("#topic_id").val(),
      section_id:$("#section_id").val(),
      caption:$("#caption").val(),
      author:$("#author").val(),
      reference:$("#reference").val(),
      reference_url:$("#reference_url").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_plot_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_plot_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_plot_reply").text("Stored");
      }
    }
  });
}

function load_sections(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbread.php",
    type: "get",
    data: {
      cmd:"get_sections"
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#section_id").html("");
      for(section of reply){
        $("#section_id").append('<option value="'+section["section_id"]+'">'+section["name"]+'</option>');
      }
    }
  });
}

function load_topics(){
  $.ajax({
    url: "<?=$gobase;?>/PublicPlots/dbread.php",
    type: "get",
    data: {
      cmd:"get_topics"
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#topic_id").html("");
      for(topic of reply){
        $("#topic_id").append('<option value="'+topic["topic_id"]+'">'+topic["name"]+'</option>');
      }
    }
  });
}
</script>
