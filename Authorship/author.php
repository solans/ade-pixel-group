<h2>Author</h2>
<form method="GET" id="author">
<table>
  <tr><th>First Name</th><td><input type="text" id="firstname"></td></tr>
  <tr><th>Last Name</th><td><input type="text" id="lastname"></td></tr>
  <tr><th>Initials</th><td><input type="text" id="initials"></td></tr>
<!--
    <tr><th>Topic</th><td>
    <input type="checkbox" id="topics_1" name="topics" value="PL"/><label for="topics_1">PL</label>
    <input type="checkbox" id="topics_2" name="topics" value="Supervision"/><label for="topics_2">Supervision</label>
    <input type="checkbox" id="topics_3" name="topics" value="Design"/><label for="topics_3">Design</label>
    <input type="checkbox" id="topics_4" name="topics" value="Simulation"/><label for="topics_4">Simulation</label>
    <input type="checkbox" id="topics_5" name="topics" value="Readout"/><label for="topics_5">Readout</label>
    <input type="checkbox" id="topics_6" name="topics" value="Testing"/><label for="topics_6">Testing</label>
    <input type="checkbox" id="topics_7" name="topics" value="Analysis"/><label for="topics_7">Analysis</label>
    <input type="checkbox" id="topics_8" name="topics" value="MonoPix"/><label for="topics_8">MonoPix</label>
    <input type="checkbox" id="topics_9" name="topics" value="CASSIA"/><label for="topics_9">CASSIA</label>
    <input type="hidden" id="topics" value="">
  </td></tr>
-->
  <tr><th>Comments</th><td><input type="text" id="comments"/></td></tr>  
</table>
<input type="hidden" id="author_id" value="<?=@$_GET["author_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="author_reply" style="display:inline-block;"></div>

<script>
$(function() {
  if($("#author_id").val()!=""){load_author();}
});
  
$("#author").submit(function(){
  //arr=$("input[name=topics]:checked").map(function(){return this.getAttribute("value");}).get().toString();
  //$("#topics").val(arr);
  if($("#author_id").val()==""){add_author();}
  else{update_author();}
  return false;
});

function load_author(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_author",
      author_id:$("#author_id").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      row=reply[0];
      $("#firstname").val(row["firstname"]);
      $("#lastname").val(row["lastname"]);
      $("#initials").val(row["initials"]);
      //$("#topics").val(row["topics"]);
      //$.each($("#topics").val().split(","),function(k,v){$("input[value="+v+"]").prop("checked",true);})
      $("#comments").val(row["comments"]);
    }
  });
}

function add_author(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_author",
      firstname:$("#firstname").val(),
      lastname:$("#lastname").val(),
      initials:$("#initials").val(),
      //topics:$("#topics").val(),
      comments:$("#comments").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Stored";
        if(typeof load_authors === 'function'){load_authors();}
        //$("#author_id").val(reply["last_insert_id"]);
        $("#author").trigger("reset");
      }
      $("#author_reply").text(msg);
    }
  });
}

function update_author(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_author",
      author_id:$("#author_id").val(),
      firstname:$("#firstname").val(),
      lastname:$("#lastname").val(),
      initials:$("#initials").val(),
      //topics:$("#topics").val(),
      comments:$("#comments").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Updated";
        if(typeof load_authors === 'function'){load_authors();}
      }
      $("#author_reply").text(msg);
    }
  });
}
</script>
