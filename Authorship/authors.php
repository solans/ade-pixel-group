<h2>Authors</h2>
<button id="authors_reset">Reset filters</button>
<button id="authors_export">Export</button>
<div id="authors_found" style="display:inline-block"></div>
<table id="authors" class="tablesorter">
	<thead>
		<th>First Name</th>
		<th>Initials</th>
		<th>Last Name</th>
		<!--<th>Topics</th>-->
		<th>Comments</th>
	  <th>Actions</th>
	</thead>
	<tbody id="authors_body">
	</tbody>
</table>
<div id="authors_reply" style="display:inline-block"></div>

<script>

$(function() {
  $("#authors").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_authors();
});

$("#authors").tablesorter({
  theme: 'blue',
  sortList: [[0,0],[1,0]],
  widgets: ['filter','zebra','output']
});

$("#authors").on("filterEnd",function(){
  $("#authors_found").html("Found: "+($("#authors tr:visible").length-2))
});

$("#authors_export").click(function() {
  $("#authors").trigger("outputTable");
});

$("#authors_reset").click(function() {
  $("#authors").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_authors(){
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbread.php',
    type: 'get',
    data:{
      cmd:"get_authors"
    },
    success: function(data) {
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#authors_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["firstname"]+"</td>";
        tt+="<td>"+row["initials"]+"</td>";
        tt+="<td>"+row["lastname"]+"</td>";
        //tt+="<td>"+row["topics"]+"</td>";
        tt+="<td>"+row["comments"].substr(0,300)+"</td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=author&author_id="+row["author_id"]+"\">edit</a>";
        tt+="&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_author("+row["author_id"]+");\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n"; 
        $("#authors_body").append(tt);
      }
      $("#authors").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#authors_found").html("Found: "+($("#authors tr:visible").length-2));
    }
  });
};

function delete_author(author_id){
  if(!window.confirm("Are you sure to delete the author?")) return false;
  $("#authors_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_author",
      author_id:author_id
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Deleted";
        if(typeof load_authors === 'function'){load_authors();}
      }
      $("#authors_reply").text(msg);
    }
  });
  return false;
};
</script>
