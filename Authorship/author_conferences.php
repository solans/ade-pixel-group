<h2>Author conferences</h2>
<button id="author_conferences_reset">Reset filters</button>
<button id="author_conferences_export">Export</button>
<div id="author_conferences_found" style="display:inline-block"></div>
<table id="author_conferences" class="tablesorter">
	<thead>
		<th class="filter-select" data-placeholder="Search...">Conference</th>
		<th class="filter-select" data-placeholder="Search...">Start date</th>
		<th class="filter-select" data-placeholder="Search...">Venue</th>
		<th>Conference webpage</th>
		<th>Contribution</th>
		<th>Actions</th>
	</thead>
	<tbody id="author_conferences_body">
	</tbody>
</table>
<div id="author_conferences_reply" style="display:inline-block"></div>

<script>

$(function() {
  $("#author_conferences").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_author_conferences();
});

$("#author_conferences").tablesorter({
  theme: 'blue',
  sortList: [[0,0],[1,0]],
  widgets: ['filter','zebra','output']
});

$("#author_conferences").on("filterEnd",function(){
  $("#author_conferences_found").html("Found: "+($("#author_conferences tr:visible").length-2))
});

$("#author_conferences_export").click(function() {
  $("#author_conferences").trigger("outputTable");
});

$("#author_conferences_reset").click(function() {
  $("#author_conferences").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_author_conferences(){
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbread.php',
    type: 'get',
    data:{
      cmd:"get_conferences_by_author",
      author_id:$("#author_id").val()
    },
    success: function(data) {
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#author_conferences_body").empty();
      for (row of rows){
        tt="<tr>\n";
				tt+="<td>"+row["name"]+"</td>";
        tt+="<td>"+row["startdate"]+"</td>";
        tt+="<td>"+row["venue"]+"</td>";
        tt+="<td>"+row["webpage"]+"</td>";
        tt+="<td>"+row["contribution"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=conference&conference_id="+row["conference_id"]+"\">edit</a>";
        tt+="&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_conference("+row["conference_id"]+");\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n"; 
        $("#author_conferences_body").append(tt);
      }
      $("#author_conferences").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#author_conferences_found").html("Found: "+($("#author_conferences tr:visible").length-2));
    }
  });
};

function delete_conference(affiliation_id){
  if(!window.confirm("Are you sure to delete the affiliation?")) return false;
  $("#conferences_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_conference",
      conference_id:conference_id
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Deleted";
        if(typeof load_conferences === 'function'){load_conferences();}
      }
      $("#conferences_reply").text(msg);
    }
  });
  return false;
};
</script>
