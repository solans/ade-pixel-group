<h2>Institute</h2>
<form method="GET" id="institute">
<table>
  <tr><th>Name</th><td><input type="text" id="name"></td></tr>
</table>
<input type="hidden" id="institute_id" value="<?=@$_GET["institute_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="institute_reply" style="display:inline-block;"></div>

<script>
$(function() {
  if($("#institute_id").val()!=""){load_institute();}
});
  
$("#institute").submit(function(){
  if($("#institute_id").val()==""){add_institute();}
  else{update_institute();}
  return false;
});

function load_institute(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_institute",
      institute_id:$("#institute_id").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      row=reply[0];
      $("#name").val(row["name"]);
    }
  });
}

function add_institute(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_institute",
      name:$("#name").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Stored";
        if(typeof load_institutes === 'function'){load_institutes();}
        //$("#institute_id").val(reply["last_insert_id"]);
      }
      $("#institute_reply").text(msg);
    }
  });
}

function update_institute(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_institute",
      institute_id:$("#institute_id").val(),
      name:$("#name").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Updated";
        if(typeof load_institutes === 'function'){load_institutes();}
      }
      $("#institute_reply").text(msg);
    }
  });
}
</script>
