<h2>Author conference</h2>
<form method="GET" id="author_conference">
<table>
  <tr><th>Conference</th><td><input type="text" id="name"></td></tr>
  <tr><th>Start date</th><td><input type="text" id="startdate"></td></tr>
  <tr><th>Venue</th><td><input type="text" id="venue"></td></tr>
  <tr><th>Webpage</th><td><input type="text" id="webpage"></td></tr>
  <tr><th>Contribution</th><td>
    <select id="contribution">
			<option value=""></option>
			<option value="oral">Oral</option>
			<option value="poster">Poster</option>
		</select>
	</td></tr>
  <tr><th>Title</th><td><input type="text" id="title"></td></tr>
  <tr><th>Abstract</th><td><textarea id="abstract"></textarea></tr>
</table>
<input type="hidden" id="author_conference_id" value="<?=@$_GET["author_conference_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="author_conference_reply" style="display:inline-block;"></div>

<script>
$(function() {
  $("#startdate").datepicker({dateFormat:'yy-mm-dd'});
});
  
$("#author_conference").submit(function(){
  add_author_conference();
  return false;
});

function add_author_conference(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_conference",
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      topics:$("#topics").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Stored";
        if(typeof load_author_conferences === 'function'){load_author_conferences();}
        $("#author_conference").trigger("reset");
      }
      $("#author_conference_reply").text(msg);
    }
  });
}

</script>
