<h2>Conference</h2>
<button onclick="load_authors_on_date()">Only allowed</button>&nbsp;
<button onclick="load_authors()">All</button>
<form method="GET" id="conference">
<table>
  <tr><th>Conference</th><td><input type="text" id="name"></td></tr>
  <tr><th>Start date</th><td><input type="text" id="startdate"></td></tr>
  <tr><th>Venue</th><td><input type="text" id="venue" style="width:500px;"></td></tr>
  <tr><th>Webpage</th><td><input type="text" id="webpage" style="width:500px;"></td></tr>
  <tr><th>Author</th><td><select id="author_id"></select>&nbsp;</td></tr>
  <tr><th>Contribution</th><td>
    <select id="contribution">
			<option value=""></option>
			<option value="oral">Oral</option>
			<option value="poster">Poster</option>
		</select>
	</td></tr>
  <tr><th>Title</th><td><input type="text" id="title" style="width:500px;"></td></tr>
  <tr><th>Abstract</th><td><textarea id="abstract" style="width:500px;height:300px;"></textarea></tr>
  <tr><th>Material</th><td><input type="text" id="material" style="width:500px;"></td></tr>
  <tr><th>Proceedings</th><td><input type="text" id="proceedings" style="width:500px;"></td></tr>
</table>
<input type="hidden" id="conference_id" value="<?=@$_GET["conference_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="conference_reply" style="display:inline-block;"></div>

<script>
$(function() {
  $("#startdate").datepicker({dateFormat:'yy-mm-dd'});
  load_authors(load_conference);
});
  
$("#conference").submit(function(){
  arr=$("input[name=topics]:checked").map(function(){return this.getAttribute("value");}).get().toString();
  $("#topics").val(arr);
  if($("#conference_id").val()==""){add_conference();}
  else{update_conference();}
  return false;
});

function load_authors(callback){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_authors"
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      tt="";
			tt+="<option value=\"\"></option>\n";
      for(row of reply){
        tt+="<option value='"+row["author_id"]+"'>"+row["firstname"]+" "+row["lastname"]+"</option>\n";
      }
      $("#author_id").html(tt);
      if (typeof callback === 'function'){callback();}
		}
  }); 
}

function load_authors_on_date(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_authorlist_on_date",
      date:$("#startdate").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      tt="";
			tt+="<option value=\"\"></option>\n";
      for(row of reply){
        tt+="<option value='"+row["author_id"]+"'>"+row["firstname"]+" "+row["lastname"]+"</option>\n";
      }
      $("#author_id").html(tt);
    }
  });
	return false;
}

function load_conference(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_conference",
      conference_id:$("#conference_id").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      row=reply[0];
      $("#name").val(row["name"]);
      $("#startdate").val(row["startdate"]);
      $("#venue").val(row["venue"]);
      $("#webpage").val(row["webpage"]);
      $("#contribution").val(row["contribution"]);
      $("#author_id").val(row["author_id"]);
      $("#title").val(row["title"]);
      $("#abstract").val(row["abstract"]);
      $("#material").val(row["material"]);
      $("#proceedings").val(row["proceedings"]);
		}
  });
}

function add_conference(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_conference",
      name:$("#name").val(),
      startdate:$("#startdate").val(),
      venue:$("#venue").val(),
      webpage:$("#webpage").val(),
      contribution:$("#contribution").val(),
      author_id:$("#author_id").val(),
      title:$("#title").val(),
      abstract:$("#abstract").val(),
      material:$("#material").val(),
      proceedings:$("#proceedings").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Stored";
        if(typeof load_conferences === 'function'){load_conferences();}
      }
      $("#conference_reply").text(msg);
      //$("#conference").trigger("reset");
    }
  });
}

function update_conference(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_conference",
      conference_id:$("#conference_id").val(),
      name:$("#name").val(),
      startdate:$("#startdate").val(),
      venue:$("#venue").val(),
      webpage:$("#webpage").val(),
      contribution:$("#contribution").val(),
      author_id:$("#author_id").val(),
      title:$("#title").val(),
      abstract:$("#abstract").val(),
      material:$("#material").val(),
      proceedings:$("#proceedings").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Updated";
        if(typeof load_conferences === 'function'){load_conferences();}
      }
      $("#conference_reply").text(msg);
    }
  });
}
</script>
