<h2>Affiliations</h2>
<button id="affiliations_reset">Reset filters</button>
<button id="affiliations_export">Export</button>
<div id="affiliations_found" style="display:inline-block"></div>
<table id="affiliations" class="tablesorter">
	<thead>
		<th class="filter-select" data-placeholder="Search...">First name</th>
		<th class="filter-select" data-placeholder="Search...">Last name</th>
		<th class="filter-select" data-placeholder="Search...">Institute</th>
		<th>Start date</th>
		<th>End date</th>
		<th>Details</th>
		<th>Topics</th>
		<th>Actions</th>
	</thead>
	<tbody id="affiliations_body">
	</tbody>
</table>
<div id="affiliations_reply" style="display:inline-block"></div>

<script>

$(function() {
  $("#affiliations").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_affiliations();
});

$("#affiliations").tablesorter({
  theme: 'blue',
  sortList: [[0,0],[1,0]],
  widgets: ['filter','zebra','output']
});

$("#affiliations").on("filterEnd",function(){
  $("#affiliations_found").html("Found: "+($("#affiliations tr:visible").length-2))
});

$("#affiliations_export").click(function() {
  $("#affiliations").trigger("outputTable");
});

$("#affiliations_reset").click(function() {
  $("#affiliations").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_affiliations(){
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbread.php',
    type: 'get',
    data:{
      cmd:"get_affiliations"
    },
    success: function(data) {
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#affiliations_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["firstname"]+"</td>";
        tt+="<td>"+row["lastname"]+"</td>";
        tt+="<td>"+row["name"]+"</td>";
        tt+="<td>"+row["startdate"]+"</td>";
        tt+="<td>"+row["enddate"]+"</td>";
        tt+="<td>"+row["topics"]+"</td>";
        tt+="<td>"+row["details"].substr(0,50)+"</td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=author&author_id="+row["author_id"]+"\">author</a>";
        tt+="&nbsp;";
        tt+="<a href=\"index.php?page=affiliation&affiliation_id="+row["affiliation_id"]+"\">edit</a>";
        tt+="&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_affiliation("+row["affiliation_id"]+");\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n"; 
        $("#affiliations_body").append(tt);
      }
      $("#affiliations").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#affiliations_found").html("Found: "+($("#affiliations tr:visible").length-2));
    }
  });
};

function delete_affiliation(affiliation_id){
  if(!window.confirm("Are you sure to delete the affiliation?")) return false;
  $("#affiliations_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_affiliation",
      affiliation_id:affiliation_id
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Deleted";
        if(typeof load_affiliations === 'function'){load_affiliations();}
      }
      $("#affiliations_reply").text(msg);
    }
  });
  return false;
};
</script>
