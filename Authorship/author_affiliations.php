<h2>Author Affiliations</h2>
<button id="author_affiliations_reset">Reset filters</button>
<button id="author_affiliations_export">Export</button>
<div id="author_affiliations_found" style="display:inline-block"></div>
<table id="author_affiliations" class="tablesorter">
	<thead>
		<th>Start date</th>
		<th>End date</th>
		<th class="filter-select" data-placeholder="Search...">Institute</th>
		<th>Topics</th>
		<th>Details</th>
		<th>Actions</th>
	</thead>
	<tbody id="author_affiliations_body">
	</tbody>
</table>
<div id="author_affiliations_reply" style="display:inline-block"></div>

<script>

$(function() {
  $("#author_affiliations").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_author_affiliations();
});

$("#author_affiliations").tablesorter({
  theme: 'blue',
  sortList: [[0,0],[1,0]],
  widgets: ['filter','zebra','output']
});

$("#author_affiliations").on("filterEnd",function(){
  $("#author_affiliations_found").html("Found: "+($("#author_affiliations tr:visible").length-2))
});

$("#author_affiliations_export").click(function() {
  $("#author_affiliations").trigger("outputTable");
});

$("#author_affiliations_reset").click(function() {
  $("#author_affiliations").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_author_affiliations(){
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbread.php',
    type: 'get',
    data:{
      cmd:"get_affiliations_by_author",
      author_id:$("#author_id").val()
    },
    success: function(data) {
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#author_affiliations_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["startdate"]+"</td>";
        tt+="<td>"+row["enddate"]+"</td>";
        tt+="<td>"+row["name"]+"</td>";
        tt+="<td>"+row["topics"]+"</td>";
        tt+="<td>"+row["details"].substr(0,50)+"</td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=affiliation&affiliation_id="+row["affiliation_id"]+"\">edit</a>";
        tt+="&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_affiliation("+row["affiliation_id"]+");\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n"; 
        $("#author_affiliations_body").append(tt);
      }
      $("#author_affiliations").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#author_affiliations_found").html("Found: "+($("#author_affiliations tr:visible").length-2));
    }
  });
};

function delete_affiliation(affiliation_id){
  if(!window.confirm("Are you sure to delete the affiliation?")) return false;
  $("#author_affiliations_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_affiliation",
      affiliation_id:affiliation_id
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Deleted";
        if(typeof load_author_affiliations === 'function'){load_author_affiliations();}
      }
      $("#author_affiliations_reply").text(msg);
    }
  });
  return false;
};
</script>
