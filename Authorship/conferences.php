<h2>Conferences</h2>
<button id="conferences_reset">Reset filters</button>
<button id="conferences_export">Export</button>
<div id="conferences_found" style="display:inline-block"></div>
<table id="conferences" class="tablesorter">
	<thead>
		<th class="filter-select" data-placeholder="Search...">Conference</th>
		<th class="filter-select" data-placeholder="Search...">Start date</th>
		<th class="filter-select" data-placeholder="Search...">Venue</th>
		<th>Conference webpage</th>
		<th class="filter-select" data-placeholder="Search...">Speaker</th>
		<th class="filter-select" data-placeholder="Search...">Contribution</th>
	  <?php if(isAuthorised()){ ?>
			<th>Actions</th>
		<?php } ?>
	</thead>
	<tbody id="conferences_body">
	</tbody>
</table>
<div id="conferences_reply" style="display:inline-block"></div>

<script>

$(function() {
  $("#conferences").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_conferences();
});

$("#conferences").tablesorter({
  theme: 'blue',
  sortList: [[1,1]],
  widgets: ['filter','zebra','output']
});

$("#conferences").on("filterEnd",function(){
  $("#conferences_found").html("Found: "+($("#conferences tr:visible").length-2))
});

$("#conferences_export").click(function() {
  $("#conferences").trigger("outputTable");
});

$("#conferences_reset").click(function() {
  $("#conferences").trigger("filterReset").trigger("sorton",[[[1, 1]]]);
});

function load_conferences(){
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbread.php',
    type: 'get',
    data:{
      cmd:"get_conferences_with_author"
    },
    success: function(data) {
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#conferences_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["name"]+"</td>";
        tt+="<td>"+row["startdate"]+"</td>";
        tt+="<td>"+row["venue"]+"</td>";
        tt+="<td>"+row["webpage"]+"</td>";
        tt+="<td>"+row["firstname"]+"&nbsp;"+row["lastname"]+"</td>";
        tt+="<td>"+row["contribution"]+"</td>";
        <?php if(isAuthorised()){ ?>
				tt+="<td>";
        tt+="<a href=\"index.php?page=author&author_id="+row["author_id"]+"\">author</a>";
        tt+="&nbsp;";
        tt+="<a href=\"index.php?page=conference&conference_id="+row["conference_id"]+"\">edit</a>";
        tt+="&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_conference("+row["conference_id"]+");\">delete</a>";
        tt+="</td>";
        <?php } ?>
				tt+="</tr>\n"; 
        $("#conferences_body").append(tt);
      }
      $("#conferences").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#conferences_found").html("Found: "+($("#conferences tr:visible").length-2));
    }
  });
};

function delete_conference(conference_id){
  if(!window.confirm("Are you sure to delete the conference?")) return false;
  $("#conferences_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_conference",
      conference_id:conference_id
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Deleted";
        if(typeof load_conferences === 'function'){load_conferences();}
      }
      $("#conferences_reply").text(msg);
    }
  });
  return false;
};
</script>
