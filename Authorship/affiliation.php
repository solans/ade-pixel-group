<h2>Affiliation</h2>
<form method="GET" id="affiliation">
<table>
  <tr><th>Institute</th><td><select id="institute_id"></select></td></tr>
  <tr><th>Author</th><td><select id="author_id"></select></td></tr>
  <tr><th>Start date</th><td><input type="text" id="startdate"></td></tr>
  <tr><th>End date</th><td><input type="text" id="enddate"></td></tr>
  <tr><th>Topic</th><td>
    <input type="checkbox" id="topics_1" name="topics" value="PL"/><label for="topics_1">PL</label>
    <input type="checkbox" id="topics_2" name="topics" value="Supervision"/><label for="topics_2">Supervision</label>
    <input type="checkbox" id="topics_3" name="topics" value="Design"/><label for="topics_3">Design</label>
    <input type="checkbox" id="topics_4" name="topics" value="Simulation"/><label for="topics_4">Simulation</label>
    <input type="checkbox" id="topics_5" name="topics" value="Readout"/><label for="topics_5">Readout</label>
    <input type="checkbox" id="topics_6" name="topics" value="Testing"/><label for="topics_6">Testing</label>
    <input type="checkbox" id="topics_7" name="topics" value="Analysis"/><label for="topics_7">Analysis</label>
    <input type="checkbox" id="topics_8" name="topics" value="MonoPix"/><label for="topics_8">MonoPix</label>
    <input type="checkbox" id="topics_9" name="topics" value="CASSIA"/><label for="topics_9">CASSIA</label>
    <input type="hidden" id="topics" value="">
  </td></tr>
  <tr><th>Details</th><td><input type="text" id="details"></td></tr>  
</table>
<input type="hidden" id="affiliation_id" value="<?=@$_GET["affiliation_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="affiliation_reply" style="display:inline-block;"></div>

<script>
$(function() {
  $("#startdate").datepicker({dateFormat:'yy-mm-dd'});
  $("#enddate").datepicker({dateFormat:'yy-mm-dd'});
  load_institutes();
});
  
$("#affiliation").submit(function(){
  arr=$("input[name=topics]:checked").map(function(){return this.getAttribute("value");}).get().toString();
  $("#topics").val(arr);
  if($("#affiliation_id").val()==""){add_affiliation();}
  else{update_affiliation();}
  return false;
});

function load_institutes(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_institutes"
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      tt="";
      for(row of reply){
        tt+="<option value="+row["institute_id"]+">"+row["name"]+"</option>\n";
      }
      $("#institute_id").html(tt);
      load_authors();
    }
  }); 
}

function load_authors(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_authors"
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      tt="";
      for(row of reply){
        tt+="<option value="+row["author_id"]+">"+row["initials"]+" "+row["lastname"]+"</option>\n";
      }
      $("#author_id").html(tt);
      if($("#affiliation_id").val()!=""){load_affiliation();}
    }
  }); 
}

function load_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_affiliation",
      affiliation_id:$("#affiliation_id").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      row=reply[0];
      $("#author_id").val(row["author_id"]);
      $("#institute_id").val(row["institute_id"]);
      $("#startdate").val(row["startdate"]);
      $("#enddate").val(row["enddate"]);
      $("#details").val(row["details"]);
      $("#topics").val(row["topics"]);
      if($("#topics").val()){ $.each($("#topics").val().split(","),function(k,v){$("input[value="+v+"]").prop("checked",true);})}
    }
  });
}

function add_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_affiliation",
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      topics:$("#topics").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Stored";
        if(typeof load_affiliations === 'function'){load_affiliations();}
      }
      $("#affiliation_reply").text(msg);
      $("#affiliation").trigger("reset");
    }
  });
}

function update_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_affiliation",
      affiliation_id:$("#affiliation_id").val(),
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      topics:$("#topics").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Updated";
        if(typeof load_affiliations === 'function'){load_affiliations();}
      }
      $("#affiliation_reply").text(msg);
    }
  });
}
</script>
