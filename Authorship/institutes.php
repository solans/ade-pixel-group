<h2>Institutes</h2>
<button id="institutes_reset">Reset filters</button>
<button id="institutes_export">Export</button>
<div id="institutes_found" style="display:inline-block"></div>
<table id="institutes" class="tablesorter">
	<thead>
		<th>Name</th>
		<th>Actions</th>
	</thead>
	<tbody id="institutes_body">
	</tbody>
</table>
<div id="institutes_reply" style="display:inline-block"></div>

<script>

$(function() {
  $("#institutes").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_institutes();
});

$("#institutes").tablesorter({
  theme: 'blue',
  sortList: [[0,0],[1,0]],
  widgets: ['filter','zebra','output']
});

$("#institutes").on("filterEnd",function(){
  $("#institutes_found").html("Found: "+($("#institutes tr:visible").length-2))
});

$("#institutes_export").click(function() {
  $("#institutes").trigger("outputTable");
});

$("#institutes_reset").click(function() {
  $("#institutes").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function load_institutes(){
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbread.php',
    type: 'get',
    data:{
      cmd:"get_institutes"
    },
    success: function(data) {
      console.log(data);
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#institutes_body").empty();
      for (row of rows){
        tt="<tr>\n";
        tt+="<td>"+row["name"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"index.php?page=institute&institute_id="+row["institute_id"]+"\">edit</a>";
        tt+="&nbsp;";
        tt+="<a href=\"#\" onclick=\"delete_institute("+row["institute_id"]+");\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n"; 
        $("#institutes_body").append(tt);
      }
      $("#institutes").trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#institutes_found").html("Found: "+($("#institutes tr:visible").length-2));
    }
  });
};

function delete_institute(institute_id){
  if(!window.confirm("Are you sure to delete the institute?")) return false;
  $("#institutes_reply").text("");
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbwrite.php',
    type: 'get',
    data: {
      cmd:"delete_institute",
      institute_id:institute_id
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Deleted";
        if(typeof load_institutes === 'function'){load_institutes();}
      }
      $("#institutes_reply").text(msg);
    }
  });
  return false;
};
</script>
