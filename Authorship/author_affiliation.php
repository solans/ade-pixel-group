<h2>Author Affiliation</h2>
<form method="GET" id="author_affiliation">
<table>
  <tr><th>Start date</th><td><input type="text" id="startdate"></td></tr>
  <tr><th>End date</th><td><input type="text" id="enddate"></td></tr>
  <tr><th>Institute</th><td><select id="institute_id"></select></td></tr>
  <tr><th>Topic</th><td>
    <input type="checkbox" id="topics_1" name="topics" value="PL"/><label for="topics_1">PL</label>
    <input type="checkbox" id="topics_2" name="topics" value="Supervision"/><label for="topics_2">Supervision</label>
    <input type="checkbox" id="topics_3" name="topics" value="Design"/><label for="topics_3">Design</label>
    <input type="checkbox" id="topics_4" name="topics" value="Simulation"/><label for="topics_4">Simulation</label>
    <input type="checkbox" id="topics_5" name="topics" value="Readout"/><label for="topics_5">Readout</label>
    <input type="checkbox" id="topics_6" name="topics" value="Testing"/><label for="topics_6">Testing</label>
    <input type="checkbox" id="topics_7" name="topics" value="Analysis"/><label for="topics_7">Analysis</label>
    <input type="checkbox" id="topics_8" name="topics" value="MonoPix"/><label for="topics_8">MonoPix</label>
    <input type="checkbox" id="topics_9" name="topics" value="CASSIA"/><label for="topics_9">CASSIA</label>
    <input type="hidden" id="topics" value="">
  </td></tr>
  <tr><th>Details</th><td><input type="text" id="details"></td></tr>  
</table>
<input type="hidden" id="author_affiliation_id" value="<?=@$_GET["author_affiliation_id"];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="author_affiliation_reply" style="display:inline-block;"></div>

<script>
$(function() {
  load_institutes();
  $("#startdate").datepicker({dateFormat:'yy-mm-dd'});
  $("#enddate").datepicker({dateFormat:'yy-mm-dd'});
});
  
$("#author_affiliation").submit(function(){
  arr=$("input[name=topics]:checked").map(function(){return this.getAttribute("value");}).get().toString();
  $("#topics").val(arr);
  add_author_affiliation();
  return false;
});

function load_institutes(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_institutes"
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      tt="";
      for(row of reply){
        tt+="<option value="+row["institute_id"]+">"+row["name"]+"</option>\n";
      }
      $("#institute_id").html(tt);
    }
  });
}

function load_author_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbread.php",
    type: "get",
    data: {
      cmd:"get_author_affiliation",
      author_affiliation_id:$("#author_affiliation_id").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      row=reply[0];
      $("#institute_id").val(row["institute_id"]);
      $("#startdate").val(row["startdate"]);
      $("#enddate").val(row["enddate"]);
      $("#details").val(row["details"]);
      $("#topics").val(row["topics"]);
      if($("#topics").val()){ $.each($("#topics").val().split(","),function(k,v){$("input[value="+v+"]").prop("checked",true);})}
    }
  });
}

function add_author_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"add_affiliation",
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      topics:$("#topics").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Stored";
        if(typeof load_author_affiliations === 'function'){load_author_affiliations();}
        //$("#author_affiliation_id").val(reply["last_insert_id"]);
        $("#author_affiliation").trigger("reset");
      }
      $("#author_affiliation_reply").text(msg);
    }
  });
}

function update_author_affiliation(){
  $.ajax({
    url: "<?=$gobase;?>/Authorship/dbwrite.php",
    type: "get",
    data: {
      cmd:"update_affiliation",
      author_affiliation_id:$("#author_affiliation_id").val(),
      author_id:$("#author_id").val(),
      institute_id:$("#institute_id").val(),
      startdate:$("#startdate").val(),
      enddate:$("#enddate").val(),
      topics:$("#topics").val(),
      details:$("#details").val()
    },
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      msg="";
      if      ("error" in reply){msg=reply["error"];}
      else if (reply["affected_rows"]==0){msg="Something went wrong";}
      else if (reply["affected_rows"]==1){msg="Updated";
        if(typeof load_author_affiliations === 'function'){load_author_affiliations();}
      }
      $("#author_affiliation_reply").text(msg);
    }
  });
}
</script>
