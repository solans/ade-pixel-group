<?php
//echo "hello";
include_once("includes.php");

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  die("Connection failed: " . mysqli_connect_error());
}
//echo "open";

$d=$_GET;

if($d["cmd"]=="get_authors"){
  $sql ="SELECT * FROM authors ORDER BY firstname";
}
else if($d["cmd"]=="get_author"){
  $sql ="SELECT * FROM authors WHERE author_id='".$d["author_id"]."';";
}
else if($d["cmd"]=="get_institutes"){
  $sql ="SELECT * FROM institutes ";
  $sql.="ORDER BY institutes.name ASC ";
}
else if($d["cmd"]=="get_institute"){
  $sql ="SELECT * FROM institutes WHERE institute_id='".$d["institute_id"]."' ";
}
else if($d["cmd"]=="get_affiliations"){
  $sql ="SELECT authors.firstname,authors.lastname,affiliations.*,institutes.* FROM affiliations ";
  $sql.="INNER JOIN authors ON affiliations.author_id=authors.author_id ";
  $sql.="INNER JOIN institutes ON affiliations.institute_id=institutes.institute_id ";
}
else if($d["cmd"]=="get_affiliations_by_author"){
  $sql ="SELECT * FROM affiliations ";
  $sql.="INNER JOIN institutes ON affiliations.institute_id=institutes.institute_id ";
  $sql.="WHERE author_id='".$d["author_id"]."';";
}
else if($d["cmd"]=="get_affiliation"){
  $sql ="SELECT * FROM affiliations WHERE affiliation_id='".$d["affiliation_id"]."';";
}
else if($d["cmd"]=="get_authorlist"){
  $sql ="SELECT * FROM affiliations ";
  $sql.="INNER JOIN authors ON affiliations.author_id=authors.author_id ";
  $sql.="INNER JOIN institutes ON affiliations.institute_id=institutes.institute_id ";
  $sql.="WHERE affiliations.startdate<=NOW() AND affiliations.enddate>NOW() ";
  $sql.="ORDER BY authors.lastname ASC ";
}
else if($d["cmd"]=="get_authorlist_on_date"){
  $sql ="SELECT * FROM affiliations ";
  $sql.="INNER JOIN authors ON affiliations.author_id=authors.author_id ";
  $sql.="INNER JOIN institutes ON affiliations.institute_id=institutes.institute_id ";
  $sql.="WHERE affiliations.startdate<='".$d["date"]."' AND affiliations.enddate>'".$d["date"]."' ";
  $sql.="AND affiliations.`topics` NOT LIKE 'CASSIA'";
  $sql.="ORDER BY authors.lastname ASC ";
} 
else if($d["cmd"]=="get_conference"){
  $sql ="SELECT * FROM conferences WHERE conference_id='".$d["conference_id"]."';";
}
else if($d["cmd"]=="get_conferences"){
  $sql ="SELECT * FROM conferences;";
}
else if($d["cmd"]=="get_conferences_with_author"){
  $sql ="SELECT * FROM conferences ";
  $sql.="LEFT JOIN authors ON conferences.author_id=authors.author_id ";
}
else if($d["cmd"]=="get_conferences_by_author"){
  $sql ="SELECT conferences.* FROM conferences ";
  $sql.="LEFT JOIN authors ON conferences.author_id=authors.author_id ";
  $sql.="WHERE conferences.author_id='".$d["author_id"]."';"; 
}



if($debug) echo $sql;
$result=$conn->query($sql);
$ret = array();
while($row = $result->fetch_assoc()) {
  //$ret[] = $row;
  $row2=array();
  foreach($row as $k=>$v){
    //$row2[$k]=htmlentities($v,ENT_COMPAT,'ISO-8859-1', true);
    $row2[$k]=mb_convert_encoding($v,"UTF-8","ISO-8859-1");
    //$row2[$k]=utf8_encode($v);
    //$row2[$k]=$v;
  }
  $ret[]=$row2;
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>