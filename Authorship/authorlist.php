<h2>Authorlist</h2>
<div id="authorlist_reply"></div>
<br/>
<form method="GET" id="authorlist">
<table>
  <tr><th>Date</th><td><input type="text" id="date"></td></tr>
</table>
<input type="submit" value="Show">
</form>


<script>
$(function() {
  $("#date").datepicker({dateFormat:'yy-mm-dd'});
  authorlist_init();
});

function authorlist_init(){
  var dt = new Date();
  var today = dt.getFullYear() + '-' + ((dt.getMonth()+1)<10 ? '0' : '') + (dt.getMonth()+1) + '-' + (dt.getDate()<10 ? '0' : '') + dt.getDate();
  $("#date").val(today);
  load_authorlist();
}

$("#authorlist").submit(function(){
  load_authorlist();
  return false;
});

function load_authorlist(){
  $.ajax({
    url: '<?=$gobase;?>/Authorship/dbread.php',
    type: 'get',
    data: {
      cmd:"get_authorlist_on_date",
      date:$("#date").val()
    },
    success: function(data) {
      rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#authorlist_reply").empty();
      //merge double affiliations
      tt="";
      prev_author_id=-1;
      for (row of rows){
        if (row["author_id"]!=prev_author_id){
          if (prev_author_id!=-1){
            tt+="), ";
          }
          tt+=row["initials"]+" "+row["lastname"];
          tt+=" ("+row["name"];
        }else{
          tt+=", "+row["name"];
        }
        prev_author_id=row["author_id"];
      }
      tt+=")";
      $("#authorlist_reply").html(tt);
    }
  });
}

</script>
