<?
include_once("helper.php");
include_once("activity.php");
include_once("querystring.php");
if(!isset($_GET['date'])){$_GET["date"]=date("d-m-Y");}

$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
$collection = $helper->GetData();
$qs = new querystring();
?>
<h2>
  <table>
    <tr>
      <td><a href="<?$qs=new querystring();$qs->set('date',date('d-m-Y', strtotime(@$_GET['date'].' -1 month')));echo $qs->toFullString();?>">&#10096;</a></td>
      <td><?=date('M Y', strtotime(@$_GET['date'].' +0 days'));?></td>
      <td><a href="<?$qs=new querystring();$qs->set('date',date('d-m-Y', strtotime(@$_GET['date'].' +1 month')));echo $qs->toFullString();?>">&#10097;</a></td>
    </tr>
  </table>
</h2>
<?
include_once('SimpleCalendar.php');
$calendar = new donatj\SimpleCalendar();
$calendar->setStartOfWeek('Monday');
//foreach($collection->data as $p){
foreach($collection->GetDataOrderBy("lab") as $p){
  $c=explode("-",$p->GetValue("lab"));
  $l=$c[2];
  $a='<a href="?action=edit&id='.$p->GetId().'">'.$l.': '.$p->GetValue("task").'</a>';
  $calendar->addDailyHtml( $a, $p->GetValue("startdate"), $p->GetValue("enddate") );
}  
$calendar->setDate($_GET["date"]);
$calendar->show(true);
?>

<p class="SUBTITLE">Register a new activity</p>
<?
$activity=new activity();
echo $activity->ToHtmlForm(array("url"=>"logic.php", "method"=>"POST", "action"=>"Add"));  
?>
