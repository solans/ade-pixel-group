<?
class querystring{
	
	var $params;
		
	function __construct(){
		$this->params = $_GET;
	}
	
	function remove($token){
		unset($this->params[$token]);
	} 
	
	function set($token,$value){
		if(isset($this->params[$token])) $this->remove($token);
		$this->params[$token]=$value;
	}
	
	
	function get($token){
		return $this->params[$token]; 	
	}
	
	
	function parseString($s){
		unset($this->params);	
		$this->params = array();
		$qs=substr($s,strpos($s,"?")+1);
		if(strpos($qs,"&")===FALSE) return;
		$qsa=explode("&",$qs); 
		foreach ($qsa as $e){ 
			$ea = explode("=",$e);
			$this->params[$ea[0]]=$ea[1];
		}  
	}
	
	function toString(){
		$s = array();
		foreach($this->params as $k=>$v){
			array_push($s,$k.'='.$v);
		}
		return join("&",$s);		   
	}
	
	function toFullString(){
		$s = $this->toString();	
		if(strlen($s)>0){ 
			$s = '?'.$s;
		}
		return $s;
	}
}
?>