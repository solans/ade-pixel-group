<?
include_once("helper.php");
include_once("schema.php");
include_once("querystring.php");
if(!isset($_GET['date'])){$_GET["date"]=date("d-m-Y");}

$qs = new querystring();
$filename = getcwd()."/data/data.xml";
$helper = new Helper();
$helper->verbose = (@$_GET["debug"]=="true"?true:false);
$helper->Parse($filename);    
?>

<p class="SUBTITLE">Edit entry</p>
<?
$collection = $helper->GetData();
$object=$collection->Get($qs->get("id"));
if(!$object){echo "<h3>Entry not found id: ".$qs->get("id")."<h3>";}
else{
  echo $object->ToHtmlForm(array("url"=>"logic.php", "method"=>"POST", "action"=>"Edit", "redirect"=>"?action=edit&id=".$qs->get("id")));  
}
?>
