<?
/* Date: August 2020 */

include_once("helper.php");

Helper::AddType("Computer");

class Computer extends Collectible{

  function __construct(){
    $this->members = array(
      "id"=>array("type"=>"string","required"=>true,"display"=>"hidden","label"=>"Entry","value"=>"0"),
      "hostname"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Host name", "value"=>""),
      "type"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Type", "value"=>""),
      "model"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Model","value"=>""),
      "serialnumber"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Serial number","value"=>""),
      "mac0"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"MAC 0","value"=>""),
      "mac1"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"MAC 1","value"=>""),
      "cerninventory"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Inventory number", "value"=>""),
      "edh"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Purchase","value"=>""),
      "deleted"=>array("type"=>"bool","required"=>true,"display"=>"50","label"=>"Deleted","value"=>""),
      "date"=>array("type"=>"date","required"=>true,"display"=>"50","label"=>"Date","value"=>""),
      "comment"=>array("type"=>"string","required"=>true,"display"=>"50","label"=>"Comment","value"=>"")  
    );
    $this->classname = "Computer";
    $this->tag = "computer";
    $this->id = "id";
  }
  
}

?>