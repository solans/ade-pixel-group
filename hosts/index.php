<?php
$time_start = microtime(true);
include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<title>Hosts</title>
<style>
.ui-datepicker {
  background: #fff;
}
</style>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
  $dt1 = round(microtime(true) - $time_start,1);
?>
<div class="CONTENT">
<?php   
  show_certificate();
  show_login(true); 
  $dt2 = round(microtime(true) - $time_start,1);
  ?>
<p class="TITLE">Hosts</p>

<?  
  include_once("querystring.php");
  $qs=new querystring();
  
  $qs_back=new querystring();
  $qs_back->remove("action");
  $qs_back->remove("id");  
  $qs_list=new querystring();
  $qs_list->set("action","list");
  $qs_list->remove("id");
  $qs_add =new querystring();
  $qs_add->set("action","add");
  $qs_add->remove("id");
  $a_back='<a href="index.php'.$qs_back->toFullString().'">Back</a>';
  $a_list='<a href="'.$qs_list->toFullString().'">List</a>';
  $a_add ='<a href="'.$qs_add ->toFullString().'">Add</a>';
  
  $dt3 = round(microtime(true) - $time_start,1);
  
  if(@$qs->get("action")=="add"){
    echo $a_back."&nbsp;".$a_list;
    include_once("add.php");    
  }else if(@$qs->get("action")=="list"){
    echo $a_back."&nbsp;".$a_add;
    include_once("list.php");    
  }else if(@$qs->get("action")=="edit" && @$qs->get("id")>=0){
    echo $a_back."&nbsp;".$a_add."&nbsp;".$a_list;
    include_once("edit.php");    
  }else{
    echo $a_back."&nbsp;".$a_add."&nbsp;".$a_list;
    include_once("latest.php");    
  }
?>
<p class="SUBTITLE">Links</p>
<ul>
  <li><a href="https://inventorycheck.cern.ch/inventorycheck/index.gsp?&request=Check">Inventory</a></li>
</ul>
</div>
<?php
  show_footer();
  // Display Script End time
  $time_end = microtime(true);
  $dt = round($time_end - $time_start,1);
  echo '<b>Total Execution Time:</b> '.$dt.' Sec '.$dt1.' '.$dt2.' '.$dt3;
?>
</div>

</body>
</html>

