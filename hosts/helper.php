<?
/** August 2020 **/

interface CollectibleInterface {
    public function GetId();
    public function GetTag();
    public function Dump();
    public function ToXml($ident);
    public function FromArray($arr);
    public function GetClassName();
}

class Collectible implements CollectibleInterface{
  public $members;
  public $classname;
  public $tag;
  public $id;
  
  //Constructor: should be overloaded
  function __construct(){}
  
  //GetId
  function GetId(){
    return $this->members[$this->id]["value"];
  }
  
  //GetTag
  function GetTag(){
    return $this->tag;
  }
  
  //Get class name
  function GetClassName(){
    return $this->classname;
  }
    
  //Get member
  function GetMember($key){
    return $this->members[$key];
  }
  
  //Get member
  function GetMembers(){
    $arr=array();
    foreach($this->members as $k=>$v){
      $arr[]=$k;
    }
    return $arr;
  }
  
  function GetLabel($key){
    return $this->members[$key]["label"];
  }
  
  function GetValue($key){
    return $this->members[$key]["value"];
  }
  
  function SetValue($key,$value){
    $this->members[$key]["value"]=$value;
  }
  
  //Dump
  function Dump(){
    foreach($this->members as $k){
      echo $k["value"]."&nbsp;";
    }
  }
  
  //Dump to XML
  function ToXml($ident=0){
    $s = str_repeat(" ",$ident);
    $xml = $s."<".$this->tag.">\n";
    foreach($this->members as $k=>$v){
      $xml .= $s.$s."<".$k.">".htmlentities($v["value"])."</".$k.">\n";
    }
    $xml .= $s."</".$this->tag.">\n";
    return $xml;
  }
  
  //Dump to XML
  function ToString($ident=0){
    $s = str_repeat(" ",$ident);
    $n = "\n";
    $str = "";
    foreach($this->members as $k=>$v){
      $str .= $s.$s.$v["label"].": ".htmlentities($v["value"]).$n;
    }
    return $str;
  }

  //From array
  function FromArray($arr){
    //print_r($arr);
    foreach($arr as $k=>$v){
      foreach($this->members as $mk=>$mv){
        $k2=strtolower($k);
        if($k2==$mk){
          //print " change ".$mk." value to ".html_entity_decode($v)."<br>\n";
          $this->members[$mk]["value"]=html_entity_decode($v);} 
      }
    }
  }

  //To array
  function ToArray(){
    $arr=array();
    foreach($this->members as $mk=>$mv){
      $arr[$mk]=$mv["value"];
    }
    return $arr;
  }

  /**
    * Params: name, action, method, submit, redirect
    *
    * <form name="editor" action="<?=$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];?>" method="POST">
    * <input type="hidden" name="action" value="add"/>
    * <input type="hidden" name="id" value="<?=randomstring();?>"/>
    * <label style="display:inline-block;width:140px;">Group(*):</label><input type="text" name="group" size="50"/><br/>
    * <label style="display:inline-block;width:140px;">Name:</label><input type="text" name="responsible" size="50"/><br/>
    * <label style="display:inline-block;width:140px;">E-mail(*):</label><input type="text" name="email" size="50"/><br/>
    * <label style="display:inline-block;width:140px;">Preferred start date(*):</label><input id="startdate" type="text" name="startdate" size="50"/><br/>
    * <label style="display:inline-block;width:140px;">Preferred end date(*):</label><input id="enddate" type="text" name="enddate" size="50"/><br/>
    * <label style="display:inline-block;width:140px;">Dose rate:</label><input type="text" name="doserate" size="50"/><br/>
    * <label style="display:inline-block;width:140px;vertical-align:top;">Comments:</label><textarea name="comments" cols="40" rows="4"></textarea><br/>
    * <input name="submit" type="submit" value="Request" /><br>
    * * Required fields
    * <script language="javascript">
    *  $(function() {
    *    $( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd', firstDay: 1}); 
    *    $( "#enddate" ).datepicker({ dateFormat: 'yy-mm-dd', firstDay: 1}); 
    *  });
    * </script>
    * </form>
    **/    
  function ToHtmlForm($params){
    $nn = "\n";
    if(!array_key_exists("post",$params)){$params["post"]="POST";}
    if(!array_key_exists("url",$params)){$params["url"]="";}
    if(!array_key_exists("name",$params)){$params["name"]="editor";}
    if(!array_key_exists("action",$params)){$params["action"]="Send";}
    $html = '<form name="'.$params["name"].'" action="'.$params["url"].'" method="'.$params["post"].'" enctype="application/x-www-form-urlencoded">'.$nn;
    //$html .= '<input type="hidden" name="redirect" value="'.urlencode($params["redirect"]).'" />';
    foreach($this->members as $mk=>$mv){
      if($mv["display"]!="hidden"){continue;}
      $html .= '<input type="hidden" name="'.$mk.'" value="'.$mv["value"].'"/></td>'.$nn;      
    } 
    $html .= '<table>'.$nn;
    foreach($this->members as $mk=>$mv){
      if($mv["display"]=="hidden"){continue;}
      $html .= '<tr>';
      $html .= '<td><label>'.$mv["label"].'</label></td>';
      if($mv["type"]=="string" || $mv["type"]=="date"){
        $html .= '<td><input type="text" id="'.$mk.'" name="'.$mk.'" size="'.$mv["display"].'" value="'.$mv["value"].'"/></td>'.$nn;
      }
      else if($mv["type"]=="datetime"){
        $mv["date"]=parsedatetime($mv["value"],'%Y-%m-%d %H:%M');
        $html .= '<td>';
        $html .= '<input type="hidden" id="'.$mk.'" name="'.$mk.'" value="'.$mv["value"].'"/>'.$nn;
        $html .= '<input type="text" id="'.$mk.'_d" name="'.$mk.'_d" value="'.$mv["date"]["date"].'" size="'.$mv["display"].'"/>'.$nn;
        $html .= '<select id="'.$mk.'_h" name="'.$mk.'_h">'.$nn;
        for($x=7;$x<24;$x++){
          $sel="";
          if($x==$mv["date"]["hour"]){$sel=" selected";}
          $opt=sprintf("%02d",$x);
          $html .= '<option value="'.$opt.'" '.$sel.'>'.$opt.'</option>'.$nn;  
        }
        $html .= '</select>'.$nn;
        $html .= '<select id="'.$mk.'_m" name="'.$mk.'_m">'.$nn;
        for($x=0;$x<55;$x=$x+5){
          $sel="";
          if($x==$mv["date"]["min"]){$sel=" selected";}
          $opt=sprintf("%02d",$x);
          $html .= '<option value="'.$opt.'" '.$sel.'>'.$opt.'</option>'.$nn;  
        }
        $html .= '</select>'.$nn;
        $html .= '</td>'.$nn;
      }
      else if($mv["type"]=="time"){
        $html .= '<td>';
        $html .= '<input type="hidden" id="'.$mk.'" name="'.$mk.'" value="'.$mv["value"].'"/></td>'.$nn;
        $html .= '<select id="'.$mk.'_h" name="'.$mk.'_h">'.$nn;
        for($x=7;$x<24;$x++){
          $sel="";
          if($x==$mv["value"]){$sel=" selected ";}
          $opt=sprintf("%02d",$x);
          $html .= '<option value="'.$opt.'" '.$sel.'>'.$opt.'</option>'.$nn;  
        }
        $html .= '</select>'.$nn;
        $html .= '<select id="'.$mk.'_m" name="'.$mk.'_m">'.$nn;
        for($x=0;$x<55;$x=$x+5){
          $sel="";
          if($x==$mv["value"]){$sel=" selected ";}
          $opt=sprintf("%02d",$x);
          $html .= '<option value="'.$opt.'" '.$sel.'>'.$opt.'</option>'.$nn;  
        }
        $html .= '</select>'.$nn;
        $html .= '</td>'.$nn;
      }
      else if($mv["type"]=="enum"){
        $html .= '<td><select name="'.$mk.'">'.$nn;
        foreach($mv["values"] as $opt){
          $sel="";
          if($opt==$mv["value"]){$sel=" selected ";}
          $html .= '<option value="'.$opt.'" '.$sel.'>'.$opt.'</option>'.$nn;  
        }
        $html .= '</select></td>'.$nn;
      }
      else if($mv["type"]=="bool"){
        $html .= '<td><input type="checkbox" name="'.$mk.'"';
        if($mv["value"]!=""){ $html .= ' checked '; }
        $html .= '>'.$nn;
      }
      else if($mv["type"]=="text"){
        $html .= '<td><textarea name="'.$mk.'" cols="'.$mv["cols"].'" rows="'.$mv["rows"].'">'.$nn;
        $html .= $mv["value"];//str_replace("\r","",str_replace("\n","",$mv["value"]));
        $html .= '</textarea></td>'.$nn;
      }
      $html .= '</tr>'.$nn;
    }
    $html .= '</table>'.$nn;
    $html .= '<input type="submit" name="action" value="'.$params["action"].'" />';
    $html .= '</form>'.$nn;
    $html .= '<script language="javascript">'.$nn;
    $html .= ' $(function() {'.$nn;
    foreach($this->members as $mk=>$mv){
      if($mv["type"]=="date"){
        $html .= '   $("#'.$mk.'").datepicker({ dateFormat: "yy-mm-dd", firstDay: 1});'.$nn;
      }
      else if($mv["type"]=="datetime"){
        $html .= '   $("#'.$mk.'_d").datepicker({ dateFormat: "yy-mm-dd", firstDay: 1});'.$nn;
        $html .= '   $("#'.$mk.'_d").on("change", function() {$("#'.$mk.'").val($("#'.$mk.'_d").val()+" "+$("#'.$mk.'_h").val()+":"+$("#'.$mk.'_m").val())});'.$nn;
        $html .= '   $("#'.$mk.'_h").on("change", function() {$("#'.$mk.'").val($("#'.$mk.'_d").val()+" "+this.value+":"+$("#'.$mk.'_m").val())});'.$nn;
        $html .= '   $("#'.$mk.'_m").on("change", function() {$("#'.$mk.'").val($("#'.$mk.'_d").val()+" "+$("#'.$mk.'_h").val()+":"+this.value)});'.$nn;
      }
      else if($mv["type"]=="time"){
        $html .= '   $("#'.$mk.'_h").on("change", function() {$("#'.$mk.'").val(this.value+":"+$("#'.$mk.'_m").val())});'.$nn;
        $html .= '   $("#'.$mk.'_m").on("change", function() {$("#'.$mk.'").val($("#'.$mk.'_h").val()+":"+this.value)});'.$nn;
      }
    }
    $html .= ' });'.$nn;
    $html .= '</script>'.$nn;
    return $html;
  }
}

class Collection{

  //internal data
  var $data;
  var $order;
  
  //Constructor
  function __construct(){
    $this->data = array();
  }
  
  //dump function
  function Dump(){
    ?>
    <ul>
    <?
    foreach($this->data as $c){
      ?>
      <li><?=$c->Dump();?></li>
      <?
    }
    ?>
    </ul>
    <?
  }
  
  //helper
  function Add($p){
    if($this->Get($p->GetId())!=NULL) return;
    $this->data[]=$p;
  }
  
  //
  function Get($id){
    foreach($this->data as $e){
      if($e->GetId() == $id){
        return $e;
      }
    }
    return NULL;
  }
  
  //Get the next id
  function GetNextId(){
    $max=0;
    foreach($this->data as $e){
      $id=intval($e->GetId());
      if($id>$max){$max=$id;}
    }
    return $max+1;
  }
  
  //Remove a given publications
  function Del($id){
    for($i=0;$i<count($this->data);$i++){
      if($this->data[$i]->GetId()==$id){
        unset($this->data[$i]);
      }
    }
  }
  
  //Dump to XML
  function ToXml(){
    $nn = "\n";
    $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>'.$nn;
    $xml .= '<collection>'.$nn;
    foreach($this->data as $e){
      $xml .= $e->ToXml(1);
    }
    $xml .= '</collection>'.$nn;
    return $xml;
  }

  //Write
  function Write($filename){
    $fh = fopen($filename, 'w');
    fwrite($fh, $this->ToXml());
    fclose($fh);
  }
  
  //sort
  function GetDataOrderBy($field){
    $this->order=$field;
    usort($this->data, array($this,"Sort"));
    return $this->data;
  }
  
  function Sort($a,$b){
    return strcmp($a->GetValue($this->order), $b->GetValue($this->order));
  }
  
  //select
  function Select($key1,$key2){
    $keys=array();
    foreach($this->data as $p){
      $val1=$p->GetValue($key1);
      $val2=$p->GetValue($key2);
      if($key2=="date"){$val2=strtotime($val2);}
      if(!array_key_exists($val1,$keys)){$keys[$val1]=array($key2=>$val2,"data"=>$p);}
      if($val2>$keys[$val1][$key2]){$keys[$val1]=array($key2=>$val2,"data"=>$p);}
    }
    $selection=array();
    foreach($keys as $k => $v){
      $selection[]=$v["data"];
    }
    return $selection;
  }
}

class Helper{
  
  //data
  var $collection;
  var $element;
  
  //internal
  var $verbose;
  static $types = array();
  
  //Constructor
  function __construct(){
    $this->tag = "";
    $this->collection = new Collection();
  }
  
  //Register collectible
  static function addType($type){
    if(in_array($type,self::$types)) return;
    self::$types[]=$type;
  }
  
  //helper function
  function GetData(){
    return $this->collection;
  }
  
  //helper function
  function SetData($collection){
    $this->collection=$collection;
  }
  
  //Enable the verbose mode
  function SetVerbose($enable){
    $this->verbose=$enable;
  }
  
  //Open tag
  function startElement($parser, $name, $attrs){
    if($this->verbose){echo "Open tag: ".$name."<br>\n";}
    $this->tag=$name;
    foreach(self::$types as $type){
      if($this->tag==strtoupper($type)){
        $this->element = array();
        break;
      }
    } 
  }

  // Close tag
  function endElement($parser, $name){
    if($this->verbose){echo "Close tag: ".$name."<br>\n";}    
    foreach(self::$types as $type){
      if($name==strtoupper($type)){
        $obj = new $type();
        $obj->FromArray($this->element);
        $this->collection->Add($obj);
       break;
      }
    }
    //unset($this->element);
    $this->tag="";
  } 
  
  // Value inside a tag
  function characterData($parser, $data){
    if($this->verbose){echo "Data: ".$data."<br>\n";}
    if(!isset($this->element[$this->tag])){$this->element[$this->tag]="";}
    $this->element[$this->tag].=$data;
  }
  
  // Read from file
  function OldParse($filename){
    $xmlParser = xml_parser_create("ISO-8859-1");
    xml_set_object($xmlParser,$this);
    xml_set_element_handler($xmlParser, "startElement", "endElement");
    xml_set_character_data_handler($xmlParser, "characterData");
    $fp = fopen($filename, "r");
    while($data = fread($fp, filesize($filename))){
      xml_parse($xmlParser, $data, feof($fp));
    }
    fclose($fp);
    xml_parser_free($xmlParser);
  }
  
  // Read from file
  function Parse($filename){
    $xmlParser = xml_parser_create("ISO-8859-1");
    xml_set_object($xmlParser,$this);
    xml_set_element_handler($xmlParser, "startElement", "endElement");
    xml_set_character_data_handler($xmlParser, "characterData");
    xml_parse($xmlParser, file_get_contents($filename));
    xml_parser_free($xmlParser);
  }
 
  // Write to file 
  function Write($filename){
    $fh = fopen($filename, 'w');
    fwrite($fh, $this->collection->ToXml());
    fclose($fh);
  }
  
}

function parsedatetime($val,$fmt){
  if($val==""){
    return array("year"=>"","month"=>"","day"=>"","hour"=>"","min"=>"","sec"=>"","date"=>"","time"=>"");
  }
  $tm=strptime($val,$fmt);
  $arr=array(
    "year"=>$tm['tm_year']+1900,
    "month"=>$tm['tm_mon']+1,
    "day"=>$tm['tm_mday'],
    "hour"=>$tm['tm_hour'],
    "min"=>$tm['tm_min'],
    "sec"=>$tm['tm_sec'],
  );  
  $arr["date"]=sprintf("%d-%02d-%02d",$arr["year"],$arr["month"],$arr["day"]);
  $arr["time"]=sprintf("%02d:%02d:%02d",$arr["hour"],$arr["min"],$arr["sec"]);
  return $arr;
}

?>