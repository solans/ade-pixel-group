<?php
//echo "hello";
include_once("includes.php");

//echo "open";
$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  die("Connection failed: " . mysqli_connect_error());
}

$d=$_GET;

if($d["cmd"]=="get_sample"){
  $sql ="SELECT * FROM samples WHERE sample_id='".$d["sample_id"]."';";
}
else if($d["cmd"]=="get_samples"){
  $sql ="SELECT * FROM samples;";
}
else if($d["cmd"]=="get_last_measurements"){
  $sql ="SELECT MAX(date) as measurement_date, measurement, unit FROM measurements GROUP BY sample_id;";
}
else if($d["cmd"]=="get_samples_summary"){
  $sql ="SELECT samples.sample_id, samples.other_id, samples.description, ";
  $sql.="mm.measurement_date, mm.measurement, mm.measurement_unit, ";
  $sql.="ll.location_date, ll.location ";
  $sql.="FROM samples ";
  $sql.="LEFT JOIN (";
  $sql.=" SELECT sample_id, MAX(date) as measurement_date, ANY_VALUE(measurement) AS measurement, ANY_VALUE(unit) as measurement_unit FROM measurements GROUP BY sample_id ";
  $sql.=") AS mm ON mm.sample_id=samples.sample_id ";
  $sql.="LEFT JOIN ( "; 
  $sql.=" SELECT sample_id, MAX(date) as location_date, location FROM locations GROUP BY sample_id, location ";
  $sql.=") AS ll ON ll.sample_id=samples.sample_id; ";
}
else if($d["cmd"]=="get_measurement"){
  $sql ="SELECT * FROM measurements WHERE measurement_id='".$d["measurement_id"]."';";
}
else if($d["cmd"]=="get_measurements"){
  $sql ="SELECT * FROM measurements WHERE sample_id='".$d["sample_id"]."';";
}
else if($d["cmd"]=="get_location"){
  $sql ="SELECT * FROM locations WHERE location_id='".$d["location_id"]."';";
}
else if($d["cmd"]=="get_locations"){
  $sql ="SELECT * FROM locations WHERE sample_id='".$d["sample_id"]."';";
}
  
echo $sql;
$result=$conn->query($sql);
$ret = array();
while($row = $result->fetch_assoc()) {
  //$ret[] = $row;
  $row2=array();
  foreach($row as $k=>$v){
    //$row2[$k]=htmlentities($v,ENT_COMPAT,'ISO-8859-1', true);
    $row2[$k]=mb_convert_encoding($v,"UTF-8","ISO-8859-1");
    //$row2[$k]=utf8_encode($v);
    //$row2[$k]=$v;
  }
  $ret[]=$row2;
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>