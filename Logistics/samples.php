<h2>Samples</h2>
<button id="samples_reset">Reset filters</button>
<button id="samples_export">Export</button>
<div id="samples_found" style="display:inline-block"></div>
<table id="samples" class="tablesorter">
	<thead>
		<th data-placeholder="Search...">Reference</th>
		<th class="filter-select">Type</th>
		<th class="filter-select">Subsystem</th>
		<th data-placeholder="Search...">Responsible</th>
		<th data-placeholder="Search...">Handler</th>
		<th>Actions</th>
	</thead>
	<tbody id="samples_body">
	</tbody>
</table>

<div id="samples_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#samples").trigger("update").trigger("appendCache").trigger("applyWidgets");
  load_samples();
});

$("#samples").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
});

$("#samples").on("filterEnd",function(){
  $("#samples_found").html("Found: "+($("#samples tr:visible").length-2))
});

$("#samples_export").click(function() {
  $("#samples").trigger("outputTable");
});

$("#samples_reset").click(function() {
  $("#samples").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
});

function build_reference(sample){
  ref="ITK-"+sample["type"]+"-"+sample["subsystem"]+"-";
  for(i=0;i<4-sample["sample_id"].length;i++){ref+="0";}
  ref+=sample["sample_id"];
  return ref;
}

/** Then load members **/
function load_samples(){
  $.ajax({
    url: '../dbread.php',
    type: 'get',
    data: {cmd:"get_samples"},
    success: function(data) {
      console.log(data);
      samples=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#samples_body").empty();
      for (sample of samples){
        tt="<tr>\n";
        tt+="<td>"+build_reference(sample)+"</td>";
        tt+="<td>"+sample["type"]+"</td>";
        tt+="<td>"+sample["subsystem"]+"</td>";
        tt+="<td>"+sample["responsible"]+"</td>";
        tt+="<td>"+sample["handler"]+"</td>";
        tt+="<td><a href=\"index.php?page=sample&sample_id="+sample["sample_id"]+"\" title=\"Open\">edit</a></td>";
        tt+="</tr>\n"; 
        $("#samples_body").append(tt);
      }
      $('#samples').trigger("update").trigger("appendCache").trigger("applyWidgets");
      $("#samples_found").html("Found: "+($("#samples tr:visible").length-2));
    }
  });
}


</script>
