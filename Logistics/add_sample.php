<h2>Add sample</h2>
<form method="GET" id="add_sample">
<table>
  <tr><th>Reference</th><td><div id="reference"></div></td></tr>
  <tr><th>Type</th><td>
    <select id="type">
      <option value=""></option>
      <option value="CBL">CBL</option>
    </select>
  </td></tr>
  <tr><th>Subsystem</th><td>
    <select id="subsystem">
      <option value=""></option>
      <option value="SRV">SRV</option>
      <option value="GEN">GEN</option>
      <option value="PIX">PIX</option>
      <option value="STR">STR</option>
      <option value="CEL">CEL</option>
      <option value="CEM">CEM</option>
    </select>
  </td></tr>
  <tr><th>Other ID</th><td><input id="other_id" type="text"/></td></tr>
  <tr><th>Responsible</th><td><input id="responsible" type="text"/></td></tr>
  <tr><th>Handler</th><td><input id="handler" type="text"/></td></tr>
  <tr><th>Description</th><td><input id="description" type="text"/></td></tr>
</table>
<input type="hidden" name="sample_id" value="<?=@$_GET['sample_id'];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>

<div id="add_sample_reply" style="display:inline-block;"></div>

<script>
$("#add_sample").submit(function(){
  add_sample();
  return false;
});

function add_sample(){
  $.ajax({
    url: "../dbwrite.php",
    type: "get",
    data: {
      cmd:"add_sample",
      type:$("#type").val(),
      subsystem:$("#subsystem").val(),
      responsible:$("#responsible").val(),
      handler:$("#handler").val(),
      description:$("#description").val(),
      other_id:$("#other_id").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      if (reply["affected_rows"]==0){
        $("#add_sample_reply").text("Something went wrong");
      }else if ("error" in reply){
        $("#add_sample_reply").text(reply["error"]);
      }else if (reply["affected_rows"]==1){
        $("#add_sample_reply").text("Stored");
      }
    }
  });
}
</script>
