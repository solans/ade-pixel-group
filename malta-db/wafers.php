<h2>Wafers</h2>
<button onclick="wafers_export();">CSV</button>Export <span id="wafers_counter"></span> 
<button onclick="wafers_reset();">Reset filters</button>
<table id="wafers" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th class="filter-select" data-placeholder="Select...">Number</th>
		<th class="filter-select" data-placeholder="Select...">SerialNumber</th> 
		<th class="filter-select" data-placeholder="Select...">Submission</th>
		<th class="filter-select" data-placeholder="Select...">Substrate</th>
		<th class="filter-select" data-placeholder="Select...">Doping</th>
		<th class="filter-select" data-placeholder="Select...">Date</th>
		<th >Actions</th>
	</thead>
	<tbody id="wafers_body">
	</tbody>
</table>
<div id="wafers_reply"></div>

<script>
/* Load the table sorter **/
$("#wafers").tablesorter({
  theme: 'blue',
  sortList: [[0, 0]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#wafers_counter").html("("+($('#wafers tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#wafers").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function wafers_reset(){
  $("#wafers").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export */
function wafers_export(){
  $("#wafers").trigger('outputTable');
}

/* onload */
$(function() {
  wafers_load();
});

/* load */
function wafers_load(){
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_wafers"},
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#wafers_body").html("");
      if (reply.length==0) return;
      for (row of reply){
        tt ="<tr>\n";
        tt+="<td>"+row["number"]+"</td>";
        tt+="<td>"+row["serialnumber"]+"</td>";
        tt+="<td>"+row["submission"]+"</td>";
        tt+="<td>"+row["substrate"]+"</td>";
        tt+="<td>"+row["doping"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"?page=wafer&wafer_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"?page=samples&wafer_id="+row["id"]+"\">samples</a>&nbsp;";
        //tt+="<a href=\"#\" onclick=\"wafers_delete('"+row["id"]+"')\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#wafers_body").append(tt);
      }
      $("#wafers_counter").html("("+reply.length+")");
      $("#wafers").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete */
function wafers_delete(id){
  if(!window.confirm("Are you sure to delete sample?")) return false;
  $("#wafers_reply").text("");
  $.ajax({
    url: '/malta-db/dbwrite.php',
    type: 'get',
    data: {cmd:"delete_wafer",wafer_id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; wafers_load();}
      $("#wafers_reply").text(sreply);
    }
  });
  return false;
};

</script>

