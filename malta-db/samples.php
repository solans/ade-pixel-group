<h2>Samples</h2>
<button onclick="samples_export();">CSV</button>Export <span id="samples_counter"></span> 
<button onclick="samples_reset();">Reset filters</button>
<table id="samples" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th class="filter-select" data-placeholder="Search...">Sensor</th>
		<th data-placeholder="Search...">Sample</th>
		<th data-placeholder="Search...">Split</th>
		<th class="filter-select" data-placeholder="Search...">Submission</th>
		<th class="filter-select" data-placeholder="Search...">Substrate</th>
		<th class="filter-select" data-placeholder="Search...">Flavour</th>
		<th data-placeholder="Search...">Thickness</th>
		<th class="filter-select" data-placeholder="Search...">Doping</th>
		<th class="filter-select" data-placeholder="Search...">Dose</th>
		<th class="filter-select" data-placeholder="Search...">WBD</th>
		<th class="filter-select" data-placeholder="Search...">WBL</th>
		<th >Actions</th>
	</thead>
	<tbody id="samples_body">
	</tbody>
</table>
<div id="samples_reply"></div>


<script>

/* Load the table sorter **/
$("#samples").tablesorter({
  theme: 'blue',
  sortList: [[3, 1]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#samples_counter").html("("+($('#samples tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#samples").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function samples_reset(){
  $("#samples").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export */
function samples_export(){
  $("#samples").trigger('outputTable');
}

/* onload */
$(function() {
  samples_load();
});

/* load */
function samples_load(){
	req={cmd:"get_samples"};
	params=new URLSearchParams(window.location.search);
	if(params.has("wafer_id")){req["wafer_id"]=params.get("wafer_id");}
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: req,
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#samples_body").html("");
      if (reply.length==0) return;
      for (row of reply){
        tt ="<tr>\n";
        tt+="<td>"+row["sensor"]+"</td>";
        tt+="<td>"+row["sample"]+"</td>";
        tt+="<td>"+row["split"]+"</td>";
        tt+="<td>"+row["submission"]+"</td>";
        tt+="<td>"+row["substrate"]+"</td>";
        tt+="<td>"+row["flavour"]+"</td>";
        tt+="<td>"+row["thickness"]+"</td>";
        tt+="<td>"+row["doping"]+"</td>";
        tt+="<td>"+row["dose"]+"</td>";
        tt+="<td>"+row["wire_bonding_date"]+"</td>";
        tt+="<td>"+row["wire_bonding_location"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"?page=sample&sample_id="+row["id"]+"\">edit</a>&nbsp;";
        //tt+="<a href=\"#\" onclick=\"samples_delete('"+row["id"]+"')\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#samples_body").append(tt);
      }
      $("#samples_counter").html("("+reply.length+")");
      $("#samples").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete */
function samples_delete(id){
  if(!window.confirm("Are you sure to delete sample?")) return false;
  $("#samples_reply").text("");
  $.ajax({
    url: '/malta-db/dbwrite.php',
    type: 'get',
    data: {cmd:"delete_sample",id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; samples_load();}
      $("#samples_reply").text(sreply);
    }
  });
  return false;
};

</script>

