<h2>Search</h2>
<form method="GET" id="search">
  <table>
    <tr><th>Sample</th><td><input id="search_sample" type="text"/></td></tr>
    <tr><th>TREC</th><td><input id="search_trec" type="text"/></td></tr>
    <tr><th>Submission</th><td>
    <input type="checkbox" id="search_submission_STREAM_1A"><label for="search_submission_STREAM_1A">STREAM 1A</label>
    <input type="checkbox" id="search_submission_STREAM_1B"><label for="search_submission_STREAM_1B">STREAM 1B</label>
    <input type="checkbox" id="search_submission_STREAM_1B_WF"><label for="search_submission_STREAM_1B_WF">STREAM 1B WF</label>
    <input type="checkbox" id="search_submission_WTN"><label for="search_submission_WTN">WTN</label>
    <input type="checkbox" id="search_submission_STREAM_C"><label for="search_submission_STREAM_C">STREAM C</label>
    <input type="checkbox" id="search_submission_STREAM_B"><label for="search_submission_STREAM_B">STREAM B</label>
    <input type="checkbox" id="search_submission_MPW1"><label for="search_submission_MPW1">MPW1</label>
    <input type="checkbox" id="search_submission_ATTRACT"><label for="search_submission_ATTRACT">ATTRACT</label>
    <input type="checkbox" id="search_submission_STREAM2"><label for="search_submission_STREAM2">STREAM2</label>
    <input type="checkbox" id="search_submission_PADSTREAM"><label for="search_submission_PADSTREAM">PADSTREAM</label>
    <input type="checkbox" id="search_submission_PADSTREAM2"><label for="search_submission_PADSTREAM2">PADSTREAM2</label>
    <input type="checkbox" id="search_submission_MPW2"><label for="search_submission_MPW2">MPW2</label>
    </td></tr>
    <tr><th>Sensor</th><td>
    <input type="checkbox" id="search_sensor_MALTA"><label for="search_sensor_MALTA">MALTA</label>
    <input type="checkbox" id="search_sensor_MALTA2"><label for="search_sensor_MALTA2">MALTA2</label>
    <input type="checkbox" id="search_sensor_MiniMALTA"><label for="search_sensor_MiniMALTA">MiniMALTA</label>
    <input type="checkbox" id="search_sensor_MiniMALTA3"><label for="search_sensor_MiniMALTA3">MiniMALTA3</label>
    <input type="checkbox" id="search_sensor_LAPA"><label for="search_sensor_LAPA">LAPA</label>
    <input type="checkbox" id="search_sensor_MONOPIX"><label for="search_sensor_MONOPIX">MONOPIX</label>
    <input type="checkbox" id="search_sensor_MONOPIX2"><label for="search_sensor_MONOPIX2">MONOPIX2</label>
    <input type="checkbox" id="search_sensor_CLICTD"><label for="search_sensor_CLICTD">CLICTD</label>
    <input type="checkbox" id="search_sensor_FASTPIX"><label for="search_sensor_FASTPIX">FASTPIX</label>
    </td></tr>
  </table>
  <input type="submit" value="Search">
  <input type="reset" value="Reset">
</form>

<h2>Results</h2>
<button id="results_reset">Reset filters</button>
<button id="results_export">Export</button>
<div id="results_found" style="display:inline-block"></div>
<table id="results_table" class="tablesorter" style="font-size: smaller;">
  <thead>
		<th class="first-name filter-select" data-placeholder="Select...">Sensor</th>
		<th data-placeholder="Search...">Sample </th>
		<th class="first-name filter-select" data-placeholder="Select...">Split</th>
		<th class="first-name filter-select" data-placeholder="Select...">Submission</th>
		<th class="first-name filter-select" data-placeholder="Select...">SerialNumber</th>
		<th class="first-name filter-select" data-placeholder="Select...">Substrate</th>
		<th class="first-name filter-select" data-placeholder="Select...">Flavor</th>
		<th class="first-name filter-select" data-placeholder="Select...">Dose</th>
		<th class="first-name filter-select" data-placeholder="Select...">Dicing</th>
		<th class="first-name filter-select" data-placeholder="Select...">TREC</th>
		<th class="first-name filter-select" data-placeholder="Select...">Location</th>
		<th data-placeholder="Search...">WBD</th>
		<th data-placeholder="Search...">WBL</th>
		<th>Actions</th>
	</thead>
	<tbody id="results_body"></tbody>
</table>
</span>
<script>

  $("#results_table").tablesorter({
    theme: 'blue',
    sortList: [[0, 0], [1, 0]],
    widgets: ['filter','zebra','output']
  });

  $("#results_table").on("filterEnd",function(){
    $("#members_found").html("Found: "+($("#members tr:visible").length-2))
  });

  $("#results_export").click(function() {
    $("#results_table").trigger("outputTable");
  });

  $("#results_reset").click(function() {
    $("#results_table").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
  });

  $(function() {
    $("#results_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
  });

  $("#search").submit(function(){
    req={cmd:"search"};
    if($("#search_sample").val()){req["sample"]=$("#search_sample").val();}
    if($("#search_trec").val()){req["trec"]=$("#search_trec").val();}
    
    submissions=['STREAM_1A','STREAM_1B','STREAM_1B_WF','WTN','STREAM_C','STREAM_B','MPW1','ATTRACT','STREAM2','PADSTREAM','PADSTREAM2','MPW2'];
    sensors=['MALTA','MiniMALTA','MiniMALTA3','LAPA','MONOPIX','MALTA2','MONOPIX2','CLICTD','FASTPIX'];
    sel_submissions=[];
    for(submission of submissions){
      if($("#search_submission_"+submission).is(":checked")){sel_submissions.push(submission);}
    }
    if(sel_submissions.length>0){req["submission"]=sel_submissions;}
    sel_sensors=[];
    for(sensor of sensors){
      if($("#search_sensor_"+sensor).is(":checked")){sel_sensors.push(sensor);}
    }
    if(sel_sensors.length>0){req["sensor"]=sel_sensors;}
    //console.log(req);
    search(req);
    return false;
  });	

  function search(req){
    $.ajax({
      url: '/malta-db/dbread.php',
      type: 'get',
      data: req,
      success: function(data) {
        //console.log(data);
        rows=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
        $("#results_body").empty();
        for (row of rows){
          tt ="<tr>";
          tt+="<td>"+row["sensor"]+"</td>";
          tt+="<td>"+row["sample"]+"</td>";
          tt+="<td>"+row["split"]+"</td>";
          tt+="<td>"+row["submission"]+"</td>";
          tt+="<td>"+row["serialnumber"]+"</td>";
          tt+="<td>"+row["substrate"]+"</td>";
          tt+="<td>"+row["flavour"]+"</td>";
          tt+="<td>"+row["dose"]+"</td>";
          tt+="<td>"+row["dicing"]+"</td>";
          tt+="<td>"+row["trec"]+"</td>";
          tt+="<td>"+row["location"]+"</td>";
          tt+="<td>"+row["wire_bonding_date"]+"</td>";
          tt+="<td>"+row["wire_bonding_location"]+"</td>";
          tt+="<td><a href=\"?page=sample&sample_id="+row["id"]+"\">show</a></td>";
          tt+="</tr>";
          $("#results_body").append(tt);
        }
        $("#results_table").trigger("update").trigger("appendCache").trigger("applyWidgets");
        $("#results_found").html("Found: "+($("#results_table tr:visible").length-2));
        
      }
    });    
  }


</script>

