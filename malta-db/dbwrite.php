<?php
include_once('includes.php');

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) {
  echo "Error connecting to database";
  exit();
}

$d=$_GET;

if($d["cmd"]=="add_wafer"){
  $sql ="INSERT INTO wafers ";
  $sql.=" (`number`,`serialnumber`,`submission`,`substrate`,`doping`,`date`)";
  $sql.="VALUES ( ";
  $sql.="'".$d["number"]."',"; 
  $sql.="'".$d["serialnumber"]."', ";
  $sql.="'".$d["submission"]."', ";
  $sql.="'".$d["substrate"]."', ";
  $sql.="'".$d["doping"]."', ";
  $sql.="'".$d["date"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_wafer"){
  $sql ="UPDATE wafers SET ";
  $ss=array();
  if($d["number"]!=""){ $ss[]="`number`='".$d["number"]."'"; }
  if($d["serialnumber"]!=""){ $ss[]="`serialnumber`='".$d["serialnumber"]."'"; }
  if($d["submission"]!=""){ $ss[]="`submission`='".$d["submission"]."'"; }
  if($d["substrate"]!=""){ $ss[]="`substrate`='".$d["substrate"]."'"; }
  if($d["doping"]!=""){ $ss[]="`doping`='".$d["doping"]."'"; }
  if($d["date"]!=""){ $ss[]="`date`='".$d["date"]."'"; }
  $sql.=implode(", ",$ss);
  $sql.=" WHERE ";
  $sql.="`id`='".$d["wafer_id"]."' "; 
  $sql.=";"; 
}
else if($d["cmd"]=="add_sample"){
  $sql ="INSERT INTO samples ";
  $sql.=" (`sensor`,`split`,`submission`,`substrate`,`flavour`,`thickness`,`serialnumber`,`dicing`,`doping`,";      
  $sql.="  `comment`,`dose`,`board`,`wire_bonding_date`,`wire_bonding_location`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["sensor"]."',"; 
  $sql.="'".$d["split"]."', ";
  $sql.="'".$d["submission"]."', ";
  $sql.="'".$d["substrate"]."', ";
  $sql.="'".$d["flavour"]."', ";
  $sql.="'".$d["thickness"]."', ";
  $sql.="'".$d["serialnumber"]."', ";
  $sql.="'".$d["dicing"]."', ";
  $sql.="'".$d["doping"]."', ";
  $sql.="'".$d["comment"]."', ";
  $sql.="'".$d["dose"]."', ";
  $sql.="'".$d["board"]."', ";
  $sql.="'".$d["wire_bonding_date"]."', ";
  $sql.="'".$d["wire_bonding_location"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_sample"){
  $sql ="UPDATE samples SET ";
  $ss=array();
  if($d["sample"]!=""){                $ss[]="`sample`='".$d["sample"]."'"; }
  if($d["sensor"]!=""){                $ss[]="`sensor`='".$d["sensor"]."'"; }
  if($d["split"]!=""){                 $ss[]="`split`='".$d["split"]."'"; }
  if($d["submission"]!=""){            $ss[]="`submission`='".$d["submission"]."'"; }
  if($d["substrate"]!=""){             $ss[]="`substrate`='".$d["substrate"]."'"; }
  if($d["flavour"]!=""){               $ss[]="`flavour`='".$d["flavour"]."'"; }
  if($d["thickness"]!=""){             $ss[]="`thickness`='".$d["thickness"]."'"; }
  if($d["serialnumber"]!=""){          $ss[]="`serialnumber`='".$d["serialnumber"]."'"; }
  if($d["dicing"]!=""){                $ss[]="`dicing`='".$d["dicing"]."'"; }
  if($d["doping"]!=""){                $ss[]="`doping`='".$d["doping"]."'"; }
  if($d["comment"]!=""){               $ss[]="`comment`='".$d["comment"]."'"; }
  if($d["dose"]!=""){                  $ss[]="`dose`='".$d["dose"]."'"; }
  if($d["board"]!=""){                 $ss[]="`board`='".$d["board"]."'"; }
  if($d["wire_bonding_date"]!=""){     $ss[]="`wire_bonding_date`='".$d["wire_bonding_date"]."'"; }
  if($d["wire_bonding_location"]!=""){ $ss[]="`wire_bonding_location`='".$d["wire_bonding_location"]."'"; }
  $sql.=implode(", ",$ss);
  $sql.="WHERE ";
  $sql.="`id`='".$d["sample_id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="add_location"){
  $sql ="INSERT INTO locations ";
  $sql.=" (`sample_id`,`date`,`location`,`status`,`comment`,`EDH`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["sample_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["location"]."', ";
  $sql.="'".$d["status"]."', ";
  $sql.="'".$d["comment"]."', ";
  $sql.="'".$d["edh"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_location"){
  $sql ="UPDATE locations SET ";
  $sql.="`sample_id`='".$d["sample_id"]."', ";
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`location`='".$d["location"]."', ";
  $sql.="`status`='".$d["status"]."', ";
  $sql.="`comment`='".$d["comment"]."', ";
  $sql.="`edh`='".$d["edh"]."' ";
  $sql.="WHERE ";
  $sql.="`id`='".$d["id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_location"){
  $sql ="DELETE FROM locations WHERE ";
  $sql.="`id`='".$d["id"]."' "; 
  $sql.="LIMIT 1"; 
  $sql.=";";
}
else if($d["cmd"]=="add_trec"){
  $sql ="INSERT INTO trec ";
  $sql.=" (`sample_id`,`date`,`trec`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["sample_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["trec"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_trec"){
  $sql ="UPDATE trec SET ";
  $sql.="`sample_id`='".$d["sample_id"]."', ";
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`trec`='".$d["trec"]."' ";
  $sql.="WHERE ";
  $sql.="`id`='".$d["id"]."' ";
  $sql.=";";
}
else if($d["cmd"]=="delete_trec"){
  $sql ="DELETE FROM trec WHERE ";
  $sql.="`id`='".$d["id"]."' "; 
  $sql.="LIMIT 1"; 
  $sql.=";";
}
else if($d["cmd"]=="add_test"){
  $sql ="INSERT INTO tests ";
  $sql.=" (`sample_id`,`date`,`setup`,`tester`,`location`,`scan`,`result`,`comment`) ";
  $sql.="VALUES ( ";
  $sql.="'".$d["sample_id"]."',"; 
  $sql.="'".$d["date"]."', ";
  $sql.="'".$d["setup"]."', ";
  $sql.="'".$d["tester"]."', ";
  $sql.="'".$d["location"]."', ";
  $sql.="'".$d["scan"]."', ";
  $sql.="'".$d["result"]."', ";
  $sql.="'".$d["comment"]."' ";
  $sql.=");";
}
else if($d["cmd"]=="update_test"){
  $sql ="UPDATE tests SET ";
  $sql.="`sample_id`='".$d["sample_id"]."', ";
  $sql.="`date`='".$d["date"]."', ";
  $sql.="`setup`='".$d["setup"]."', ";
  $sql.="`tester`='".$d["tester"]."', ";
  $sql.="`location`='".$d["location"]."', ";
  $sql.="`scan`='".$d["scan"]."', ";
  $sql.="`result`='".$d["result"]."', ";
  $sql.="`comment`='".$d["comment"]."' ";
  $sql.="WHERE ";
  $sql.="`id`='".$d["id"]."' "; 
  $sql.=";";
}
else if($d["cmd"]=="delete_test"){
  $sql ="DELETE FROM tests WHERE ";
  $sql.="`id`='".$d["id"]."' "; 
  $sql.="LIMIT 1"; 
  $sql.=";";
}


echo $sql;
$ret = array();
if($conn->query($sql)){
  $ret["affected_rows"]=$conn->affected_rows;
}else{
  $ret["error"]=$conn->error;  
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>