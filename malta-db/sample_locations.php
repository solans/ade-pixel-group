<h2>Sample Locations</h2>
<button onclick="sample_locations_export();">CSV</button>Export <span id="sample_locations_counter"></span> 
<button onclick="sample_locations_reset();">Reset filters</button>
<table id="sample_locations" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th data-placeholder="Search...">Date</th>
		<th data-placeholder="Search...">Location</th>
		<th data-placeholder="Search...">Status</th>
		<th data-placeholder="Search...">Comment</th>
		<th data-placeholder="Search...">EDH</th>
	  <th >Actions</th>
	</thead>
	<tbody id="sample_locations_body">
	</tbody>
</table>
<div id="sample_locations_reply"></div>


<script>

/* Load the table sorter **/
$("#sample_locations").tablesorter({
  theme: 'blue',
  sortList: [[0, 1]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#sample_locations_counter").html("("+($('#sample_locations tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#sample_locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function sample_locations_reset(){
  $("#sample_locations").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export */
function sample_locations_export(){
  $("#sample_locations").trigger('outputTable');
}

/* onload */
$(function() {
  sample_locations_load();
});

/* load */
function sample_locations_load(){
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_sample_locations",sample_id:$("#sample_sample_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#sample_locations_body").empty();
      if (reply.length==0) return;
      for (row of reply){
        tt ="<tr>\n";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+row["status"]+"</td>";
        tt+="<td>"+row["comment"]+"</td>";
        tt+="<td>"+row["edh"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"https://edh.cern.ch/Document/"+row["edh"]+"\" target='_blank'>EDH</a>&nbsp;";
        //tt+="<a href=\"?page=sample_trec&sample_trec_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"?page=location&location_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"sample_locations_delete('"+row["id"]+"');return false;\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#sample_locations_body").append(tt);
      }
      $("#sample_locations_counter").html("("+reply.length+")");
      $("#sample_locations").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete */
function sample_locations_delete(id){
  if(!isAuthorised()){window.alert("Not authorised"); return false;}
  if(!window.confirm("Are you sure to delete sample location?")) return false;
  $("#sample_locations_reply").text("");
  $.ajax({
    url: '/malta-db/dbwrite.php',
    type: 'get',
    data: {cmd:"delete_location",id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; 
        if(typeof sample_locations_load === 'function'){sample_locations_load();}
      }
      $("#sample_locations_reply").text(sreply);
    }
  });
  return false;
};

</script>

