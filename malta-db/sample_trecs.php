<h2>Sample TRECs</h2>
<button onclick="sample_trecs_export();">CSV</button>Export <span id="sample_trecs_counter"></span> 
<button onclick="sample_trecs_reset();">Reset filters</button>
<table id="sample_trecs" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th data-placeholder="Search...">TREC</th>
		<th data-placeholder="Search...">Date</th>
	  <th >Actions</th>
	</thead>
	<tbody id="sample_trecs_body">
	</tbody>
</table>
<div id="sample_trecs_reply"></div>


<script>

/* Load the table sorter **/
$("#sample_trecs").tablesorter({
  theme: 'blue',
  sortList: [[0, 0], [1, 0]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#sample_trecs_counter").html("("+($('#sample_trecs tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#sample_trecs").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function host_cards_reset(){
  $("#sample_trecs").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export */
function host_cards_export(){
  $("#sample_trecs").trigger('outputTable');
}

/* onload */
$(function() {
  sample_trecs_load();
});

/* load */
function sample_trecs_load(){
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_sample_trecs",sample_id:$("#sample_sample_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#sample_trecs_body").empty();
      if (reply.length==0) return;
      for (row of reply){
        tt ="<tr>\n";
        tt+="<td>"+row["trec"]+"</td>";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"https://eamlight.cern.ch/asset/"+row["trec"]+"\" target='_blank'>EAM</a>&nbsp;";
        //tt+="<a href=\"?page=sample_trec&sample_trec_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"?page=trec&trec_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"sample_trecs_delete('"+row["id"]+"');return false;\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#sample_trecs_body").append(tt);
      }
      $("#sample_trecs_counter").html("("+reply.length+")");
      $("#sample_trecs").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete */
function sample_trecs_delete(id){
  if(!isAuthorised()){window.alert("Not authorised"); return false;}
  if(!window.confirm("Are you sure to delete sample trec?")) return false;
  $("#sample_trecs_reply").text("");
  $.ajax({
    url: '/malta-db/dbwrite.php',
    type: 'get',
    data: {cmd:"delete_trec",id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; sample_trecs_load();}
      $("#sample_trecs_reply").text(sreply);
    }
  });
  return false;
};

</script>

