<h2>Wafer <?=@$_GET['wafer_id'];?></h2>
<form method="GET" id="wafer">
<table class="border" >
  <tr><th class='regform-done-caption'>Number</th><td><input type='text' id='wafer_number'></td></tr>
  <tr><th class='regform-done-caption'>Serial number</th><td><input type='text' id='wafer_serialnumber'></td></tr>
  <tr><th class='regform-done-caption'>Submission</th><td>
  	<select id="wafer_submission">
      <option selected value="">other</option>
      <option value="STREAM_1A">STREAM_1A</option>
      <option value="STREAM_1B">STREAM_1B</option>
      <option value="PADSTREAM">PADSTREAM</option>    
      <option value="PADSTREAM2">PADSTREAM2</option>
      <option value="STREAM_C">STREAM_C</option>    
      <option value="STREAM2">STREAM2</option>    
      <option value="WTN">WTN</option>
      <option value="ATTRACT">ATTRACT</option>    
      <option value="MPW1">MPW1</option>    
      <option value="MPW2">MPW2</option>    
  	</select></td></tr>
  <tr><th class='regform-done-caption'>Substrate</th><td>
	<select id="wafer_substrate">
    	<option selected value="">other</option>
    	<option value="EPI">EPI</option>
    	<option value="EPI 25">EPI 25</option>
    	<option value="EPI 30">EPI 30</option>    
    	<option value="Cz">Cz</option>
	</select></td></tr>
  <tr><th class='regform-done-caption'>Doping</th><td>
  	<select id="wafer_doping">
      	<option selected value=""></option>
      	<option value="low">Low</option>
      	<option value="high">High</option>
      	<option value="very high">Very high</option>
  	</select></td></tr>
  <tr><th class='regform-done-caption'>Date</th><td><input type='text' id='wafer_date'></td></tr>
</table>
<input type="hidden" id="wafer_wafer_id" value="<?=@$_GET['wafer_id'];?>" >
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>
<div id="wafer_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#wafer_date").datepicker({dateFormat:"yy-mm-dd",firstDay: 1});
  load_wafer();
});

function load_wafer(){
  if($("#wafer_wafer_id").val()==""){return;}
  console.log("Load wafer");
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_wafer", wafer_id:$("#wafer_wafer_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      obj=reply[0];
      $("#wafer_number").val(obj["number"]);
      $("#wafer_serialnumber").val(obj["serialnumber"]);
      $("#wafer_submission").val(obj["submission"]);
      $("#wafer_substrate").val(obj["substrate"]);
      $("#wafer_doping").val(obj["doping"]); 
      $("#wafer_date").val(obj["date"]); 
    }
  });
}

$("#wafer").submit(function(){
  update_wafer();
  return false;
});

function update_wafer(){
  $.ajax({
    url: "/malta-db/dbwrite.php",
    type: "get",
    data: {
      cmd:($("#wafer_wafer_id").val()==""?"add_wafer":"update_wafer"),
      wafer_id:$("#wafer_wafer_id").val(),
      number:$("#wafer_number").val(),
      serialnumber:$("#wafer_serialnumber").val(),
      submission:$("#wafer_submission").val(),
      substrate:$("#wafer_substrate").val(),
      doping:$("#wafer_doping").val(),
	  date:$("#wafer_date").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Stored";
	   if(typeof wafers_load === 'function'){wafers_load();$("#wafer").trigger("reset");}
  	  }
      $("#wafer_reply").text(sreply);
    }
  });
}

</script>

