<h2>Sample Tests</h2>
<button onclick="sample_tests_export();">CSV</button>Export <span id="sample_tests_counter"></span> 
<button onclick="sample_tests_reset();">Reset filters</button>
<table id="sample_tests" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th data-placeholder="Search...">Date</th>
		<th data-placeholder="Search...">Setup</th>
		<th data-placeholder="Search...">Tester</th>
		<th data-placeholder="Search...">Location</th>
		<th data-placeholder="Search...">Scan</th>
		<th data-placeholder="Search...">Result</th>
		<th data-placeholder="Search...">Comment</th>
	  <th >Actions</th>
	</thead>
	<tbody id="sample_tests_body">
	</tbody>
</table>
<div id="sample_tests_reply"></div>


<script>

/* Load the table sorter **/
$("#sample_tests").tablesorter({
  theme: 'blue',
  sortList: [[0, 1]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#sample_tests_counter").html("("+($('#sample_tests tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#sample_tests").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function sample_tests_reset(){
  $("#sample_tests").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export */
function sample_tests_export(){
  $("#sample_tests").trigger('outputTable');
}

/* onload */
$(function() {
  sample_tests_load();
});

/* load */
function sample_tests_load(){
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_sample_tests",sample_id:$("#sample_sample_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#sample_tests_body").empty();
      if (reply.length==0) return;
      for (row of reply){
        tt ="<tr>\n";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["setup"]+"</td>";
        tt+="<td>"+row["tester"]+"</td>";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+row["scan"]+"</td>";
        tt+="<td>"+row["result"]+"</td>";
        tt+="<td>"+row["comment"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"?page=sample_test&test_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"sample_tests_delete('"+row["id"]+"');return false;\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#sample_tests_body").append(tt);
      }
      $("#sample_tests_counter").html("("+reply.length+")");
      $("#sample_tests").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete */
function sample_tests_delete(id){
  if(!isAuthorised()){window.alert("Not authorised"); return false;}
  if(!window.confirm("Are you sure to delete sample location?")) return false;
  $("#sample_tests_reply").text("");
  $.ajax({
    url: '/malta-db/dbwrite.php',
    type: 'get',
    data: {cmd:"delete_test",id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; 
        if(typeof sample_tests_load === 'function'){sample_tests_load();}
      }
      $("#sample_tests_reply").text(sreply);
    }
  });
  return false;
};

</script>

