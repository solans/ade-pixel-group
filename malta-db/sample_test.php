<h2>Sample Test</h2>
<form method="GET" id="sample_test">
<table class="border" >
  <tr><th class='regform-done-caption'>Date</th><td><input type='text' id='sample_test_date'></td></tr>
  <tr><th class='regform-done-caption'>Setup</th><td><input type='text' id='sample_test_setup'></td></tr>
  <tr><th class='regform-done-caption'>Tester</th><td><input type='text' id='sample_test_tester'></td></tr>
  <tr><th class='regform-done-caption'>Location</th><td><input type='text' id='sample_test_location'></td></tr>
  <tr><th class='regform-done-caption'>Scan</th><td><select id="sample_test_scan">
    <option selected value="other">other</option>
    <option value="tap">tap</option>
    <option value="analog">analog</option>
    <option value="noise">noise</option>
    <option value="threshold">threshold</option>
    <option value="Fe55">Fe55</option>
    <option value="digitaldac">digitaldac</option>
    <option value="other">other</option>    
  </select></td></tr>
  <tr><th class='regform-done-caption'>Result</th><td><select id="sample_test_result"><option selected value="pass">pass</option><option value="fail">fail</option></select></td></tr>
  <tr><th class='regform-done-caption'>Comment</th><td><input type='text' id='sample_test_comment'></td></tr>
</table>
<input type="hidden" id="sample_test_id" value="<?=@$_GET['test_id'];?>">
<input type="hidden" id="sample_test_sample_id" value="<?=@$_GET['sample_id'];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>
<div id="sample_test_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#sample_test_date").datepicker({dateFormat:"yy-mm-dd",firstDay: 1});
  load_sample_test();
});

function load_sample_test(){
  if($("#sample_test_id").val()==""){return;}
  console.log("Load sample trec");
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_test", test_id:$("#sample_test_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      sample=reply[0];      
      $("#sample_test_date").val(sample["date"]);
      $("#sample_test_setup").val(sample["setup"]);
      $("#sample_test_tester").val(sample["tester"]);
      $("#sample_test_location").val(sample["location"]);
      $("#sample_test_scan").val(sample["scan"]);
      $("#sample_test_result").val(sample["result"]);
      $("#sample_test_comment").val(sample["comment"]);
			$("#sample_test_sample_id").val(sample["sample_id"]);
    }
  });
}

$("#sample_test").submit(function(){
  update_sample_test();
  return false;
});

function update_sample_test(){
  $.ajax({
    url: "/malta-db/dbwrite.php",
    type: "get",
    data: {
      cmd:($("#sample_test_id").val()==""?"add_test":"update_test"),
      id:$("#sample_test_id").val(),
      sample_id:$("#sample_test_sample_id").val(),
      date:$("#sample_test_date").val(),
      setup:$("#sample_test_setup").val(),
      tester:$("#sample_test_tester").val(),
      location:$("#sample_test_location").val(),
      scan:$("#sample_test_scan").val(),
      result:$("#sample_test_result").val(),
      comment:$("#sample_test_comment").val()
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Stored";
        if(typeof sample_tests_load === 'function'){sample_tests_load();}
        $("#sample_test").trigger("reset");
      }
      $("#sample_test_reply").text(sreply);
      
    }
  });
}

</script>

