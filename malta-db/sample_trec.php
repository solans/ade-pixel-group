<h2>Sample TREC</h2>
<form method="GET" id="sample_trec">
<table class="border" >
  <tr><th class='regform-done-caption'>TREC</th><td><input type='text' id='sample_trec_trec'></td></tr>
  <tr><th class='regform-done-caption'>Date</th><td><input type='text' id='sample_trec_date'></td></tr>
</table>
<input type="hidden" id="sample_trec_id" value="<?=@$_GET['sample_trec_id'];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>
<div id="sample_trec_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#sample_trec_date").datepicker({dateFormat:"yy-mm-dd",firstDay: 1});
  load_sample_trec();
});

function load_sample_trec(){
  if($("#sample_trec_id").val()==""){return;}
  console.log("Load sample trec");
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_sample_trec", sample_trec_id:$("#sample_trec_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      sample=reply[0];      
      $("#sample_trec_trec").val(sample["trec"]);
      $("#sample_trec_date").val(sample["date"]);
    }
  });
}

$("#sample_trec").submit(function(){
  update_sample_trec();
  return false;
});

function update_sample_trec(){
  $.ajax({
    url: "/malta-db/dbwrite.php",
    type: "get",
    data: {
      cmd:($("#sample_trec_id").val()==""?"add_trec":"update_trec"),
      sample_id:$("#sample_sample_id").val(),
      id:$("#sample_trec_id").val(),
      trec:$("#sample_trec_trec").val(),
      date:$("#sample_trec_date").val()      
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Stored";
        if(typeof sample_trecs_load === 'function'){sample_trecs_load();}
        $("#sample_trec").trigger("reset");
      }
      $("#sample_trec_reply").text(sreply);
      
    }
  });
}

</script>

