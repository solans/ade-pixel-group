<h2>TREC</h2>
<form method="GET" id="trec">
<table class="border" >
  <tr><th class='regform-done-caption'>Sample ID</th><td><input type='text' id='trec_sample_id'></td></tr>
  <tr><th class='regform-done-caption'>TREC</th><td><input type='text' id='trec_trec'></td></tr>
  <tr><th class='regform-done-caption'>Date</th><td><input type='text' id='trec_date'></td></tr>
</table>
<input type="hidden" id="trec_id" value="<?=@$_GET['trec_id'];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>
<div id="trec_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#trec_date").datepicker({dateFormat:"yy-mm-dd",firstDay: 1});
  trec_load();
});

function trec_load(){
  if($("#trec_id").val()==""){return;}
  console.log("Load trec");
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_trec", trec_id:$("#trec_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      sample=reply[0];
      $("#trec_sample_id").val(sample["sample_id"]);
      $("#trec_trec").val(sample["trec"]);
      $("#trec_date").val(sample["date"]);
    }
  });
}

$("#trec").submit(function(){
  trec_update();
  return false;
});

function trec_update(){
  $.ajax({
    url: "/malta-db/dbwrite.php",
    type: "get",
    data: {
      cmd:($("#trec_id").val()==""?"add_trec":"update_trec"),
      sample_id:$("#trec_sample_id").val(),
      id:$("#trec_id").val(),
      trec:$("#trec_trec").val(),
      date:$("#trec_date").val()      
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Stored";
        if(typeof trec_load === 'function'){trec_load();}
        $("#trec").trigger("reset");
      }
      $("#trec_reply").text(sreply);
      
    }
  });
}

</script>

