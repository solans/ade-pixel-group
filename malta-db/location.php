<h2>Location</h2>
<form method="GET" id="location">
<table class="border" >
  <tr><th class='regform-done-caption'>Sample ID</th><td><input type='text' id='location_sample_id'></td></tr>
  <tr><th class='regform-done-caption'>Date</th><td><input type='text' id='location_date'></td></tr>
  <tr><th class='regform-done-caption'>Location</th><td><input type='text' id='location_location'></td></tr>
  <tr><th class='regform-done-caption'>Status</th><td><select id="location_status"><option selected value="OK">OK</option><option value="BROKEN">Broken</option></select></td></tr>
  <tr><th class='regform-done-caption'>Comment</th><td><input type='text' id='location_comment'></td></tr>
  <tr><th class='regform-done-caption'>EDH</th><td><input type='text' id='location_edh'></td></tr>
</table>
<input type="hidden" id="location_id" value="<?=@$_GET['location_id'];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>
<div id="location_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#location_date").datepicker({dateFormat:"yy-mm-dd",firstDay: 1});
  location_load();
});

function location_load(){
  if($("#location_id").val()==""){return;}
  console.log("Load location");
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_location", location_id:$("#location_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      sample=reply[0];
      $("#location_sample_id").val(sample["sample_id"]);
      $("#location_date").val(sample["date"]);
      $("#location_location").val(sample["location"]);
      $("#location_status").val(sample["status"]);
      $("#location_comment").val(sample["comment"]);
      $("#location_edh").val(sample["edh"]); 
    }
  });
}

$("#location").submit(function(){
  location_update();
  return false;
});

function location_update(){
  $.ajax({
    url: "/malta-db/dbwrite.php",
    type: "get",
    data: {
      cmd:($("#location_id").val()==""?"add_location":"update_location"),
      sample_id:$("#location_sample_id").val(),
      id:$("#location_id").val(),
      date:$("#location_date").val(),
      location:$("#location_location").val(),
      status:$("#location_status").val(),
      comment:$("#location_comment").val(),
      edh:$("#location_edh").val()      
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Stored";
        if(typeof location_load === 'function'){location_load();}
        $("#location").trigger("reset");
      }
      $("#location_reply").text(sreply); 
    }
  });
}

</script>

