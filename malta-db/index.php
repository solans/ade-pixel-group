<?php
include_once('../functions.php');
include_once('includes.php');
?>
<!DOCTYPE html>
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">-->
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="<?=$gobase;?>css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="<?=$gobase;?>img/ATLAS-icon.ico">
<script src="<?=$gobase;?>JS/jquery-3.5.1.min.js"></script>
<script src="<?=$gobase;?>JS/jquery-simple-upload.js"></script>
<script src="<?=$gobase;?>JS/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?=$gobase;?>JS/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/jquery.tablesorter.widgets.min.js"></script>
<script src="<?=$gobase;?>JS/tablesorter/js/widgets/widget-output.min.js"></script>
<script src="<?=$gobase;?>JS/tableexport.js"></script>
<script src="<?=$gobase;?>JS/functions.js"></script>
<link href="<?=$gobase;?>JS/tablesorter/css/theme.blue.css" rel="stylesheet" type="text/css" />
<link href="<?=$gobase;?>malta-db/style.css" rel="stylesheet" type="text/css" />
<title>MALTA sample database</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>
<div class="CONTENT">
<?php
  show_login(); 
  show_authorised();
?>
<p id="title" class="TITLE">MALTA sample database</p>

<ul>
  <li><a href="?page=search">Search</a></li> 
  <li><a href="?page=samples">Samples</a></li> 
  <li><a href="?page=wafers">Wafers</a></li> 
  <!--<li><a href="?page=tests">Tests</a></li> -->
  <?php if(isAuthorised()){ ?>
  <li><a href="?page=sample">Add sample</a></li> 
  <li><a href="?page=test">Add test</a></li> 
  <li><a href="?page=wafer">Add wafer</a></li> 
  <?php } ?>
</ul>
 
<?php
if(@$_GET['page']=="search"){
  include("search.php");
}
else if(@$_GET['page']=="samples"){  
  include("samples.php");
  if(isAuthorised()){ include("sample.php"); }
}
else if(@$_GET['page']=="sample"){
  include("sample.php");
  include("sample_trecs.php");
  if(isAuthorised()){ include("sample_trec.php"); }
  include("sample_locations.php");
  if(isAuthorised()){ include("sample_location.php"); }
  include("sample_tests.php");
  if(isAuthorised()){ include("sample_test.php"); }
}
else if(@$_GET['page']=="trec"){
   if(isAuthorised()){ include("trec.php"); }
   else{include("notallowed.php");}
}
else if(@$_GET['page']=="location"){
   if(isAuthorised()){ include("location.php"); }
   else{include("notallowed.php");}
}
else if(@$_GET['page']=="test"){
   if(isAuthorised()){ include("test.php"); }
   else{include("notallowed.php");}
}
else if(@$_GET['page']=="tests"){
   if(isAuthorised()){ include("tests.php"); }
   else{include("notallowed.php");}
}
else if(@$_GET['page']=="wafers"){  
  include("wafers.php");
  if(isAuthorised()){ include("wafer.php"); }
}
else if(@$_GET['page']=="wafer"){  
  include("wafer.php");
}
else if(@$_GET['page']=="sample_test"){
   if(isAuthorised()){ include("sample_test.php"); }
   else{include("notallowed.php");}
}
?>



</div>
<?php
  show_footer();
?>
</div>
</body>
</html>

