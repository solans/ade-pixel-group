<?php
include_once("includes.php");

$conn = new mysqli($db["host"],$db["user"],$db["pass"],$db["name"],$db["port"]);
if ($conn->connect_error) { die("Connection failed: " . mysqli_connect_error());}

$d=$_GET;

if($d["cmd"]=="get_wafers"){
  $sql ="SELECT * FROM wafers";
}
else if($d["cmd"]=="get_wafer"){
  $sql ="SELECT * FROM wafers WHERE id='".$d["wafer_id"]."'";
}
else if($d["cmd"]=="get_samples"){
  $sql ="SELECT * FROM samples";
  $conds=array();
  if(array_key_exists("wafer_id",$d)){$conds[]="wafer_id='".$d["wafer_id"]."'";}
  $conds[]="(sensor LIKE '%MALTA%' OR sensor LIKE 'LAPA')";
  if(count($conds)>0){$sql.=" WHERE ".join(" AND ",$conds);}
}
else if($d["cmd"]=="get_sample"){
  $sql ="SELECT * FROM samples WHERE id='".$d["sample_id"]."'";
}
else if($d["cmd"]=="get_sample_trecs"){
  $sql ="SELECT * FROM trec WHERE sample_id='".$d["sample_id"]."'";
}
else if($d["cmd"]=="get_sample_locations"){
  $sql ="SELECT * FROM locations WHERE sample_id='".$d["sample_id"]."'";
}
else if($d["cmd"]=="get_sample_tests"){
  $sql ="SELECT * FROM tests WHERE sample_id='".$d["sample_id"]."';";
}
else if($d["cmd"]=="get_test"){
  $sql ="SELECT * FROM tests WHERE id='".$d["test_id"]."';";
}
else if($d["cmd"]=="get_trec"){
  $sql ="SELECT * FROM trec WHERE id='".$d["trec_id"]."'";
}
else if($d["cmd"]=="get_tests"){
  $sql ="SELECT * FROM tests";
}
else if($d["cmd"]=="get_location"){
  $sql ="SELECT * FROM locations WHERE id='".$d["location_id"]."'";
}
else if($d["cmd"]=="search"){
  $fields=array();
  $tables=array();
  $conds=array();
  $fields[]="samples.*";
  $fields[]="locations.location";
  $fields[]="trec.trec";
  $tables[]="samples";
  $tables[]="LEFT JOIN locations ON locations.sample_id=samples.id";
  //$tables[]="LEFT JOIN (SELECT sample_id, MAX(trec) AS trec FROM trec GROUP BY sample_id) as trec ON samples.id=trec.sample_id";
  $conds[]="locations.date=(SELECT MAX(date) FROM locations WHERE samples.id=locations.sample_id)";
  if(array_key_exists("sample",$d)){$conds[]="samples.`sample` LIKE '".$d["sample"]."'";}
  if(array_key_exists("trec",$d)){
    $tables[]="LEFT JOIN trec ON samples.id=trec.sample_id"; 
    $conds[]="trec.`trec` LIKE '".$d["trec"]."'";
  }else{
    $tables[]="LEFT JOIN (SELECT sample_id, MAX(trec) AS trec FROM trec GROUP BY sample_id) as trec ON samples.id=trec.sample_id";
  }
  if(array_key_exists("submission",$d)){
    $submissions=array();
    foreach($d["submission"] as $submission){
      $submissions[]="samples.`submission`='".$submission."'"; 
    }
    $conds[]="(".join(" OR ",$submissions).")";
  }
  if(array_key_exists("sensor",$d)){
    $sensors=array();
    foreach($d["sensor"] as $sensor){
      $sensors[]="samples.`sensor`='".$sensor."'"; 
    }
    $conds[]="(".join(" OR ",$sensors).")";
  }
  if(array_key_exists("wafer_id",$d)){$conds[]="wafer_id='".$d["wafer_id"]."'";}
  $sql="SELECT ".join(", ",$fields);
  $sql.=" FROM ".join(" ",$tables);
  if(count($conds)>0){$sql.=" WHERE ".join(" AND ",$conds);}

}
echo $sql;
$result=$conn->query($sql);
$ret = array();
while($row = $result->fetch_assoc()) {
  //$ret[] = $row;
  $row2=array();
  foreach($row as $k=>$v){
    //$row2[$k]=htmlentities($v,ENT_COMPAT,'ISO-8859-1', true);
    $row2[$k]=mb_convert_encoding($v,"UTF-8","ISO-8859-1");
    //$row2[$k]=utf8_encode($v);
    //$row2[$k]=$v;
  }
  $ret[]=$row2;
}
//echo "close";
$conn->close();

echo json_encode($ret);
?>