<h2>Sample <?=@$_GET['sample_id'];?></h2>
<form method="GET" id="sample">
<table class="border" >
  <tr><th class='regform-done-caption'>Sample</th><td><input type='text' id='sample_sample' readonly='readonly'></td></tr>
  <tr><th class='regform-done-caption'>Sensor</th><td><input type='text' id='sample_sensor' readonly='readonly'></td></tr>
  <tr><th class='regform-done-caption'>Split</th><td><input type='text' id='sample_split' readonly='readonly'></td></tr>
  <tr><th class='regform-done-caption'>Submission</th><td><input type='text' id='sample_submission' readonly='readonly'></td></tr>
  <tr><th class='regform-done-caption'>Substrate</th><td><input type='text' id='sample_substrate' readonly='readonly'></td></tr>
  <tr><th class='regform-done-caption'>Flavour</th><td><input type='text' id='sample_flavour'></td></tr>
  <tr><th class='regform-done-caption'>Thickness</th><td><input type='text' id='sample_thickness' readonly='readonly'></td></tr>
  <tr><th class='regform-done-caption'>Serial number</th><td><input type='text' id='sample_serialnumber' readonly='readonly'></td></tr>
  <tr><th class='regform-done-caption'>Dicing</th><td><input type='text' id='sample_dicing'></td></tr>
  <tr><th class='regform-done-caption'>Doping</th><td><input type='text' id='sample_doping' readonly='readonly'></td></tr>
  <tr><th class='regform-done-caption'>Comment</th><td><input type='text' id='sample_comment' style='width: 400px;'></td></tr>
  <tr><th class='regform-done-caption'>Dose</th><td><input type='text' id='sample_dose'></td></tr>
  <tr><th class='regform-done-caption'>Board</th><td><input type='text' id='sample_board' ></td></tr>
  <tr><th class='regform-done-caption'>Wire-bonding date</th><td><input type='text' id='sample_wire_bonding_date' ></td></tr>
  <tr><th class='regform-done-caption'>Wire-bonding location</th><td><input type='text' id='sample_wire_bonding_location' ></td></tr>
</table>
<input type="hidden" id="sample_sample_id" value="<?=@$_GET['sample_id'];?>" >
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>
<div id="sample_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#sample_wire_bonding_date").datepicker({dateFormat:"yy-mm-dd",firstDay: 1});
  load_sample();
});

function load_sample(){
  if($("#sample_sample_id").val()==""){return;}
  console.log("Load sample");
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_sample", sample_id:$("#sample_sample_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      sample=reply[0];
      $("#sample_sample").val(sample["sample"]);
      $("#sample_sensor").val(sample["sensor"]);
      $("#sample_split").val(sample["split"]);
      $("#sample_submission").val(sample["submission"]);
      $("#sample_substrate").val(sample["substrate"]);
      $("#sample_flavour").val(sample["flavour"]);
      $("#sample_thickness").val(sample["thickness"]);
      $("#sample_serialnumber").val(sample["serialnumber"]);
      $("#sample_dicing").val(sample["dicing"]);
      $("#sample_doping").val(sample["doping"]);
      $("#sample_comment").val(sample["comment"]);
      $("#sample_dose").val(sample["dose"]);
      $("#sample_board").val(sample["board"]);
      $("#sample_wire_bonding_date").val(sample["wire_bonding_date"]);
      $("#sample_wire_bonding_location").val(sample["wire_bonding_location"]);
    }
  });
}

$("#sample").submit(function(){
  update_sample();
  return false;
});

function update_sample(){
  $.ajax({
    url: "/malta-db/dbwrite.php",
    type: "get",
    data: {
      cmd:($("#sample_sample_id").val()==""?"add_sample":"update_sample"),
      sample_id:$("#sample_sample_id").val(),
      sensor:$("#sample_sensor").val(),
      split:$("#sample_split").val(),
      submission:$("#sample_submission").val(),
      substrate:$("#sample_substrate").val(),
      flavour:$("#sample_flavour").val(),
      thickness:$("#sample_thickness").val(),
      serialnumber:$("#sample_serialnumber").val(),
      dicing:$("#sample_dicing").val(),
      doping:$("#sample_doping").val(),
      comment:$("#sample_comment").val(),
      dose:$("#sample_dose").val(),
      board:$("#sample_board").val(),
      wire_bonding_date:$("#sample_wire_bonding_date").val(),
      wire_bonding_location:$("#sample_wire_bonding_location").val()
      
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Stored";}
      $("#sample_reply").text(sreply);
      
    }
  });
}

</script>

