<h2>Sample Location</h2>
<form method="GET" id="sample_location">
<table class="border" >
  <tr><th class='regform-done-caption'>Date</th><td><input type='text' id='sample_location_date'></td></tr>
  <tr><th class='regform-done-caption'>Location</th><td><input type='text' id='sample_location_location'></td></tr>
  <tr><th class='regform-done-caption'>Status</th><td><select id="sample_location_status"><option selected value="OK">OK</option><option value="BROKEN">Broken</option></select></td></tr>
  <tr><th class='regform-done-caption'>Comment</th><td><input type='text' id='sample_location_comment'></td></tr>
  <tr><th class='regform-done-caption'>EDH</th><td><input type='text' id='sample_location_edh'></td></tr>
</table>
<input type="hidden" id="sample_location_id" value="<?=@$_GET['sample_location_id'];?>">
<input type="submit" value="Save">
<input type="reset" value="Reset">
</form>
<div id="sample_location_reply" style="display:inline-block;"></div>

<script>

$(function() {
  $("#sample_location_date").datepicker({dateFormat:"yy-mm-dd",firstDay: 1});
  load_sample_location();
});

function load_sample_location(){
  if($("#sample_location_id").val()==""){return;}
  console.log("Load sample trec");
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_sample_location", sample_location_id:$("#sample_location_id").val()},
    success: function(data) {
      console.log(data);   
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      if (reply.length==0) return;
      sample=reply[0];      
      $("#sample_location_date").val(sample["date"]);
      $("#sample_location_location").val(sample["location"]);
      $("#sample_location_status").val(sample["status"]);
      $("#sample_location_comment").val(sample["comment"]);
      $("#sample_location_edh").val(sample["edh"]);
    }
  });
}

$("#sample_location").submit(function(){
  update_sample_location();
  return false;
});

function update_sample_location(){
  $.ajax({
    url: "/malta-db/dbwrite.php",
    type: "get",
    data: {
      cmd:($("#sample_location_id").val()==""?"add_location":"update_location"),
      id:$("#sample_location_id").val(),
      sample_id:$("#sample_sample_id").val(),
      date:$("#sample_location_date").val(),
      location:$("#sample_location_location").val(),
      status:$("#sample_location_status").val(),
      comment:$("#sample_location_comment").val(),
      edh:$("#sample_location_edh").val()    
    },
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Stored";
        if(typeof sample_locations_load === 'function'){sample_locations_load();}
        $("#sample_location").trigger("reset");
      }
      $("#sample_location_reply").text(sreply);
      
    }
  });
}

</script>

