<h2>Tests</h2>
<button onclick="tests_export();">CSV</button>Export <span id="tests_counter"></span> 
<button onclick="tests_reset();">Reset filters</button>
<table id="samples" class="tablesorter" style="font-size: smaller;width:initial">
	<thead>
		<th data-placeholder="Search...">Date</th>
		<th data-placeholder="Search...">Setup</th>
		<th data-placeholder="Search...">Tester</th>
		<th data-placeholder="Search...">Scan</th>
		<th data-placeholder="Search...">Result</th>
		<th data-placeholder="Search...">Location</th>
		<th data-placeholder="Search...">Comment</th>
		<th >Actions</th>
	</thead>
	<tbody id="tests_body">
	</tbody>
</table>
<div id="tests_reply"></div>


<script>

/* Load the table sorter **/
$("#samples").tablesorter({
  theme: 'blue',
  sortList: [[3, 1]],
  widgets: ['filter','zebra','output']
}).bind('filterEnd', function() {
  $("#tests_counter").html("("+($('#samples tr:visible').length-2)+")");
});

/* Trigger the tablesorter */
$(function() {
  $("#samples").trigger("update").trigger("appendCache").trigger("applyWidgets");
});

/* clear the filters */
function tests_reset(){
  $("#samples").trigger("filterReset").trigger("sorton",[[[0, 0], [1, 0]]]);
};

/* declare the export */
function tests_export(){
  $("#samples").trigger('outputTable');
}

/* onload */
$(function() {
  tests_load();
});

/* load */
function tests_load(){
  $.ajax({
    url: '/malta-db/dbread.php',
    type: 'get',
    data: {cmd:"get_tests"},
    success: function(data) {
      reply=JSON.parse(data.slice(data.indexOf("["),data.indexOf("]")+1));
      $("#tests_body").html("");
      if (reply.length==0) return;
      for (row of reply){
        tt ="<tr>\n";
        tt+="<td>"+row["date"]+"</td>";
        tt+="<td>"+row["setup"]+"</td>";
        tt+="<td>"+row["tester"]+"</td>";
        tt+="<td>"+row["scan"]+"</td>";
        tt+="<td>"+row["result"]+"</td>";
        tt+="<td>"+row["location"]+"</td>";
        tt+="<td>"+row["comment"]+"</td>";
        tt+="<td>";
        tt+="<a href=\"?page=sample&sample_id="+row["id"]+"\">edit</a>&nbsp;";
        tt+="<a href=\"#\" onclick=\"tests_delete('"+row["id"]+"')\">delete</a>";
        tt+="</td>";
        tt+="</tr>\n";
        $("#tests_body").append(tt);
      }
      $("#tests_counter").html("("+reply.length+")");
      $("#samples").trigger("update").trigger("appendCache").trigger("applyWidgets");
    }
  });
}

/* delete */
function tests_delete(id){
  if(!window.confirm("Are you sure to delete sample?")) return false;
  $("#tests_reply").text("");
  $.ajax({
    url: '/malta-db/dbwrite.php',
    type: 'get',
    data: {cmd:"delete_test",id:id},
    success: function(data) {
      console.log(data);
      reply=JSON.parse(data.slice(data.indexOf("{"),data.indexOf("}")+1));
      sreply="";
      if ("error" in reply){ sreply=reply["error"];}
      else if (reply["affected_rows"]==0){ sreply="No changes"; }
      else if (reply["affected_rows"]==1){ sreply="Deleted"; tests_load();}
      $("#tests_reply").text(sreply);
    }
  });
  return false;
};

</script>

