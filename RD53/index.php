<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="/JS/toc.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<title>RD53</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<h1 class="TITLE">RD53A Setup</h1>
<p>Currently we have a BDAQ readout and a YARR readout, each with a dedicated computer. There are two BDAQ boards (#011 and #023) and one PCIe card (XpressK7) with the Ohio FMC for YARR.
</p>

<p>
<img class="IMAGEW600" src="images/RD53ASetup_small_withYarr.png"/>
</p>

<h2 id="documentation_and_links" class="SUBTITLE">Documentaion and links</h2>
<p>
  <ul>
    <li></li>
  </ul>
</p>


</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
