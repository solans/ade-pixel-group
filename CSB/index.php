<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>CSB</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="IMAGERIGHT"><img class="SMALLIMAGE" alt="CSB v1" src="CSB-1.png"></div>
<div class="CONTENT">

<p class="TITLE">CSB</p>
<p>The Cable Saver Board (CSB) for ATLAS ITK outer barrel demonstrator 
is an interface between the stave flex and the off-stave services.</p>
<p>The demonstrator program envisages the construction of four 1:1 scale prototypes: Short Integration, Short Electrical, Long Thermal, Thermofluidic and Full demonstrator stave. To this end, CSBs of varying geometries have been designed for these prototypes, to cater to the various powering and data transmission requirements needed.</p>
<p>A comprehensive manual has been compiled covering all aspects from connector pinouts to design specifications of each of the Cable Saver Boards designed to date.</p>

<p class="SUBTITLE">Presentations</p>

<ul>
  <li><a href="https://indico.cern.ch/event/712222/contributions/2927603/attachments/1617911/2572258/ASharma_CSB_Update_2018_03_15.pdf">A. Sharma, ITK pixel barrel demonstrator general meeting, March 2018</a></li>
  <li><a href="https://indico.cern.ch/event/674774/contributions/2772906/attachments/1551618/2437798/CSB_20171102.pdf">A. Sharma, ITK pixel demonstrator meeting, November 2017</a></li>
  <li><a href="https://indico.cern.ch/event/658201/contributions/2700198/attachments/1512471/2359079/CSB_20170824.pdf">A. Sharma, ITK pixel demonstrator meeting, August 2017</a></li>
  <li><a href="https://indico.cern.ch/event/655604/contributions/2680043/attachments/1503429/2342159/CSB_20170802.pdf">A. Sharma, ITK pixel demonstrator meeting, August 2017</a>
</ul>

<div><img alt="Integrated CSBs" src="IntegratedCSBs.png" width="100%" height="autp">


<p class="SUBTITLE">Documentation and Links</p>
<ul>
  <li><a href="https://edms.cern.ch/document/1900180">Cable Saver Board for ITK Pixel outer barrel demonstrator (EDMS)</a></li>
  <li><a href="https://edms.cern.ch/document/1900181">Cable Saver Board for the full thermal ITK Pixel demonstrator (EDMS)</a></li>
  <li><a href="https://edms.cern.ch/document/1900182">Cable Saver Board for the short integration prototype of the ITK Pixel demonstrator (EDMS)</a></li>
  <li><a href="https://edms.cern.ch/document/3006309">comprehensive manual</a></li>
  <li><a href="<?=$gobase;?>../CSB/2017_07_27_CSB_Data_VHDCI_Pinout_2col_Symmetrical.xlsx">CSB data pinout mapping</a></li>
</ul>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
