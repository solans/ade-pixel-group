<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="/img/ATLAS-icon.ico">
<title>MALTA Software</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">MALTA read-out and software</p>

<h2 class="SUBTITLE">Description</h2>

<p>
MALTA software is the collection of software to operate the MALTA family of Monolithic Pixel detectors (MALTA and Mini-MALTA), 
that is mostly written in C++, and includes Python tools and wrapper modules for the C++ libraries. 
It is divided into several packages that provide different functionalities, based on the ATLAS TDAQ CMake project policy. 
TDAQ is required to compile and in some cases to run the software.

FMC is used to connect the MALTA carrier boards to commercial FPGA evaluation boards, that run a custom firmware.
Communication with the FPGA is done through IPbus that is a protocol that is based on UDP/IP over ethernet.
The read-out of MALTA provides full hit information, and a fast trigger signal OR from a programmable region of interest from the matrix.

As a result multiple MALTA planes can be operated simultaneously as a telescope. MALTA read-out can also be integrated into AIDA telescopes.
</p>

<img src="MALTA-web.png" class="MEDIUMIMAGE"/>

<p class="SUBTITLE">Documentation and links</p>
<ul>
  <li><a href="<?=$gobase;?>/MaltaSW/html">MALTA software online documentation</a></li>
  <li><a href="https://gitlab.cern.ch/malta">MALTA gitlab repository</a></li>
</ul>
</p>

</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
