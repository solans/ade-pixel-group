<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Laboratories</title>

</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Laboratories</p>

<img class="IMAGE IMAGERIGHT" src="lab-floorplan.png" style="max-width:400px;width:30%;"/>

<p class="SUBTITLE">Description</p>
<p>ATLAS CERN Team group laboratories are located in 
  <a href="Lab23.php">161/1-023</a> and <a href="Lab24.php">161/1-024</a>. 
  They are semi-clean rooms equipped with all the necessary tools.
  <ul>
    <li>Sensor and Module motion controls</li>
    <li>Microscopes and optical inspection hoods</li>
    <li>Oscilloscopes, differential probes, low current meters, LV sources</li>
    <li>Climate chamber, dry air system, laser setup, laminar flowbox</li>
    <li>Probe stations for FE chips and sensors</li>
    <li>DAQ and read-out systems for pixel detectors</li>
    <li><a href="LaserSetup.php">Laser setup for TCT measurements</a></li>
  </ul>
</p>

<p class="SUBTITLE">Lab rules
  <ol>
    <li>Don't eat or drink in the lab.</li>
    <li>Wear dosimeter, cover shoes and lab coat.</li>
    <li>Return tools after use.</li>
    <li>Respect the setup of others.</li>
    <li>Call for help if needed.</li>
  </ol>
</p>

<p class="SUBTITLE">Documents and links
<ul>
  <li><a href="LaserSetup.php">Laser setup</a></li>
  <li><a href="../Sources/login">Available sources</a></li>
  <li><a href="../IrradiatedSamples/login">Samples in storage</a></li>
  <li><a href="http://service-rp-sources.web.cern.ch/service-rp-sources/">RP service</a></li>
</ul>
</p>

<p class="SUBTITLE">Chillers</p>

<ul>
  <li><a href="JULABO_FP51-SL.pdf">Julabo FP51-SL manual</a></li>
  <li><a href="JULABO_FP50-HE.pdf">Julabo FP50-HE manual</a></li>
  <li><a href="JULABO-THERMAL-H10.pdf">Julabo H10 thermal fluid details</a></li>
  <li><a href="JULABO-THERMAL-H10-SDS.pdf">Julabo H10 thermal fluid safety data sheet</a></li>
  <li><a href="Compatible-Control_Ministat_Chiller_V3.7_EN.pdf">HUBER Intelligent Control manual</a></li>
  <li><a href="HUBER_IC012.pdf">HUBER Circulation Cooler IC012 manual</a></li>
  <li><a href="S276984_1001.0020.01_Unistat_705_gb.pdf">HUBER Unistat 705 manual</a></li>
</ul>

<p class="SUBTITLE">Radio Protection Safety</p>
<ul>
  <li><a href="RPLeaflet_SSF.pdf">RP Leaflet</a></li>
</ul>


</div>

<?php
	show_footer();
?>
</div>

</body>
</html>
