<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/ATLAS-icon.ico">
<title>Lab07</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Lab07</p>

<!--<p><img width="100%" src="Finished_Lab_20160114_small.jpg"></p>-->

<p><img width="100%" src="Lab07.png"></p>

Laboratory 161-1-007 is a CERN ATLAS team radiation laboratory for pixel detector characterization.
Equipment present in the lab includes:

<ul>
  <li>X-ray chamber</li>
  <li>Freezer for irradiated samples</li>
  <li>Workstation</li>
</ul>


<p class="SUBTITLE">Links</p>
<ul>
  <li><a href="https://edms.cern.ch/document/2768547">Safety file X-ray machine</a></li>
  <li><a href="xray/">X-ray machine</a></li>
  <li><a href="xrayschedule/">X-ray schedule</a></li>  
</ul>

</div>
<?php
  show_footer();
?>
</div>

</body>
</html>
