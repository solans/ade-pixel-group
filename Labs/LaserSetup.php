<?php
	include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="/JS/toc.js"></script>
<title>Laser setup</title>
</head>
<body>

<div class="ARTBOARD">
<?php
	show_header();
	show_navbar();
?>

<div class="CONTENT">

<h1 class="TITLE">Laser Setup</h1>
<p>A custom Laser setup allows to perform Transient Current Technique (TCT) measurments on pixel sensors
  that allow the characterization of the charge collection properties of different prototypes.
  The induced current is proportional to the collected charge and 
  the number of electron-hole pairs produced is characteristic of each sensor. 
</p>

<script>
$(document).ready(loadToc);
</script>
  
<h2 class="SUBTITLE">Presentations</h2>
<ul>
  <li>Lukas Katzenmeier, Sebastian Dehe. <!--Upgrade des ATLAS-Pixeldetektors, Programme f&uuml;r Messungen an Sensorchips. -->
    <a href="https://indico.cern.ch/conferenceDisplay.py?confId=117532">German Internship Programme 2011.</a>
    <a href="https://indico.cern.ch/event/117532/session/2/contribution/5/attachments/57219/82350/Upgrade_des_ATLAS-Pixeldetektors_2.pptx">Slides</a>
  </li>
  <li>
    Branislav Ristic. <!--Design and commissioning of a LASER setup for testing ATLAS pixel sensors. -->
    <a href="https://indico.cern.ch/conferenceDisplay.py?confId=135603">Summer Student session 2011.</a>
    <a href="http://indico.cern.ch/getFile.py/access?resId=0&materialId=slides&confId=135603">Slides</a>
  </li>
  <li>Yasin Buyukalp. <!--The Automated Control of the ATLAS Pixel Sensor LASER Setup.-->
    <a href="https://indico.cern.ch/conferenceDisplay.py?confId=203713">Summer Student session 2012.</a>
    <a href="https://indico.cern.ch/getFile.py/access?resId=0&materialId=slides&confId=203713">Slides</a>
  </li>
  <li>
    Julian Bender, Marc Syv&auml;ri. <!--Kahlung einer Testapparatur fur Sensorchips --> 
    <a href="https://indico.cern.ch/conferenceDisplay.py?confId=174713">German Internship Programme 2012.</a>
    <a href="https://indico.cern.ch/getFile.py/access?contribId=17&sessionId=3&resId=1&materialId=slides&confId=174713">Slides</a>
  </li>
  <li>
    Florian Haslbeck. <!--Measurement of the charge collection properties of a pixel sensor for ITK --> 
    <a href="https://indico.cern.ch/conferenceDisplay.py?confId=463114">German Internship Programme 2016.</a>
    <a href="  https://indico.cern.ch/event/463114/contributions/2144835/attachments/1262131/1866095/1120_Florian.pdf">Slides</a>
  </li>
</ul>

<h2 class="SUBTITLE">Hardware</h2>
<p>
  The Laser setup is composed of the following elements that 
  are fully controlled through python classes available <a href="<?=$gobase;?>Software/Hardware.php">here</a>.
  <ul>
    <li>Motion controller</li>
    <li>Amplifier (Bias-T)</li>
    <li>Oscilloscope</li>
    <li>Laser pulser</li>
    <li>Laser head</li>
  </ul>
</p>

<img class="IMAGE" width="10%" src="LaserSetup-2016-03-04.png">

<h2 class="SUBTITLE">USB to Serial port</h2>

<p>
  Almost all hardware is controlled through the serial port. 
  As current personal computers don't come equipped with one, the recommended USB-to-RS232 adapter model is a 
  <a href="https://www.exsys.ch/de/usb-seriell/rs-232/4-port/usb-2.0-4s-seriell-rs-232-port-ftdi-chip-set.html">Exsys EX-1324</a>,
  based on the open hardware FTDI chip-set. Python class is available <a href="<?=$gobase;?>Software/Hardware.php">here</a>.
</p>

<img class="SMALLIMAGE" width="10%" src="EX-1324.jpg">


<h2 class="SUBTITLE">Motion controller</h2>

<p>The Motion controller is a  
<a href="http://www.newport.com/ESP301-3-Axis-DC-and-Stepper-Motion-Controller/771081/1033/info.aspx">
  Newport ESP301
</a> box, operated through RS232 serial port, equipped with three linear stages.
<br/>
Protocol specifications are given in the ESP301 <a href="ESP301_Manual.pdf">manual</a>.
Python class is available <a href="<?=$gobase;?>Software/Hardware.php">here</a>.
Stand-alone control software is also available in <a href="Serial.zip">C++</a> and <a href="NewPortConsole.jar">Java</a>. 
</p>
<div>
  <img class="SMALLIMAGE" width="10%" src="NewportMotionController-ESP301.png">
  <img class="SMALLIMAGE" width="10%" src="UTS150pp.jpg">
  <!--<img class="SMALLIMAGE" width="10%" src="UZS80pp.jpg">-->
  <img class="SMALLIMAGE" width="10%" src="IMS100V.jpg">
</div>

<p>Two linear stages 
<a href="http://search.newport.com/?x2=sku&q2=UTS100PP">UTS100PP</a> 
(<a href="UTS-SeriesXYStageUserManual.pdf">Manual</a>) and one 
<!--<a href="http://search.newport.com/?x2=sku&q2=UZS80PP">UZS80PP</a> 
(<a href="UZS-SeriesZStageUserManual.pdf">Manual</a>)-->
<a href="http://search.newport.com/?x2=sku&q2=IMS100V">IMS100V</a> 
(<a href="IMS-V-User-Manual.pdf">Manual</a>)
  inside the Laser box control the movement in X, Y and Z direction independently.
Control cables (DB25M to DB15F, and DB25M to DB25M) are connected from the ESP301 to a patch panel on the surface of the Laser box,
where Subminiature-D gender changers are mounted, 
DB15M to DB15F <a href="https://sc2.premierfarnell.com/sc/product.aspx?productid=1427693">MULTICOMP SPC15359</a> (Farnell)
and DB25F to DB25F <a href="https://sc2.premierfarnell.com/sc/product.aspx?productid=1525743">VIDEK 8105</a> (Farnell).
Inside the box, another set of two DB15M to DB15F cables 
<a href="http://www.digikey.com/product-detail/en/DSUB-15-MF-CBL/288-1344-ND/1143808">DSUB-15-MF-CBL</a>, and
one DB25M to DB25M cable
<a href="https://sc2.premierfarnell.com/sc/product.aspx?productid=2444219">ROLINE 11.01.3518</a>
connect the patch panel to the linear stages.
</p>

<div>
  <img class="MEDIUMIMAGE" src="Motion_Controller_Cabling_20160722.png">
</div>

<p>Instructions to test the Java motion controller console</p>
<ol>
  <li>Download jar file <a href="NewPortConsole.jar">NewportConsole.jar</a> to your computer</li>
  <li>Connect USB232 dongle to your computer</li>
  <li>Open a terminal</li>
  <li>Get the list of available serial ports
    <pre>java -cp NewPortConsole.jar NewportCtrl.NewportCtrl
  Usage: NewportCtrl <portname>
  Available ports:
  cu.Bluetooth-Incoming-Port
  tty.Bluetooth-Incoming-Port
  cu.usbserial-FTXG6Y8TD
  tty.usbserial-FTXG6Y8TD
  cu.usbserial-FTXG6Y8TB
  tty.usbserial-FTXG6Y8TB
  cu.usbserial-FTXG6Y8TA
  tty.usbserial-FTXG6Y8TA
  cu.usbserial-FTXG6Y8TC
  tty.usbserial-FTXG6Y8TC
  </pre></li>
  <li>Run the console on a given port
    <pre>java -cp NewPortConsole.jar NewportCtrl.NewportCtrl tty.usbserial-FTXG6Y8TA</pre></li>
</ol>

<p>Instructions to test the Java motion controller GUI</p>
<ol>
  <li>Download jar file <a href="JNewportCtrl.jar">JNewportCtrl.jar</a> to your computer</li>
  <li>Connect USB232 dongle to your computer</li>
  <li>Execute or double click on JNewportCtrl.jar</li>
</ol>

<img src="NewportCtrl_MainFrame.png" class="MEDIUMIMAGE"><br>

Explanation of the GUI:
<ul>  
  <li>Update will refresh the list of serial ports available. Select the one connected to the motion controller.</li>
  <li>Connect starts the connection between the program and the motion controller. Disconnect will close it.</li>
  <li>Enable axis movement by clicking on the Axis checkbox</li>
  <li>Read the axis position with Read button</li>
  <li>Set the desired position with Set text box and click Move</li>
</ul>


<h2 class="SUBTITLE">PicoQuant LASER</h2>

<p>
A single channel Laser driver 
<a href="https://www.picoquant.com/products/category/picosecond-pulsed-driver/pdl-800-b-picosecond-pulsed-diode-laser-driver">
PicoQuant PDL 800-B</a> (<a href="pdl800b-manual.pdf">Manual</a>)
drives the Laser emission with frequencies from 5 to 80 MHz. The pulse width is 50 ps. 
Two pulsed picosecond diode laser heads
<a href="https://www.picoquant.com/products/category/picosecond-pulsed-sources/ldh-series-picosecond-pulsed-diode-laser-heads">
PicoQuant LDH Series</a> (<a href="ldh_series.pdf">Manual</a>)
LDH-P-C-660 emit light in the visible red (<a href="LDH-P-C-660.jpg">660 nm</a>) 
and LDH-P-C-1060 in the near infrared (<a href="LDH-P-C-1060.jpg">1060 nm</a>).<br>

The Laser emission is controlled by three mechanisms. 
First a key on the front-panel of the laser driver must be turned into the on position.
Second, an interlock circuit is enabled through a software controlled photo-mosfet. 
Third the door of the setup must be closed for the interlock circuit to be closed.

Opening of the Laser heads is coupled to single mode optical fibers that 
transmit the light to the focus optics inside an irradiation chamber, which is a faraday cage made out of copper.
Laser fibers used are TN1064R2A1B and TN1064R2F1B.
Focus optics is installed inside the irradiation chamber below line of sight and pointing downwards.
</p>

<div>
  <img src="pdl800b.jpg" class="SMALLIMAGE">
  <img src="LDH-P-C.jpg" class="SMALLIMAGE">
</div>

<h2 class="SUBTITLE">Particulars LASER</h2>

<p>Another Laser driver is available from <a href="http://www.particulars.si/products.php?prod=lasers.html">Particulars</a> model LA-01 IR FC, which is a 1064 nm fiber coupled LASER. </p>
<p>Software to control the Particulars laser is available <a href="https://gitlab.cern.ch/solans/LaserControl">here</a>.</p>
  
<div>
  <img src="LaserParticulars.png" class="SMALLIMAGE">
</div>
 
 
<h2 class="SUBTITLE">Signal detector</h2>
<p>A High-Speed Fiber-Coupled InGaAs Detector in the range of 800 nm to 1700 nm from <a href="https://www.thorlabs.com">Thorlabs</a> DET01CFC is available for tests.</p>

<div>
  <img src="DET01.jpg" class="SMALLIMAGE">
</div>
 
 
 
 
<h2 class="SUBTITLE">Laser optics</h2>

 <img class="IMAGE" src="laser-optics-2016-07-01.png">

<p>Optics equipment is the following</p>
<table style="margin-left: 1em;">
  <tr>
    <td>F220APC-1064</td>
    <td>Collimator micro-focus</td>
  </tr>
  <tr>
    <td>AD1109F</td>
    <td>Diameter adapter</td> 
  </tr>
  <tr>
    <td>GBE15-C</td>
    <td>Galilean beam expander 15X</td> 
  </tr>
  <tr>
    <td>LA1050-C-ML</td>
    <td>Plano convex lens 10 mm focus length</td> 
  </tr>
</table>
  
<p>Optics working distance is 54 mm. Beam spot diameter is W0 = f(microfocus)/f(collimator) &times; MFD = 60/60.5 &times; 6.4 = 6 &mu;m. 
  Rayeigh length, distance from waist at which the beamspot is twice the size, is 
  ZR=&pi; &times; W0<sup>2</sup> / &lambda; = &pi; &times; 6<sup>2</sup> / 1.060 = 106 &mu;m. </p>
<div>
  <img src="GaussianBeamWaist.png" class="SMALLIMAGE">
</div>



<h2 class="SUBTITLE">Amplifier</h2>
<p>
A <a href="https://cividec.at/">Cividec</a> C2 Broadband Amplifier 
(<a href="Cividec_C2_manual.pdf">manual</a>)
is used to amplify the output signal from the DUT. 
</p>
<div><img src="cividec_cx.jpg" class="SMALLIMAGE"></div>



<h2 class="SUBTITLE">Readout</h2>
<p>
A PSI <a href="https://www.psi.ch/drs/evaluation-board">DRS4</a> evaluation board is used to sample the data.
Manual is available <a href="DRS_manual_rev50.pdf">here</a>.
It is connected to the workstation via USB. This requires dedicated <a href="https://www.psi.ch/drs/software-download">software</a>.
On top of which a python wrapper has been developed to integrate with the rest of the control software.
Python module is available <a href="https://gitlab.cern.ch/solans/drsboard">here</a>.<br>

Data aquisition is performed in a data driven mode, the trigger is provided by the Laser driver and read-out 
is performed on the channel connected on the amplifier. Data is stored in the form of a ROOT Tree.
</p>
<div>
  <img src="drs4-small.png">
</div>


<h2 class="SUBTITLE">Mechanical support structure</h2>
A Cradle holds the amplifier and the DUT. 
This has been developed in Autocad (<a href="Cradle.dwg">model</a>), 
exported to STEP format (<a href="Cradle-CB.stp">file</a>), and 3D printed.

<div>
  <img src="Cradle_CAD.png" class="SMALLIMAGE">
  <img src="Cradle_printed.jpg" class="SMALLIMAGE">
</div>

<h2 class="SUBTITLE">Software</h2>

<p>The software release needed for data taking is organized in multiple packages.
All packages need to be checked out and compiled into a working directory.
The software is available in AFS.
</p>

<table style="margin: 1em;">
  <tr>
    <td><a href="https://gitlab.cern.ch/solans/PySerialComm">PySerialComm</a></td>
    <td>Serial communication tools</td>
  </tr>
  <tr>
    <td><a href="https://gitlab.cern.ch/solans/drsboard">Drsboard</a></td>
    <td>Python wrapper module for DRS hardware library</td> 
  </tr>
  <tr>
    <td><a href="https://gitlab.cern.ch/solans/LaserTCT">LaserTCT</a></td>
    <td>Laser setup specific tools</td> 
  </tr>
  <tr>
    <td><a href="https://gitlab.cern.ch/solans/LaserControl">LaserControl</td>
    <td>Control software for the Particulars Laser</td>
  </tr>
</table>




<h2 class="SUBTITLE">Documents and links</h2>
<ul>
  <li><a href="https://edms.cern.ch/document/3006310">The CERN ATLAS Pixel Group TCT Laser Setup - C. Solans, A. Sharma, S. Dungs</a></li>
  <li><a href="https://twiki.cern.ch/twiki/bin/view/Main/CernAtlasPixelSensorsLaserSetup">Previous twiki documentation</a></li>
</ul>



</div>
<?php
	show_footer();
?>
</div>

</body>
</html>
