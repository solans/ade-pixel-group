<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/ATLAS-icon.ico">
<title>Lab23</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Lab23</p>

<!--<p><img width="100%" src="Finished_Lab_20160114_small.jpg"></p>-->

<p><img width="100%" src="Lab23left.png"></p>
<p><img width="100%" src="Lab23right.png"></p>

Laboratory 161-1-023 is a CERN ATLAS team general purpose laboratory for pixel detector characterization.
Equipment present in the lab includes:

<ul>
  <li>USBPix setup</li>
  <li>YARR setup</li>
  <li>Microscope</li>
  <li>Climate chamber</li>
  <li>Laser setup</li>
</ul>

<p><img class="IMAGE" src="Lab23-Floorplan-20160421_small.png"></p>


<p class="SUBTITLE">Links</p>
<ul>
  <li><a href="RPLeaflet_SSF.pdf">RP Leaflet</a></li>
  <li><a href="LaserSetup.php">Laser setup</a></li>
  <li><a href="https://www.cts-umweltsimulation.de/images/produkte/ps-baureihe-t/download/CTS_Temperature_Test_Chambers_Bench_Top_Version_eng.pdf">climate chamber CTS T-65/50</a><br>
    <ul>
    <li><a href="CTSManual/">Manual (English)</a></li>
    <li><a href="CTSManualDe/">Manual (German)</a></li>
    </ul>
  </li>
  <li><a href="http://perneg.web.cern.ch/perneg/SR/SR-Dry-Air/Catalogue_Compair_technique.pdf">CompAir dry air unit</a></li>
  
</ul>

</div>
<?php
  show_footer();
?>
</div>

</body>
</html>
