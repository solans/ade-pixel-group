<?php
  include_once('../functions.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../ATLAS-icon.ico">
<title>Lab24</title>
</head>
<body>

<div class="ARTBOARD">
<?php
  show_header();
  show_navbar();
?>

<div class="CONTENT">

<p class="TITLE">Lab24</p>

<p><img width="100%" src="Lab24_20230114.png"></p>

Laboratory 161-1-024 is an ATLAS ITK module production testing facility.

Access is controlled by dosimeter reader. Request access through 
<a href="https://apex-sso.cern.ch/pls/htmldb_edmsdb/f?p=404:1120:13508100105021::NO:RP:P1120_PERMISSIONID:457">ADAMS</a>. 


Floorplan to be updated
<p><img class="IMAGE" src="Floorplan-20160420.png"><br></p>


<p class="SUBTITLE">Weiss Technik LabEvent T/110/70 EMC</p>

<ul>
    <li><a href="Weisstechnik/">Manual</a></li>
</ul>





<p class="SUBTITLE">Ultra-air heatless adsorption dryer ultra.dry UDC-B</p>

<ul>
  <li><a href="UDC-B_-_manual_-_EN_-uf_-_R06_-_2019-04-01.pdf">Operating manual</a></li>
  <li><a href="UDC-B_-_ultra_matic_14_-_manual_-_EN_-_uf_-_R02_-_2019-04-01.pdf">Service manual</a></li>
  <li><a href="UDC-B_0005-0110_-_HeatLess_-_GB_-_Datenblatt_-_2018-01-01.pdf">Product data sheet</a></li>
</ul>

</div>
<?php
  show_footer();
?>
</div>

</body>
</html>
